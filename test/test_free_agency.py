import sys
import random
import unittest
from datetime import datetime, date, timedelta
from app import create_app, db
from app.models import (
    Asset,
    Contract,
    ContractYear,
    Team,
    Person,
    Season,
    GoalieSeason,
    SkaterSeason,
)

from app.main import free_agency

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class FreeAgencyTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_query_last_aav(self):
        PERSON = 7
        self.assertEqual(free_agency.query_last_aav(PERSON), 0)

        asset = Asset()
        db.session.add(asset)
        db.session.flush()
        contract = Contract(id=asset.id, person_id=PERSON)
        contract_year = ContractYear(contract_id=asset.id, aav=5000)
        db.session.add(contract)
        db.session.add(contract_year)
        db.session.commit()
        self.assertEqual(free_agency.query_last_aav(PERSON), 5000)

    def test_query_last_team(self):
        PERSON = 7
        TEAM = 6
        self.assertEqual(
            free_agency.query_last_team(PERSON),
            {"name": None, "abbreviation": None, "id": None, "logo": None},
        )

        asset = Asset(current_team=TEAM)
        db.session.add(asset)
        db.session.flush()
        contract = Contract(id=asset.id, person_id=PERSON)
        team = Team(
            id=TEAM,
            abbreviation="TST",
            name="Free Agency Test",
            logo="not an actual svg",
        )
        db.session.add(contract)
        db.session.add(team)
        db.session.commit()
        self.assertEqual(
            free_agency.query_last_team(PERSON),
            {
                "name": "Free Agency Test",
                "abbreviation": "TST",
                "id": TEAM,
                "logo": "not an actual svg",
            },
        )

    def test_get_free_agents(self):
        self.assertEqual(free_agency.get_free_agents(), [])

        person_one = Person(
            active=True,
            name="ZYX ABC",
            birthdate=date.today(),
            position="R",
            shoots="G",
            current_status="RFA",
        )
        person_two = Person(
            active=True,
            name="ABC XYZ",
            birthdate=date.today(),
            position="E",
            shoots="F",
            current_status="UFA",
        )
        person_three = Person(
            active=True,
            name="Not There",
            birthdate=date.today(),
            position="E",
            shoots="F",
            current_status="Under Contract",
        )
        db.session.add(person_one)
        db.session.add(person_two)
        db.session.add(person_three)
        db.session.commit()
        free_agents = free_agency.get_free_agents()
        empty_team = {"name": None, "abbreviation": None, "id": None, "logo": None}
        self.assertEqual(len(free_agents), 2)
        self.assertEqual(free_agents[0]["age"], 0)
        self.assertEqual(free_agents[0]["id"], person_two.id)
        self.assertEqual(free_agents[0]["name"], person_two.name)
        self.assertEqual(free_agents[0]["handed"], person_two.shoots)
        self.assertEqual(free_agents[0]["position"], person_two.position)
        self.assertEqual(free_agents[0]["last_aav"], 0)
        self.assertEqual(free_agents[0]["last_team"], empty_team)
        self.assertEqual(free_agents[1]["age"], 0)
        self.assertEqual(free_agents[1]["id"], person_one.id)
        self.assertEqual(free_agents[1]["name"], person_one.name)
        self.assertEqual(free_agents[1]["handed"], person_one.shoots)
        self.assertEqual(free_agents[1]["position"], person_one.position)
        self.assertEqual(free_agents[1]["last_aav"], 0)
        self.assertEqual(free_agents[1]["last_team"], empty_team)

        ufas = free_agency.get_free_agents(rfa=False)
        self.assertEqual(len(ufas), 1)
        self.assertEqual(ufas[0]["age"], 0)
        self.assertEqual(ufas[0]["id"], person_two.id)
        self.assertEqual(ufas[0]["name"], person_two.name)
        self.assertEqual(ufas[0]["handed"], person_two.shoots)
        self.assertEqual(ufas[0]["position"], person_two.position)
        self.assertEqual(ufas[0]["last_aav"], 0)
        self.assertEqual(ufas[0]["last_team"], empty_team)

        rfas = free_agency.get_free_agents(ufa=False)
        self.assertEqual(rfas[0]["age"], 0)
        self.assertEqual(rfas[0]["id"], person_one.id)
        self.assertEqual(rfas[0]["name"], person_one.name)
        self.assertEqual(rfas[0]["handed"], person_one.shoots)
        self.assertEqual(rfas[0]["position"], person_one.position)
        self.assertEqual(rfas[0]["last_aav"], 0)
        self.assertEqual(rfas[0]["last_team"], empty_team)

        team = Team(name="Test", abbreviation="TT", logo="No")
        db.session.add(team)
        db.session.flush()
        asset_one = Asset()
        asset_two = Asset(current_team=team.id)
        db.session.add(asset_one)
        db.session.add(asset_two)
        db.session.add(team)
        db.session.flush()
        contract_one = Contract(id=asset_one.id, person_id=person_one.id)
        contract_year_one = ContractYear(contract_id=asset_one.id, aav=2345)
        contract_two = Contract(id=asset_two.id, person_id=person_two.id)
        db.session.add(contract_one)
        db.session.add(contract_two)
        db.session.add(contract_year_one)
        db.session.commit()
        free_agents = free_agency.get_free_agents()
        self.assertEqual(len(free_agents), 2)
        self.assertEqual(free_agents[0]["name"], person_one.name)
        self.assertEqual(free_agents[0]["last_aav"], 2345)
        self.assertEqual(free_agents[1]["name"], person_two.name)
        self.assertEqual(free_agents[1]["last_team"]["name"], team.name)
        self.assertEqual(free_agents[1]["last_team"]["id"], team.id)
        self.assertEqual(free_agents[1]["last_team"]["logo"], team.logo)
        self.assertEqual(free_agents[1]["last_team"]["abbreviation"], team.abbreviation)

    def test_get_teams(self):
        self.assertEqual(free_agency.get_teams(), [])

        team = Team(name="Test Zebra")
        db.session.add(team)
        db.session.commit()
        self.assertEqual(free_agency.get_teams(), [(1, "Test Zebra")])

        team = Team(name="Test Puppy")
        db.session.add(team)
        db.session.commit()
        self.assertEqual(
            free_agency.get_teams(), [(2, "Test Puppy"), (1, "Test Zebra")]
        )

    def test_get_signings(self):
        self.assertEqual(free_agency.get_signings(), [])
        # Setup
        teams = [
            Team(id=88, abbreviation="T1", logo="T1.logo"),
            Team(id=52, abbreviation="T2", logo="T2.logo"),
            Team(id=42, abbreviation="T3", logo="T3.logo"),
        ]
        for team in teams:
            db.session.add(team)
        assets = [
            Asset(id=22, originating_team=teams[0].id),
            Asset(id=33, originating_team=teams[0].id),
            Asset(id=44, originating_team=teams[1].id),
            Asset(id=55, originating_team=teams[1].id),
            Asset(id=66, originating_team=teams[2].id),
            Asset(id=77, originating_team=teams[2].id),
        ]
        for asset in assets:
            db.session.add(asset)
        people = [
            Person(
                id=101,
                name="David Test",
                birthdate=date(year=1991, month=1, day=1),
                position="Z",
            ),
            Person(
                id=102,
                name="Mikey Test",
                birthdate=date(year=1990, month=1, day=1),
                position="P",
            ),
            Person(
                id=103,
                name="Jonny Test",
                birthdate=date(year=1989, month=1, day=1),
                position="C",
            ),
            Person(
                id=104,
                name="Matty Test",
                birthdate=date(year=1988, month=1, day=1),
                position="t",
            ),
            Person(
                id=105,
                name="Eddie Test",
                birthdate=date(year=1987, month=1, day=1),
                position="k",
            ),
            Person(
                id=106,
                name="Pauly Test",
                birthdate=date(year=1993, month=1, day=1),
                position="Q",
            ),
        ]
        for person in people:
            db.session.add(person)
        contracts = [
            Contract(
                id=assets[0].id,
                person_id=people[0].id,
                thirty_five_plus=False,
                entry_level=False,
                years=2,
                total_value=1600000,
                signing_date=date(year=1990, month=6, day=4),
            ),
            Contract(
                id=assets[1].id,
                person_id=people[1].id,
                thirty_five_plus=True,
                entry_level=False,
                years=3,
                total_value=4500000,
                signing_date=date(year=1995, month=5, day=8),
            ),
            Contract(
                id=assets[2].id,
                person_id=people[2].id,
                thirty_five_plus=False,
                entry_level=True,
                years=4,
                total_value=9000000,
                signing_date=date(year=2000, month=4, day=12),
            ),
            Contract(
                id=assets[3].id,
                person_id=people[3].id,
                thirty_five_plus=True,
                entry_level=False,
                years=5,
                total_value=2000000,
                signing_date=date(year=2005, month=3, day=16),
            ),
            Contract(
                id=assets[4].id,
                person_id=people[4].id,
                thirty_five_plus=False,
                entry_level=False,
                years=4,
                total_value=8000000,
                signing_date=date(year=2010, month=2, day=20),
            ),
            Contract(
                id=assets[5].id,
                person_id=people[5].id,
                thirty_five_plus=False,
                entry_level=True,
                years=3,
                total_value=3000000,
                signing_date=date(year=2015, month=1, day=24),
            ),
        ]
        for contract in contracts:
            db.session.add(contract)
        db.session.commit()

        # Query
        data = free_agency.get_signings()
        self.assertEqual(len(data), 6)
        self.assertEqual(data[0]["person_id"], people[5].id)
        self.assertEqual(data[0]["person_name"], people[5].name)
        self.assertEqual(data[0]["position"], people[5].position)
        self.assertEqual(data[0]["team"], teams[2].abbreviation)
        self.assertEqual(data[0]["team_id"], teams[2].id)
        self.assertEqual(data[0]["team_logo"], teams[2].logo)
        self.assertEqual(
            data[0]["signing_date"],
            contracts[5].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[0]["thirty_five_plus"], contracts[5].thirty_five_plus)
        self.assertEqual(data[0]["entry_level"], contracts[5].entry_level)
        self.assertEqual(data[0]["years"], contracts[5].years)
        self.assertEqual(data[0]["value"], contracts[5].total_value)
        self.assertEqual(data[1]["person_id"], people[4].id)
        self.assertEqual(data[1]["person_name"], people[4].name)
        self.assertEqual(data[1]["position"], people[4].position)
        self.assertEqual(data[1]["team"], teams[2].abbreviation)
        self.assertEqual(data[1]["team_id"], teams[2].id)
        self.assertEqual(data[1]["team_logo"], teams[2].logo)
        self.assertEqual(
            data[1]["signing_date"],
            contracts[4].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[1]["thirty_five_plus"], contracts[4].thirty_five_plus)
        self.assertEqual(data[1]["entry_level"], contracts[4].entry_level)
        self.assertEqual(data[1]["years"], contracts[4].years)
        self.assertEqual(data[1]["value"], contracts[4].total_value)
        self.assertEqual(data[2]["person_id"], people[3].id)
        self.assertEqual(data[2]["person_name"], people[3].name)
        self.assertEqual(data[2]["position"], people[3].position)
        self.assertEqual(data[2]["team"], teams[1].abbreviation)
        self.assertEqual(data[2]["team_id"], teams[1].id)
        self.assertEqual(data[2]["team_logo"], teams[1].logo)
        self.assertEqual(
            data[2]["signing_date"],
            contracts[3].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[2]["thirty_five_plus"], contracts[3].thirty_five_plus)
        self.assertEqual(data[2]["entry_level"], contracts[3].entry_level)
        self.assertEqual(data[2]["years"], contracts[3].years)
        self.assertEqual(data[2]["value"], contracts[3].total_value)
        self.assertEqual(data[3]["person_id"], people[2].id)
        self.assertEqual(data[3]["person_name"], people[2].name)
        self.assertEqual(data[3]["position"], people[2].position)
        self.assertEqual(data[3]["team"], teams[1].abbreviation)
        self.assertEqual(data[3]["team_id"], teams[1].id)
        self.assertEqual(data[3]["team_logo"], teams[1].logo)
        self.assertEqual(
            data[3]["signing_date"],
            contracts[2].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[3]["thirty_five_plus"], contracts[2].thirty_five_plus)
        self.assertEqual(data[3]["entry_level"], contracts[2].entry_level)
        self.assertEqual(data[3]["years"], contracts[2].years)
        self.assertEqual(data[3]["value"], contracts[2].total_value)
        self.assertEqual(data[4]["person_id"], people[1].id)
        self.assertEqual(data[4]["person_name"], people[1].name)
        self.assertEqual(data[4]["position"], people[1].position)
        self.assertEqual(data[4]["team"], teams[0].abbreviation)
        self.assertEqual(data[4]["team_id"], teams[0].id)
        self.assertEqual(data[4]["team_logo"], teams[0].logo)
        self.assertEqual(
            data[4]["signing_date"],
            contracts[1].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[4]["thirty_five_plus"], contracts[1].thirty_five_plus)
        self.assertEqual(data[4]["entry_level"], contracts[1].entry_level)
        self.assertEqual(data[4]["years"], contracts[1].years)
        self.assertEqual(data[4]["value"], contracts[1].total_value)
        self.assertEqual(data[5]["person_id"], people[0].id)
        self.assertEqual(data[5]["person_name"], people[0].name)
        self.assertEqual(data[5]["position"], people[0].position)
        self.assertEqual(data[5]["team"], teams[0].abbreviation)
        self.assertEqual(data[5]["team_id"], teams[0].id)
        self.assertEqual(data[5]["team_logo"], teams[0].logo)
        self.assertEqual(
            data[5]["signing_date"],
            contracts[0].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[5]["thirty_five_plus"], contracts[0].thirty_five_plus)
        self.assertEqual(data[5]["entry_level"], contracts[0].entry_level)
        self.assertEqual(data[5]["years"], contracts[0].years)
        self.assertEqual(data[5]["value"], contracts[0].total_value)

        # from_date limit
        data = free_agency.get_signings(0, "2000-01-01")
        self.assertEqual(len(data), 4)
        self.assertEqual(data[0]["person_id"], people[5].id)
        self.assertEqual(data[0]["person_name"], people[5].name)
        self.assertEqual(data[0]["position"], people[5].position)
        self.assertEqual(data[0]["team"], teams[2].abbreviation)
        self.assertEqual(data[0]["team_id"], teams[2].id)
        self.assertEqual(data[0]["team_logo"], teams[2].logo)
        self.assertEqual(
            data[0]["signing_date"],
            contracts[5].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[0]["thirty_five_plus"], contracts[5].thirty_five_plus)
        self.assertEqual(data[0]["entry_level"], contracts[5].entry_level)
        self.assertEqual(data[0]["years"], contracts[5].years)
        self.assertEqual(data[0]["value"], contracts[5].total_value)
        self.assertEqual(data[1]["person_id"], people[4].id)
        self.assertEqual(data[1]["person_name"], people[4].name)
        self.assertEqual(data[1]["position"], people[4].position)
        self.assertEqual(data[1]["team"], teams[2].abbreviation)
        self.assertEqual(data[1]["team_id"], teams[2].id)
        self.assertEqual(data[1]["team_logo"], teams[2].logo)
        self.assertEqual(
            data[1]["signing_date"],
            contracts[4].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[1]["thirty_five_plus"], contracts[4].thirty_five_plus)
        self.assertEqual(data[1]["entry_level"], contracts[4].entry_level)
        self.assertEqual(data[1]["years"], contracts[4].years)
        self.assertEqual(data[1]["value"], contracts[4].total_value)
        self.assertEqual(data[2]["person_id"], people[3].id)
        self.assertEqual(data[2]["person_name"], people[3].name)
        self.assertEqual(data[2]["position"], people[3].position)
        self.assertEqual(data[2]["team"], teams[1].abbreviation)
        self.assertEqual(data[2]["team_id"], teams[1].id)
        self.assertEqual(data[2]["team_logo"], teams[1].logo)
        self.assertEqual(
            data[2]["signing_date"],
            contracts[3].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[2]["thirty_five_plus"], contracts[3].thirty_five_plus)
        self.assertEqual(data[2]["entry_level"], contracts[3].entry_level)
        self.assertEqual(data[2]["years"], contracts[3].years)
        self.assertEqual(data[2]["value"], contracts[3].total_value)
        self.assertEqual(data[3]["person_id"], people[2].id)
        self.assertEqual(data[3]["person_name"], people[2].name)
        self.assertEqual(data[3]["position"], people[2].position)
        self.assertEqual(data[3]["team"], teams[1].abbreviation)
        self.assertEqual(data[3]["team_id"], teams[1].id)
        self.assertEqual(data[3]["team_logo"], teams[1].logo)
        self.assertEqual(
            data[3]["signing_date"],
            contracts[2].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[3]["thirty_five_plus"], contracts[2].thirty_five_plus)
        self.assertEqual(data[3]["entry_level"], contracts[2].entry_level)
        self.assertEqual(data[3]["years"], contracts[2].years)
        self.assertEqual(data[3]["value"], contracts[2].total_value)

        # to_date limit
        data = free_agency.get_signings(0, None, "2010-01-01")
        self.assertEqual(len(data), 4)
        self.assertEqual(data[0]["person_id"], people[3].id)
        self.assertEqual(data[0]["person_name"], people[3].name)
        self.assertEqual(data[0]["position"], people[3].position)
        self.assertEqual(data[0]["team"], teams[1].abbreviation)
        self.assertEqual(data[0]["team_id"], teams[1].id)
        self.assertEqual(data[0]["team_logo"], teams[1].logo)
        self.assertEqual(
            data[0]["signing_date"],
            contracts[3].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[0]["thirty_five_plus"], contracts[3].thirty_five_plus)
        self.assertEqual(data[0]["entry_level"], contracts[3].entry_level)
        self.assertEqual(data[0]["years"], contracts[3].years)
        self.assertEqual(data[0]["value"], contracts[3].total_value)
        self.assertEqual(data[1]["person_id"], people[2].id)
        self.assertEqual(data[1]["person_name"], people[2].name)
        self.assertEqual(data[1]["position"], people[2].position)
        self.assertEqual(data[1]["team"], teams[1].abbreviation)
        self.assertEqual(data[1]["team_id"], teams[1].id)
        self.assertEqual(data[1]["team_logo"], teams[1].logo)
        self.assertEqual(
            data[1]["signing_date"],
            contracts[2].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[1]["thirty_five_plus"], contracts[2].thirty_five_plus)
        self.assertEqual(data[1]["entry_level"], contracts[2].entry_level)
        self.assertEqual(data[1]["years"], contracts[2].years)
        self.assertEqual(data[1]["value"], contracts[2].total_value)
        self.assertEqual(data[2]["person_id"], people[1].id)
        self.assertEqual(data[2]["person_name"], people[1].name)
        self.assertEqual(data[2]["position"], people[1].position)
        self.assertEqual(data[2]["team"], teams[0].abbreviation)
        self.assertEqual(data[2]["team_id"], teams[0].id)
        self.assertEqual(data[2]["team_logo"], teams[0].logo)
        self.assertEqual(
            data[2]["signing_date"],
            contracts[1].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[2]["thirty_five_plus"], contracts[1].thirty_five_plus)
        self.assertEqual(data[2]["entry_level"], contracts[1].entry_level)
        self.assertEqual(data[2]["years"], contracts[1].years)
        self.assertEqual(data[2]["value"], contracts[1].total_value)
        self.assertEqual(data[3]["person_id"], people[0].id)
        self.assertEqual(data[3]["person_name"], people[0].name)
        self.assertEqual(data[3]["position"], people[0].position)
        self.assertEqual(data[3]["team"], teams[0].abbreviation)
        self.assertEqual(data[3]["team_id"], teams[0].id)
        self.assertEqual(data[3]["team_logo"], teams[0].logo)
        self.assertEqual(
            data[3]["signing_date"],
            contracts[0].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[3]["thirty_five_plus"], contracts[0].thirty_five_plus)
        self.assertEqual(data[3]["entry_level"], contracts[0].entry_level)
        self.assertEqual(data[3]["years"], contracts[0].years)
        self.assertEqual(data[3]["value"], contracts[0].total_value)

        # to and from
        data = free_agency.get_signings(0, "2000-01-01", "2010-01-01")
        self.assertEqual(len(data), 2)
        self.assertEqual(data[0]["person_id"], people[3].id)
        self.assertEqual(data[0]["person_name"], people[3].name)
        self.assertEqual(data[0]["position"], people[3].position)
        self.assertEqual(data[0]["team"], teams[1].abbreviation)
        self.assertEqual(data[0]["team_id"], teams[1].id)
        self.assertEqual(data[0]["team_logo"], teams[1].logo)
        self.assertEqual(
            data[0]["signing_date"],
            contracts[3].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[0]["thirty_five_plus"], contracts[3].thirty_five_plus)
        self.assertEqual(data[0]["entry_level"], contracts[3].entry_level)
        self.assertEqual(data[0]["years"], contracts[3].years)
        self.assertEqual(data[0]["value"], contracts[3].total_value)
        self.assertEqual(data[1]["person_id"], people[2].id)
        self.assertEqual(data[1]["person_name"], people[2].name)
        self.assertEqual(data[1]["position"], people[2].position)
        self.assertEqual(data[1]["team"], teams[1].abbreviation)
        self.assertEqual(data[1]["team_id"], teams[1].id)
        self.assertEqual(data[1]["team_logo"], teams[1].logo)
        self.assertEqual(
            data[1]["signing_date"],
            contracts[2].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[1]["thirty_five_plus"], contracts[2].thirty_five_plus)
        self.assertEqual(data[1]["entry_level"], contracts[2].entry_level)
        self.assertEqual(data[1]["years"], contracts[2].years)
        self.assertEqual(data[1]["value"], contracts[2].total_value)

        # offset
        data = free_agency.get_signings(1, "2000-01-01", "2010-01-01")
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]["person_id"], people[2].id)
        self.assertEqual(data[0]["person_name"], people[2].name)
        self.assertEqual(data[0]["position"], people[2].position)
        self.assertEqual(data[0]["team"], teams[1].abbreviation)
        self.assertEqual(data[0]["team_id"], teams[1].id)
        self.assertEqual(data[0]["team_logo"], teams[1].logo)
        self.assertEqual(
            data[0]["signing_date"],
            contracts[2].signing_date.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(data[0]["thirty_five_plus"], contracts[2].thirty_five_plus)
        self.assertEqual(data[0]["entry_level"], contracts[2].entry_level)
        self.assertEqual(data[0]["years"], contracts[2].years)
        self.assertEqual(data[0]["value"], contracts[2].total_value)

        # offset
        data = free_agency.get_signings(1, "2000-01-01", "2010-01-01")

    def test_get_pending_rfas(self):
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"], free_agency_opening=date.today()
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                free_agency_opening=date.today() + timedelta(days=1),
            ),
        ]
        db.session.add(seasons[0])
        db.session.add(seasons[1])
        db.session.commit()
        self.assertEqual(free_agency.get_pending_rfas(), [])
        self.assertEqual(free_agency.get_pending_rfas(24), [])
        assets = [
            Asset(id=821, current_team=22, originating_team=22, type="Contract"),
            Asset(id=841, current_team=24, originating_team=22, type="Contract"),
            Asset(id=221, current_team=26, originating_team=22, type="Contract"),
        ]
        people = [
            Person(id=22, active=True, name="Test One"),
            Person(id=29, active=True, name="Test Two"),
            Person(id=12, active=True, name="Test Three"),
        ]
        contracts = [
            Contract(id=assets[0].id, person_id=people[1].id, expiration_status="RFA"),
            Contract(id=assets[1].id, person_id=people[2].id, expiration_status="RFA"),
            Contract(id=assets[2].id, person_id=people[0].id, expiration_status="UFA"),
        ]
        contract_years = [
            ContractYear(
                contract_id=contracts[0].id, season_id=self.app.config["CURRENT_SEASON"]
            ),
            ContractYear(
                contract_id=contracts[1].id, season_id=self.app.config["CURRENT_SEASON"]
            ),
            ContractYear(
                contract_id=contracts[2].id, season_id=self.app.config["CURRENT_SEASON"]
            ),
        ]
        for record in assets + contracts + contract_years + people:
            db.session.add(record)
        db.session.commit()

        people = free_agency.get_pending_rfas()
        self.assertEqual(len(people), 2)
        self.assertEqual(people[0], (12, "Test Three"))
        self.assertEqual(people[1], (29, "Test Two"))

        people = free_agency.get_pending_rfas(24)
        self.assertEqual(people, [(12, "Test Three")])

        db.session.add(
            ContractYear(
                contract_id=contracts[1].id,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
            )
        )
        db.session.commit()
        self.assertEqual(free_agency.get_pending_rfas(24), [])

        people = free_agency.get_pending_rfas()
        self.assertEqual(people, [(29, "Test Two")])

    def test_calculate_qualifying_offer(self):
        seasons = [
            Season(
                name="Test Zero",
                id=self.app.config["CURRENT_SEASON"],
                free_agency_opening=date.today(),
            ),
            Season(
                name="Test Apple",
                id=83,
                free_agency_opening=date.today() - timedelta(days=1),
            ),
            Season(
                name="Test Broccoli",
                id=33,
                free_agency_opening=date.today() - timedelta(days=2),
            ),
            Season(
                name="Test Pie",
                id=13,
                free_agency_opening=date.today() - timedelta(days=3),
            ),
        ]
        for season in seasons:
            db.session.add(season)
        person = Person(
            name="Test Person",
            id=284,
            position="F",
        )
        db.session.add(person)
        contract = Contract(
            id=1233,
            person_id=person.id,
            signing_date=date(year=2019, month=7, day=1),
        )
        db.session.add(contract)
        contract_year = ContractYear(
            contract_id=contract.id,
            season_id=seasons[0].id,
            aav=1000000,
            nhl_salary=750000,
        )
        db.session.add(contract_year)
        stats = [
            SkaterSeason(
                team_id=1,
                season_id=seasons[1].id,
                person_id=person.id,
                games_played=90,
                goals=4,
                assists=20,
                points=24,
                shots=200,
                hits=5,
            ),
            SkaterSeason(
                team_id=1,
                season_id=seasons[2].id,
                person_id=person.id,
                games_played=90,
                goals=20,
                assists=4,
                points=24,
                shots=250,
                hits=0,
            ),
            GoalieSeason(
                team_id=1,
                season_id=seasons[1].id,
                person_id=person.id,
                games_played=60,
                wins=20,
                losses=20,
                shutouts=5,
                shots_against=1000,
                goals_against=97,
            ),
            GoalieSeason(
                team_id=1,
                season_id=seasons[2].id,
                person_id=person.id,
                games_played=40,
                wins=30,
                losses=20,
                shutouts=5,
                shots_against=3000,
                goals_against=97,
            ),
        ]
        for season in stats:
            db.session.add(season)
        db.session.commit()

        skater_stats = ["games_played", "goals", "assists", "points", "shots", "hits"]
        data = free_agency.calculate_qualifying_offer(person.id)
        self.assertEqual(data["name"], person.name)
        self.assertEqual(data["id"], person.id)
        self.assertEqual(data["last_salary"], contract_year.nhl_salary)
        self.assertEqual(data["last_aav"], contract_year.aav)
        self.assertEqual(data["contract_signing"], contract.signing_date)
        self.assertEqual(data["one_way"], True)
        self.assertEqual(data["qualifying_salary"], 787500)
        self.assertEqual(data["stat_names"], skater_stats)
        self.assertEqual(data["stats"][0], ["Test Apple", 90, 4, 20, 24, 200, 5])
        self.assertEqual(data["stats"][1], ["Test Broccoli", 90, 20, 4, 24, 250, 0])

        person.position = "G"
        contract_year.nhl_salary = 2000000
        goalie_stats = [
            "games_played",
            "wins",
            "losses",
            "shutouts",
            "shots_against",
            "goals_against",
        ]
        db.session.add(person)
        db.session.add(contract_year)
        db.session.commit()
        data = free_agency.calculate_qualifying_offer(person.id)
        self.assertEqual(data["last_salary"], contract_year.nhl_salary)
        self.assertEqual(data["one_way"], False)
        self.assertEqual(data["qualifying_salary"], 2000000)
        self.assertEqual(data["stat_names"], goalie_stats)
        self.assertEqual(data["stats"][0], ["Test Apple", 60, 20, 20, 5, 1000, 97])
        self.assertEqual(data["stats"][1], ["Test Broccoli", 40, 30, 20, 5, 3000, 97])

        contract.signing_date = date.today()
        db.session.add(contract)
        db.session.commit()
        data = free_agency.calculate_qualifying_offer(person.id)
        self.assertEqual(data["qualifying_salary"], 1200000)


if __name__ == "__main__":
    unittest.main(verbosity=2)
