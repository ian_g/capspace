import sys
import unittest
from wtforms.validators import ValidationError
from app import create_app, db
from app.models import User
from app.factories import UserFactory

from app.user.forms import RegistrationForm, EditProfileForm


class UserTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_reg_validate_username(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = RegistrationForm()
        form.username.data = "ig"
        self.assertIsNone(form.validate_username(form.username))

        UserFactory(username=form.username.data)

        with self.assertRaises(ValidationError) as exc:
            form.validate_username(form.username)
            self.assertEqual(
                exc.exception,
                "Username ig already registered. Please choose a different username",
            )

        ctx.pop()

    def test_reg_validate_email(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = RegistrationForm()
        form.email.data = "myexample@fake.domain"
        self.assertIsNone(form.validate_email(form.email))

        UserFactory(email=form.email.data)

        with self.assertRaises(ValidationError) as exc:
            form.validate_email(form.email)
            self.assertEqual(
                exc.exception,
                "An account already exists for myexample@fake.domain. Please use a different email address",
            )

        ctx.pop()

    def test_update_validate_username(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = EditProfileForm("ig")

        form.username.data = "ig"
        self.assertIsNone(form.validate_username(form.username))

        UserFactory(username=form.username.data)
        self.assertIsNone(form.validate_username(form.username))

        form.username.data = "ig2"
        self.assertIsNone(form.validate_username(form.username))

        UserFactory(username=form.username.data)

        with self.assertRaises(ValidationError) as exc:
            form.validate_username(form.username)
            self.assertEqual(
                exc.exception,
                "Username ig2 already registered. Please choose a different username",
            )

        ctx.pop()


if __name__ == "__main__":
    unittest.main(verbosity=2)
