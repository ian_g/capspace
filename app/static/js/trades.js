const BATCH_SIZE = document.querySelectorAll(".trades .trade-viz").length;
let loadMore = document.getElementById("load-more");
if (loadMore) {
    loadMore.onclick = loadTrades;
}
document.getElementById("team-filter").value = "";
document.getElementById("team-filter").onchange = filterTrades;
shadeTrades();

async function loadTrades() {
    let trades = document.querySelector(".trades");
    let filterTeam = document.getElementById("team-filter").value;
    let offset = trades.querySelectorAll(".trade-viz").length;
    let loadURL = new URL(window.location.href);
    loadURL.pathname = `/api/load_trades/${offset}`
    let response = await fetch(loadURL);
    let data = await response.json()
    console.log(data);
    let loadMore = document.getElementById("load-more");
    loadMore.insertAdjacentHTML("beforebegin", data[0]);
    if (data[1]) {
        loadMore.remove();
    }
    filterTrades(null, filterTeam);
    shadeTrades();
}

async function shadeTrades() {
    document
        .querySelectorAll(".trades .trade-viz:not(.hide)")
        .forEach((elem, idx) => {
            if (idx % 2 == 0) {
                elem.classList.remove("shaded");
            } else {
                elem.classList.add("shaded");
            }
        });
}

function filterTrades(event, filterTeam) {
    let team = filterTeam || document.getElementById("team-filter").value;
    if (!team) {
        document
            .querySelectorAll(".trades .trade-viz")
            .forEach(elem => elem.classList.remove("hide"));
    } else {
        document
            .querySelectorAll(`.trades .trade-viz:is(.${team})`)
            .forEach(elem => elem.classList.remove("hide"));
        document
            .querySelectorAll(`.trades .trade-viz:not(.${team})`)
            .forEach(elem => elem.classList.add("hide"));
    }
    document.getElementById("team-filter").value = team;
    shadeTrades();
}
