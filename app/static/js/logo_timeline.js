const TIMELINE = document.getElementById("timeline");
let fillColors = [];
let present = false;
document
    .querySelectorAll(".seasons")
    .forEach(elem => present = present || elem.innerText.includes("Present"));
if (present) {
    document
        .querySelector("#line")
        .setAttribute("marker-end", "url(#arrow)");
}

let seasons = document.querySelectorAll(".seasons")
let firstSeason = getFirstSeason(seasons[0]);
console.log("firstSeason:", firstSeason);
let lastSeason = seasons[seasons.length - 1]
    .innerText
    .split(/(: |\n)/)
    .pop();
let currentSeason = (new Date()).getFullYear();
if ((new Date()).getMonth() < 6) {
    currentSeason--;
}
console.log("currentSeason:", currentSeason);
if (isNaN(parseInt(lastSeason))) {
    lastSeason = currentSeason;
} else {
    lastSeason = lastSeason
        .split("-")
        .map(Number)
        .shift();
}
console.log("lastSeason:", lastSeason);
let seasonCount = lastSeason - firstSeason;
const SEASONINCREMENT = Math.round(100*95/seasonCount)/100;
const EVENLYSPACED = Math.round(100*100/seasons.length)/100;
console.log("seasonCount:", seasonCount);
console.log("seasonIncrement:", SEASONINCREMENT);


let newLogoSeasons = Array
    .from(seasons)
    .map(getFirstSeason);
let logoEndSeasons = Array
    .from(seasons)
    .map(getLastSeason);
console.log("seasons", seasons);
console.log("newLogoSeasons:", newLogoSeasons);
console.log("logoEndSeasons:", logoEndSeasons);

document.addEventListener("DOMContentLoaded", () => {
    setOnTimeline(newLogoSeasons, logoEndSeasons, lastSeason);
    setTimeout(getFillColors, 1000);
    setTimeout(getFillColors, 5000);
});

function getFirstSeason(season) {
    return season
        .innerText
        .split(/(: |\n)/)
        .filter(val => !isNaN(parseInt(val[0])))
        .shift()
        .split("-")
        .map(Number)
        .shift();
}

function getLastSeason(season) {
    return season
        .innerText
        .split(/(: |\n)/)
        .filter(val => !isNaN(parseInt(val[0])))
        .pop()
        .split("-")
        .map(Number)
        .pop();
}


function setOnTimeline(seasonStarts, seasonEnds, last) {
    //Calculate marking locations for each season
    //Draw ticks with season labels for each new logo
    //Draw colored boxes for each logo's time with small logo at left edge
    //On hover over box, highlight that specific logo in a zoomed in modal
    let startPositions = seasonStarts.map(val => (val - seasonStarts[0])*SEASONINCREMENT);
    let endPositions = seasonEnds.map(val => (val - seasonStarts[0])*SEASONINCREMENT);
    //let endPositions = startPositions.slice(1);
    let logos = document.querySelectorAll(".display object");
    endPositions.pop();
    let belowLineCheck = startPositions
        .map((val,idx) => Math.max(...startPositions.slice(0, idx)) >= val);
    let belowLine = belowLineCheck
        .map((val, idx) => (belowLineCheck.slice(0, idx+1).indexOf(true) > -1));
    if (TIMELINE.querySelector("#line").getAttribute("marker-end")) {
        endPositions.push(98.125); //Against the arrow
    } else {
        endPositions.push(98.75); //To the end of the line sans arrow
    }
    console.log("startPositions:", startPositions);
    console.log("endPositions:", endPositions);
    console.log("belowLineCheck:", belowLineCheck);
    console.log("belowLine:", belowLine);
    console.log("logos:", logos);

    startPositions
        .map((val, idx) => [val, seasonStarts[idx], endPositions[idx], belowLine[idx]])
        .forEach(addToTimeline);
}

function addToTimeline(position, idx) {
    let svgns = "http://www.w3.org/2000/svg";
    let container = document.createElementNS(svgns, "g");

    console.log(`createTick(${position})`);
    let tick = document.createElementNS(svgns, "rect");
    tick.setAttribute("width", 0.25);
    tick.setAttribute("height", 2);
    tick.setAttribute("x", position[0]);
    tick.setAttribute("y", 1);
    if (position[3]) {
        tick.setAttribute("y", 3);
    }
    tick.setAttribute("fill", "#000");
    tick.setAttribute("id", `tick-${idx}`);
    container.prepend(tick);
    
    //Labels are 5.5 long
    //If next tick is less than 5.5 from the 
    let season = `${position[1]}-${position[1]+1}`;
    console.log(`  seasonLabel=${season}`);
    let label = document.createElementNS(svgns, "text");
    //label.setAttribute("x", EVENLYSPACED*idx);
    label.setAttribute("x", position[0]);
    label.setAttribute("y", 8);
    label.setAttribute("id", `label-${idx}`);
    label.textContent=season;
    container.prepend(label);

    //Draw box from tick to tick
    let box = document.createElementNS(svgns, "rect");
    box.classList.add("box");
    box.setAttribute("x", position[0] + 0.2)
    box.setAttribute("y", 1)
    box.setAttribute("height", 1.875)
    box.setAttribute("width", position[2] - position[0] - 0.15);
    if (position[3]) {
        box.setAttribute("y", 3.15);
    }
    box.setAttribute("fill", "#E8E8E8");
    container.prepend(box);

    //Draw lines from tick to season
    let line = document.createElementNS(svgns, "rect");
    line.setAttribute("fill", "#000");
    line.setAttribute("width", "0.25");
    line.setAttribute("x", position[0]);
    line.setAttribute("y", 3);
    line.setAttribute("height", label.attributes.y.value - 4.25);
    line.classList.add("line");
    container.appendChild(line);

    container.onpointerover = highlightLogo;
    container.onpointerleave = dehighlightLogo;

    TIMELINE.prepend(container);
}

function getFillColors() {
    document.querySelectorAll("object").forEach(obj => {
        obj.getSVGDocument()
            .documentElement
            .querySelectorAll("*")
            .forEach(el => fillColors.push(el.getAttribute("fill")));
    });
    fillColors = Array.from(new Set(
        fillColors
            .filter(val => val)
            .filter(val => val.toLowerCase() != "#fff")
            .filter(val => val.toLowerCase() != "#ffffff")
            .filter(val => val.toLowerCase() != "none")
    ));
    TIMELINE
        .querySelectorAll(".box")
        .forEach(
            (box, idx) => box.setAttribute("fill", fillColors[idx % fillColors.length])
        );
    console.log(fillColors);

    let containers = TIMELINE.querySelectorAll("g");
    let logos = document.querySelectorAll(".display object");
    containers.forEach((el, idx) => {
        let logoIdx = idx;
        let dialogHTML = logos[logoIdx].parentElement.innerHTML;
        let dialog = document.createElement("dialog");
        dialog.innerHTML = dialogHTML;
        document.querySelector(".logo-display").prepend(dialog);
    });
}

function highlightLogo(event) {
    let container = event.target.parentElement;
    let containers = TIMELINE.querySelectorAll("g");
    let check = [];
    containers.forEach(el => check.push(el == container));
    let logoIdx = check.indexOf(true);
    let logo = document.querySelectorAll(".display object")[logoIdx];
    let highlight = logo.parentElement;
    let dialog = document.getElementsByTagName("dialog")[logoIdx];
    dialog.show();
}

function dehighlightLogo(event) {
    document.querySelectorAll("dialog")
        .forEach(el => el.close());
}
