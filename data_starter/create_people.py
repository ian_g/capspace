import sys
import json
import urllib.request
from urllib.error import HTTPError, URLError
from time import sleep
from datetime import date
from app import create_app, db
from app.models import Person


def main(args):
    app = create_app("default")
    app_context = app.app_context()
    app_context.push()

    if args:
        app.config["SQLALCHEMY_DATABASE_URI"] = args[0]
    MAX = 8484000
    # MIN = 8444850
    MIN = 8480000
    for nhl_id in range(MAX, MIN - 1, -1):
        add_to_database(nhl_id)
        sleep(1)

    app_context.pop()


def add_to_database(nhl_id):
    data = download_player(nhl_id)
    if data is None:
        return
    player_exists = Person.query.filter_by(nhl_id=nhl_id).count()
    if player_exists != 0:
        update_database(data["people"][0], nhl_id)
    else:
        create_player(data["people"][0], nhl_id)


def download_player(nhl_id, retry=False):
    try:
        data = json.loads(
            urllib.request.urlopen(
                f"https://statsapi.web.nhl.com/api/v1/people/{nhl_id}"
            ).read()
        )
    except HTTPError as e:
        print(f"{nhl_id}: {e}", file=sys.stderr)
        return None
    except URLError as e:
        print(f"{nhl_id}: {e}", file=sys.stderr)
        if not retry:
            sleep(5)
            data = download_player(nhl_id, True)
        else:
            return None
    if nhl_id % 1000 == 0:
        print(nhl_id)
    return data


def update_database(data, nhl_id):
    person = Person.query.filter_by(nhl_id=data["id"]).one_or_none()
    if None:
        print(f"{data['id']}: Found 0 or more than 1 person for ID", file=sys.stderr)
        return None
    person.position = data["primaryPosition"]["abbreviation"]

    if "active" in data:
        person.active = data["active"]
    if "primaryNumber" in data:
        person.number = data["primaryNumber"]
    if "alternateCaptain" in data:
        person.alternate_captain = data["alternateCaptain"]
    if "captain" in data:
        person.captain = data["captain"]
    if "rookie" in data:
        person.rookie = data["rookie"]
    if "rosterStatus" in data:
        person.non_roster = data["rosterStatus"] == "N"
    if "shootsCatches" in data:
        shoots = data["shootsCatches"]
    if "height" in data:
        person.height = 12 * int(data["height"][0]) + int(data["height"][3:-1])
    if "weight" in data:
        person.weight = int(data["weight"])
    if "nationality" in data:
        person.nationality = data["nationality"]

    db.session.add(person)
    db.session.commit()


def create_player(data, nhl_id):
    birthplace = None
    try:
        birthplace = data["birthCountry"]
        if "birthStateProvince" in data:
            birthplace = f"{data['birthStateProvince']} ,{birthplace}"
        birthplace = f"{data['birthCity']} ,{birthplace}"
    except KeyError as e:
        print(f"{nhl_id}: Birthplace missing: {e}", file=sys.stderr)
        if birthplace is None:
            return
        else:
            print(f"{nhl_id}: Using birthplace {birthplace}")

    person = Person(
        nhl_id=data["id"],
        name=data["fullName"],
    )

    if "birthDate" in data:
        person.birthdate = date.fromisoformat(data["birthDate"])
    else:
        print(f"{nhl_id}: Missing birthDate", file=sys.stderr)

    if birthplace:
        person.birthplace = birthplace
    else:
        print(f"{nhl_id}: Missing birthplace", file=sys.stderr)

    if "nationality" in data:
        person.nationality = data["nationality"]
    else:
        print(f"{nhl_id}: Missing nationality", file=sys.stderr)

    if "primaryPosition" in data:
        person.position = data["primaryPosition"]["abbreviation"]
    else:
        print(f"{nhl_id}: Missing position", file=sys.stderr)

    if "height" in data:
        person.height = 12 * int(data["height"][0]) + int(data["height"][3:-1])
    else:
        print(f"{nhl_id}: Missing height", file=sys.stderr)

    if "weight" in data:
        person.weight = int(data["weight"])
    else:
        print(f"{nhl_id}: Missing weight", file=sys.stderr)

    if "shootsCatches" in data:
        person.shoots = data["shootsCatches"]
    else:
        print(f"{nhl_id}: Missing shoot/catch hand", file=sys.stderr)

    if "active" in data:
        person.active = data["active"]
    if "primaryNumber" in data:
        person.number = data["primaryNumber"]
    if "alternateCaptain" in data:
        person.alternate_captain = data["alternateCaptain"]
    if "captain" in data:
        person.captain = data["captain"]
    if "rookie" in data:
        person.rookie = data["rookie"]
    if "rosterStatus" in data:
        person.non_roster = data["rosterStatus"] == "N"

    db.session.add(person)
    db.session.commit()


if __name__ == "__main__":
    main(sys.argv[1:])
