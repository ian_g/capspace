"""add 'compliance' boolean to 'Buyout Year'

Revision ID: faa9c31b36c3
Revises: 4039bf662e16
Create Date: 2022-02-15 16:58:56.505368

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "faa9c31b36c3"
down_revision = "4039bf662e16"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("buyout_year", sa.Column("compliance", sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("buyout_year", "compliance")
    # ### end Alembic commands ###
