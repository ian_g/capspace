"""remove draft_pick.note column

Revision ID: a26c0048fbb6
Revises: 734ed1e3348f
Create Date: 2022-07-01 00:11:06.886350

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "a26c0048fbb6"
down_revision = "734ed1e3348f"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("draft_pick", "note")
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("draft_pick", sa.Column("note", sa.VARCHAR(), nullable=True))
    # ### end Alembic commands ###
