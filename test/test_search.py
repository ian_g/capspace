import sys
import random
import unittest
from datetime import datetime, date
from app import create_app, db
from app.models import Team, Person, Contract, Asset, Alias

from app.main import search

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class SearchTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_team_search(self):
        self.assertEqual(search.team_search(["New York"]), [])
        teams = [
            Team(id=1, logo="testNYR", name="New York Rangers"),
            Team(id=2, logo="testNYI", name="New York Islanders"),
            Team(id=3, logo="testNJD", name="New Jersey Devils"),
        ]
        for team in teams:
            db.session.add(team)
        db.session.commit()

        data = search.team_search(["New York"])
        self.assertEqual(len(data), 2)
        self.assertEqual(data[0]["name"], "New York Islanders")
        self.assertEqual(data[0]["id"], 2)
        self.assertEqual(data[0]["logo"], "testNYI")
        self.assertEqual(data[1]["name"], "New York Rangers")
        self.assertEqual(data[1]["id"], 1)
        self.assertEqual(data[1]["logo"], "testNYR")

        data = search.team_search(["York"])
        self.assertEqual(len(data), 2)
        self.assertEqual(data[0]["name"], "New York Islanders")
        self.assertEqual(data[0]["id"], 2)
        self.assertEqual(data[0]["logo"], "testNYI")
        self.assertEqual(data[1]["name"], "New York Rangers")
        self.assertEqual(data[1]["id"], 1)
        self.assertEqual(data[1]["logo"], "testNYR")

        data = search.team_search(["New"])
        self.assertEqual(len(data), 3)
        self.assertEqual(data[0]["name"], "New Jersey Devils")
        self.assertEqual(data[0]["id"], 3)
        self.assertEqual(data[0]["logo"], "testNJD")
        self.assertEqual(data[1]["name"], "New York Islanders")
        self.assertEqual(data[1]["id"], 2)
        self.assertEqual(data[1]["logo"], "testNYI")
        self.assertEqual(data[2]["name"], "New York Rangers")
        self.assertEqual(data[2]["id"], 1)
        self.assertEqual(data[2]["logo"], "testNYR")

    def test_current_team(self):
        PERSON = 92
        TEAM = 88

        self.assertIsNone(search.current_team(PERSON))
        db.session.add(Person(id=PERSON))
        asset = Asset(id=22, current_team=TEAM, active=True)
        db.session.add(asset)
        db.session.add(Contract(id=22, person_id=PERSON))
        db.session.commit()

        self.assertIsNone(search.current_team(PERSON))
        db.session.add(Team(id=TEAM, name="Test Team", logo="logo", abbreviation="TST"))
        db.session.commit()

        self.assertEqual(
            search.current_team(PERSON),
            {"id": TEAM, "logo": "logo", "abbreviation": "TST"},
        )

        asset.active = False
        db.session.add(asset)
        db.session.commit()
        self.assertIsNone(search.current_team(PERSON))

    def test_person_search(self):
        people = [
            Person(
                id=34,
                name="Cam Ward",
                nationality="CAN",
                position="G",
                birthdate=date(year=1983, month=5, day=6),
            ),
            Person(
                id=22,
                name="Cam York",
                nationality="USA",
                position="D",
                birthdate=date(year=1993, month=5, day=6),
            ),
            Person(
                id=89,
                name="Mike York",
                nationality="USA",
                position="C",
                birthdate=date(year=1973, month=5, day=6),
            ),
            Person(
                id=98,
                name="Joe Thornton",
                nationality="CAN",
                position="C",
                birthdate=date(year=1970, month=5, day=6),
            ),
        ]
        aliases = [
            Alias(
                person_id=people[0].id,
                value=people[0].name,
            ),
            Alias(
                person_id=people[1].id,
                value=people[1].name,
            ),
            Alias(
                person_id=people[2].id,
                value=people[2].name,
            ),
            Alias(
                person_id=people[3].id,
                value=people[3].name,
            ),
        ]
        for record in people + aliases:
            db.session.add(record)
        db.session.commit()

        self.assertEqual(search.person_search(["Chara"]), [])
        data = search.person_search(["Cam"])
        self.assertEqual(len(data), 2)
        self.assertEqual(data[0]["id"], 22)
        self.assertEqual(data[0]["name"], "Cam York")
        self.assertEqual(data[0]["nationality"], "USA")
        self.assertEqual(data[0]["position"], "D")
        self.assertEqual(data[1]["id"], 34)
        self.assertEqual(data[1]["name"], "Cam Ward")
        self.assertEqual(data[1]["nationality"], "CAN")
        self.assertEqual(data[1]["position"], "G")

        data = search.person_search(["Cam", "Joe"])
        self.assertEqual(len(data), 3)
        self.assertEqual(data[0]["id"], 22)
        self.assertEqual(data[1]["id"], 34)
        self.assertEqual(data[2]["id"], 98)
        self.assertEqual(data[2]["name"], "Joe Thornton")
        self.assertEqual(data[2]["nationality"], "CAN")
        self.assertEqual(data[2]["position"], "C")

    def test_search_data(self):
        self.assertEqual(
            search.search_data(""), {"person": [], "team": [], "search": ""}
        )
        self.assertEqual(
            search.search_data("Cam York"),
            {
                "team": [],
                "defunct_team": [],
                "person": [],
                "search": "Cam York",
            },
        )
        people = [
            Person(
                id=34,
                name="Cam Ward",
                nationality="CAN",
                position="G",
                birthdate=date(year=1983, month=5, day=6),
            ),
            Person(
                id=22,
                name="Cam York",
                nationality="USA",
                position="D",
                birthdate=date(year=1993, month=5, day=6),
            ),
            Person(
                id=89,
                name="Mike York",
                nationality="USA",
                position="C",
                birthdate=date(year=1973, month=5, day=6),
            ),
            Person(
                id=98,
                name="Joe Thornton",
                nationality="CAN",
                position="C",
                birthdate=date(year=1970, month=5, day=6),
            ),
        ]
        aliases = [
            Alias(
                person_id=people[0].id,
                value=people[0].name,
            ),
            Alias(
                person_id=people[1].id,
                value=people[1].name,
            ),
            Alias(
                person_id=people[2].id,
                value=people[2].name,
            ),
            Alias(
                person_id=people[3].id,
                value=people[3].name,
            ),
        ]
        teams = [
            Team(id=1, logo="testNYR", name="New York Rangers"),
            Team(id=2, logo="testNYI", name="New York Islanders"),
            Team(id=3, logo="testNJD", name="New Jersey Devils"),
        ]
        for record in people + teams + aliases:
            db.session.add(record)
        db.session.commit()

        data = search.search_data("Cam York")
        self.assertEqual(data["search"], "Cam York")
        self.assertEqual(len(data["team"]), 2)
        self.assertEqual(data["team"][0]["name"], "New York Islanders")
        self.assertEqual(data["team"][1]["name"], "New York Rangers")
        self.assertEqual(len(data["person"]), 3)
        self.assertEqual(data["person"][0]["name"], "Cam York")
        self.assertEqual(data["person"][1]["name"], "Cam Ward")
        self.assertEqual(data["person"][2]["name"], "Mike York")


if __name__ == "__main__":
    unittest.main(verbosity=2)
