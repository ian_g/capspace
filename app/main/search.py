from datetime import date
from flask import current_app
from sqlalchemy import or_, select, func, case
from .. import db
from ..models import Team, Person, Contract, Asset, Alias


def search_data(search_text):
    if not search_text:
        return {"team": [], "person": [], "search": ""}
    if search_text[0] == search_text[-1] and search_text[0] == '"':
        search_text = [search_text[1:-1]]
    else:
        search_text = search_text.split()
    return {
        "team": team_search(search_text),
        "defunct_team": team_search(search_text, False),
        "person": person_search(search_text),
        "search": " ".join(search_text),
    }


def team_search(search_text, active=True):
    data = (
        Team.query.filter(or_(*[Team.name.like(f"%{word}%") for word in search_text]))
        .filter(Team.active == active)
        .with_entities(
            Team.id,
            Team.name,
            Team.logo,
        )
        .order_by(
            Team.name.asc(),
        )
        .all()
    )

    return [
        {
            "id": team[0],
            "name": team[1],
            "logo": team[2],
        }
        for team in data
    ]


def person_search(search_text):
    today = date.today()
    data = (
        Person.query.filter(
            Person.id.in_(
                select(
                    Alias.query.filter(
                        or_(*[Alias.value.like(f"%{word}%") for word in search_text])
                    )
                    .with_entities(
                        Alias.person_id,
                    )
                    .subquery()
                )
            )
        )
        .with_entities(
            Person.id,
            Person.name,
            Person.nationality,
            Person.position,
            Person.birthdate,
            case(
                (Person.death_date == None, today),
                else_=Person.death_date,
            ),
        )
        .order_by(
            Person.active.desc(),
            Person.ep_id == None,
            Person.birthdate.desc(),
        )
        .all()
    )
    print(data)
    return [
        {
            "id": person[0],
            "name": person[1],
            "nationality": person[2],
            "position": person[3],
            "age": (
                person[5].year
                - person[4].year
                - ((person[5].month, person[5].day) < (person[4].month, person[4].day))
                if person[4]
                else ""
            ),
            "team": current_team(person[0]),
            "deceased": person[5] != today,
        }
        for person in data
    ]


def current_team(person_id):
    data = (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .join(Team, Team.id == Asset.current_team)
        .filter(Asset.active == True)
        .filter(Contract.person_id == person_id)
        .with_entities(
            Team.abbreviation,
            Team.id,
            Team.logo,
        )
        .first()
    )
    if not data:
        return None
    return {
        "abbreviation": data[0],
        "id": data[1],
        "logo": data[2],
    }
