document
    .querySelectorAll('#team option')
    .forEach((el) => {
        if (el.attributes.hasOwnProperty('selected')) el.attributes.removeNamedItem('selected')
    });
