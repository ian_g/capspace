import sys
import json
import urllib.request
from time import sleep
from datetime import date, timedelta
from urllib.error import HTTPError, URLError
from http.client import RemoteDisconnected
from flask import current_app
from flask_wtf import FlaskForm
from sqlalchemy import or_, and_, func, select
from wtforms import (
    DateField,
    StringField,
    HiddenField,
    IntegerField,
    SelectMultipleField,
    BooleanField,
    SubmitField,
)
from wtforms.validators import DataRequired, NumberRange, Length, Optional

from .. import db, scheduler
from ..models import (
    Person,
    Team,
    Asset,
    Contract,
    ContractYear,
    SkaterSeason,
    GoalieSeason,
    Season,
    Alias,
    Transaction,
    TransactionAsset,
)
from .team_season import update_all_teams_seasons


@scheduler.task(
    "cron", id="monthly_active_user", day=1, hour=1, timezone="America/New_York"
)
def run_monthly_update():
    with scheduler.app.app_context():
        if current_app.config["DISABLE_SCHEDULED"]:
            return
        people = (
            Person.query.filter(Person.active == True)
            .filter(Person.nhl_id != None)
            .with_entities(
                Person.id,
                Person.nhl_id,
                Person.position,
            )
            .all()
        )
        season = Season.query.get(scheduler.app.config["CURRENT_SEASON"]).name.replace(
            "-", ""
        )
        if date.today().month > 9:
            deactivate_tryouts()
        for person in people:
            current_app.logger.info(f"Monthly update: {person[0]}-{person[1]}")
            scheduled_person_update(*person, season)
            sleep(0.5)


def deactivate_tryouts():
    Asset.query.filter(Asset.active == True).filter(
        Asset.type == "Professional Tryout"
    ).update({Asset.active: False}, synchronize_session="fetch")
    db.session.commit()


def update_contract_exempt():
    # Less than 11 games this season
    skater_games = (
        SkaterSeason.query.filter(
            SkaterSeason.season_id == current_app.config["CURRENT_SEASON"]
        )
        .filter(SkaterSeason.games_played >= 11)
        .with_entities(SkaterSeason.person_id)
        .subquery()
    )
    goalie_games = (
        GoalieSeason.query.filter(
            GoalieSeason.season_id == current_app.config["CURRENT_SEASON"]
        )
        .filter(GoalieSeason.games_played >= 11)
        .with_entities(GoalieSeason.person_id)
        .subquery()
    )

    # On loan
    on_loan = (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .filter(Contract.on_loan == True)
        .filter(Asset.active == True)
        .with_entities(Contract.person_id)
        .subquery()
    )

    active_contracts = (
        Asset.query.filter(Asset.active == True)
        .filter(Asset.type == "Contract")
        .with_entities(Asset.id)
        .subquery()
    )

    # Less than 20yo at the start of the season
    cutoff_date = Season.query.get(
        current_app.config["CURRENT_SEASON"]
    ).regular_season_start
    cutoff_date = cutoff_date.replace(year=cutoff_date.year - 20)
    under_20 = (
        Person.query.filter(Person.birthdate <= cutoff_date)
        .with_entities(Person.id)
        .subquery()
    )

    Contract.query.filter(Contract.id.in_(select(active_contracts))).update(
        {
            Contract.contract_limit_exempt: False,
        },
        synchronize_session="fetch",
    )

    Contract.query.filter(Contract.person_id.not_in(select(under_20))).filter(
        Contract.person_id.not_in(select(skater_games))
    ).filter(Contract.person_id.not_in(select(goalie_games))).filter(
        Contract.person_id.in_(select(on_loan))
    ).update(
        {
            Contract.contract_limit_exempt: True,
        },
        synchronize_session="fetch",
    )
    db.session.commit()


def get_person_stats(person_id):
    season_name = Season.query.get(scheduler.app.config["CURRENT_SEASON"]).name.replace(
        "-", ""
    )
    try:
        data = json.loads(
            urllib.request.urlopen(
                urllib.request.Request(
                    f"https://api-web.nhle.com/v1/player/{person_id}/game-log/{season_name}/2",
                    data=None,
                    headers={
                        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0"
                    },
                )
            ).read()
        )
    except HTTPError as e:
        current_app.logger.error(f"{nhl_id}: {e}")
        return
    except URLError as e:
        current_app.logger.error(f"{nhl_id}: {e}")
        return
    except RemoteDisconnected as e:
        current_app.logger.error(f"{nhl_id}: {e}")
        sleep(40)
        return
    return data


def update_goalie_stats(person_id):
    data = get_person_stats(person_id)
    if not data:
        return
    if "gameLog" not in data:
        return
    aggregate_base = {
        "starts": 0,
        "wins": 0,
        "losses": 0,
        "overtime_losses": 0,
        "shutouts": 0,
        "shots_against": 0,
        "goals_against": 0,
        "games_played": 0,
    }
    aggregate = {}
    for game in data["gameLog"]:
        team = game["teamAbbrev"]
        if team not in aggregate:
            aggregate[team] = aggregate_base.copy()
        aggregate[team]["starts"] += game["gamesStarted"]
        aggregate[team]["wins"] += game["decision"] == "W"
        aggregate[team]["losses"] += game["decision"] == "L"
        aggregate[team]["overtime_losses"] += game["decision"] == "O"
        aggregate[team]["shutouts"] += game["shutouts"]
        aggregate[team]["shots_against"] += game["shotsAgainst"]
        aggregate[team]["goals_against"] += game["goalsAgainst"]
        aggregate[team]["games_played"] += 1
    for team in aggregate:
        record = Team.query.filter(Team.abbreviation == team).first()
        if not record:
            current_app.logger.error(
                f"Team {team} does not exist for person {person_id}"
            )
            continue
        stats = (
            GoalieSeason.query.filter(GoalieSeason.person_id == person_id)
            .filter(GoalieSeason.season_id == scheduler.app.config["CURRENT_SEASON"])
            .filter(GoalieSeason.team_id == record.id)
            .first()
        )
        if stats is None:
            stats = GoalieSeason(
                person_id=person_id,
                season_id=scheduler.app.config["CURRENT_SEASON"],
                team_id=record.id,
            )
        stats.starts = aggregate[team]["starts"]
        stats.wins = aggregate[team]["wins"]
        stats.losses = aggregate[team]["losses"]
        stats.overtime_losses = aggregate[team]["overtime_losses"]
        stats.shutouts = aggregate[team]["shutouts"]
        stats.shots_against = aggregate[team]["shots_against"]
        stats.goals_against = aggregate[team]["goals_against"]
        stats.games_played = aggregate[team]["games_played"]
        db.session.add(stats)
    return


def update_skater_stats(person_id):
    data = get_person_stats(person_id)
    if not data:
        current_app.logger.debug("Data not found")
        return
    if "gameLog" not in data:
        current_app.logger.debug("gameLog not in data")
        return
    aggregate_base = {
        "goals": 0,
        "assists": 0,
        "points": 0,
        "games_played": 0,
        "hits": 0,
        "shots": 0,
        "penalty_minutes": 0,
    }
    aggregate = {}
    for game in data["gameLog"]:
        team = game["teamAbbrev"]
        if team not in aggregate:
            aggregate[team] = aggregate_base.copy()
        aggregate[team]["goals"] += game["goals"]
        aggregate[team]["assists"] += game["assists"]
        aggregate[team]["points"] += game["points"]
        aggregate[team]["games_played"] += 1
        aggregate[team]["shots"] += game["shots"]
        aggregate[team]["penalty_minutes"] += game["pim"]
    for team in aggregate:
        record = Team.query.filter(Team.abbreviation == team).first()
        if not record:
            current_app.logger.error(
                f"Team {team} does not exist for person {person_id}"
            )
            continue
        stats = (
            SkaterSeason.query.filter(SkaterSeason.person_id == person_id)
            .filter(SkaterSeason.season_id == scheduler.app.config["CURRENT_SEASON"])
            .filter(SkaterSeason.team_id == record.id)
            .first()
        )
        if stats is None:
            stats = SkaterSeason(
                person_id=person_id,
                season_id=scheduler.app.config["CURRENT_SEASON"],
                team_id=record.id,
            )
        stats.goals = aggregate[team]["goals"]
        stats.assists = aggregate[team]["assists"]
        stats.points = aggregate[team]["points"]
        stats.games_played = aggregate[team]["games_played"]
        stats.shots = aggregate[team]["shots"]
        stats.penalty_minutes = aggregate[team]["penalty_minutes"]
        db.session.add(stats)
    return


def update_skater_waiver_exemption(person_id, age):
    if age is None:
        return False
    elif type(age).__name__ == "str" and not age.isdigit():
        return False
    elif type(age).__name__ == "str" and age.isdigit():
        age = int(age)
    elif type(age).__name__ == "int":
        pass
    else:
        return False
    data = (
        SkaterSeason.query.join(Season, Season.id == SkaterSeason.season_id)
        .filter(SkaterSeason.person_id == person_id)
        .order_by(Season.regular_season_start.asc())
        .group_by(SkaterSeason.season_id)
        .with_entities(
            func.sum(SkaterSeason.games_played),
        )
        .all()
    )
    games = sum([season[0] for season in data])
    seasons = (
        Season.query.filter(
            Season.regular_season_start
            >= (
                Contract.query.join(Season, Season.id == Contract.signing_season_id)
                .filter(Contract.person_id == person_id)
                .with_entities(func.min(Season.regular_season_start))
                .scalar_subquery()
            )
        )
        .filter(Season.free_agency_opening <= date.today())
        .with_entities(func.count(Season.id))
        .first()[0]
    )

    if seasons > 5:
        return False
    if age >= 25 and seasons > 1:
        return False
    elif age == 24 and (seasons > 2 or games > 60):
        return False
    elif age == 23 and (seasons > 3 or games > 60):
        return False
    elif age == 22 and (seasons > 3 or games > 70):
        return False
    elif age == 21 and (seasons > 3 or games > 80):
        return False
    elif age == 20 and (seasons > 3 or games > 160):
        return False
    elif (
        age == 19
        and len(data) > 0
        and len(data[0]) > 0
        and data[0][0] > 10
        and (seasons > 3 or games > 160)
    ):
        return False
    elif age == 19 and (seasons > 4 or games > 160):
        return False
    elif (
        age == 18
        and len(data) > 0
        and len(data[0]) > 0
        and data[0][0] > 10
        and (seasons > 3 or games > 160)
    ):
        return False
    elif age == 18 and (seasons > 5 or games > 160):
        return False
    return True


def update_goalie_waiver_exemption(person_id, age):
    if age is None:
        return False
    elif type(age).__name__ == "str" and not age.isdigit():
        return False
    elif type(age).__name__ == "str" and age.isdigit():
        age = int(age)
    elif type(age).__name__ == "int":
        pass
    else:
        return False
    data = (
        GoalieSeason.query.join(Season, Season.id == GoalieSeason.season_id)
        .filter(GoalieSeason.person_id == person_id)
        .group_by(GoalieSeason.season_id)
        .order_by(Season.regular_season_start.asc())
        .with_entities(
            func.sum(GoalieSeason.games_played),
        )
        .all()
    )
    games = sum([entry[0] for entry in data])
    seasons = (
        Season.query.filter(
            Season.regular_season_start
            >= (
                Contract.query.join(Season, Season.id == Contract.signing_season_id)
                .filter(Contract.person_id == person_id)
                .with_entities(func.min(Season.regular_season_start))
                .scalar_subquery()
            )
        )
        .filter(Season.free_agency_opening <= date.today())
        .with_entities(func.count(Season.id))
        .first()[0]
    )

    if seasons > 6:
        return False
    elif age >= 25 and seasons > 1:
        return False
    elif age == 24 and (seasons > 2 or games > 60):
        return False
    elif age == 23 and (seasons > 3 or games > 60):
        return False
    elif (age == 22 or age == 21) and (seasons > 4 or games > 60):
        return False
    elif age == 20 and (seasons > 4 or games > 80):
        return False
    elif (
        age == 19
        and len(data) > 0
        and len(data[0]) > 0
        and data[0][0] > 10
        and (seasons > 4 or games > 80)
    ):
        return False
    elif age == 19 and (seasons > 5 or games > 80):
        return False
    elif (
        age == 18
        and len(data) > 0
        and len(data[0]) > 0
        and data[0][0] > 10
        and (seasons > 4 or games > 80)
    ):
        return False
    elif age == 18 and (seasons > 6 or games > 80):
        return False
    return True


def scheduled_person_update(person_id, nhl_id, position, season):
    try:
        data = json.loads(
            urllib.request.urlopen(
                urllib.request.Request(
                    f"https://api-web.nhle.com/v1/player/{nhl_id}/landing",
                    data=None,
                    headers={
                        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0"
                    },
                )
            ).read()
        )
        # urllib.request.urlopen(
        #    f"https://statsapi.web.nhl.com/api/v1/people/{nhl_id}?expand=person.stats&stats=statsSingleSeason&season={season}"
        # ).read()
    except HTTPError as e:
        current_app.logger.error(f"{nhl_id}: {e}")
        return
    except URLError as e:
        current_app.logger.error(f"{nhl_id}: {e}")
        return
    except RemoteDisconnected as e:
        current_app.logger.error(f"{nhl_id}: {e}")
        sleep(40)
        return
    person = Person.query.get(person_id)
    if "weightInPounds" in data:
        person.weight = data["weightInPounds"]
    if "sweaterNumber" in data:
        person.number = data["sweaterNumber"]
    current_app.logger.info(
        f"Updating stats: name={person.name} id={person.id} nhl_id={person.nhl_id}"
    )

    if person.position == "G":
        update_goalie_stats(person_id)
        person.waivers_exempt = update_goalie_waiver_exemption(
            person_id, person.waivers_signing_age
        )
    else:
        update_skater_stats(person_id)
        person.waivers_exempt = update_skater_waiver_exemption(
            person_id, person.waivers_signing_age
        )

    db.session.add(person)
    db.session.commit()


def validate_person(name):
    if "-" in str(name):
        name = str(name).split("-")[-1]
    name_count = Person.query.filter(
        or_(
            Person.id == name,
            Person.nhl_id == name,
            Person.name == name,
        )
    ).count()
    if name_count != 1:
        return {"error": f"Identifier {name} belongs to {name_count} people"}
    person = Person.query.filter(
        or_(
            Person.id == name,
            Person.nhl_id == name,
            Person.name == name,
        )
    ).first()
    if not person:
        return {"error": f"Person {name} does not exist"}
    return {
        "name": person.name,
        "id": person.id,
        "nhl_id": person.nhl_id,
        "birthdate": (
            person.birthdate.strftime(current_app.config["DATE_FMT"])
            if person.birthdate
            else None
        ),
        "birthplace": person.birthplace,
        "nationality": person.nationality,
        "position": person.position,
        "number": person.number,
        "weight": person.weight,
        "current_status": person.current_status or "",
        "active": person.active,
        "rookie": person.rookie,
        "captain": person.captain,
        "alternate_captain": person.alternate_captain,
        "non_roster": person.non_roster,
        "ep_id": person.ep_id,
        "waivers_exempt": person.waivers_exempt,
        "aliases": get_aliases(person.id),
    }


def get_aliases(person_id):
    data = (
        Alias.query.filter(Alias.person_id == person_id)
        .filter(Alias.search == False)
        .with_entities(
            Alias.value,
        )
        .all()
    )
    return [value[0] for value in data]


def create_transaction(status, person_id, date=None):
    query = (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .join(ContractYear, ContractYear.contract_id == Contract.id)
        .filter(Contract.person_id == person_id)
        .filter(Asset.active == True)
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .with_entities(
            Contract.id,
            Asset.current_team,
        )
        .first()
    )
    contract = query[0]
    team = query[1]
    transaction = Transaction(
        type=status,
        season_id=current_app.config["CURRENT_SEASON"],
        date=date,
    )
    db.session.add(transaction)
    db.session.flush()
    transaction_asset = TransactionAsset(
        transaction_id=transaction.id,
        asset_id=contract,
        former_team=team,
        new_team=team,
    )
    db.session.add(transaction_asset)
    db.session.commit()


def create_status_transactions(previous, current, person_id, date=None):
    additions = current - previous
    ir_statuses = ["IR", "LTIR", "SOIR"]

    added_to_ir = False
    for status in ir_statuses:
        if status in additions:
            create_transaction(f"Placed on {status}", person_id, date)
            added_to_ir = True

    if added_to_ir:
        return
    if "Suspended" in additions:
        create_transaction(f"Suspended", person_id, date)
        return

    removals = previous - current
    removed_from_ir = False
    for status in ir_statuses:
        if status in removals:
            create_transaction(f"Activated from {status}", person_id, date)

    if "Suspended" in removals:
        create_transaction(f"Activated from suspension", person_id, date)


class UpdatePerson(FlaskForm):
    def set_values(self, data):
        self.person.data = data["id"]
        self.id.data = data["id"]
        self.nhl_id.data = data["nhl_id"]
        self.number.data = data["number"]
        self.weight.data = data["weight"]
        self.current_status.data = data["current_status"].split(",")
        self.active.data = data["active"]
        self.rookie.data = data["rookie"]
        self.captain.data = data["captain"]
        self.alternate_captain.data = data["alternate_captain"]
        self.non_roster.data = data["non_roster"]
        self.ep_id.data = data["ep_id"]
        self.waivers_exempt.data = data["waivers_exempt"]
        self.aliases.data = data["aliases"]

    def update_person(self):
        person = Person.query.get(self.id.data)
        previous_status = set(person.current_status.split(","))
        current_status = set(self.current_status.data)
        create_status_transactions(
            previous_status, current_status, self.id.data, self.date.data
        )

        if person.weight != self.weight.data:
            person.weight = self.weight.data
        if person.number != self.number.data:
            person.number = self.number.data
        if person.non_roster != self.non_roster.data:
            person.non_roster = self.non_roster.data
        if person.alternate_captain != self.alternate_captain.data:
            person.alternate_captain = self.alternate_captain.data
        if person.captain != self.captain.data:
            person.captain = self.captain.data
        if person.rookie != self.rookie.data:
            person.rookie = self.rookie.data
        if person.active != self.active.data:
            person.active = self.active.data
        if person.ep_id is None and self.ep_id.data is not None:
            person.ep_id = self.ep_id.data
        person.set_status(self.current_status.data)
        person.waivers_exempt = self.waivers_exempt.data
        db.session.add(person)
        current_team = (
            Asset.query.join(Contract, Contract.id == Asset.id)
            .filter(Contract.person_id == self.id.data)
            .filter(Asset.active == True)
            .with_entities(
                Asset.current_team,
            )
            .first()
        )

        if self.aliases.data:
            self.update_aliases()
        if current_team:
            update_all_teams_seasons(current_team[0])
        db.session.commit()
        return person.id

    def update_aliases(self):
        # Delete un-mentioned aliases
        Alias.query.filter(Alias.search == False).filter(
            Alias.person_id == self.id.data
        ).filter(and_(*[Alias.value != alias for alias in self.aliases.data])).delete()
        for alias in self.aliases.data:
            # Create any new ones that don't exist
            exists = (
                Alias.query.filter(Alias.search == False)
                .filter(Alias.person_id == self.id.data)
                .filter(Alias.value == alias)
                .count()
            )
            if exists > 0:
                continue
            db.session.add(
                Alias(
                    person_id=self.id.data,
                    search=False,
                    value=alias,
                )
            )

    def validate_ep_id(self, ep_id):
        if ep_id.data is None:
            return True
        person = Person.query.filter(Person.ep_id == ep_id.data).all()
        if len(person) == 0:
            return True
        if person[0].id == int(self.id.data):
            return True
        ep_id.errors.append("EliteProspects ID already found in database")
        return False

    person = StringField("Name/ID")
    id = HiddenField("ID", validators=[DataRequired()])
    nhl_id = HiddenField("NHL ID")

    ep_id = IntegerField("EliteProspects ID", validators=[Optional()])
    weight = IntegerField(
        "Weight (lbs)", validators=[NumberRange(min=48, max=400), Optional()]
    )
    date = DateField("Date", default=date.today)
    number = IntegerField("Number", validators=[NumberRange(min=1, max=98), Optional()])
    active = BooleanField("Active", default=True)
    rookie = BooleanField("Rookie", default=True)
    captain = BooleanField("Captain", default=False)
    alternate_captain = BooleanField("Alternate Captain", default=False)
    non_roster = BooleanField("Non-roster player", default=True)
    current_status = SelectMultipleField(
        "Current Status",
        choices=[
            "RFA",
            "UFA",
            "Under Contract",
            "IR",
            "LTIR",
            "SOIR",
            "Non-Roster",
            "Minors",
            "Juniors",
            "Suspended",
        ],
        validate_choice=False,
    )
    aliases = SelectMultipleField(
        "Visible Aliases",
        choices=[],
        validators=[Optional()],
        id="aliases",
        validate_choice=False,
    )
    waivers_exempt = BooleanField("Waivers Exempt", default=False)
    submit = SubmitField("Update User")
