import sys
import random
import unittest
from datetime import datetime, date, timedelta
from sqlalchemy import func
from app import create_app, db
from app.models import (
    ContractYear,
    Asset,
    Season,
    RetainedSalary,
    BuyoutYear,
    PenaltyYear,
    TeamSeason,
    Contract,
    Person,
    DailyCap,
    Team,
    SkaterSeason,
    GoalieSeason,
)

from app.admin import team_season

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class TeamSeasonTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_get_contract_count(self):
        team_id = 7
        season_id = 4
        count = random.randint(1, 50)
        for idx in range(count):
            db.session.add(
                Asset(
                    id=idx,
                    current_team=team_id,
                )
            )
            db.session.add(
                ContractYear(
                    contract_id=idx,
                    season_id=season_id,
                )
            )
            db.session.add(
                Contract(
                    id=idx,
                )
            )
        db.session.commit()
        self.assertEqual(team_season.get_contract_count(team_id, season_id), count)

    def test_get_retained_count(self):
        team_id = 7
        season_id = 4
        self.assertEqual(team_season.get_retained_count(team_id, season_id), 0)

        season = Season(
            id=4,
            name="Test Szn",
            playoffs_start=date(year=2020, month=1, day=1),
        )
        asset = Asset(id=1)
        retained = RetainedSalary(
            id=1,
            contract_id=1,
            retained_by=team_id,
            retained_from=date(year=2020, month=6, day=1),
        )
        contract_year = ContractYear(
            contract_id=1,
            season_id=season_id,
        )
        db.session.add(asset)
        db.session.add(season)
        db.session.add(retained)
        db.session.add(contract_year)
        db.session.commit()
        self.assertEqual(team_season.get_retained_count(team_id, season_id), 0)

        retained.retained_from = date(year=2019, month=6, day=1)
        db.session.add(retained)
        db.session.commit()
        self.assertEqual(team_season.get_retained_count(team_id, season_id), 1)

    def test_get_max_ltir_relief(self):
        team_id = 3
        season_id = self.app.config["CURRENT_SEASON"]
        contract_id = 5
        person_id = 88
        season = Season(id=season_id, salary_cap=1500, draft_date=date.today())
        db.session.add(season)
        self.assertEqual(team_season.get_max_ltir_relief(team_id, season_id), 0)

        person = Person(id=person_id, name="Test Person", current_status="LTIR")
        asset = Asset(id=contract_id, current_team=team_id)
        contract = Contract(id=contract_id, person_id=person_id, non_roster=False)
        contract_year = ContractYear(
            contract_id=contract_id, season_id=season_id, cap_hit=1500
        )
        db.session.add(person)
        db.session.add(asset)
        db.session.add(contract)
        db.session.add(contract_year)
        db.session.commit()
        self.assertEqual(team_season.get_max_ltir_relief(team_id, season_id), 1500)

        season.salary_cap = 2500
        db.session.add(season)
        db.session.commit()
        # self.assertEqual(team_season.get_max_ltir_relief(team_id, season_id), 500)
        self.assertEqual(team_season.get_max_ltir_relief(team_id, season_id), 1500)

        person2 = Person(id=person_id + 1, name="Test Person Too")
        asset2 = Asset(id=contract_id + 1, current_team=team_id)
        contract2 = Contract(
            id=contract_id + 1, person_id=person_id + 1, non_roster=False
        )
        contract_year2 = ContractYear(
            contract_id=contract_id + 1, season_id=season_id, cap_hit=500
        )
        db.session.add(person2)
        db.session.add(asset2)
        db.session.add(contract2)
        db.session.add(contract_year2)
        db.session.commit()
        # self.assertEqual(team_season.get_max_ltir_relief(team_id, season_id), 1000)
        self.assertEqual(team_season.get_max_ltir_relief(team_id, season_id), 1500)

        contract_year.cap_hit = 1000
        person2.current_status = "LTIR"
        db.session.add(contract_year)
        db.session.add(person2)
        db.session.commit()
        # self.assertEqual(team_season.get_max_ltir_relief(team_id, season_id), 500)
        self.assertEqual(team_season.get_max_ltir_relief(team_id, season_id), 1500)

    def test_get_contract_cap_hit(self):
        team_id = 7
        season_id = 4
        self.assertEqual(team_season.get_contract_cap_hit(team_id, season_id), 0)

        years = [
            ContractYear(contract_id=1, season_id=season_id, cap_hit=940),
            ContractYear(contract_id=2, season_id=season_id, cap_hit=486),
            ContractYear(contract_id=3, season_id=season_id, cap_hit=461),
            ContractYear(contract_id=4, season_id=season_id, cap_hit=123),
            ContractYear(contract_id=5, season_id=season_id, cap_hit=235),
            ContractYear(contract_id=6, season_id=season_id, cap_hit=583),
        ]
        assets = [
            Asset(id=1, current_team=team_id),
            Asset(id=2, current_team=team_id),
            Asset(id=3, current_team=team_id),
            Asset(id=4, current_team=team_id),
            Asset(id=5, current_team=team_id),
            Asset(id=6, current_team=team_id),
        ]
        contracts = [
            Contract(id=1),
            Contract(id=2),
            Contract(id=3),
            Contract(id=4),
            Contract(id=5),
            Contract(id=6),
        ]
        for year in years:
            db.session.add(year)
        for asset in assets:
            db.session.add(asset)
        for contract in contracts:
            db.session.add(contract)
        db.session.commit()
        self.assertEqual(team_season.get_contract_cap_hit(team_id, season_id), 2828)

    def test_get_buyout_cap_hit(self):
        team_id = 7
        season_id = 4
        self.assertEqual(team_season.get_buyout_cap_hit(team_id, season_id), 0)

        years = [
            BuyoutYear(contract_id=1, season_id=season_id, cap_hit=940),
            BuyoutYear(contract_id=2, season_id=season_id, cap_hit=486),
            BuyoutYear(contract_id=3, season_id=season_id, cap_hit=461),
            BuyoutYear(contract_id=4, season_id=season_id, cap_hit=225),
            BuyoutYear(contract_id=5, season_id=season_id, cap_hit=335),
            BuyoutYear(contract_id=6, season_id=season_id, cap_hit=583),
        ]
        assets = [
            Asset(id=1, current_team=team_id),
            Asset(id=2, current_team=team_id),
            Asset(id=3, current_team=team_id),
            Asset(id=4, current_team=team_id),
            Asset(id=5, current_team=team_id),
            Asset(id=6, current_team=team_id),
        ]
        for year in years:
            db.session.add(year)
        for asset in assets:
            db.session.add(asset)
        db.session.commit()
        self.assertEqual(team_season.get_buyout_cap_hit(team_id, season_id), 3030)

    def test_get_retention_addition(self):
        team_id = 7
        season_id = 4
        self.assertEqual(team_season.get_retention_addition(team_id, season_id), 0)

        season = Season(
            id=4,
            name="Test Szn",
            playoffs_start=date(year=2020, month=1, day=1),
        )
        asset = Asset(id=1)
        retained = RetainedSalary(
            id=1,
            contract_id=1,
            retained_by=team_id,
            retained_from=date(year=2020, month=6, day=1),
            retention=25,
        )
        contract_year = ContractYear(
            contract_id=1,
            season_id=season_id,
            cap_hit=34982,
        )
        db.session.add(asset)
        db.session.add(season)
        db.session.add(retained)
        db.session.add(contract_year)
        db.session.commit()

        self.assertEqual(team_season.get_retention_addition(team_id, season_id), 0)
        retained.retained_from = date(year=2019, month=6, day=1)
        db.session.add(retained)
        db.session.commit()
        self.assertEqual(team_season.get_retention_addition(team_id, season_id), 8745)

    def test_get_retention_decrease(self):
        team_id = 7
        season_id = 4
        self.assertEqual(team_season.get_retention_decrease(team_id, season_id), 0)

        season = Season(
            id=4,
            name="Test Szn",
            playoffs_start=date(year=2020, month=1, day=1),
        )
        asset = Asset(
            id=1,
            current_team=team_id,
        )
        retained = RetainedSalary(
            id=1,
            contract_id=1,
            retained_from=date(year=2020, month=6, day=1),
            retention=35,
        )
        contract_year = ContractYear(
            contract_id=1,
            season_id=season_id,
            cap_hit=35872,
        )
        db.session.add(asset)
        db.session.add(season)
        db.session.add(retained)
        db.session.add(contract_year)
        db.session.commit()
        self.assertEqual(team_season.get_retention_decrease(team_id, season_id), 0)

        retained.retained_from = date(year=2019, month=6, day=1)
        db.session.add(retained)
        db.session.commit()
        self.assertEqual(team_season.get_retention_decrease(team_id, season_id), 12555)

    def test_get_cap_hit_and_create_team_season(self):
        team_id = 7
        season_id = 4
        team = Team(
            id=team_id,
            name="Test Team",
            logo="testing",
        )
        season = Season(
            id=4,
            name="Test Szn",
            playoffs_start=date(year=2020, month=1, day=1),
            max_buriable_hit=1000,
            regular_season_start=date.today() + timedelta(2),
            regular_season_end=date.today() + timedelta(12),
        )
        contract_years = [
            ContractYear(contract_id=1, season_id=season_id, cap_hit=940),
            ContractYear(contract_id=2, season_id=season_id, cap_hit=486),
            ContractYear(contract_id=3, season_id=season_id, cap_hit=461),
            ContractYear(contract_id=4, season_id=season_id, cap_hit=123),
            ContractYear(contract_id=5, season_id=season_id, cap_hit=235),
            ContractYear(contract_id=6, season_id=season_id, cap_hit=583),
        ]
        assets = [
            Asset(id=1, current_team=team_id),
            Asset(id=2, current_team=team_id),
            Asset(id=3, current_team=team_id),
            Asset(id=4, current_team=team_id),
            Asset(id=5, current_team=team_id),
            Asset(id=6, current_team=team_id),
        ]
        contracts = [
            Contract(id=1),
            Contract(id=2),
            Contract(id=3),
            Contract(id=4),
            Contract(id=5),
            Contract(id=6),
        ]
        db.session.add(team)
        db.session.add(season)
        for year in contract_years:
            db.session.add(year)
        for asset in assets:
            db.session.add(asset)
        for contract in contracts:
            db.session.add(contract)
        db.session.commit()
        self.assertEqual(team_season.get_cap_hit(team_id, season_id), 2828)

        b_years = [
            BuyoutYear(contract_id=1, season_id=season_id, cap_hit=940),
            BuyoutYear(contract_id=2, season_id=season_id, cap_hit=486),
            BuyoutYear(contract_id=3, season_id=season_id, cap_hit=461),
            BuyoutYear(contract_id=4, season_id=season_id, cap_hit=225),
            BuyoutYear(contract_id=5, season_id=season_id, cap_hit=335),
            BuyoutYear(contract_id=6, season_id=season_id, cap_hit=583),
        ]
        for year in b_years:
            db.session.add(year)
        db.session.commit()
        self.assertEqual(team_season.get_cap_hit(team_id, season_id), 5858)

        contracts[-1].non_roster = True
        db.session.add(contracts[-1])
        db.session.flush()
        self.assertEqual(team_season.get_cap_hit(team_id, season_id), 5275)
        db.session.rollback()

        asset = Asset(
            id=8,
            current_team=team_id + 1,
        )
        retained = RetainedSalary(
            id=8,
            contract_id=7,
            retained_from=date(year=2019, month=6, day=1),
            retained_by=team_id,
            retention=25,
        )
        contract_year = ContractYear(
            contract_id=7,
            season_id=season_id,
            cap_hit=34982,
        )
        db.session.add(asset)
        db.session.add(retained)
        db.session.add(contract_year)
        db.session.commit()
        self.assertEqual(team_season.get_cap_hit(team_id, season_id), 14603)

        asset = Asset(
            id=10,
            current_team=team_id,
        )
        contract = Contract(
            id=10,
        )
        asset2 = Asset(
            id=9,
        )
        retained = RetainedSalary(
            id=9,
            contract_id=10,
            retained_from=date(year=2019, month=6, day=1),
            retention=35,
        )
        contract_year = ContractYear(
            contract_id=10,
            season_id=season_id,
            cap_hit=35872,
        )
        db.session.add(asset2)
        db.session.add(asset)
        db.session.add(contract)
        db.session.add(season)
        db.session.add(retained)
        db.session.add(contract_year)
        db.session.commit()
        self.assertEqual(team_season.get_cap_hit(team_id, season_id), 37920)
        # Larger because have to include 35872 we're retaining against

        team_season.create_team_season(team_id, season_id)
        db.session.commit()
        team_szn = (
            TeamSeason.query.filter(TeamSeason.team_id == team_id)
            .filter(TeamSeason.season_id == season_id)
            .first()
        )
        self.assertEqual(team_szn.cap_hit, 37920)
        self.assertEqual(team_szn.projected_cap_hit, 37920)
        self.assertEqual(team_szn.contract_count, 7)
        self.assertEqual(team_szn.retained_contracts, 1)
        self.assertEqual(team_szn.max_ltir_relief, 0)
        TeamSeason.query.delete()
        db.session.commit()

        season.regular_season_start = date.today() - timedelta(days=2)
        season.regular_season_end = date.today() + timedelta(days=8)
        db.session.add(season)
        daily_cap = [
            DailyCap(
                team_id=team_id,
                season_id=season_id,
                date=date.today() - timedelta(days=1),
                contract_id=1,
                cap_hit=100,
            ),
            DailyCap(
                team_id=team_id,
                season_id=season_id,
                date=date.today() - timedelta(days=1),
                contract_id=10,
                cap_hit=2755,
            ),
        ]
        for day in daily_cap:
            db.session.add(day)
        db.session.commit()
        team_season.create_team_season(team_id, season_id)
        team_szn = (
            TeamSeason.query.filter(TeamSeason.team_id == team_id)
            .filter(TeamSeason.season_id == season_id)
            .first()
        )
        self.assertEqual(team_szn.cap_hit, 5885)
        self.assertEqual(team_szn.contract_count, 7)
        self.assertEqual(team_szn.retained_contracts, 1)
        self.assertEqual(team_szn.max_ltir_relief, 0)
        self.assertEqual(team_szn.projected_cap_hit, 37920)

    def test_get_buried_cap_penalty(self):
        team_id = 7
        season_id = 4
        team = Team(
            id=team_id,
            name="Test Team",
            logo="testing",
        )
        season = Season(id=season_id, max_buriable_hit=0)
        contract_years = [
            ContractYear(contract_id=1, season_id=season_id, cap_hit=940),
            ContractYear(contract_id=2, season_id=season_id, cap_hit=486),
            ContractYear(contract_id=3, season_id=season_id, cap_hit=461),
        ]
        assets = [
            Asset(id=1, current_team=team_id),
            Asset(id=2, current_team=team_id),
            Asset(id=3, current_team=team_id),
        ]
        contracts = [
            Contract(id=1, non_roster=True),
            Contract(id=2, non_roster=False),
            Contract(id=3, non_roster=True),
        ]
        db.session.add(team)
        db.session.add(season)
        for year in contract_years:
            db.session.add(year)
        for asset in assets:
            db.session.add(asset)
        for contract in contracts:
            db.session.add(contract)
        db.session.commit()
        self.assertEqual(team_season.get_buried_cap_penalty(team_id, season_id), 1401)

        season.max_buriable_hit = 1000
        db.session.add(season)
        db.session.commit()
        self.assertEqual(team_season.get_buried_cap_penalty(team_id, season_id), 0)

    def test_get_team_season(self):
        team_id = 5
        self.assertEqual(team_season.get_team_season(team_id, None), {})
        db.session.add(
            TeamSeason(
                team_id=team_id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=12345,
                projected_cap_hit=5231,
                contract_count=4,
                retained_contracts=15,
                max_ltir_relief=1209,
            )
        )
        db.session.commit()
        team_szn = team_season.get_team_season(team_id, None)
        self.assertEqual(team_szn["cap_hit"], 5231)
        self.assertEqual(team_szn["contracts"], 4)
        self.assertEqual(team_szn["retained_contracts"], 15)
        self.assertEqual(team_szn["max_ltir_relief"], 1209)

    def test_get_team_season2(self):
        team_id = 5
        self.assertEqual(team_season.get_team_season(team_id, None), {})
        db.session.add(
            TeamSeason(
                team_id=team_id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=12345,
                projected_cap_hit=5231,
                contract_count=4,
                retained_contracts=15,
            )
        )
        db.session.commit()
        team_szn = team_season.get_team_season(team_id, None)
        self.assertEqual(team_szn["cap_hit"], 5231)
        self.assertEqual(team_szn["contracts"], 4)
        self.assertEqual(team_szn["retained_contracts"], 15)
        self.assertEqual(team_szn["max_ltir_relief"], 0)

    def test_update_team_season_regular_season(self):
        team_id = 7
        season_id = 4
        team = Team(
            id=team_id,
            name="Test team",
            logo="testing",
        )
        season = Season(
            id=4,
            name="Test Szn",
            playoffs_start=date(year=2020, month=1, day=1),
            max_buriable_hit=1000,
            regular_season_start=date.today() - timedelta(days=2),
            regular_season_end=date.today() + timedelta(days=8),
        )
        ts = TeamSeason(
            team_id=team_id,
            season_id=season_id,
            cap_hit=66,
            projected_cap_hit=77,
            contract_count=8,
            max_ltir_relief=0,
        )
        contract_years = [
            ContractYear(contract_id=1, season_id=season_id, cap_hit=940),
            ContractYear(contract_id=2, season_id=season_id, cap_hit=486),
            ContractYear(contract_id=3, season_id=season_id, cap_hit=461),
            ContractYear(contract_id=4, season_id=season_id, cap_hit=123),
            ContractYear(contract_id=5, season_id=season_id, cap_hit=235),
            ContractYear(contract_id=6, season_id=season_id, cap_hit=583),
        ]
        assets = [
            Asset(id=1, current_team=team_id),
            Asset(id=2, current_team=team_id),
            Asset(id=3, current_team=team_id),
            Asset(id=4, current_team=team_id),
            Asset(id=5, current_team=team_id),
            Asset(id=6, current_team=team_id),
        ]
        contracts = [
            Contract(id=1),
            Contract(id=2),
            Contract(id=3),
            Contract(id=4),
            Contract(id=5),
            Contract(id=6),
        ]
        daily_cap = [
            DailyCap(
                team_id=team_id,
                season_id=season_id,
                contract_id=assets[0].id,
                date=date.today() - timedelta(days=1),
                cap_hit=94,
            ),
            DailyCap(
                team_id=team_id,
                season_id=season_id,
                contract_id=assets[1].id,
                date=date.today() - timedelta(days=1),
                cap_hit=48.6,
            ),
            DailyCap(
                team_id=team_id,
                season_id=season_id,
                contract_id=assets[2].id,
                date=date.today() - timedelta(days=1),
                cap_hit=46.1,
            ),
            DailyCap(
                team_id=team_id,
                season_id=season_id,
                contract_id=assets[3].id,
                date=date.today() - timedelta(days=1),
                cap_hit=12.3,
            ),
            DailyCap(
                team_id=team_id,
                season_id=season_id,
                contract_id=assets[4].id,
                date=date.today() - timedelta(days=1),
                cap_hit=23.5,
            ),
            DailyCap(
                team_id=team_id,
                season_id=season_id,
                contract_id=assets[5].id,
                date=date.today() - timedelta(days=1),
                cap_hit=58.3,
            ),
            DailyCap(
                team_id=team_id,
                season_id=season_id,
                contract_id=assets[0].id,
                date=date.today(),
                cap_hit=94,
            ),
            DailyCap(
                team_id=team_id,
                season_id=season_id,
                contract_id=assets[1].id,
                date=date.today(),
                cap_hit=48.6,
            ),
            DailyCap(
                team_id=team_id,
                season_id=season_id,
                contract_id=assets[2].id,
                date=date.today(),
                cap_hit=46.1,
            ),
            DailyCap(
                team_id=team_id,
                season_id=season_id,
                contract_id=assets[3].id,
                date=date.today(),
                cap_hit=12.3,
            ),
            DailyCap(
                team_id=team_id,
                season_id=season_id,
                contract_id=assets[4].id,
                date=date.today(),
                cap_hit=23.5,
            ),
            DailyCap(
                team_id=team_id,
                season_id=season_id,
                contract_id=assets[5].id,
                date=date.today(),
                cap_hit=58.3,
            ),
        ]
        db.session.add(team)
        db.session.add(season)
        db.session.add(ts)
        for record in contract_years + assets + contracts + daily_cap:
            db.session.add(record)
        db.session.commit()

        team_szn = (
            TeamSeason.query.filter(TeamSeason.team_id == team_id)
            .filter(TeamSeason.season_id == season_id)
            .first()
        )
        self.assertEqual(team_szn.cap_hit, 66)
        self.assertEqual(team_szn.projected_cap_hit, 77)
        self.assertEqual(team_szn.contract_count, 8)
        self.assertEqual(team_szn.max_ltir_relief, 0)
        team_season.update_team_season(team_id, season_id)
        self.assertEqual(team_szn.cap_hit, 565.6)
        self.assertEqual(team_szn.projected_cap_hit, 2828)
        self.assertEqual(team_szn.contract_count, 6)
        self.assertEqual(team_szn.max_ltir_relief, 0)

    def test_update_team_season_offseason(self):
        team_id = 7
        team = Team(
            id=team_id,
            name="Test Team",
        )
        season_id = 4
        season = Season(
            id=4,
            name="Test Szn",
            playoffs_start=date(year=2020, month=1, day=1),
            max_buriable_hit=1000,
        )
        contract_years = [
            ContractYear(contract_id=1, season_id=season_id, cap_hit=940),
            ContractYear(contract_id=2, season_id=season_id, cap_hit=486),
            ContractYear(contract_id=3, season_id=season_id, cap_hit=461),
            ContractYear(contract_id=4, season_id=season_id, cap_hit=123),
            ContractYear(contract_id=5, season_id=season_id, cap_hit=235),
            ContractYear(contract_id=6, season_id=season_id, cap_hit=583),
        ]
        assets = [
            Asset(id=1, current_team=team_id),
            Asset(id=2, current_team=team_id),
            Asset(id=3, current_team=team_id),
            Asset(id=4, current_team=team_id),
            Asset(id=5, current_team=team_id),
            Asset(id=6, current_team=team_id),
        ]
        contracts = [
            Contract(id=1),
            Contract(id=2),
            Contract(id=3),
            Contract(id=4),
            Contract(id=5),
            Contract(id=6),
        ]
        db.session.add(team)
        db.session.add(season)
        for year in contract_years:
            db.session.add(year)
        for asset in assets:
            db.session.add(asset)
        for contract in contracts:
            db.session.add(contract)
        db.session.commit()
        team_season.update_team_season(team_id, season_id)
        team_szn = (
            TeamSeason.query.filter(TeamSeason.team_id == team_id)
            .filter(TeamSeason.season_id == season_id)
            .first()
        )
        self.assertEqual(team_szn.cap_hit, 2828)
        self.assertEqual(team_szn.projected_cap_hit, 2828)
        self.assertEqual(team_szn.contract_count, 6)
        self.assertEqual(team_szn.retained_contracts, 0)
        self.assertEqual(team_szn.max_ltir_relief, 0)

        b_years = [
            BuyoutYear(contract_id=1, season_id=season_id, cap_hit=940),
            BuyoutYear(contract_id=2, season_id=season_id, cap_hit=486),
            BuyoutYear(contract_id=3, season_id=season_id, cap_hit=461),
            BuyoutYear(contract_id=4, season_id=season_id, cap_hit=225),
            BuyoutYear(contract_id=5, season_id=season_id, cap_hit=335),
            BuyoutYear(contract_id=6, season_id=season_id, cap_hit=583),
        ]
        for year in b_years:
            db.session.add(year)
        db.session.commit()
        team_season.update_team_season(team_id, season_id)
        team_szn = (
            TeamSeason.query.filter(TeamSeason.team_id == team_id)
            .filter(TeamSeason.season_id == season_id)
            .first()
        )
        self.assertEqual(team_szn.cap_hit, 5858)
        self.assertEqual(team_szn.projected_cap_hit, 5858)
        self.assertEqual(team_szn.contract_count, 6)
        self.assertEqual(team_szn.retained_contracts, 0)
        self.assertEqual(team_szn.max_ltir_relief, 0)

    def test_update_all_teams_seasons(self):
        team_id = 7
        team = Team(
            id=team_id,
            name="Test team",
        )
        season_id = self.app.config["CURRENT_SEASON"]
        this_month = date.today().month
        season = Season(
            id=season_id,
            name="Test Szn",
            salary_cap=2000,
            playoffs_start=date(year=2020, month=1, day=1),
            free_agency_opening=date.today().replace(month=7),
            max_buriable_hit=1000,
            draft_date=date(year=2000, month=6, day=3),
        )
        season2 = Season(
            id=season_id + 1,
            name="Test Szn2",
            playoffs_start=date(year=2020, month=1, day=1),
            free_agency_opening=date.today().replace(month=9),
            max_buriable_hit=1000,
        )
        person = Person(id=5, current_status="LTIR")
        contract_years = [
            ContractYear(contract_id=1, season_id=season_id, cap_hit=940),
            ContractYear(contract_id=2, season_id=season_id, cap_hit=486),
            ContractYear(contract_id=3, season_id=season_id, cap_hit=461),
            ContractYear(contract_id=4, season_id=season_id + 1, cap_hit=123),
            ContractYear(contract_id=5, season_id=season_id + 1, cap_hit=235),
            ContractYear(contract_id=6, season_id=season_id + 1, cap_hit=583),
        ]
        assets = [
            Asset(id=1, current_team=team_id),
            Asset(id=2, current_team=team_id),
            Asset(id=3, current_team=team_id),
            Asset(id=4, current_team=team_id),
            Asset(id=5, current_team=team_id),
            Asset(id=6, current_team=team_id),
        ]
        contracts = [
            Contract(id=1, person_id=5),
            Contract(id=2),
            Contract(id=3),
            Contract(id=4),
            Contract(id=5),
            Contract(id=6),
        ]
        db.session.add(person)
        db.session.add(team)
        db.session.add(season)
        db.session.add(season2)
        for year in contract_years:
            db.session.add(year)
        for asset in assets:
            db.session.add(asset)
        for contract in contracts:
            db.session.add(contract)
        db.session.commit()
        team_season.update_all_teams_seasons(team_id)

        team_szn_count = TeamSeason.query.count()
        self.assertEqual(team_szn_count, 2)
        first = (
            TeamSeason.query.filter(TeamSeason.team_id == team_id)
            .filter(TeamSeason.season_id == season_id)
            .first()
        )
        self.assertIsNotNone(first)
        self.assertEqual(first.cap_hit, 1887)
        self.assertEqual(first.projected_cap_hit, 1887)
        self.assertEqual(first.contract_count, 3)
        self.assertEqual(first.retained_contracts, 0)
        # self.assertEqual(first.max_ltir_relief, 827)
        self.assertEqual(first.max_ltir_relief, 940)

    def test_retained_buyout(self):
        team_id = 29
        asset = Asset(
            id=6515,
            originating_team=28,
            current_team=29,
            active=False,
        )
        asset_two = Asset(
            id=6516,
            originating_team=28,
            current_team=28,
            active=False,
        )
        contract = Contract(
            id=asset.id,
            years=5,
            total_value=1000000,
        )
        contract_year = ContractYear(
            contract_id=asset.id,
            nhl_salary=1000000,
            minors_salary=1000000,
            cap_hit=1000000,
            aav=1000000,
            signing_bonus=0,
            performance_bonuses=0,
            season_id=self.app.config["CURRENT_SEASON"],
        )
        buyout_year = BuyoutYear(
            contract_id=asset.id,
            nhl_salary=1000000,
            cap_hit=1000000,
            signing_bonus=0,
            season_id=self.app.config["CURRENT_SEASON"],
        )
        retained_salary = RetainedSalary(
            id=asset_two.id,
            contract_id=asset.id,
            retention=25,
            retained_from=date.today(),
            retained_by=28,
        )
        db.session.add(asset)
        db.session.add(asset_two)
        db.session.add(contract)
        db.session.add(contract_year)
        db.session.add(buyout_year)
        db.session.add(retained_salary)
        db.session.commit()

        buyout_hit = team_season.get_buyout_cap_hit(
            29, self.app.config["CURRENT_SEASON"]
        )
        self.assertEqual(buyout_hit, 750000)

        retained_buyout_hit = team_season.get_retained_buyout_cap_hit(
            28, self.app.config["CURRENT_SEASON"]
        )

    def test_create_nhl_cap_hit(self):
        # Need: Contract, Season, ContractYear, Asset, RetainedSalary
        # One unretained contract
        # One once retained contract
        # One twice retained contract
        season = Season(
            id=self.app.config["CURRENT_SEASON"],
            regular_season_start=date(year=2020, month=1, day=1),
            regular_season_end=date(year=2020, month=1, day=21),
        )
        db.session.add(season)
        assets = [
            Asset(id=1, active=True, current_team="4", type="Contract"),
            Asset(id=2, active=True, current_team="4", type="Contract"),
            Asset(id=3, active=True, current_team="4", type="Contract"),
            Asset(id=4, active=True, current_team="5", type="Retained Salary"),
            Asset(id=5, active=True, current_team="5", type="Retained Salary"),
            Asset(id=6, active=True, current_team="6", type="Retained Salary"),
        ]
        for asset in assets:
            db.session.add(asset)
        contracts = [
            Contract(
                id=assets[0].id,
                person_id=7,
                non_roster=False,
            ),
            Contract(
                id=assets[1].id,
                person_id=44,
                non_roster=False,
            ),
            Contract(
                id=assets[2].id,
                person_id=74,
                non_roster=False,
            ),
        ]
        for contract in contracts:
            db.session.add(contract)
        people = [
            Person(id=7, current_status="Emergency Callup"),
            Person(id=44, current_status="Suspended"),
            Person(id=74),
        ]
        for person in people:
            db.session.add(person)
        contract_years = [
            ContractYear(
                contract_id=contracts[0].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=100000,
            ),
            ContractYear(
                contract_id=contracts[1].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=200000,
            ),
            ContractYear(
                contract_id=contracts[2].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=400000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        retained_salary = [
            RetainedSalary(
                id=assets[3].id,
                contract_id=contracts[1].id,
                retention=25,
            ),
            RetainedSalary(
                id=assets[4].id,
                contract_id=contracts[1].id,
                retention=30,
            ),
            RetainedSalary(
                id=assets[5].id,
                contract_id=contracts[2].id,
                retention=40,
            ),
        ]
        for retention in retained_salary:
            db.session.add(retention)
        db.session.commit()
        team_season.create_nhl_cap_hit()

        daily_cap = DailyCap.query.order_by(DailyCap.contract_id.asc()).all()
        self.assertEqual(len(daily_cap), 3)
        self.assertEqual(daily_cap[0].team_id, 4)
        self.assertEqual(daily_cap[0].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[0].contract_id, contracts[0].id)
        self.assertEqual(daily_cap[0].date, date.today())
        self.assertEqual(daily_cap[0].non_roster, False)
        self.assertEqual(daily_cap[0].retained, False)
        self.assertEqual(daily_cap[0].cap_hit, 5000.0)
        self.assertEqual(daily_cap[0].suspended, False)
        self.assertEqual(daily_cap[0].emergency_loan, True)
        self.assertEqual(daily_cap[1].emergency_loan, False)
        self.assertEqual(daily_cap[1].team_id, 4)
        self.assertEqual(daily_cap[1].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[1].contract_id, contracts[1].id)
        self.assertEqual(daily_cap[1].suspended, True)
        self.assertEqual(daily_cap[1].date, date.today())
        self.assertEqual(daily_cap[1].non_roster, False)
        self.assertEqual(daily_cap[1].retained, False)
        self.assertEqual(daily_cap[1].cap_hit, 4500.0)
        self.assertEqual(daily_cap[2].team_id, 4)
        self.assertEqual(daily_cap[2].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[2].contract_id, contracts[2].id)
        self.assertEqual(daily_cap[2].date, date.today())
        self.assertEqual(daily_cap[2].non_roster, False)
        self.assertEqual(daily_cap[2].retained, False)
        self.assertEqual(daily_cap[2].cap_hit, 12000.0)
        self.assertEqual(daily_cap[2].suspended, False)
        self.assertEqual(daily_cap[2].emergency_loan, False)

        extra_asset = Asset(id=7, active=True, current_team="3", type="Contract")
        extra_contract = Contract(
            id=extra_asset.id,
            person_id=74,
            non_roster=False,
        )
        extra_year = ContractYear(
            contract_id=extra_contract.id,
            season_id=self.app.config["CURRENT_SEASON"],
            cap_hit=250000,
        )
        db.session.add(extra_asset)
        db.session.add(extra_contract)
        db.session.add(extra_year)
        db.session.commit()
        self.assertEqual(
            team_season.create_nhl_cap_hit(
                self.app.config["CURRENT_SEASON"], date.today(), [3]
            ),
            1,
        )
        daily_cap = DailyCap.query.order_by(DailyCap.contract_id.asc()).all()
        self.assertEqual(len(daily_cap), 4)
        self.assertEqual(daily_cap[3].team_id, 3)
        self.assertEqual(daily_cap[3].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[3].contract_id, extra_contract.id)
        self.assertEqual(daily_cap[3].date, date.today())
        self.assertEqual(daily_cap[3].non_roster, False)
        self.assertEqual(daily_cap[3].retained, False)
        self.assertEqual(daily_cap[3].cap_hit, 12500.0)

    def test_create_retained_nhl_cap_hit(self):
        # One unretained contract
        # One once retained contract
        # One twice retained contract
        season = Season(
            id=self.app.config["CURRENT_SEASON"],
            regular_season_start=date(year=2020, month=1, day=1),
            regular_season_end=date(year=2020, month=1, day=21),
        )
        db.session.add(season)
        assets = [
            Asset(id=1, active=True, current_team="4", type="Contract"),
            Asset(id=2, active=True, current_team="4", type="Contract"),
            Asset(id=3, active=True, current_team="4", type="Contract"),
            Asset(id=4, active=True, current_team="5", type="Retained Salary"),
            Asset(id=5, active=True, current_team="5", type="Retained Salary"),
            Asset(id=6, active=True, current_team="6", type="Retained Salary"),
        ]
        for asset in assets:
            db.session.add(asset)
        contracts = [
            Contract(
                id=assets[0].id,
                person_id=7,
                non_roster=False,
            ),
            Contract(
                id=assets[1].id,
                person_id=44,
                non_roster=False,
            ),
            Contract(
                id=assets[2].id,
                person_id=74,
                non_roster=False,
            ),
        ]
        for contract in contracts:
            db.session.add(contract)
        people = [
            Person(id=7),
            Person(id=44),
            Person(id=74),
        ]
        for person in people:
            db.session.add(person)
        contract_years = [
            ContractYear(
                contract_id=contracts[0].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=100000,
            ),
            ContractYear(
                contract_id=contracts[1].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=200000,
            ),
            ContractYear(
                contract_id=contracts[2].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=400000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        retained_salary = [
            RetainedSalary(
                id=assets[3].id,
                contract_id=contracts[1].id,
                retention=25,
                retained_by=5,
            ),
            RetainedSalary(
                id=assets[4].id,
                contract_id=contracts[1].id,
                retention=30,
                retained_by=5,
            ),
            RetainedSalary(
                id=assets[5].id,
                contract_id=contracts[2].id,
                retention=40,
                retained_by=6,
            ),
        ]
        for retention in retained_salary:
            db.session.add(retention)
        db.session.commit()
        team_season.create_retained_nhl_cap_hit()

        daily_cap = DailyCap.query.order_by(
            DailyCap.contract_id.asc(), DailyCap.cap_hit.desc()
        ).all()
        self.assertEqual(len(daily_cap), 3)
        self.assertEqual(daily_cap[0].team_id, 5)
        self.assertEqual(daily_cap[0].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[0].contract_id, contracts[1].id)
        self.assertEqual(daily_cap[0].date, date.today())
        self.assertEqual(daily_cap[0].non_roster, False)
        self.assertEqual(daily_cap[0].retained, True)
        self.assertEqual(daily_cap[0].cap_hit, 3000.0)
        self.assertEqual(daily_cap[1].team_id, 5)
        self.assertEqual(daily_cap[1].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[1].contract_id, contracts[1].id)
        self.assertEqual(daily_cap[1].date, date.today())
        self.assertEqual(daily_cap[1].non_roster, False)
        self.assertEqual(daily_cap[1].retained, True)
        self.assertEqual(daily_cap[1].cap_hit, 2500.0)
        self.assertEqual(daily_cap[2].team_id, 6)
        self.assertEqual(daily_cap[2].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[2].contract_id, contracts[2].id)
        self.assertEqual(daily_cap[2].date, date.today())
        self.assertEqual(daily_cap[2].non_roster, False)
        self.assertEqual(daily_cap[2].retained, True)
        self.assertEqual(daily_cap[2].cap_hit, 8000.0)

        extra_asset = Asset(id=7, active=True, current_team=3, type="Retained Salary")
        extra_retention = RetainedSalary(
            id=extra_asset.id,
            contract_id=contracts[2].id,
            retention=40,
            retained_by=3,
        )
        db.session.add(extra_asset)
        db.session.add(extra_retention)
        db.session.commit()

        self.assertEqual(
            team_season.create_retained_nhl_cap_hit(
                self.app.config["CURRENT_SEASON"], date.today(), [3]
            ),
            1,
        )
        daily_cap = DailyCap.query.order_by(
            DailyCap.contract_id.asc(), DailyCap.cap_hit.desc()
        ).all()
        self.assertEqual(len(daily_cap), 4)
        self.assertEqual(daily_cap[3].team_id, 3)
        self.assertEqual(daily_cap[3].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[3].contract_id, contracts[2].id)
        self.assertEqual(daily_cap[3].date, date.today())
        self.assertEqual(daily_cap[3].non_roster, False)
        self.assertEqual(daily_cap[3].retained, True)
        self.assertEqual(daily_cap[3].cap_hit, 8000.0)

    def test_create_minors_cap_hit(self):
        # One unretained contract
        # One retained contract
        season = Season(
            id=self.app.config["CURRENT_SEASON"],
            regular_season_start=date(year=2020, month=1, day=1),
            regular_season_end=date(year=2020, month=1, day=21),
            max_buriable_hit=10000,
        )
        db.session.add(season)
        assets = [
            Asset(id=1, active=True, current_team="4", type="Contract"),
            Asset(id=2, active=True, current_team="4", type="Contract"),
            Asset(id=3, active=True, current_team="5", type="Retained Salary"),
        ]
        for asset in assets:
            db.session.add(asset)
        contracts = [
            Contract(
                id=assets[0].id,
                person_id=7,
                non_roster=True,
                on_loan=True,
            ),
            Contract(
                id=assets[1].id,
                person_id=44,
                non_roster=True,
                in_juniors=True,
            ),
        ]
        for contract in contracts:
            db.session.add(contract)
        people = [
            Person(id=7),
            Person(id=44),
            Person(id=74),
        ]
        for person in people:
            db.session.add(person)
        contract_years = [
            ContractYear(
                contract_id=contracts[0].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=100000,
                two_way=False,
            ),
            ContractYear(
                contract_id=contracts[1].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=200000,
                two_way=True,
                minors_salary=50000,
                signing_bonus=60000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        retained_salary = [
            RetainedSalary(
                id=assets[2].id,
                contract_id=contracts[1].id,
                retention=25,
                retained_by=5,
            ),
        ]
        for retention in retained_salary:
            db.session.add(retention)
        db.session.commit()
        team_season.create_minors_cap_hit()

        daily_cap = DailyCap.query.order_by(
            DailyCap.contract_id.asc(), DailyCap.cap_hit.desc()
        ).all()
        self.assertEqual(len(daily_cap), 2)
        self.assertEqual(daily_cap[0].team_id, 4)
        self.assertEqual(daily_cap[0].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[0].contract_id, contracts[0].id)
        self.assertEqual(daily_cap[0].date, date.today())
        self.assertEqual(daily_cap[0].non_roster, True)
        self.assertEqual(daily_cap[0].on_loan, True)
        self.assertEqual(daily_cap[0].in_juniors, False)
        self.assertEqual(daily_cap[0].retained, False)
        self.assertEqual(daily_cap[0].cap_hit, 4500.0)
        self.assertEqual(daily_cap[1].team_id, 4)
        self.assertEqual(daily_cap[1].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[1].contract_id, contracts[1].id)
        self.assertEqual(daily_cap[1].date, date.today())
        self.assertEqual(daily_cap[1].non_roster, True)
        self.assertEqual(daily_cap[1].retained, False)
        self.assertEqual(daily_cap[1].cap_hit, 3750.0)
        self.assertEqual(daily_cap[1].on_loan, False)
        self.assertEqual(daily_cap[1].in_juniors, True)

        extra_asset = Asset(id=7, active=True, current_team="3", type="Contract")
        extra_contract = Contract(
            id=extra_asset.id,
            person_id=74,
            non_roster=True,
        )
        extra_year = ContractYear(
            contract_id=extra_contract.id,
            season_id=self.app.config["CURRENT_SEASON"],
            cap_hit=250000,
            two_way=False,
        )
        db.session.add(extra_asset)
        db.session.add(extra_contract)
        db.session.add(extra_year)
        db.session.commit()

        self.assertEqual(
            team_season.create_minors_cap_hit(
                self.app.config["CURRENT_SEASON"], date.today(), [3]
            ),
            1,
        )
        daily_cap = DailyCap.query.order_by(
            DailyCap.contract_id.asc(), DailyCap.cap_hit.desc()
        ).all()
        self.assertEqual(len(daily_cap), 3)
        self.assertEqual(daily_cap[2].team_id, 3)
        self.assertEqual(daily_cap[2].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[2].contract_id, extra_contract.id)
        self.assertEqual(daily_cap[2].date, date.today())
        self.assertEqual(daily_cap[2].non_roster, True)
        self.assertEqual(daily_cap[2].retained, False)
        self.assertEqual(daily_cap[2].cap_hit, 12000.0)

    def test_create_retained_minors_cap_hit(self):
        # One unretained contract
        # One retained contract
        season = Season(
            id=self.app.config["CURRENT_SEASON"],
            regular_season_start=date(year=2020, month=1, day=1),
            regular_season_end=date(year=2020, month=1, day=21),
            max_buriable_hit=10000,
        )
        db.session.add(season)
        assets = [
            Asset(id=1, active=True, current_team="4", type="Contract"),
            Asset(id=2, active=True, current_team="4", type="Contract"),
            Asset(id=3, active=True, current_team="5", type="Retained Salary"),
        ]
        for asset in assets:
            db.session.add(asset)
        contracts = [
            Contract(
                id=assets[0].id,
                person_id=7,
                non_roster=True,
            ),
            Contract(
                id=assets[1].id,
                person_id=44,
                non_roster=True,
            ),
        ]
        for contract in contracts:
            db.session.add(contract)
        people = [
            Person(id=7),
            Person(id=44),
            Person(id=74),
        ]
        for person in people:
            db.session.add(person)
        contract_years = [
            ContractYear(
                contract_id=contracts[0].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=100000,
                two_way=False,
            ),
            ContractYear(
                contract_id=contracts[1].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=200000,
                two_way=True,
                minors_salary=50000,
                signing_bonus=60000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        retained_salary = [
            RetainedSalary(
                id=assets[2].id,
                contract_id=contracts[1].id,
                retention=25,
                retained_by=5,
            ),
        ]
        for retention in retained_salary:
            db.session.add(retention)
        db.session.commit()
        team_season.create_retained_minors_cap_hit()

        daily_cap = DailyCap.query.order_by(
            DailyCap.contract_id.asc(), DailyCap.cap_hit.desc()
        ).all()
        self.assertEqual(len(daily_cap), 1)
        self.assertEqual(daily_cap[0].team_id, 5)
        self.assertEqual(daily_cap[0].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[0].contract_id, contracts[1].id)
        self.assertEqual(daily_cap[0].date, date.today())
        self.assertEqual(daily_cap[0].non_roster, True)
        self.assertEqual(daily_cap[0].retained, True)
        self.assertEqual(daily_cap[0].cap_hit, 1250.0)

        extra_asset = Asset(id=7, active=True, current_team=3, type="Retained Salary")
        extra_retention = RetainedSalary(
            id=extra_asset.id,
            contract_id=contracts[0].id,
            retention=40,
            retained_by=3,
        )
        db.session.add(extra_asset)
        db.session.add(extra_retention)
        db.session.commit()
        self.assertEqual(
            team_season.create_retained_minors_cap_hit(
                self.app.config["CURRENT_SEASON"], date.today(), [3]
            ),
            1,
        )

        daily_cap = DailyCap.query.order_by(
            DailyCap.contract_id.asc(), DailyCap.cap_hit.desc()
        ).all()
        self.assertEqual(len(daily_cap), 2)
        self.assertEqual(daily_cap[0].team_id, 3)
        self.assertEqual(daily_cap[0].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[0].contract_id, contracts[0].id)
        self.assertEqual(daily_cap[0].date, date.today())
        self.assertEqual(daily_cap[0].non_roster, True)
        self.assertEqual(daily_cap[0].retained, True)
        self.assertEqual(daily_cap[0].cap_hit, 1800.0)

    def test_create_minors_thirtyfive_cap_hit(self):
        # One unretained contract
        # One retained contract
        season = Season(
            id=self.app.config["CURRENT_SEASON"],
            regular_season_start=date(year=2020, month=1, day=1),
            regular_season_end=date(year=2020, month=1, day=21),
            max_buriable_hit=10000,
        )
        db.session.add(season)
        assets = [
            Asset(id=1, active=True, current_team="4", type="Contract"),
            Asset(id=2, active=True, current_team="4", type="Contract"),
            Asset(id=3, active=True, current_team="5", type="Retained Salary"),
        ]
        for asset in assets:
            db.session.add(asset)
        contracts = [
            Contract(
                id=assets[0].id,
                person_id=7,
                non_roster=True,
                thirty_five_plus=True,
            ),
            Contract(
                id=assets[1].id,
                person_id=44,
                non_roster=True,
                thirty_five_plus=True,
            ),
        ]
        for contract in contracts:
            db.session.add(contract)
        people = [
            Person(id=7),
            Person(id=44),
            Person(id=74),
        ]
        for person in people:
            db.session.add(person)
        contract_years = [
            ContractYear(
                contract_id=contracts[0].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=500000,
                two_way=False,
            ),
            ContractYear(
                contract_id=contracts[1].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=200000,
                two_way=True,
                minors_salary=50000,
                signing_bonus=60000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        retained_salary = [
            RetainedSalary(
                id=assets[2].id,
                contract_id=contracts[0].id,
                retention=25,
                retained_by=5,
            ),
        ]
        for retention in retained_salary:
            db.session.add(retention)
        db.session.commit()
        team_season.create_minors_cap_hit()
        self.assertEqual(DailyCap.query.count(), 0)
        team_season.create_minors_thirtyfive_cap_hit()

        daily_cap = DailyCap.query.order_by(
            DailyCap.contract_id.asc(), DailyCap.cap_hit.desc()
        ).all()
        self.assertEqual(len(daily_cap), 2)
        self.assertEqual(daily_cap[0].team_id, 4)
        self.assertEqual(daily_cap[0].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[0].contract_id, contracts[0].id)
        self.assertEqual(daily_cap[0].date, date.today())
        self.assertEqual(daily_cap[0].non_roster, True)
        self.assertEqual(daily_cap[0].retained, False)
        self.assertEqual(daily_cap[0].cap_hit, 15000.0)
        self.assertEqual(daily_cap[1].team_id, 4)
        self.assertEqual(daily_cap[1].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[1].contract_id, contracts[1].id)
        self.assertEqual(daily_cap[1].date, date.today())
        self.assertEqual(daily_cap[1].non_roster, True)
        self.assertEqual(daily_cap[1].retained, False)
        self.assertEqual(daily_cap[1].cap_hit, 500.0)

        extra_asset = Asset(id=7, active=True, current_team="3", type="Contract")
        extra_contract = Contract(
            id=extra_asset.id,
            person_id=74,
            non_roster=True,
            thirty_five_plus=True,
        )
        extra_year = ContractYear(
            contract_id=extra_contract.id,
            season_id=self.app.config["CURRENT_SEASON"],
            cap_hit=250000,
            two_way=False,
        )
        db.session.add(extra_asset)
        db.session.add(extra_contract)
        db.session.add(extra_year)
        db.session.commit()

        self.assertEqual(
            team_season.create_minors_thirtyfive_cap_hit(
                self.app.config["CURRENT_SEASON"], date.today(), [3]
            ),
            1,
        )
        daily_cap = DailyCap.query.order_by(
            DailyCap.contract_id.asc(), DailyCap.cap_hit.desc()
        ).all()
        self.assertEqual(len(daily_cap), 3)
        self.assertEqual(daily_cap[2].team_id, 3)
        self.assertEqual(daily_cap[2].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[2].contract_id, extra_contract.id)
        self.assertEqual(daily_cap[2].date, date.today())
        self.assertEqual(daily_cap[2].non_roster, True)
        self.assertEqual(daily_cap[2].retained, False)
        self.assertEqual(daily_cap[2].cap_hit, 7500.0)

    def test_create_retained_minors_thirtyfive_cap_hit(self):
        # One unretained contract
        # One retained contract
        season = Season(
            id=self.app.config["CURRENT_SEASON"],
            regular_season_start=date(year=2020, month=1, day=1),
            regular_season_end=date(year=2020, month=1, day=21),
            max_buriable_hit=10000,
        )
        db.session.add(season)
        assets = [
            Asset(id=1, active=True, current_team="4", type="Contract"),
            Asset(id=2, active=True, current_team="4", type="Contract"),
            Asset(id=3, active=True, current_team="5", type="Retained Salary"),
        ]
        for asset in assets:
            db.session.add(asset)
        contracts = [
            Contract(
                id=assets[0].id,
                person_id=7,
                non_roster=True,
                thirty_five_plus=True,
            ),
            Contract(
                id=assets[1].id,
                person_id=44,
                non_roster=True,
                thirty_five_plus=True,
            ),
        ]
        for contract in contracts:
            db.session.add(contract)
        people = [
            Person(id=7),
            Person(id=44),
            Person(id=74),
        ]
        for person in people:
            db.session.add(person)
        contract_years = [
            ContractYear(
                contract_id=contracts[0].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=500000,
                two_way=False,
            ),
            ContractYear(
                contract_id=contracts[1].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=200000,
                two_way=True,
                minors_salary=50000,
                signing_bonus=60000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        retained_salary = [
            RetainedSalary(
                id=assets[2].id,
                contract_id=contracts[0].id,
                retention=25,
                retained_by=5,
            ),
        ]
        for retention in retained_salary:
            db.session.add(retention)
        db.session.commit()
        team_season.create_retained_minors_cap_hit()
        self.assertEqual(DailyCap.query.count(), 0)
        team_season.create_retained_minors_thirtyfive_cap_hit()

        daily_cap = DailyCap.query.order_by(
            DailyCap.contract_id.asc(), DailyCap.cap_hit.desc()
        ).all()
        self.assertEqual(len(daily_cap), 1)
        self.assertEqual(daily_cap[0].team_id, 5)
        self.assertEqual(daily_cap[0].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[0].contract_id, contracts[0].id)
        self.assertEqual(daily_cap[0].date, date.today())
        self.assertEqual(daily_cap[0].non_roster, True)
        self.assertEqual(daily_cap[0].retained, True)
        self.assertEqual(daily_cap[0].cap_hit, 5000.0)

        extra_asset = Asset(id=7, active=True, current_team=3, type="Retained Salary")
        extra_retention = RetainedSalary(
            id=extra_asset.id,
            contract_id=contracts[1].id,
            retention=40,
            retained_by=3,
        )
        db.session.add(extra_asset)
        db.session.add(extra_retention)
        db.session.commit()
        self.assertEqual(
            team_season.create_retained_minors_thirtyfive_cap_hit(
                self.app.config["CURRENT_SEASON"], date.today(), [3]
            ),
            1,
        )
        daily_cap = DailyCap.query.order_by(
            DailyCap.contract_id.asc(), DailyCap.cap_hit.desc()
        ).all()
        self.assertEqual(len(daily_cap), 2)
        self.assertEqual(daily_cap[1].team_id, 3)
        self.assertEqual(daily_cap[1].season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(daily_cap[1].contract_id, contracts[1].id)
        self.assertEqual(daily_cap[1].date, date.today())
        self.assertEqual(daily_cap[1].non_roster, True)
        self.assertEqual(daily_cap[1].retained, True)
        self.assertEqual(daily_cap[1].cap_hit, 200.0)

    def test_buyout_cap_hit(self):
        self.assertEqual(0.0, team_season.buyout_cap_hit(30))
        asset = Asset(
            id=88,
            current_team=28,
            originating_team=28,
            active=False,
        )
        buyout_year = BuyoutYear(
            contract_id=asset.id,
            season_id=self.app.config["CURRENT_SEASON"],
            cap_hit=1000,
        )
        db.session.add(asset)
        db.session.add(buyout_year)
        db.session.commit()

        self.assertEqual(0.0, team_season.buyout_cap_hit(30))
        self.assertEqual(1000.0, team_season.buyout_cap_hit(28))
        ret_asset = Asset(
            id=3,
            current_team=30,
            originating_team=30,
            active=True,
        )
        retention = RetainedSalary(
            id=ret_asset.id,
            contract_id=asset.id,
            retention=33.33,
            retained_by=30,
        )
        db.session.add(asset)
        db.session.add(retention)
        db.session.commit()
        self.assertEqual(666.7, team_season.buyout_cap_hit(28))

    def test_retained_buyout_cap_hit(self):
        self.assertEqual(0.0, team_season.retained_buyout_cap_hit(30))
        asset = Asset(
            id=88,
            current_team=28,
            originating_team=28,
            active=False,
        )
        buyout_year = BuyoutYear(
            contract_id=asset.id,
            season_id=self.app.config["CURRENT_SEASON"],
            cap_hit=1000,
        )
        db.session.add(asset)
        db.session.add(buyout_year)
        db.session.commit()
        self.assertEqual(0.0, team_season.retained_buyout_cap_hit(30))

        ret_asset = Asset(
            id=3,
            current_team=30,
            originating_team=30,
            active=True,
        )
        retention = RetainedSalary(
            id=ret_asset.id,
            contract_id=asset.id,
            retention=33.33,
            retained_by=30,
        )
        db.session.add(asset)
        db.session.add(retention)
        db.session.commit()
        self.assertEqual(333.3, team_season.retained_buyout_cap_hit(30))

        ret_asset = Asset(
            id=33,
            current_team=30,
            originating_team=30,
            active=True,
        )
        retention = RetainedSalary(
            id=ret_asset.id,
            contract_id=asset.id,
            retention=25,
            retained_by=30,
        )
        db.session.add(asset)
        db.session.add(retention)
        db.session.commit()
        self.assertEqual(583.3, team_season.retained_buyout_cap_hit(30))

    def test_penalty_cap_hit(self):
        self.assertEqual(0.0, team_season.penalty_cap_hit(30))
        asset = Asset(
            id=83,
            current_team=29,
            originating_team=29,
            active=True,
        )
        penalty_year = PenaltyYear(
            penalty_id=asset.id,
            season_id=self.app.config["CURRENT_SEASON"],
            cap_hit=10000,
        )
        db.session.add(asset)
        db.session.add(penalty_year)
        db.session.commit()
        self.assertEqual(0.0, team_season.penalty_cap_hit(30))
        self.assertEqual(10000.0, team_season.penalty_cap_hit(29))

    def test_daily_cap(self):
        self.assertEqual(team_season.daily_cap_used(28), 0)
        # Results ordered here based on order functions called in team_season.create_daily_cap
        # Reordering those functions will break this
        season = Season(
            id=self.app.config["CURRENT_SEASON"],
            regular_season_start=date.today() - timedelta(days=2),
            regular_season_end=date.today() + timedelta(days=8),
            max_buriable_hit=10,
        )
        assets = [
            Asset(id=238, current_team=28, originating_team=30, active=True),
            Asset(id=248, current_team=28, originating_team=30, active=True),
            Asset(id=338, current_team=28, originating_team=30, active=True),
        ]
        contracts = [
            Contract(
                id=assets[0].id, person_id=74, non_roster=False, thirty_five_plus=False
            ),
            Contract(
                id=assets[1].id, person_id=44, non_roster=True, thirty_five_plus=False
            ),
            Contract(
                id=assets[2].id, person_id=7, non_roster=True, thirty_five_plus=True
            ),
        ]
        people = [
            Person(id=7),
            Person(id=44),
            Person(id=74),
        ]
        for person in people:
            db.session.add(person)
        contract_years = [
            ContractYear(
                contract_id=assets[0].id,
                season_id=season.id,
                cap_hit=10000,
                two_way=False,
            ),
            ContractYear(
                contract_id=assets[1].id,
                season_id=season.id,
                cap_hit=10000,
                minors_salary=1000,
                signing_bonus=0,
                two_way=True,
            ),
            ContractYear(
                contract_id=assets[2].id,
                season_id=season.id,
                cap_hit=10000,
                minors_salary=5000,
                signing_bonus=100000,
                two_way=True,
            ),
        ]
        ret_assets = [
            Asset(id=2, current_team=30, originating_team=30, active=True),
            Asset(id=8, current_team=30, originating_team=30, active=True),
            Asset(id=4, current_team=30, originating_team=30, active=True),
        ]
        retention = [
            RetainedSalary(
                id=ret_assets[0].id,
                contract_id=assets[0].id,
                retained_by=30,
                retention=33.3,
            ),
            RetainedSalary(
                id=ret_assets[1].id,
                contract_id=assets[1].id,
                retained_by=30,
                retention=33.3,
            ),
            RetainedSalary(
                id=ret_assets[2].id,
                contract_id=assets[2].id,
                retained_by=30,
                retention=33.3,
            ),
        ]
        db.session.add(season)
        for record in assets + contracts + contract_years + ret_assets + retention:
            db.session.add(record)
        db.session.commit()
        team_season.create_daily_cap_records(season.id)

        daily_cap = DailyCap.query.all()
        self.assertEqual(len(daily_cap), 6)

        self.assertEqual(daily_cap[0].team_id, assets[0].current_team)
        self.assertEqual(daily_cap[0].season_id, season.id)
        self.assertEqual(daily_cap[0].contract_id, assets[0].id)
        self.assertEqual(daily_cap[0].date, date.today())
        self.assertEqual(daily_cap[0].cap_hit, 667.0)
        self.assertEqual(daily_cap[0].non_roster, False)
        self.assertEqual(daily_cap[0].retained, False)
        self.assertEqual(daily_cap[1].team_id, assets[2].current_team)
        self.assertEqual(daily_cap[1].season_id, season.id)
        self.assertEqual(daily_cap[1].contract_id, assets[2].id)
        self.assertEqual(daily_cap[1].date, date.today())
        self.assertEqual(daily_cap[1].cap_hit, 333.5)
        self.assertEqual(daily_cap[1].non_roster, True)
        self.assertEqual(daily_cap[1].retained, False)
        self.assertEqual(daily_cap[2].team_id, assets[1].current_team)
        self.assertEqual(daily_cap[2].season_id, season.id)
        self.assertEqual(daily_cap[2].contract_id, assets[1].id)
        self.assertEqual(daily_cap[2].date, date.today())
        self.assertEqual(daily_cap[2].cap_hit, 66.03)
        self.assertEqual(daily_cap[2].non_roster, True)
        self.assertEqual(daily_cap[2].retained, False)

        self.assertEqual(daily_cap[3].team_id, ret_assets[0].current_team)
        self.assertEqual(daily_cap[3].season_id, season.id)
        self.assertEqual(daily_cap[3].contract_id, assets[0].id)
        self.assertEqual(daily_cap[3].date, date.today())
        self.assertEqual(daily_cap[3].cap_hit, 333.0)
        self.assertEqual(daily_cap[3].non_roster, False)
        self.assertEqual(daily_cap[3].retained, True)
        self.assertEqual(daily_cap[4].team_id, ret_assets[2].current_team)
        self.assertEqual(daily_cap[4].season_id, season.id)
        self.assertEqual(daily_cap[4].contract_id, assets[2].id)
        self.assertEqual(daily_cap[4].date, date.today())
        self.assertEqual(daily_cap[4].cap_hit, 166.5)
        self.assertEqual(daily_cap[4].non_roster, True)
        self.assertEqual(daily_cap[4].retained, True)
        self.assertEqual(daily_cap[5].team_id, ret_assets[1].current_team)
        self.assertEqual(daily_cap[5].season_id, season.id)
        self.assertEqual(daily_cap[5].contract_id, assets[1].id)
        self.assertEqual(daily_cap[5].date, date.today())
        self.assertEqual(daily_cap[5].cap_hit, 32.97)
        self.assertEqual(daily_cap[5].non_roster, True)
        self.assertEqual(daily_cap[5].retained, True)

        self.assertEqual(team_season.daily_cap_used(30), 532.47)
        self.assertEqual(team_season.daily_cap_used(28), 1066.53)

        DailyCap.query.delete()
        db.session.commit()
        self.assertEqual(DailyCap.query.count(), 0)

        ret_assets2 = [
            Asset(id=3, current_team=32, originating_team=32, active=True),
            Asset(id=9, current_team=32, originating_team=32, active=True),
            Asset(id=5, current_team=32, originating_team=32, active=True),
        ]
        retention2 = [
            RetainedSalary(
                id=ret_assets2[0].id,
                contract_id=assets[0].id,
                retained_by=32,
                retention=16.7,
            ),
            RetainedSalary(
                id=ret_assets2[1].id,
                contract_id=assets[1].id,
                retained_by=32,
                retention=16.7,
            ),
            RetainedSalary(
                id=ret_assets2[2].id,
                contract_id=assets[2].id,
                retained_by=32,
                retention=16.7,
            ),
        ]
        for record in ret_assets2 + retention2:
            db.session.add(record)
        db.session.commit()
        team_season.create_daily_cap_records(season.id)

        daily_cap = DailyCap.query.all()
        self.assertEqual(len(daily_cap), 9)
        self.assertEqual(daily_cap[0].team_id, assets[0].current_team)
        self.assertEqual(daily_cap[0].season_id, season.id)
        self.assertEqual(daily_cap[0].contract_id, assets[0].id)
        self.assertEqual(daily_cap[0].date, date.today())
        self.assertEqual(daily_cap[0].cap_hit, 500.0)
        self.assertEqual(daily_cap[0].non_roster, False)
        self.assertEqual(daily_cap[0].retained, False)
        self.assertEqual(daily_cap[1].team_id, assets[2].current_team)
        self.assertEqual(daily_cap[1].season_id, season.id)
        self.assertEqual(daily_cap[1].contract_id, assets[2].id)
        self.assertEqual(daily_cap[1].date, date.today())
        self.assertEqual(daily_cap[1].cap_hit, 250.0)
        self.assertEqual(daily_cap[1].non_roster, True)
        self.assertEqual(daily_cap[1].retained, False)
        self.assertEqual(daily_cap[2].team_id, assets[1].current_team)
        self.assertEqual(daily_cap[2].season_id, season.id)
        self.assertEqual(daily_cap[2].contract_id, assets[1].id)
        self.assertEqual(daily_cap[2].date, date.today())
        self.assertEqual(daily_cap[2].cap_hit, 49.5)
        self.assertEqual(daily_cap[2].non_roster, True)
        self.assertEqual(daily_cap[2].retained, False)

        self.assertEqual(daily_cap[3].team_id, ret_assets[0].current_team)
        self.assertEqual(daily_cap[3].season_id, season.id)
        self.assertEqual(daily_cap[3].contract_id, assets[0].id)
        self.assertEqual(daily_cap[3].date, date.today())
        self.assertEqual(daily_cap[3].cap_hit, 333.0)
        self.assertEqual(daily_cap[3].non_roster, False)
        self.assertEqual(daily_cap[3].retained, True)
        self.assertEqual(daily_cap[5].team_id, ret_assets[2].current_team)
        self.assertEqual(daily_cap[5].season_id, season.id)
        self.assertEqual(daily_cap[5].contract_id, assets[2].id)
        self.assertEqual(daily_cap[5].date, date.today())
        self.assertEqual(daily_cap[5].cap_hit, 166.5)
        self.assertEqual(daily_cap[5].non_roster, True)
        self.assertEqual(daily_cap[5].retained, True)
        self.assertEqual(daily_cap[7].team_id, ret_assets[1].current_team)
        self.assertEqual(daily_cap[7].season_id, season.id)
        self.assertEqual(daily_cap[7].contract_id, assets[1].id)
        self.assertEqual(daily_cap[7].date, date.today())
        self.assertEqual(daily_cap[7].cap_hit, 32.97)
        self.assertEqual(daily_cap[7].non_roster, True)
        self.assertEqual(daily_cap[7].retained, True)

        self.assertEqual(daily_cap[4].team_id, ret_assets2[0].current_team)
        self.assertEqual(daily_cap[4].season_id, season.id)
        self.assertEqual(daily_cap[4].contract_id, assets[0].id)
        self.assertEqual(daily_cap[4].date, date.today())
        self.assertEqual(daily_cap[4].cap_hit, 167.0)
        self.assertEqual(daily_cap[4].non_roster, False)
        self.assertEqual(daily_cap[4].retained, True)
        self.assertEqual(daily_cap[6].team_id, ret_assets2[2].current_team)
        self.assertEqual(daily_cap[6].season_id, season.id)
        self.assertEqual(daily_cap[6].contract_id, assets[2].id)
        self.assertEqual(daily_cap[6].date, date.today())
        self.assertEqual(daily_cap[6].cap_hit, 83.5)
        self.assertEqual(daily_cap[6].non_roster, True)
        self.assertEqual(daily_cap[6].retained, True)
        self.assertEqual(daily_cap[8].team_id, ret_assets2[1].current_team)
        self.assertEqual(daily_cap[8].season_id, season.id)
        self.assertEqual(daily_cap[8].contract_id, assets[1].id)
        self.assertEqual(daily_cap[8].date, date.today())
        self.assertEqual(daily_cap[8].cap_hit, 16.53)
        self.assertEqual(daily_cap[8].non_roster, True)
        self.assertEqual(daily_cap[8].retained, True)

        self.assertEqual(team_season.daily_cap_used(32), 267.03)
        self.assertEqual(team_season.daily_cap_used(30), 532.47)
        self.assertEqual(team_season.daily_cap_used(28), 799.5)

        DailyCap.query.delete()
        self.assertEqual(DailyCap.query.count(), 0)
        self.assertEqual(TeamSeason.query.count(), 0)
        team_season.daily_cap_updates()
        self.assertEqual(DailyCap.query.count(), 9)
        self.assertEqual(TeamSeason.query.count(), 0)

        DailyCap.query.delete()
        teams = [Team(id=28), Team(id=30), Team(id=32)]
        for team in teams:
            db.session.add(team)
        db.session.commit()
        self.assertEqual(DailyCap.query.count(), 0)
        self.assertEqual(TeamSeason.query.count(), 0)
        team_season.daily_cap_updates()
        self.assertEqual(DailyCap.query.count(), 9)
        self.assertEqual(TeamSeason.query.count(), 3)

    def test_soir_ratio(self):
        PREVIOUS = 283
        CURRENT = self.app.config["CURRENT_SEASON"]
        PERSON = 7
        CONTRACT = 29
        DC = DailyCap(
            team_id=1,
            cap_hit=1,
            retained=False,
            date=date.today(),
            contract_id=CONTRACT,
            season_id=CURRENT,
            non_roster=False,
        )
        current = Season(
            id=CURRENT,
            regular_season_start=date(year=2020, month=1, day=1),
            regular_season_end=date(year=2020, month=1, day=11),
            free_agency_opening=date(year=2019, month=12, day=25),
        )
        previous = Season(
            id=PREVIOUS,
            regular_season_start=date(year=2019, month=1, day=1),
            regular_season_end=date(year=2019, month=1, day=11),
            free_agency_opening=date(year=2018, month=12, day=25),
        )
        person = Person(id=PERSON, name="Test Person", position="G")
        contract = Contract(id=CONTRACT, person_id=PERSON)
        goalie_season = GoalieSeason(
            person_id=PERSON,
            season_id=PREVIOUS,
            games_played=41,
        )
        skater_season = SkaterSeason(
            person_id=PERSON,
            season_id=PREVIOUS,
            games_played=20,
        )
        daily_cap = [
            DailyCap(
                cap_hit=1,
                retained=False,
                team_id=1,
                date=date.today(),
                contract_id=CONTRACT,
                season_id=PREVIOUS,
                non_roster=False,
            ),
            DailyCap(
                cap_hit=1,
                retained=False,
                team_id=1,
                date=date.today(),
                contract_id=CONTRACT,
                season_id=PREVIOUS,
                non_roster=False,
            ),
        ]
        db.session.add(current)
        db.session.add(previous)
        db.session.add(person)
        db.session.add(contract)
        db.session.commit()
        self.assertEqual(team_season.soir_ratio(DC), 0)

        db.session.add(goalie_season)
        db.session.add(skater_season)
        db.session.commit()
        self.assertEqual(team_season.soir_ratio(DC), 10 / 41)

        skater_season.person_id = PERSON + 2
        db.session.add(skater_season)
        db.session.commit()
        self.assertEqual(team_season.soir_ratio(DC), 0.5)

        db.session.add(daily_cap[0])
        db.session.add(daily_cap[1])
        db.session.commit()
        self.assertEqual(team_season.soir_ratio(DC), 0.2)

    def test_current_ltir_contracts(self):
        TEAM = 8888
        DATE = date(year=1999, month=11, day=5)
        self.assertEqual(team_season.current_ltir_contracts(TEAM), 0)
        records = [
            DailyCap(
                team_id=TEAM,
                date=DATE,
                ltir=False,
                season_id=self.app.config["CURRENT_SEASON"],
                contract_id=1,
                cap_hit=1,
            ),
            DailyCap(
                team_id=TEAM,
                date=DATE + timedelta(hours=48),
                ltir=True,
                season_id=self.app.config["CURRENT_SEASON"],
                contract_id=1,
                cap_hit=1,
            ),
            DailyCap(
                team_id=TEAM,
                date=DATE,
                ltir=True,
                season_id=self.app.config["CURRENT_SEASON"],
                contract_id=1,
                cap_hit=1,
            ),
            DailyCap(
                team_id=TEAM,
                date=DATE,
                ltir=True,
                season_id=self.app.config["CURRENT_SEASON"],
                contract_id=1,
                cap_hit=1,
            ),
        ]
        for record in records:
            db.session.add(record)
        db.session.commit()
        self.assertEqual(team_season.current_ltir_contracts(TEAM), 0)
        self.assertEqual(team_season.current_ltir_contracts(TEAM, DATE), 2)
        self.assertEqual(
            team_season.current_ltir_contracts(TEAM, DATE + timedelta(hours=48)), 1
        )

    def test_manually_update_team_season(self):
        db.session.add(
            record := TeamSeason(
                team_id=55,
                season_id=42,
                max_ltir_relief=22,
                projected_cap_hit=12,
            )
        )
        db.session.commit()
        team_season.manually_update_team_season(
            record,
            {
                "projected_cap_hit": 333,
                "cap_hit": 99,
                "team": 44,
                "season_id": 12,
            },
        )
        self.assertEqual(record.projected_cap_hit, 333)
        self.assertEqual(record.cap_hit, 99)
        self.assertEqual(record.max_ltir_relief, 22)
        self.assertEqual(record.team_id, 55)
        self.assertEqual(record.season_id, 42)

    # def test_daily_cap_updates(self):
    #   Offseason/playoffs
    #   Regular season
    # def test_get_cap_hit(self):
    # TODO: Fix tests for projected cap hit


if __name__ == "__main__":
    unittest.main(verbosity=2)
