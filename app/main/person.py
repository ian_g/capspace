import re
import urllib.request
from datetime import date, datetime, timedelta
from flask import render_template, url_for, redirect, abort, current_app, flash
from sqlalchemy import func, or_, and_
from sqlalchemy.exc import MultipleResultsFound
from defusedxml.ElementTree import fromstring
from .. import db
from ..models import (
    Person,
    DraftPick,
    Team,
    Asset,
    Season,
    SigningRights,
    ProfessionalTryout,
    Contract,
    ContractYear,
    BuyoutYear,
    Penalty,
    PenaltyYear,
    CachedContent,
    TeamSeason,
    Note,
    Alias,
)


def person_page(person_id):
    if person_id[:3].lower() == "nhl":
        person_data = Person.query.filter(Person.nhl_id == person_id[4:]).one_or_none()
    elif person_id[:2].lower() == "ep":
        person_data = Person.query.filter(Person.ep_id == person_id[3:]).one_or_none()
    elif person_id.isdigit():
        person_data = Person.query.get(person_id)
    elif "-" in person_id and person_id.split("-")[-1].isdigit():
        person_data = Person.query.get(person_id.split("-")[-1])
    else:
        person_id = person_id.replace("_", " ").replace("-", " ")
        person_data = Person.query.filter(Person.name.like(f"%{person_id}%")).all()
        if len(person_data) == 1:
            url = person_data[0].name.replace(" ", "-").lower()
            url += f"-{person_data[0].id}"
            return redirect(url_for("main.person_route", person_id=url), code=303)
        elif len(person_data) == 0:
            flash("Unrecognized person. Redirecting to search", "search")
            return redirect(
                url_for("main.search_route", search=f"{person_id}"), code=303
            )
        else:
            flash("Multiple matching people. Redirecting to search", "search")
            return redirect(
                url_for("main.search_route", search=f'"{person_id}"'), code=303
            )

    if not person_data:
        abort(404)

    age = None
    death = person_data.death_date
    if person_data.birthdate and person_data.death_date:
        birth = person_data.birthdate
        age = (
            death.year
            - birth.year
            - ((death.month, death.day) < (birth.month, birth.day))
        )
    elif person_data.birthdate:
        birth = person_data.birthdate
        today = date.today()
        age = (
            today.year
            - birth.year
            - ((today.month, today.day) < (birth.month, birth.day))
        )

    if death:
        death = death.strftime(current_app.config["DATE_FMT"])
    data = get_data(person_data.id)
    current_season = (
        Season.query.filter(Season.id == current_app.config["CURRENT_SEASON"])
        .with_entities(Season.name)
        .first()[0]
    )
    data["ep_stats"] = get_ep_stats(person_data.id, person_data.ep_id)

    return render_template(
        "person.html",
        title=person_data.name,
        css="person.css",
        age=age,
        death=death,
        data=data,
        current_season=current_season,
    )


def get_url_person(person_id, route):
    person_data = None
    id_type = None
    if len(person_id) == 7 and person_id.isdigit():
        person_data = Person.query.filter_by(nhl_id=person_id).one_or_none()
        id_type = "NHLID"
    elif person_id.isdigit():
        person_data = Person.query.filter_by(id=person_id).one_or_none()
        id_type = "ID"
    else:
        person_id = person_id.replace("-", " ").replace("_", " ")
        person_id = " ".join([word.capitalize() for word in person_id.split()])
        try:
            person_data = Person.query.filter_by(name=person_id).one_or_none()
            id_type = "NAME"
            if person_data is None:
                person_data = Person.query.filter(
                    Person.name.like(f"%{person_id}%")
                ).one_or_none()
                id_type = "NAMESTUB"
        except MultipleResultsFound:
            abort(500, f'Multiple people match the name "{person_id}"')
    if person_data is None:
        abort(404)
    current_app.logger.debug(f"\t/{route}/{id_type} - Found: {person_data.name}")
    return person_data, id_type


def get_data(person_id):
    person_data = Person.query.filter_by(id=person_id).one_or_none()
    if not person_data:
        return {"error": "Invalid or unused person ID"}
    draft_data = get_draft_pick(person_data.draft_pick)
    height = "N/A"
    if person_data.height:
        feet = person_data.height // 12
        inches = person_data.height % 12
        height = f"{feet}' {inches}\""

    api_data = {
        "id": person_data.id,
        "name": person_data.name,
        "birthdate": (
            person_data.birthdate.strftime(current_app.config["DATE_FMT"])
            if person_data.birthdate
            else ""
        ),
        "birthplace": person_data.birthplace,
        "nationality": person_data.nationality,
        "shoots": person_data.shoots,
        "number": person_data.number,
        "position": person_data.position,
        "height": height,
        "weight": f"{person_data.weight}lbs" if person_data.weight else None,
        "nhl_id": person_data.nhl_id,
        "ep_id": person_data.ep_id,
        "contracts": get_contracts(person_id),
        "draft_round": draft_data["draft_round"],
        "draft_position": draft_data["draft_position"],
        "draft_team": draft_data["draft_team"],
        "draft_year": draft_data["draft_year"],
        "draft_logo": draft_data["logo"],
        "aliases": get_aliases(person_data.id),
        "current_status": person_data.current_status,
        "hall_of_fame": person_data.hall_of_fame,
        "waivers_exempt": person_data.waivers_exempt,
        "current_team": get_current_team(person_data.id),
        "tryouts": get_professional_tryouts(person_data.id),
    }
    if "draft_overall" in draft_data:
        api_data["draft_overall"]: draft_data["draft_overall"]
    return api_data


def get_aliases(person_id):
    data = (
        Alias.query.filter(Alias.person_id == person_id)
        .filter(Alias.search == False)
        .with_entities(
            Alias.value,
        )
        .all()
    )
    return [value[0] for value in data]


def get_current_team(person_id):
    rights = db.session.query(
        SigningRights.id.label("id"),
        SigningRights.person_id.label("person_id"),
    )
    contracts = db.session.query(
        Contract.id.label("id"),
        Contract.person_id.label("person_id"),
    )
    person_rights = rights.union(contracts).subquery()
    query = (
        Asset.query.join(person_rights, person_rights.c.id == Asset.id)
        .join(Team, Team.id == Asset.current_team)
        .filter(Asset.active == True)
        .filter(person_rights.c.person_id == person_id)
        .with_entities(
            Team.name,
            Team.logo,
            Team.abbreviation,
        )
        .first()
    )
    if query:
        return {"name": query[0], "logo": query[1], "abbreviation": query[2]}
    return {"name": None, "logo": None}


def get_ep_stats(person_id, ep_id):
    cached = (
        CachedContent.query.filter(CachedContent.record_table == "person")
        .filter(CachedContent.record_id == person_id)
        .filter(
            (CachedContent.expiration >= datetime.now())
            | (CachedContent.expiration == None)
        )
        .order_by(CachedContent.created.desc())
        .first()
    )
    if cached:
        return cached.content

    # Query EP
    if not ep_id:
        return ""
    current_app.logger.info(
        f"Querying EliteProspects for new/updated stats ({person_id=} {ep_id=})"
    )
    rss = query_eliteprospects_person(ep_id)
    if rss != "":
        content = update_cached_stats(rss, person_id)
        if content:
            return content

    # Fallback to expired entry
    cached = (
        CachedContent.query.filter(CachedContent.record_table == "person")
        .filter(CachedContent.record_id == person_id)
        .order_by(CachedContent.created.desc())
        .first()
    )
    if cached:
        return cached.content

    return ""


def update_cached_stats(rss, person_id):
    data = fromstring(rss)
    namespace = {"atom": "http://www.w3.org/2005/Atom"}
    content = None
    source = None
    try:
        item = data.find("channel").find("item")
        content = item.find("description").text
    except AttributeError as e:
        current_app.logger.error(f"Unexpected EliteProspects RSS feed structure: {e}")

    try:
        source_node = data.find("channel").find("atom:link", namespace)
        source = source_node.attrib["href"]
    except AttributeError as e:
        current_app.logger.error(f"Unexpected EliteProspects RSS feed structure: {e}")
    except KeyError as e:
        current_app.logger.error(f"Attribute not found in node: {e}")

    if content and "<script>" not in content:
        expiration = datetime.now() + timedelta(days=14)
        person = Person.query.filter(Person.id == person_id).first()
        if person and person.status_is("Retired"):
            expiration = None
        if person and person.birthdate:
            age = date.today().year - person.birthdate.year
            if age > 45:
                expiration = None

        content = re.sub(r">\s+<", "><", content)
        content = re.sub(r'class="(tr|td)_stats_(odd|even)"', "", content)
        cached = CachedContent(
            record_table="person",
            record_id=person_id,
            created=datetime.now(),
            expiration=expiration,
            source=source,
            content=content,
        )
        CachedContent.query.filter(CachedContent.record_table == "person").filter(
            CachedContent.record_id == person_id
        ).filter(CachedContent.expiration < datetime.now()).delete()
        db.session.add(cached)
        db.session.commit()
        return content


def query_eliteprospects_person(ep_id):
    try:
        url = f"https://www.eliteprospects.com/rss_player_stats2.php?player={ep_id}"
        rss = urllib.request.urlopen(url, timeout=3).read()
        return rss
    except urllib.error.HTTPError as e:
        current_app.logger.error(f"EliteProspects stats query failed: {e}")
        return ""
    except urllib.error.URLError as e:
        current_app.logger.error(
            "EliteProspects URL not found. Not connected to the internet? {e}"
        )
        return ""
    except TimeoutError as e:
        current_app.logger.error(f"EliteProspects stats query timed out: {e}")
        return ""


def get_draft_pick(pick_id):
    raw_pick = (
        DraftPick.query.with_entities(
            DraftPick.round,
            DraftPick.position,
            Team.abbreviation,
            Season.draft_date,
            Season.id,
            TeamSeason.logo,
        )
        .filter(DraftPick.id == pick_id)
        .join(Asset, Asset.id == DraftPick.id)
        .join(Team, Asset.current_team == Team.id)
        .join(Season, DraftPick.season_id == Season.id)
        .join(
            TeamSeason,
            and_(
                TeamSeason.team_id == Asset.current_team,
                TeamSeason.season_id == DraftPick.season_id,
            ),
        )
        .one_or_none()
    )
    if raw_pick is None:
        current_app.logger.debug(f"Undrafted")
        return {
            "draft_round": "N/A",
            "draft_position": "N/A",
            "draft_team": "N/A",
            "draft_year": "Undrafted",
            "logo": None,
        }
    data = {
        "draft_round": raw_pick[0],
        "draft_position": raw_pick[1],
        "draft_team": raw_pick[2],
        "draft_year": raw_pick[3].year,
        "season_id": raw_pick[4],
        "logo": raw_pick[5] or None,
    }
    overall = (
        DraftPick.query.filter(DraftPick.season_id == data["season_id"])
        .filter(
            or_(
                DraftPick.round < data["draft_round"],
                and_(
                    DraftPick.round == data["draft_round"],
                    DraftPick.position <= data["draft_position"],
                ),
            )
        )
        .with_entities(func.count(DraftPick.id))
        .first()
    )
    data["draft_overall"] = overall[0]
    return data


def get_professional_tryouts(person_id):
    raw_data = (
        ProfessionalTryout.query.join(Asset, Asset.id == ProfessionalTryout.id)
        .join(Team, Team.id == Asset.current_team)
        .join(Season, Season.id == ProfessionalTryout.season_id)
        .filter(ProfessionalTryout.person_id == person_id)
        .with_entities(
            ProfessionalTryout.signing_date,
            Team.name,
            Season.name,
            Team.id,
            Season.id,
            Asset.active,
            Team.abbreviation,
        )
        .order_by(
            ProfessionalTryout.signing_date.desc(),
        )
        .all()
    )
    pto_data = [
        {
            "signing_date": tryout[0].strftime(current_app.config["DATE_FMT"]),
            "signing_team": tryout[1],
            "logo": get_contract_logo(tryout[3], tryout[4]),
            "pto": f"Professional Tryout {tryout[2]}",
            "active": tryout[5],
            "abbreviation": tryout[6],
        }
        for tryout in raw_data
    ]
    return pto_data


def get_contracts(person_id):
    raw_contracts = query_contracts(person_id)
    contract_data = [
        {
            "seasons": contract[1],
            "expires_as": contract[2],
            "total_value": contract[3],
            "signing_date": (
                contract[5].strftime(current_app.config["DATE_FMT"])
                if contract[5]
                else "N/A"
            ),
            "signing_team": contract[4],
            "current": contract[0],
            "entry_level": contract[6],
            "thirty_five_plus": contract[7],
            "years": get_contract_years(contract[8]),
            "id": contract[8],
            "buyout_date": contract[9],
            "extension": contract[10],
            "termination_date": contract[11],
            "penalty_years": get_penalty_years(contract[8]),
            "notes": get_contract_notes(contract[8]),
            "logo": get_contract_logo(contract[13], contract[14]),
            "abbreviation": contract[12],
            "offer_sheet": contract[15],
        }
        for contract in raw_contracts
    ]
    for idx in range(len(contract_data)):
        if contract_data[idx]["buyout_date"]:
            contract_data[idx]["buyout_date"] = contract_data[idx][
                "buyout_date"
            ].strftime(current_app.config["DATE_FMT"])
            contract_data[idx]["buyout_years"] = get_buyout_years(
                contract_data[idx]["id"]
            )
        else:
            del contract_data[idx]["buyout_date"]

        if contract_data[idx]["termination_date"]:
            contract_data[idx]["termination_date"] = contract_data[idx][
                "termination_date"
            ].strftime(current_app.config["DATE_FMT"])
        else:
            del contract_data[idx]["termination_date"]

        if contract_data[idx]["penalty_years"] == {}:
            del contract_data[idx]["penalty_years"]
    current_app.logger.debug(f"[/api]/person/{person_id} - Getting contracts")
    return contract_data


# 13, 14
def get_contract_logo(team_id, season_id):
    team_season = (
        TeamSeason.query.filter(TeamSeason.season_id == season_id)
        .filter(TeamSeason.team_id == team_id)
        .first()
    )
    if team_season:
        return team_season.logo
    return Team.query.get(team_id).logo


def get_contract_notes(contract_id):
    query = (
        Note.query.filter(Note.record_table == "contract")
        .filter(Note.record_id == contract_id)
        .with_entities(
            Note.note,
            Note.safe,
        )
        .all()
    )
    return [
        {
            "note": note[0],
            "safe": note[1],
        }
        for note in query
    ]


def get_contract_years(contract_id):
    raw_years = query_contract_years(contract_id)
    year_data = [
        {
            "season": year[0],
            "cap_hit": year[1],
            "aav": year[2],
            "nhl_salary": year[3],
            "minors_salary": year[4],
            "signing_bonus": year[5],
            "performance_bonuses": year[6],
            "bought_out": year[9],
            "entry_level_slide": year[10],
            "nmc": year[11],
            "ntc": year[12],
            "clause_limits": year[13],
            "terminated": year[14],
            "pre_cap": (year[15] < date(year=2005, month=7, day=1)),
        }
        for year in raw_years
    ]
    for idx in range(len(year_data)):
        if year_data[idx]["entry_level_slide"]:
            del year_data[idx]["performance_bonuses"]
            del year_data[idx]["nhl_salary"]
            del year_data[idx]["minors_salary"]
        else:
            del year_data[idx]["entry_level_slide"]
    return year_data


def get_buyout_years(contract_id):
    raw_years = query_buyout_years(contract_id)
    year_data = [
        {
            "season": year[0],
            "signing_bonus": year[1],
            "nhl_salary": year[2],
            "cap_hit": year[3],
        }
        for year in raw_years
    ]
    return year_data


def get_penalty_years(contract_id):
    penalties = (
        Penalty.query.filter(Penalty.contract_id == contract_id)
        .join(Asset, Asset.id == Penalty.id)
        .join(Team, Team.id == Asset.current_team)
        .with_entities(
            Penalty.id,
            Penalty.type,
            Team.name,
        )
        .all()
    )
    penalty_data = []
    for penalty in penalties:
        raw_years = query_penalty_years(penalty[0])
        penalty_data.append(
            {
                "type": penalty[1],
                "team": penalty[2],
                "years": [
                    {
                        "season": year[0],
                        "cap_hit": year[1],
                        "team": year[2],
                    }
                    for year in raw_years
                ],
            }
        )
    return penalty_data


def query_contracts(person_id):
    return (
        Contract.query.filter_by(person_id=person_id)
        .join(Asset, Asset.id == Contract.id)
        .join(Team, Asset.originating_team == Team.id)
        .with_entities(
            Asset.active,
            Contract.years,
            Contract.expiration_status,
            Contract.total_value,
            Team.name,
            Contract.signing_date,
            Contract.entry_level,
            Contract.thirty_five_plus,
            Contract.id,
            Contract.buyout_date,
            Contract.extension,
            Contract.termination_date,
            Team.abbreviation,
            Team.id,
            Contract.signing_season_id,
            Contract.offer_sheet,
        )
        .order_by(
            Asset.active.desc(),
            Asset.active * Contract.signing_date.asc().nullslast(),
            Contract.signing_date.desc(),
            Contract.id.desc(),
        )
        .all()
    )


def query_contract_years(contract_id):
    return (
        ContractYear.query.filter_by(contract_id=contract_id)
        .join(Season, Season.id == ContractYear.season_id)
        .with_entities(
            Season.name,
            ContractYear.cap_hit,
            ContractYear.aav,
            ContractYear.nhl_salary,
            ContractYear.minors_salary,
            ContractYear.signing_bonus,
            ContractYear.performance_bonuses,
            Season.id,
            ContractYear.id,
            ContractYear.bought_out,
            ContractYear.entry_level_slide,
            ContractYear.nmc,
            ContractYear.ntc,
            ContractYear.clause_limits,
            ContractYear.terminated,
            Season.free_agency_opening,
        )
        .order_by(Season.free_agency_opening, Season.name.asc())
        .all()
    )


def query_buyout_years(contract_id):
    return (
        BuyoutYear.query.filter(BuyoutYear.contract_id == contract_id)
        .join(Season, Season.id == BuyoutYear.season_id)
        .with_entities(
            Season.name,
            BuyoutYear.signing_bonus,
            BuyoutYear.nhl_salary,
            BuyoutYear.cap_hit,
        )
        .order_by(Season.free_agency_opening, Season.name.asc())
        .all()
    )


def query_penalty_years(penalty_id):
    return (
        PenaltyYear.query.join(Penalty, Penalty.id == PenaltyYear.penalty_id)
        .join(Season, Season.id == PenaltyYear.season_id)
        .filter(Penalty.id == penalty_id)
        .with_entities(
            Season.name,
            PenaltyYear.cap_hit,
            Penalty.type,
        )
        .order_by(
            Penalty.type,
            Season.free_agency_opening.asc(),
            Season.name.asc(),
        )
        .all()
    )
