import sys
import random
import unittest
from datetime import datetime, date, timedelta
from app import create_app, db
from app.models import (
    Asset,
    Contract,
    ContractYear,
    Team,
    Person,
    Season,
    GoalieSeason,
    SkaterSeason,
    SigningRights,
)

from app.admin import qualify_person

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class QualifyingOffersTest(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_get_teams(self):
        self.assertEqual(qualify_person.get_teams(), [])

        team = Team(name="Test Zebra")
        db.session.add(team)
        db.session.commit()
        self.assertEqual(qualify_person.get_teams(), [(1, "Test Zebra")])

        team = Team(name="Test Puppy")
        db.session.add(team)
        db.session.commit()
        self.assertEqual(
            qualify_person.get_teams(), [(2, "Test Puppy"), (1, "Test Zebra")]
        )

    def test_get_pending_rfas(self):
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"], free_agency_opening=date.today()
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                free_agency_opening=date.today() + timedelta(days=1),
            ),
        ]
        db.session.add(seasons[0])
        db.session.add(seasons[1])
        db.session.commit()
        self.assertEqual(qualify_person.get_pending_rfas(), [])
        self.assertEqual(qualify_person.get_pending_rfas(24), [])
        assets = [
            Asset(id=821, current_team=22, originating_team=22, type="Contract"),
            Asset(id=841, current_team=24, originating_team=22, type="Contract"),
            Asset(id=221, current_team=26, originating_team=22, type="Contract"),
        ]
        persons = [
            Person(id=22, active=True, name="Test One"),
            Person(id=29, active=True, name="Test Two"),
            Person(id=12, active=True, name="Test Three"),
        ]
        contracts = [
            Contract(id=assets[0].id, person_id=persons[1].id, expiration_status="RFA"),
            Contract(id=assets[1].id, person_id=persons[2].id, expiration_status="RFA"),
            Contract(id=assets[2].id, person_id=persons[0].id, expiration_status="UFA"),
        ]
        contract_years = [
            ContractYear(
                contract_id=contracts[0].id, season_id=self.app.config["CURRENT_SEASON"]
            ),
            ContractYear(
                contract_id=contracts[1].id, season_id=self.app.config["CURRENT_SEASON"]
            ),
            ContractYear(
                contract_id=contracts[2].id, season_id=self.app.config["CURRENT_SEASON"]
            ),
        ]
        for record in assets + contracts + contract_years + persons:
            db.session.add(record)
        db.session.commit()

        people = qualify_person.get_pending_rfas()
        self.assertEqual(len(people), 2)
        self.assertEqual(people[0], (12, "Test Three"))
        self.assertEqual(people[1], (29, "Test Two"))

        people = qualify_person.get_pending_rfas(24)
        self.assertEqual(people, [(12, "Test Three")])

        db.session.add(
            ContractYear(
                contract_id=contracts[1].id,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
            )
        )
        db.session.commit()
        self.assertEqual(qualify_person.get_pending_rfas(24), [])

        people = qualify_person.get_pending_rfas()
        self.assertEqual(people, [(29, "Test Two")])

        asset_sr = Asset(type="Signing Rights", active=True, id=7777)
        signing_rights = SigningRights(id=asset_sr.id, person_id=persons[1].id)
        db.session.add(asset_sr)
        db.session.add(signing_rights)
        db.session.commit()

        people = qualify_person.get_pending_rfas()
        self.assertEqual(people, [])

    def test_calculate_qualifying_offer(self):
        seasons = [
            Season(
                name="Test Zero",
                id=self.app.config["CURRENT_SEASON"],
                free_agency_opening=date.today(),
                regular_season_start=date.today(),
            ),
            Season(
                name="Test Apple",
                id=83,
                free_agency_opening=date.today() - timedelta(days=1),
                regular_season_start=date.today() - timedelta(days=1),
            ),
            Season(
                name="Test Broccoli",
                id=33,
                free_agency_opening=date.today() - timedelta(days=2),
                regular_season_start=date.today() - timedelta(days=2),
            ),
            Season(
                name="Test Pie",
                id=13,
                free_agency_opening=date.today() - timedelta(days=3),
                regular_season_start=date.today() - timedelta(days=3),
            ),
        ]
        for season in seasons:
            db.session.add(season)
        person = Person(
            name="Test Person",
            id=284,
            position="F",
        )
        db.session.add(person)
        asset = Asset(
            id=1233,
            current_team=348,
            type="Contract",
            active=True,
        )
        db.session.add(asset)
        contract = Contract(
            id=asset.id,
            person_id=person.id,
            signing_date=date(year=2019, month=7, day=1),
        )
        db.session.add(contract)
        contract_year = ContractYear(
            contract_id=contract.id,
            season_id=seasons[0].id,
            aav=1000000,
            nhl_salary=750000,
        )
        db.session.add(contract_year)
        stats = [
            SkaterSeason(
                team_id=1,
                season_id=seasons[1].id,
                person_id=person.id,
                games_played=90,
                goals=4,
                assists=20,
                points=24,
                shots=200,
                hits=5,
            ),
            SkaterSeason(
                team_id=1,
                season_id=seasons[2].id,
                person_id=person.id,
                games_played=90,
                goals=20,
                assists=4,
                points=24,
                shots=250,
                hits=0,
            ),
            GoalieSeason(
                team_id=1,
                season_id=seasons[1].id,
                person_id=person.id,
                games_played=60,
                wins=20,
                losses=20,
                shutouts=5,
                shots_against=1000,
                goals_against=97,
            ),
            GoalieSeason(
                team_id=1,
                season_id=seasons[2].id,
                person_id=person.id,
                games_played=40,
                wins=30,
                losses=20,
                shutouts=5,
                shots_against=3000,
                goals_against=97,
            ),
        ]
        for season in stats:
            db.session.add(season)
        db.session.commit()

        skater_stats = ["games_played", "goals", "assists", "points", "shots", "hits"]
        data = qualify_person.calculate_qualifying_offer(person.id)
        self.assertEqual(data["name"], person.name)
        self.assertEqual(data["id"], person.id)
        self.assertEqual(data["last_salary"], contract_year.nhl_salary)
        self.assertEqual(data["last_aav"], contract_year.aav)
        self.assertEqual(data["contract_signing"], contract.signing_date)
        self.assertEqual(data["one_way"], True)
        self.assertEqual(data["qualifying_salary"], 787500)
        self.assertEqual(data["stat_names"], skater_stats)
        self.assertEqual(data["stats"][0], ["Test Apple", 90, 4, 20, 24, 200, 5])
        self.assertEqual(data["stats"][1], ["Test Broccoli", 90, 20, 4, 24, 250, 0])

        person.position = "G"
        contract_year.nhl_salary = 2000000
        goalie_stats = [
            "games_played",
            "wins",
            "losses",
            "shutouts",
            "shots_against",
            "goals_against",
        ]
        db.session.add(person)
        db.session.add(contract_year)
        db.session.commit()
        data = qualify_person.calculate_qualifying_offer(person.id)
        self.assertEqual(data["last_salary"], contract_year.nhl_salary)
        self.assertEqual(data["one_way"], False)
        self.assertEqual(data["qualifying_salary"], 2000000)
        self.assertEqual(data["stat_names"], goalie_stats)
        self.assertEqual(data["stats"][0], ["Test Apple", 60, 20, 20, 5, 1000, 97])
        self.assertEqual(data["stats"][1], ["Test Broccoli", 40, 30, 20, 5, 3000, 97])

        contract.signing_date = date.today()
        db.session.add(contract)
        db.session.commit()
        data = qualify_person.calculate_qualifying_offer(person.id)
        self.assertEqual(data["qualifying_salary"], 1200000)

    def test_qualify_player(self):
        seasons = [
            Season(
                name="Test Zero",
                id=self.app.config["CURRENT_SEASON"],
                free_agency_opening=date.today(),
                regular_season_start=date.today(),
            ),
            Season(
                name="Test Apple",
                id=83,
                free_agency_opening=date.today() - timedelta(days=1),
                regular_season_start=date.today() - timedelta(days=1),
            ),
            Season(
                name="Test Broccoli",
                id=33,
                free_agency_opening=date.today() - timedelta(days=2),
                regular_season_start=date.today() - timedelta(days=2),
            ),
            Season(
                name="Test Pie",
                id=13,
                free_agency_opening=date.today() - timedelta(days=3),
                regular_season_start=date.today() - timedelta(days=3),
            ),
        ]
        for season in seasons:
            db.session.add(season)
        person = Person(
            name="Test Person",
            id=284,
            position="F",
        )
        db.session.add(person)
        asset = Asset(
            id=1233,
            current_team=348,
            type="Contract",
            active=True,
        )
        db.session.add(asset)
        contract = Contract(
            id=asset.id,
            person_id=person.id,
            signing_date=date(year=2019, month=7, day=1),
        )
        db.session.add(contract)
        contract_year = ContractYear(
            contract_id=contract.id,
            season_id=seasons[0].id,
            aav=1000000,
            nhl_salary=750000,
            minors_salary=80000,
        )
        db.session.add(contract_year)
        stats = [
            SkaterSeason(
                team_id=1,
                season_id=seasons[1].id,
                person_id=person.id,
                games_played=90,
                goals=4,
                assists=20,
                points=24,
                shots=200,
                hits=5,
            ),
            SkaterSeason(
                team_id=1,
                season_id=seasons[2].id,
                person_id=person.id,
                games_played=90,
                goals=20,
                assists=4,
                points=24,
                shots=250,
                hits=0,
            ),
            GoalieSeason(
                team_id=1,
                season_id=seasons[1].id,
                person_id=person.id,
                games_played=60,
                wins=20,
                losses=20,
                shutouts=5,
                shots_against=1000,
                goals_against=97,
            ),
            GoalieSeason(
                team_id=1,
                season_id=seasons[2].id,
                person_id=person.id,
                games_played=40,
                wins=30,
                losses=20,
                shutouts=5,
                shots_against=3000,
                goals_against=97,
            ),
        ]
        for season in stats:
            db.session.add(season)
        db.session.commit()

        qualify_person.qualify_player(person.id)
        self.assertEqual(SigningRights.query.count(), 1)
        signing_rights = SigningRights.query.first()
        self.assertEqual(signing_rights.person_id, person.id)
        self.assertEqual(signing_rights.qualifying_offer, True)
        self.assertEqual(signing_rights.qualifying_salary, 787500)
        self.assertEqual(signing_rights.qualifying_minors, 787500)

        person.position = "G"
        contract_year.nhl_salary = 1000000
        SigningRights.query.delete()
        db.session.add(person)
        db.session.add(contract_year)
        db.session.commit()

        qualify_person.qualify_player(person.id)
        self.assertEqual(SigningRights.query.count(), 1)
        signing_rights = SigningRights.query.first()
        self.assertEqual(signing_rights.person_id, person.id)
        self.assertEqual(signing_rights.qualifying_offer, True)
        self.assertEqual(signing_rights.qualifying_salary, 1000000)
        self.assertEqual(signing_rights.qualifying_minors, 80000)


if __name__ == "__main__":
    unittest.main(verbosity=2)
