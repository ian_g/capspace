import json
from datetime import datetime, date
from flask import render_template, abort, current_app, url_for
from sqlalchemy.sql import case
from sqlalchemy import select, or_, func
from .. import db
from ..models import (
    Person,
    Team,
    Asset,
    Contract,
    ContractYear,
    Transaction,
    TransactionAsset,
    Season,
    SigningRights,
    SkaterSeason,
    GoalieSeason,
    Note,
)


def adjustment_page(team_id):
    team = Team.query.get(team_id)
    data = {
        "roster": get_roster(team_id),
        "team_name": team.name,
        "team_id": team.id,
        "rights": get_rights(team_id),
    }
    return render_template(
        "admin/adjust_roster.html",
        title=f"Adjust Roster - {team.name}",
        css="team.css",
        data=data,
    )


def waivers_page():
    data = {
        "waivers": get_waivers(),
        "teams": get_teams(),
    }
    return render_template(
        "admin/manage_waivers.html", title="Manage Waivers", data=data
    )


def reassign(person_id, field, tr_type, tr_date=None, status=None):
    person = Person.query.get(person_id)
    if not person:
        return {"code": 500, "result": "error", "error": "Unused person ID"}
    contract = (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .filter(Contract.person_id == person_id)
        .filter(Asset.active == True)
        .with_entities(Contract.id)
        .first()
    )
    contract = Contract.query.get(contract[0])
    transaction = Transaction(
        season_id=current_app.config["CURRENT_SEASON"],
        type=tr_type,
    )
    if tr_date:
        transaction.date = datetime.strptime(tr_date, "%Y-%m-%d").date()
    db.session.add(transaction)
    db.session.flush()
    team = Asset.query.get(contract.id).current_team
    transaction_asset = TransactionAsset(
        transaction_id=transaction.id,
        asset_id=contract.id,
        former_team=team,
    )
    if tr_type != "Waived":
        transaction_asset.new_team = team
    db.session.add(transaction_asset)

    if status:
        person_status = person.get_status()
        person_status.append(status)
        person.set_status(person_status)
        db.session.add(person)

    if field == "non_roster":
        contract.non_roster = not contract.non_roster
        contract.contract_limit_exempt = is_exempt(person)
        if not contract.non_roster:
            contract.in_juniors = False
            contract.on_loan = False
    elif field == "in_juniors":
        contract.non_roster = True
        contract.contract_limit_exempt = is_exempt(person)
        contract.in_juniors = True
        contract.on_loan = True
    elif field == "on_loan":
        contract.non_roster = True
        contract.contract_limit_exempt = is_exempt(person)
        contract.in_juniors = False
        contract.on_loan = True
    elif tr_type != "ad_hoc":
        contract.non_roster = False
        contract.on_loan = False
        contract.in_juniors = False
        contract.contract_limit_exempt = False
    current_app.logger.debug(
        f"{contract.non_roster=} {contract.on_loan=} {contract.in_juniors=}"
    )
    db.session.add(contract)
    db.session.commit()
    return {"code": 200, "result": "success"}


def is_exempt(person):
    if not person.non_roster:
        return False
    current_season = Season.query.get(current_app.config["CURRENT_SEASON"])
    age = (
        current_season.regular_season_start.year
        - person.birthdate.year
        - (
            (
                current_season.regular_season_start.month,
                current_season.regular_season_start.day,
            )
            < (person.birthdate.month, person.birthdate.year)
        )
    )
    games_played = (
        SkaterSeason.query.filter(
            SkaterSeason.season_id == current_app.config["CURRENT_SEASON"]
        )
        .filter(SkaterSeason.person_id == person.id)
        .with_entities(func.sum(SkaterSeason.games_played))
        .first()
    )
    if not games_played:
        games_played = (
            GoalieSeason.query.filter(
                GoalieSeason.season_id == current_app.config["CURRENT_SEASON"]
            )
            .filter(GoalieSeason.person_id == person.id)
            .with_entities(func.sum(GoalieSeason.games_played))
            .first()
        )
    if games_played:
        games_played = games_played[0]
    if not games_played:
        games_played = 0

    if age < 20 and games_played < 11:
        return True
    return False


def loan(person_id, tr_date=None):
    person = Person.query.get(person_id)
    if not person:
        return {"code": 500, "result": "error", "error": "Unused person ID"}
    contract = (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .filter(Contract.person_id == person_id)
        .filter(Asset.active == True)
        .with_entities(Contract.id)
        .first()
    )
    contract = Contract.query.get(contract[0])
    contract.non_roster = True
    contract.on_loan = True

    transaction = Transaction(
        season_id=current_app.config["CURRENT_SEASON"],
        type="Loaned",
    )
    if tr_date:
        transaction.date = datetime.strptime(
            tr_date, current_app.config["DATE_FMT"]
        ).date()
    db.session.add(transaction)
    db.session.flush()
    team = Asset.query.get(contract.id).current_team
    transaction_asset = TransactionAsset(
        transaction_id=transaction.id,
        asset_id=contract.id,
        former_team=team,
        new_team=team,
    )
    db.session.add(transaction_asset)

    current_season = Season.query.get(current_app.config["CURRENT_SEASON"])
    age = (
        current_season.regular_season_start.year
        - person.birthdate.year
        - (
            (
                current_season.regular_season_start.month,
                current_season.regular_season_start.day,
            )
            < (person.birthdate.month, person.birthdate.year)
        )
    )
    games_played = (
        SkaterSeason.query.filter(
            SkaterSeason.season_id == current_app.config["CURRENT_SEASON"]
        )
        .filter(SkaterSeason.person_id == person.id)
        .with_entities(func.sum(SkaterSeason.games_played))
        .first()
    )
    if not games_played:
        games_played = (
            GoalieSeason.query.filter(
                GoalieSeason.season_id == current_app.config["CURRENT_SEASON"]
            )
            .filter(GoalieSeason.person_id == person.id)
            .with_entities(func.sum(GoalieSeason.games_played))
            .first()
        )
    if games_played:
        games_played = games_played[0]
    if not games_played:
        games_played = 0

    if age < 20 and games_played < 11:
        contract.contract_limit_exempt = True

    db.session.add(contract)
    db.session.commit()
    return {"code": 200, "result": "success"}


def clear(transaction_asset, to_minors=True, tr_date=None):
    asset = TransactionAsset.query.get(transaction_asset)
    if not asset:
        return {"code": 500, "result": "error", "error": "Unused transaction asset ID"}
    contract = Contract.query.get(asset.asset_id)
    person = Person.query.get(contract.person_id)
    status = person.get_status()
    status.remove("On Waivers")
    person.set_status(status)
    asset.new_team = asset.former_team
    transaction = Transaction(
        season_id=current_app.config["CURRENT_SEASON"],
        type="Cleared Waivers",
    )
    if tr_date:
        transaction.date = datetime.strptime(
            tr_date, current_app.config["DATE_FMT"]
        ).date()
    db.session.add(transaction)
    db.session.flush()
    tr_asset = TransactionAsset(
        transaction_id=transaction.id,
        asset_id=asset.asset_id,
        former_team=asset.former_team,
        new_team=asset.new_team,
    )

    db.session.add(tr_asset)
    db.session.add(transaction)
    db.session.add(asset)
    db.session.add(person)
    db.session.add(contract)
    db.session.commit()
    if to_minors == True:
        reassign(person.id, "non_roster", "Reassigned to Minors", tr_date)
    return {"code": 200, "result": "success"}


def claim(transaction_asset, team_id, tr_date=None):
    transaction_asset = TransactionAsset.query.get(transaction_asset)
    if not transaction_asset:
        return {"code": 500, "result": "error", "error": "Unused transaction asset ID"}
    asset = Asset.query.get(transaction_asset.asset_id)
    person = Person.query.get(Contract.query.get(asset.id).person_id)
    transaction_asset.new_team = team_id
    asset.current_team = team_id
    status = person.get_status()
    status.remove("On Waivers")
    person.set_status(status)

    new_team = Team.query.get(team_id)
    message = 'Claimed by <a href="'
    message += url_for("main.team_route", team_id=new_team.abbreviation)
    message += '" alt="' + new_team.name
    message += ' logo"><img class="logo" src="'
    message += url_for("static", filename=new_team.logo)
    message += f'">{new_team.name} </a>'
    note = Note(
        record_id=transaction_asset.id,
        record_table="transaction_asset",
        note=message,
        safe=True,
    )
    claim = Transaction(
        season_id=current_app.config["CURRENT_SEASON"],
        type="Claimed on waivers",
    )
    if tr_date:
        claim.date = datetime.strptime(tr_date, current_app.config["DATE_FMT"]).date()
    db.session.add(claim)
    db.session.add(asset)
    db.session.add(transaction_asset)
    db.session.add(person)
    db.session.add(note)
    db.session.flush()
    claim_asset = TransactionAsset(
        transaction_id=claim.id,
        asset_id=transaction_asset.asset_id,
        former_team=transaction_asset.new_team,
        new_team=transaction_asset.new_team,
    )
    db.session.add(claim_asset)
    db.session.commit()
    return {"code": 200, "result": "success"}


def terminate(transaction_asset, tr_date=None):
    tr_asset = TransactionAsset.query.get(transaction_asset)
    if not tr_asset:
        return {"code": 500, "result": "error", "error": "Unused transaction asset ID"}
    transaction = Transaction.query.get(tr_asset.transaction_id)
    asset = Asset.query.get(tr_asset.asset_id)
    contract = Contract.query.get(tr_asset.asset_id)
    person = Person.query.get(contract.person_id)
    seasons = (
        Season.query.filter(
            Season.free_agency_opening
            >= (
                Season.query.get(
                    current_app.config["CURRENT_SEASON"]
                ).free_agency_opening
            )
        )
        .with_entities(Season.id)
        .subquery()
    )
    asset.active = False
    if tr_date:
        contract.termination_date = datetime.strptime(
            tr_date, current_app.config["DATE_FMT"]
        ).date()
    else:
        contract.termination_date = date.today()
    tr_asset.new_team = tr_asset.former_team
    transaction.type = "Unconditional Waivers"
    status = person.get_status()
    status.remove("Under Contract")
    status.append("UFA")
    person.set_status(status)
    note = Note(
        record_id=transaction_asset,
        record_table="transaction_asset",
        note="Contract terminated once waviers were cleared",
    )
    termination_transaction = Transaction(
        season_id=current_app.config["CURRENT_SEASON"],
        type="Contract terminated",
    )
    if tr_date:
        termination_transaction.date = datetime.strptime(
            tr_date, current_app.config["DATE_FMT"]
        ).date()
    db.session.add(termination_transaction)
    db.session.flush()
    termination_asset = TransactionAsset(
        transaction_id=termination_transaction.id,
        asset_id=tr_asset.asset_id,
        former_team=tr_asset.new_team,
    )
    db.session.add(termination_asset)
    db.session.add(transaction)
    db.session.add(tr_asset)
    db.session.add(asset)
    db.session.add(contract)
    db.session.add(person)
    db.session.add(note)
    ContractYear.query.filter(ContractYear.contract_id == contract.id).filter(
        ContractYear.season_id.in_(select(seasons))
    ).update(
        {
            ContractYear.terminated: True,
        },
        synchronize_session=False,
    )
    clear(transaction_asset, False, tr_date)
    db.session.commit()
    return {"code": 200, "result": "success"}


def deactivate_rights(person_id, date=None):
    person = Person.query.get(person_id)
    if not person:
        return {"code": 500, "result": "error", "error": "Unused person ID"}
    rights = (
        SigningRights.query.join(Asset, Asset.id == SigningRights.id)
        .filter(SigningRights.person_id == person_id)
        .filter(Asset.active == True)
        .with_entities(SigningRights.id)
        .first()
    )
    asset = Asset.query.get(rights[0])
    asset.active = False
    db.session.add(asset)
    db.session.commit()
    return {"code": 200, "result": "success", "id": asset.id}


def dequalify(person_id, date=None):
    deactivation = deactivate_rights(person_id, date=None)
    if deactivation["code"] != 200:
        return deactivation
    rights = SigningRights.query.get(deactivation["id"])
    rights.qualifying_offer = False

    contract_id = (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .filter(Asset.active == False)
        .filter(Contract.person_id == person_id)
        .order_by(Contract.signing_date.desc())
        .with_entities(Contract.id)
        .first()
    )
    contract = Contract.query.get(contract_id[0])
    person = Person.query.get(contract.person_id)
    contract.expiration_status = "UFA"
    status = person.get_status()
    if "RFA" in status:
        status.remove("RFA")
    status.append("UFA")
    person.set_status(status)
    db.session.add(contract)
    db.session.add(person)
    db.session.add(
        Note(
            record_table="contract",
            record_id=contract.id,
            note="Player reacted unrestricted free agency after not being tendered a qualifying offer",
        )
    )
    db.session.commit()
    return {"code": 200, "result": "success"}


def get_roster(team_id):
    waiver_query = (
        TransactionAsset.query.join(
            Transaction, Transaction.id == TransactionAsset.transaction_id
        )
        .join(Contract, Contract.id == TransactionAsset.asset_id)
        .filter(Transaction.type == "Waived")
        .with_entities(
            Transaction.id,
            Transaction.date,
            TransactionAsset.asset_id,
            TransactionAsset.former_team,
            Contract.person_id,
        )
        .subquery()
    )
    query = (
        Person.query.join(Contract, Contract.person_id == Person.id)
        .join(ContractYear, ContractYear.contract_id == Contract.id)
        .join(Asset, Contract.id == Asset.id)
        .join(waiver_query, waiver_query.c.person_id == Person.id, isouter=True)
        .join(Team, Team.id == waiver_query.c.former_team, isouter=True)
        .filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .filter(Person.current_status.notlike("%On Waivers%"))
        .group_by(Person.id)
        .with_entities(
            Person.name,
            Person.position,
            Person.waivers_exempt,
            Person.id,
            ContractYear.nmc,
            ContractYear.ntc,
            ContractYear.clause_limits,
            Person.current_status,
            Contract.non_roster,
            Contract.contract_limit_exempt,
            func.max(func.date(waiver_query.c.date)),
            Team.abbreviation,
            Team.logo,
            Contract.on_loan,
            Contract.in_juniors,
        )
        .order_by(
            case(
                (Contract.in_juniors == True, 4),
                (Contract.on_loan == True, 3),
                (Contract.non_roster == True, 2),
                else_=1,
            ),
            case((Person.position == "D", 2), (Person.position == "G", 3), else_=1),
            Person.name.asc(),
            ContractYear.cap_hit.desc(),
            Contract.total_value.desc(),
            Contract.years.desc(),
        )
        .all()
    )

    data = [
        {
            "name": contract[0],
            "position": contract[1],
            "waivers_exempt": contract[2],
            "id": contract[3],
            "nmc": contract[4],
            "ntc": contract[5],
            "clause_limits": contract[6],
            "current_status": contract[7],
            "non_roster": contract[8],
            "contract_limit_exempt": contract[9],
            "last_waived_date": contract[10],
            "last_waived_team": contract[11],
            "last_waived_logo": contract[12],
            "on_loan": contract[13],
            "in_juniors": contract[14],
        }
        for contract in query
    ]
    return data


def get_rights(team_id):
    query = (
        SigningRights.query.join(Asset, Asset.id == SigningRights.id)
        .join(Person, Person.id == SigningRights.person_id)
        .filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .with_entities(
            Person.name,
            Person.position,
            Person.waivers_exempt,
            Person.id,
            Person.current_status,
            SigningRights.expiration_date,
            SigningRights.qualifying_salary,
            SigningRights.qualifying_minors,
            SigningRights.qualifying_offer,
        )
        .order_by(
            SigningRights.qualifying_offer.asc(),
            Person.name.asc(),
        )
        .all()
    )

    data = [
        {
            "name": person[0],
            "position": person[1],
            "waivers_exempt": person[2],
            "id": person[3],
            "current_status": person[4],
            "rights_expiration": (
                datetime.strftime(person[5], current_app.config["DATE_FMT"])
                if person[5]
                else "N/A"
            ),
            "qualifying_salary": person[6],
            "qualifying_minors": person[7],
            "is_qualifying_offer": person[8],
        }
        for person in query
    ]
    return data


def get_waivers():
    query = (
        Transaction.query.join(
            TransactionAsset, TransactionAsset.transaction_id == Transaction.id
        )
        .join(Contract, Contract.id == TransactionAsset.asset_id)
        .join(Asset, Asset.id == Contract.id)
        .join(Team, Team.id == Asset.current_team)
        .join(Person, Person.id == Contract.person_id)
        .filter(Transaction.type == "Waived")
        .filter(TransactionAsset.new_team == None)
        .order_by(
            Team.name.asc(),
            Person.name.asc(),
            Contract.total_value.desc(),
        )
        .with_entities(
            Person.id,
            Person.name,
            Person.position,
            Asset.id,
            Team.id,
            Team.name,
            TransactionAsset.id,
        )
        .all()
    )

    return [
        {
            "person_id": contract[0],
            "name": contract[1],
            "position": contract[2],
            "contract_id": contract[3],
            "team_id": contract[4],
            "team_name": contract[5],
            "transaction_asset": contract[6],
        }
        for contract in query
    ]


def get_teams():
    query = (
        Team.query.with_entities(
            Team.id,
            Team.name,
        )
        .filter(Team.active == True)
        .order_by(Team.name)
        .all()
    )

    return [tuple(team) for team in query]
