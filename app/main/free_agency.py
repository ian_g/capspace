from datetime import date, datetime
from flask import render_template, url_for, redirect, abort, current_app
from sqlalchemy import or_, select

from .. import db
from ..models import (
    Person,
    Contract,
    ContractYear,
    Asset,
    Team,
    Season,
    SkaterSeason,
    GoalieSeason,
)


def free_agent_page(rfa=True, ufa=True):
    data = get_free_agents(rfa=rfa, ufa=ufa)
    current_app.logger.info(f"/free_agent/ - Rendering free agents ({rfa=} {ufa=})")
    title = "Free Agents"
    if not ufa:
        title = "Restricted Free Agents"
    elif not rfa:
        title = "Unrestricted Free Agents"

    return render_template(
        "free_agent.html", title=title, css="free_agent.css", data=data
    )


def signings_page(from_date, to_date):
    data = {
        "signings": get_signings(0, from_date, to_date),
        "teams": get_teams(),
    }
    current_app.logger.info(f"Rendering signings (from={from_date} to={to_date})")
    return render_template(
        "signings.html", title="Contract Signings", css="signings.css", data=data
    )


def qualifying_offer(form):
    return render_template(
        "qualifying_offer.html", title="Qualifying Offer", css="qualify.css", form=form
    )


def get_teams():
    raw_data = (
        Team.query.with_entities(
            Team.id,
            Team.name,
        )
        .order_by(Team.name.asc())
        .all()
    )
    data = [tuple(team) for team in raw_data]
    return data


def get_signings(offset=0, from_date=None, to_date=None):
    query = (
        Contract.query.join(Person, Person.id == Contract.person_id)
        .join(Asset, Asset.id == Contract.id)
        .join(Team, Team.id == Asset.originating_team)
    )
    if from_date:
        from_date = datetime.strptime(from_date, current_app.config["DATE_FMT"]).date()
        query = query.filter(Contract.signing_date >= from_date)
    if to_date:
        to_date = datetime.strptime(to_date, current_app.config["DATE_FMT"]).date()
        query = query.filter(Contract.signing_date <= to_date)
    raw_data = (
        query.order_by(Contract.signing_date.desc(), Contract.id.desc())
        .offset(offset)
        .limit(current_app.config["RECORD_BATCH_SIZE"])
        .with_entities(
            Person.id,
            Person.name,
            Person.birthdate,
            Person.position,
            Team.abbreviation,
            Team.id,
            Team.logo,
            Contract.signing_date,
            Contract.thirty_five_plus,
            Contract.entry_level,
            Contract.years,
            Contract.total_value,
        )
        .all()
    )
    # age = cutoff.year - birth.year - ((cutoff.month, cutoff.day) < (birth.month, birth.day))
    today = date.today()
    data = [
        {
            "person_id": row[0],
            "person_name": row[1],
            "age": today.year
            - row[2].year
            - ((today.month, today.day) < (row[2].year, row[2].day)),
            "position": row[3],
            "team": row[4],
            "team_id": row[5],
            "team_logo": row[6],
            "signing_date": row[7].strftime(current_app.config["DATE_FMT"]),
            "thirty_five_plus": row[8],
            "entry_level": row[9],
            "years": row[10],
            "value": row[11],
            "url_id": f"{row[1].lower().replace(' ', '-')}-{row[0]}",
        }
        for row in raw_data
    ]
    return data


def get_free_agents(rfa=True, ufa=True, limit=False):
    statuses = []
    if rfa:
        statuses.append("RFA")
    if ufa:
        statuses.append("UFA")
    status_filter = or_(*[Person.current_status.like(f"%{val}%") for val in statuses])
    today = date.today()
    raw_data = (
        Person.query.filter(Person.active == True)
        .filter(status_filter)
        .with_entities(
            Person.id,
            Person.name,
            Person.birthdate,
            Person.position,
            Person.shoots,
            Person.current_status,
        )
    )
    if limit:
        raw_data = raw_data.limit(current_app.config["RECORD_BATCH_SIZE"])
    raw_data = raw_data.all()
    data = [
        {
            "id": person[0],
            "name": person[1],
            "status": person[5],
            "age": today.year
            - person[2].year
            - ((today.month, today.day) < (person[2].month, person[2].day)),
            "position": person[3],
            "handed": person[4],
            "last_team": query_last_team(person[0]),
            "last_aav": query_last_aav(person[0]),
            "url_id": f"{person[1].lower().replace(' ', '-')}-{person[0]}",
        }
        for person in raw_data
    ]
    data.sort(key=lambda x: (-x["last_aav"], x["name"]))
    return data


def get_pending_rfas(team_id=None):
    next_season_id = (
        Season.query.filter(
            Season.free_agency_opening
            > Season.query.get(current_app.config["CURRENT_SEASON"]).free_agency_opening
        )
        .order_by(Season.free_agency_opening.asc())
        .with_entities(Season.id)
        .scalar_subquery()
    )
    results = (
        Person.query.join(Contract, Contract.person_id == Person.id)
        .join(ContractYear, ContractYear.contract_id == Contract.id)
        .filter(Person.active == True)
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .filter(Contract.expiration_status == "RFA")
        .filter(
            Contract.id.not_in(
                ContractYear.query.filter(ContractYear.season_id == next_season_id)
                .with_entities(ContractYear.contract_id)
                .scalar_subquery()
            )
        )
        .with_entities(
            Person.id,
            Person.name,
        )
        .order_by(Person.name.asc())
    )
    if team_id:
        results = results.join(Asset, Asset.id == Contract.id).filter(
            Asset.current_team == team_id
        )
    return [tuple(result) for result in results.all()]


def calculate_qualifying_offer(person_id):
    contract_data = (
        ContractYear.query.join(Contract, Contract.id == ContractYear.contract_id)
        .join(Season, Season.id == ContractYear.season_id)
        .filter(Contract.person_id == person_id)
        .order_by(Season.free_agency_opening.desc())
        .with_entities(
            Contract.signing_date,
            ContractYear.nhl_salary,
            ContractYear.aav,
        )
        .first()
    )

    player = Person.query.get(person_id)
    if player.position == "G":
        stats = (
            GoalieSeason.query.join(Season, Season.id == GoalieSeason.season_id)
            .filter(GoalieSeason.person_id == person_id)
            .order_by(Season.free_agency_opening.desc())
            .limit(3)
            .with_entities(
                Season.name,
                GoalieSeason.games_played,
                GoalieSeason.wins,
                GoalieSeason.losses,
                GoalieSeason.shutouts,
                GoalieSeason.shots_against,
                GoalieSeason.goals_against,
            )
            .all()
        )
        stat_names = [
            "games_played",
            "wins",
            "losses",
            "shutouts",
            "shots_against",
            "goals_against",
        ]
    else:
        stats = (
            SkaterSeason.query.join(Season, Season.id == SkaterSeason.season_id)
            .filter(SkaterSeason.person_id == person_id)
            .order_by(Season.free_agency_opening.desc())
            .limit(3)
            .with_entities(
                Season.name,
                SkaterSeason.games_played,
                SkaterSeason.goals,
                SkaterSeason.assists,
                SkaterSeason.points,
                SkaterSeason.shots,
                SkaterSeason.hits,
            )
            .all()
        )
        stat_names = ["games_played", "goals", "assists", "points", "shots", "hits"]

    qualifying_salary = contract_data[1]
    if qualifying_salary <= 660000:
        qualifying_salary *= 1.1
    elif qualifying_salary < 1000000:
        qualifying_salary *= 1.05

    if qualifying_salary > 1.2 * contract_data[2] and contract_data[0] >= date(
        year=2020, month=7, day=10
    ):
        qualifying_salary = 1.2 * contract_data[2]

    one_way = False
    if len(stats) > 0 and stats[0][1] >= 60 and sum([val[1] for val in stats]) >= 180:
        one_way = True
    # No waivers in the last season. Not implemented yet

    data = {
        "name": player.name,
        "id": player.id,
        "url_id": f"{player.name.lower().replace(' ', '-')}-{player.id}",
        "last_salary": contract_data[1],
        "last_aav": contract_data[2],
        "contract_signing": contract_data[0],
        "qualifying_salary": int(qualifying_salary),
        "one_way": one_way,
        "stat_names": stat_names,
        "stats": [list(stat) for stat in stats],
    }
    return data


def get_qualifying_offer_details(person_id):
    data = calculate_qualifying_offer(person_id)
    return render_template("_qualifying_details.html", data=data)


def query_last_aav(person_id):
    contract_id = (
        Contract.query.join(ContractYear, Contract.id == ContractYear.contract_id)
        .filter(Contract.person_id == person_id)
        .order_by(Contract.signing_date.desc())
        .with_entities(
            Contract.id,
            ContractYear.aav,
        )
        .first()
    )
    if not contract_id:
        return 0
    return contract_id[1]


def query_last_team(person_id):
    data = {
        "name": None,
        "abbreviation": None,
        "logo": None,
        "id": None,
    }
    team_id = (
        Contract.query.join(Asset, Contract.id == Asset.id)
        .filter(Contract.person_id == person_id)
        .order_by(Contract.signing_date.desc())
        .with_entities(
            Contract.id,
            Asset.current_team,
        )
        .first()
    )
    if not team_id:
        return data
    team_data = (
        Team.query.filter(Team.id == team_id[1])
        .with_entities(
            Team.id,
            Team.abbreviation,
            Team.name,
            Team.logo,
        )
        .first()
    )
    if not team_data:
        return data
    data["id"] = team_data[0]
    data["abbreviation"] = team_data[1]
    data["name"] = team_data[2]
    data["logo"] = team_data[3]
    return data
