import sys
import random
import unittest
from datetime import datetime, date
from app import create_app, db
from app.models import Note

from app.admin import notes

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class NoteTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_get_note_data(self):
        NOTE_ID = 937
        self.assertFalse(notes.get_note_data(NOTE_ID))

        memo = Note(
            id=NOTE_ID,
            record_id="test",
            record_table=4,
            note="gobbledegook",
        )
        db.session.add(memo)
        db.session.commit()

        data = notes.get_note_data(NOTE_ID)
        self.assertEqual(data["record_id"], "test")
        self.assertEqual(data["record_table"], "4")
        self.assertEqual(data["note"], "gobbledegook")

    def test_get_notes(self):
        self.assertEqual(notes.get_notes(), [])
        memos = [
            Note(record_table="test", record_id=88, note="test one"),
            Note(record_table="test", record_id=99, note="test alpha"),
            Note(record_table="test", record_id=88, note="test first"),
            Note(record_table="test2", record_id=88, note="test primary"),
        ]
        for memo in memos:
            db.session.add(memo)
        db.session.commit()

        data = notes.get_notes("test", 88)
        self.assertEqual(len(data), 2)
        self.assertEqual(data[0]["record_table"], "test")
        self.assertEqual(data[1]["record_table"], "test")
        self.assertEqual(data[0]["record_id"], 88)
        self.assertEqual(data[1]["record_id"], 88)
        self.assertEqual(data[0]["note"], "test one")
        self.assertEqual(data[1]["note"], "test first")

        data = notes.get_notes("test")
        self.assertEqual(len(data), 3)
        self.assertEqual(data[0]["record_table"], "test")
        self.assertEqual(data[1]["record_table"], "test")
        self.assertEqual(data[2]["record_table"], "test")
        self.assertEqual(data[0]["record_id"], 88)
        self.assertEqual(data[1]["record_id"], 99)
        self.assertEqual(data[2]["record_id"], 88)
        self.assertEqual(data[0]["note"], "test one")
        self.assertEqual(data[1]["note"], "test alpha")
        self.assertEqual(data[2]["note"], "test first")

    def test_write_note(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = notes.WriteNote()
        note = Note(
            id=92,
            record_table="table",
            record_id=32,
            note="test content",
        )
        db.session.add(note)
        db.session.commit()

        form.note_id.data = note.id
        form.record_id.data = note.record_id
        form.record_table.data = note.record_table
        form.note.data = "new content"
        form.delete.data = False

        # Edit
        form.write_note()
        self.assertEqual(note.note, "new content")

        # Delete
        count = Note.query.count()
        self.assertEqual(count, 1)

        form = notes.WriteNote()
        form.note_id.data = note.id
        form.record_id.data = note.record_id
        form.record_table.data = note.record_table
        form.note.data = "new content"
        form.delete.data = True
        form.write_note()
        count = Note.query.count()
        self.assertEqual(count, 0)

        # Create new
        form = notes.WriteNote()
        form.record_id.data = "idk"
        form.record_table.data = "dik"
        form.note.data = "new note"
        form.delete.data = False
        form.write_note()
        note = Note.query.first()
        self.assertEqual(note.record_id, "idk")
        self.assertEqual(note.record_table, "dik")
        self.assertEqual(note.note, "new note")

        ctx.pop()


if __name__ == "__main__":
    unittest.main(verbosity=2)
