import re
import json
from datetime import date, datetime
from flask import render_template, url_for, redirect, abort, current_app, session
from sqlalchemy import or_, and_, column
from sqlalchemy.sql import func, select
from sqlalchemy.exc import MultipleResultsFound
from .. import db
from ..models import (
    Person,
    DraftPick,
    Team,
    Asset,
    Season,
    Contract,
    ContractYear,
    Transaction,
    TransactionAsset,
    RetainedSalary,
    Season,
    SigningRights,
    TeamSeason,
)
from . import person, team_season, season


def trade_proposal(form):
    if not form:
        abort(500, "Trade proposal form not found")
    current_app.logger.info(f"/trade_propodal - Rendering")
    return render_template(
        "trade_proposal.html",
        title="Trade Proposal",
        css="trade-proposal.css",
        form=form,
    )


def team_assets(team_id):
    return_data = [[], 200]

    # Draft: [id, f"{TEAM} - Round: {rd} - Position: {pos}"]
    picks = team_picks(team_id)[0]
    for pick in picks:
        details = f"{pick['team']} - Round: {pick['round']}"
        if "position" in picks:
            details += f" - Position: {pick['position']}"
        return_data[0].append([pick["id"], details])

    # Person: [id, f"{Name} - {#} years remaining - {UFA/RFA}"]
    contracts = team_contracts(team_id)[0]
    for contract in contracts:
        return_data[0].append(
            [
                contract["id"],
                f"{contract['name']} - {contract['years']} years remaining - {contract['expires_as']}",
            ]
        )
    current_app.logger.info(f"/api/team_assets/{team_id}")
    return return_data


def team_contracts(team_id):
    current_app.logger.debug(f"/api/team_contracts/{team_id}")
    raw_contracts = query_contracts(team_id) + query_signing_rights(team_id)
    contracts = [[], 200]

    if not check_team(team_id):
        return [f"Team {team_id} not found", 404]

    for contract in raw_contracts:
        years_remaining = query_contract_years(contract[2])
        retention = get_total_retention(contract[0])
        if years_remaining == 0 and contract[4]:
            continue
        contract_data = {
            "id": contract[0],
            "name": contract[1],
            "years": years_remaining,
            "expires_as": contract[3],
            "aav": contract[4] * (100 - retention) / 100,
        }
        contracts[0].append(contract_data)
    return contracts


def team_picks(team_id):
    current_app.logger.debug(f"/api/team_picks/{team_id}")
    raw_picks = query_team_picks(team_id)
    picks = [[], 200]

    if not check_team(team_id):
        return [f"Team {team_id} not found", 404]

    for pick in raw_picks:
        pick_data = {
            "id": pick[0],
            "team": pick[1],
            "year": pick[2].year,
            "round": pick[3],
            "position": pick[4],
        }
        picks[0].append(pick_data)
    return picks


def trade_page(trade_data, record_proposal=None, apply_to_site=None):
    if type(trade_data) == type({}):
        current_app.logger.debug(
            "trade_page - Proposal data provided. Querying for details"
        )
        # data = proposal_details(trade_data)
        data = get_form_details(trade_data)
        return render_template(
            "trade.html",
            title="Trade Proposal",
            css="trade.css",
            trade=data,
            proposal=True,
            save_proposal=record_proposal,
        )
    elif type(trade_data) == type("0") and trade_data.isdigit():
        current_app.logger.debug(
            "trade_page - Trade ID provided. Querying for trade information"
        )
        data = get_transaction_details(trade_data)
        page_title = data["type"]
        proposal = data["type"].lower() == "trade proposal"
        return render_template(
            "trade.html",
            title=page_title,
            css="trade.css",
            trade=data,
            proposal=proposal,
        )
    else:
        current_app.logger.debug(f"trade_page - Undefined input type - {trade_data}")
        abort(500)


def trades_page(from_date, to_date):
    trade_data, all_loaded = load_trades(0, from_date, to_date)
    teams = (
        Team.query.filter(Team.active == True)
        .order_by(Team.name)
        .with_entities(Team.abbreviation, Team.name)
        .all()
    )
    teams = [list(team) for team in teams]
    return render_template(
        "trades.html",
        title="Trades",
        css="trade.css",
        trades=trade_data,
        teams=teams,
        all_loaded=all_loaded,
    )


def load_trades(offset=0, from_date=None, to_date=None):
    trade_ids = query_trade_ids(offset, from_date, to_date)
    trade_data = [get_transaction_details(trade) for trade in trade_ids]
    all_loaded = True
    if len(trade_data) == (current_app.config["RECORD_BATCH_SIZE"] // 2):
        all_loaded = False
    return trade_data, all_loaded


def load_additional_trades(offset, from_date, to_date):
    trade_data, all_loaded = load_trades(offset, from_date, to_date)
    trades = [
        render_template("_trade_details.html", trade=details) for details in trade_data
    ]
    return ["".join(trades), all_loaded]


def get_form_details(trade_data):
    current_app.logger.debug(f"Getting details from form data")
    data = {
        "team_one": get_form_team(trade_data["team_one"]),
        "team_two": get_form_team(trade_data["team_two"]),
        "future_considerations_one": trade_data["future_considerations_one"],
        "future_considerations_two": trade_data["future_considerations_two"],
        "future_considerations": trade_data["future_considerations"],
        "picks_one": get_form_picks(trade_data["picks_one"]),
        "picks_two": get_form_picks(trade_data["picks_two"]),
        "contracts_one": get_form_contracts(trade_data["contracts_one"]),
        "contracts_two": get_form_contracts(trade_data["contracts_two"]),
        "date": date.today().strftime(current_app.config["DATE_FMT"]),
        "clause_warning": get_clause_warning(
            trade_data["contracts_one"] + trade_data["contracts_two"]
        ),
        "cap_warning": get_form_cap_warning(trade_data),
    }
    return data


def get_transaction_details(transaction_id):
    """
    {
        'team_one': '7',
        'team_two': '16',
        'picks_one': [
            {'id': '441', 'condition': 'idk'},
            {'id': '508'}
        ],
        'contracts_one': [],
        'picks_two': [
            {'id': '449'}
        ],
        'contracts_two': [
            {'id': '670', 'retention': '0'}
        ],
        'future_considerations_one': False,
        'future_considerations_two': False,
        'future_considerations': ''
    }
    """
    current_app.logger.debug(f"Querying details from transaction ID {transaction_id}")
    data = get_transaction(transaction_id)
    # Get transaction assets
    teams = get_transaction_teams(transaction_id, data["season_id"])
    data["team_one"] = teams[0]
    data["team_two"] = teams[1]
    data["future_considerations_one"] = (
        get_future_considerations(transaction_id, data["team_one"]["id"]) or False
    )
    data["future_considerations_two"] = (
        get_future_considerations(transaction_id, data["team_two"]["id"]) or False
    )
    if data["future_considerations_one"]:
        data["future_considerations"] = data["future_considerations_one"]
        data["future_considerations_one"] = True
    if data["future_considerations_two"]:
        data["future_considerations"] = data["future_considerations_two"]
        data["future_considerations_two"] = True
    data["picks_one"] = get_transaction_picks(transaction_id, data["team_one"]["id"])
    data["picks_two"] = get_transaction_picks(transaction_id, data["team_two"]["id"])
    data["contracts_one"] = get_transaction_contracts(
        transaction_id, data["team_one"]["id"]
    )
    data["contracts_two"] = get_transaction_contracts(
        transaction_id, data["team_two"]["id"]
    )
    if data["type"] != "Trade Proposal":
        data["clause_warning"] = None
        data["cap_warning"] = None
    else:
        data["clause_warning"] = get_transaction_clause_warning(transaction_id)
        data["cap_warning"] = get_transaction_cap_warning(transaction_id)
    return data


def save_proposal(data, user_id=None):
    current_app.logger.debug(f"Saving trade proposal to database (User ID: {user_id})")
    check_data(data)
    proposal_id = create_transaction(user_id=user_id)
    create_transaction_picks(
        proposal_id, data["picks_one"], data["team_one"], data["team_two"]
    )
    create_transaction_picks(
        proposal_id, data["picks_two"], data["team_two"], data["team_one"]
    )
    create_transaction_contracts(
        proposal_id, data["contracts_one"], data["team_one"], data["team_two"]
    )
    create_transaction_contracts(
        proposal_id, data["contracts_two"], data["team_two"], data["team_one"]
    )
    if data["future_considerations_one"]:
        create_future_considerations(
            proposal_id,
            data["future_considerations"],
            data["team_one"],
            data["team_two"],
        )
    if data["future_considerations_two"]:
        create_future_considerations(
            proposal_id,
            data["future_considerations"],
            data["team_two"],
            data["team_one"],
        )
    db.session.flush()
    db.session.commit()
    return proposal_id


def get_total_retention(contract_id):
    raw_ret = (
        RetainedSalary.query.filter(RetainedSalary.contract_id == contract_id)
        .with_entities(func.sum(RetainedSalary.retention))
        .first()
    )
    return raw_ret[0] or 0


def check_data(data):
    if "team_one" not in data:
        abort(500, "Missing team one. Please make sure two teams are selected to trade")
    if "team_two" not in data:
        abort(500, "Missing team two. Please make sure two teams are selected to trade")
    if "picks_one" not in data:
        abort(500, "Missing team one picks data")
    if "picks_two" not in data:
        abort(500, "Missing team two picks data")
    for pick in data["picks_one"]:
        if "id" not in pick:
            abort(500, f"Missing pick ID for team one. Pick {pick}")
    for pick in data["picks_two"]:
        if "id" not in pick:
            abort(500, f"Missing pick ID for team two. Pick {pick}")
    if "contracts_one" not in data:
        abort(500, "Missing team one contracts data")
    if "contracts_two" not in data:
        abort(500, "Missing team two contracts data")
    for contract in data["contracts_one"]:
        if "id" not in contract:
            abort(500, f"Missing contract ID for team one. Pick {contract}")
    for contract in data["contracts_two"]:
        if "id" not in contract:
            abort(500, f"Missing contract ID for team two. Pick {contract}")
    if "future_considerations_one" not in data:
        abort(500, "Future considerations not indicated for team one")
    if "future_considerations_two" not in data:
        abort(500, "Future considerations not indicated for team two")
    if "future_considerations" not in data:
        abort(500, "Future considerations details not provided")
    return


def create_transaction(**kwargs):
    if "date" not in kwargs:
        kwargs["date"] = date.today()
    if "season_id" not in kwargs:
        kwargs["season_id"] = current_app.config["CURRENT_SEASON"]
    if "user_id" in kwargs and kwargs["user_id"] is None:
        del kwargs["user_id"]
    if "type" not in kwargs:
        kwargs["type"] = "Trade Proposal"
    keys = set(kwargs.keys())
    valid_keys = {"date", "season_id", "type", "user_id"}
    if not keys.issubset(valid_keys):
        current_app.logger.error(f"trade.create_proposal - Unknown args\n\t{kwargs}")
        abort(
            500,
            "Unknown data found while creating transaction* record\n*Trades, Trade proposals, Player reassignments, etc...",
        )
    trade = Transaction(**kwargs)
    db.session.add(trade)
    db.session.commit()
    return trade.id


def create_transaction_picks(transaction, picks, former, new):
    for pick in picks:
        transaction_pick = TransactionAsset(
            transaction_id=transaction,
            asset_id=pick["id"],
            former_team=former,
            new_team=new,
        )
        if "condition" in pick:
            transaction_pick.condition = pick["condition"]
        db.session.add(transaction_pick)
    return


def create_transaction_contracts(transaction, contracts, former, new):
    for contract in contracts:
        transaction_contract = TransactionAsset(
            transaction_id=transaction,
            asset_id=contract["id"],
            former_team=former,
            new_team=new,
        )
        if "retention" in contract and contract["retention"] != 0:
            transaction_contract.condition = f"Retention: {contract['retention']}%"
        db.session.add(transaction_contract)
    return


def create_future_considerations(transaction, considerations, former, new):
    future_considerations = TransactionAsset(
        former_team=former,
        new_team=new,
        transaction_id=transaction,
        condition="Future Considerations",
    )
    if considerations:
        future_considerations.condition += f":\n{considerations}"
    db.session.add(future_considerations)
    return


def get_asset_data(transaction_id):
    raw_data = query_asset_data(transaction_id)
    return [
        {
            "id": asset[0],
            "former_team": asset[1],
            "new_team": asset[2],
            "current_team": asset[3],
            "active": asset[4],
            "condition": asset[5],
        }
        for asset in raw_data
    ]


def get_retained_assets(transaction_id, old_team):
    raw_data = query_retained_assets(transaction_id, old_team)
    return [
        {
            "id": asset[0],
            "retention": asset[1],
        }
        for asset in raw_data
    ]


def get_transaction(transaction_id):
    raw_transaction = query_transaction(transaction_id)
    return {
        "date": raw_transaction[0].strftime(current_app.config["DATE_FMT"]),
        "type": raw_transaction[1],
        "season_id": raw_transaction[2],
        "user_id": raw_transaction[3],
        "id": transaction_id,
    }


def get_transaction_teams(transaction_id, season_id=None):
    if not season_id:
        season_id = current_app.config["CURRENT_SEASON"]
    team_ids = query_transaction_team_ids(transaction_id)
    raw_teams = (
        Team.query.join(TeamSeason, Team.id == TeamSeason.team_id)
        .filter(Team.id.in_(team_ids))
        .filter(TeamSeason.season_id == season_id)
        .with_entities(
            Team.name,
            TeamSeason.logo,
            Team.id,
            Team.abbreviation,
        )
        .all()
    )
    return [
        {
            "name": team[0],
            "logo": team[1],
            "id": team[2],
            "abbreviation": team[3],
        }
        for team in raw_teams
    ] or [{}, {}]


def get_future_considerations(transaction_id, team_id):
    raw_data = query_future_considerations(transaction_id, team_id)
    if raw_data is None:
        return None
    return raw_data[0]


def get_transaction_picks(transaction_id, team_id):
    raw_data = query_transaction_picks(transaction_id, team_id)
    data = [
        {
            "id": pick[0],
            "originating_team": pick[1],
            "round": pick[2],
            "season": pick[3].year,
            "position": pick[4],
            "condition": pick[5],
        }
        for pick in raw_data
    ]
    return data


def get_transaction_contracts(transaction_id, team_id):
    raw_data = query_transaction_contracts(transaction_id, team_id)
    raw_data += query_transaction_rights(transaction_id, team_id)
    data = [
        {
            "id": contract[1],
            "retention": contract[5],
            "name": contract[2],
            "cap_hit": contract[3],
            "nhl_salary": contract[4],
            "type": contract[6],
            "url_id": f"{contract[2].lower().replace(' ', '-')}-{contract[1]}",
        }
        for contract in raw_data
    ]
    return data


def get_transaction_clause_warning(transaction_id):
    raw_data = query_transaction_clauses(transaction_id)
    if len(raw_data) == 0:
        return None
    warnings = []
    for item in raw_data:
        warning = create_warning(item)
        if warning:
            warnings.append(warning)
    return warnings


def get_form_team(team_id):
    raw_team = query_team(team_id)
    return {
        "name": raw_team[0],
        "logo": raw_team[1],
    }


def get_clause_warning(contracts):
    ids = [contract["id"] for contract in contracts]
    raw_data = query_clauses(ids)
    if len(raw_data) == 0:
        return None
    warnings = []
    for item in raw_data:
        warning = create_warning(item)
        if warning:
            warnings.append(warning)
    return warnings


def create_warning(item):
    clauses = []
    if item[0]:
        clauses.append("NMC")
    if item[1]:
        clauses.append("NTC")
    if clauses == []:
        return
    clauses = ", ".join(clauses)
    warning = f"{item[3]}: {clauses}"
    if item[2]:
        warning += f" ({item[2]})"
    return warning


def get_form_contracts(assets):
    """
    'contracts_two': [
        {'id': '670', 'retention': '0'}
    ],
    """
    return list(
        filter(
            lambda x: x is not None, [contract_data(contract) for contract in assets]
        )
    )


def contract_data(contract):
    raw_data = query_contract(contract["id"])
    if not raw_data:
        return None
    data = {
        "id": raw_data[4],
        "retention": f"Retained: {contract['retention']}%",
        "name": raw_data[1],
        "cap_hit": raw_data[2],
        "nhl_salary": raw_data[3],
        "type": raw_data[5],
    }
    if contract["retention"] == 0:
        del data["retention"]
    return data


def get_form_picks(assets):
    """
    'picks_one': [
        {'id': '441', 'condition': 'idk'},
        {'id': '508'}
    ],
    """
    return [pick_data(pick) for pick in assets]


def pick_data(pick):
    raw_data = query_pick(pick["id"])
    data = {
        "id": pick["id"],
        "originating_team": raw_data[0],
        "round": raw_data[1],
        "season": raw_data[2].year,
        "position": raw_data[3],
        "condition": None,
    }
    if "condition" in pick:
        data["condition"] = pick["condition"]
    return data


def check_team(team_id):
    count = Team.query.filter_by(id=team_id).count()
    if count == 1:
        return True
    return False


def check_contracts(team_id, contracts):
    contract_ids = set([int(contract["id"]) for contract in contracts])
    queried_contracts = query_team_contracts(team_id)
    queried_contracts = set([value[0] for value in queried_contracts])
    if contract_ids.issubset(queried_contracts):
        return []

    errors = []
    for contract in contract_ids:
        if contract not in queried_contracts:
            error_str = f"Contract {contract} not owned by team {team_id}"
            current_app.logger.error(error_str)
            errors.append(error_str)
    return errors


def check_picks(team_id, picks):
    pick_ids = set([int(pick["id"]) for pick in picks])
    queried_picks = query_team_picks(team_id)
    queried_picks = set([value[0] for value in queried_picks])
    if pick_ids.issubset(queried_picks):
        return []

    errors = []
    for pick in pick_ids:
        if pick not in queried_picks:
            error_str = f"Pick {pick} not owned by team {team_id}"
            current_app.logger.error(error_str)
            errors.append(error_str)
    return errors


def get_transaction_cap_warning(transaction_id):
    season_data = season.get_season_data(current_app.config["CURRENT_SEASON"])
    teams = get_transaction_teams(transaction_id)
    team_one = teams[0]
    team_two = teams[1]
    team_one_season = team_season.get_team_season(team_one["id"], None)
    team_two_season = team_season.get_team_season(team_two["id"], None)
    contracts_one = get_transaction_contracts(transaction_id, team_one["id"])
    contracts_two = get_transaction_contracts(transaction_id, team_two["id"])
    cap_contracts_one = sum([contract["cap_hit"] for contract in contracts_one])
    cap_contracts_two = sum([contract["cap_hit"] for contract in contracts_two])

    cap_one = team_one_season["cap_hit"] + cap_contracts_two - cap_contracts_one
    cap_two = team_two_season["cap_hit"] + cap_contracts_one - cap_contracts_two

    warnings = []
    if cap_one > season_data["salary_cap"]:
        warnings.append(f"{team_one['name']}: over the salary cap with this trade")
    elif cap_two > season_data["salary_cap"]:
        warnings.append(f"{team_two['name']}: over the salary cap with this trade")

    if cap_one < season_data["salary_floor"]:
        warnings.append(f"{team_one['name']}: under the cap floor with this trade")
    if cap_two < season_data["salary_floor"]:
        warnings.append(f"{team_two['name']}: under the cap floor with this trade")
    return warnings


def get_form_cap_warning(data):
    season_data = season.get_season_data(current_app.config["CURRENT_SEASON"])
    team_one = team_season.get_team_season(data["team_one"], None)
    team_two = team_season.get_team_season(data["team_two"], None)
    contracts_one = get_form_contracts(data["contracts_one"])
    contracts_two = get_form_contracts(data["contracts_two"])
    cap_contracts_one = sum([contract["cap_hit"] for contract in contracts_one])
    cap_contracts_two = sum([contract["cap_hit"] for contract in contracts_two])

    cap_one = team_one["cap_hit"] + cap_contracts_two - cap_contracts_one
    cap_two = team_two["cap_hit"] + cap_contracts_one - cap_contracts_two

    warnings = []
    if cap_one > season_data["salary_cap"]:
        warnings.append(
            f"{get_form_team(data['team_one'])['name']}: over the salary cap with this trade"
        )
    elif cap_two > season_data["salary_cap"]:
        warnings.append(
            f"{get_form_team(data['team_two'])['name']}: over the salary cap with this trade"
        )

    if cap_one < season_data["salary_floor"]:
        warnings.append(
            f"{get_form_team(data['team_one'])['name']}: under the cap floor with this trade"
        )
    if cap_two < season_data["salary_floor"]:
        warnings.append(
            f"{get_form_team(data['team_two'])['name']}: under the cap floor with this trade"
        )
    return warnings


def check_previously_retained(team_id, contract_ids):
    return (
        RetainedSalary.query.filter(RetainedSalary.contract_id.in_(contract_ids))
        .filter(RetainedSalary.retained_by == team_id)
        .with_entities(RetainedSalary.contract_id)
        .order_by(RetainedSalary.contract_id.asc())
        .all()
    )


def query_trade_ids(offset=0, from_date=None, to_date=None):
    query = (
        Transaction.query.filter(Transaction.type == "Trade")
        .with_entities(Transaction.id)
        .order_by(
            Transaction.date.desc(),
            Transaction.id.desc(),
        )
    )
    if from_date:
        from_date = datetime.strptime(from_date, current_app.config["DATE_FMT"]).date()
        query = query.filter(Transaction.date >= from_date)
    if to_date:
        to_date = datetime.strptime(to_date, current_app.config["DATE_FMT"]).date()
        query = query.filter(Transaction.date <= to_date)
    return [
        id[0]
        for id in query.offset(offset)
        .limit(current_app.config["RECORD_BATCH_SIZE"] // 2)
        .all()
    ]


def get_transaction_record(transaction_id):
    return Transaction.query.filter(Transaction.id == transaction_id).first()


def query_retained_assets(transaction_id, old_team):
    return (
        TransactionAsset.query.filter(TransactionAsset.transaction_id == transaction_id)
        .filter(TransactionAsset.former_team == old_team)
        .filter(TransactionAsset.condition.like("Retention: %"))
        .with_entities(
            TransactionAsset.asset_id,
            TransactionAsset.condition,
        )
        .all()
    )


def query_asset_data(transaction_id):
    return (
        TransactionAsset.query.join(Asset, Asset.id == TransactionAsset.asset_id)
        .filter(TransactionAsset.transaction_id == transaction_id)
        .order_by(TransactionAsset.asset_id)
        .with_entities(
            TransactionAsset.asset_id,
            TransactionAsset.former_team,
            TransactionAsset.new_team,
            Asset.current_team,
            Asset.active,
            TransactionAsset.condition,
        )
        .all()
    )


def query_transaction(transaction_id):
    return (
        Transaction.query.filter(Transaction.id == transaction_id)
        .with_entities(
            Transaction.date,
            Transaction.type,
            Transaction.season_id,
            Transaction.user_id,
        )
        .first()
    )


def query_transaction_picks(transaction_id, team_id):
    """
    SELECT
        transaction_asset.asset_id,
        team.abbreviation,
        draft_pick.round,
        season.draft_date,
        draft_pick.position,
        transaction_asset.condition
    FROM transaction_asset
        JOIN asset ON transaction_asset.asset_id=asset.id
        JOIN team ON asset.originating_team=team.id
        JOIN draft_pick ON draft_pick.id=transaction_asset.asset_id
    WHERE transaction_id=4
        AND new_team=16;
    """
    return (
        TransactionAsset.query.join(Asset, Asset.id == TransactionAsset.asset_id)
        .join(DraftPick, DraftPick.id == TransactionAsset.asset_id)
        .join(Team, Team.id == Asset.originating_team)
        .join(Season, Season.id == DraftPick.season_id)
        .filter(TransactionAsset.transaction_id == transaction_id)
        .filter(TransactionAsset.former_team == team_id)
        .with_entities(
            TransactionAsset.asset_id,
            Team.abbreviation,
            DraftPick.round,
            Season.draft_date,
            DraftPick.position,
            TransactionAsset.condition,
        )
        .all()
    )


def query_transaction_contracts(transaction_id, team_id):
    season_id = (
        Season.query.filter(
            Season.free_agency_opening
            < Transaction.query.filter(Transaction.id == transaction_id)
            .with_entities(Transaction.date)
            .limit(1)
            .scalar_subquery()
        )
        .order_by(Season.free_agency_opening.desc())
        .with_entities(Season.id)
        .limit(1)
        .scalar_subquery()
    )
    next_season = (
        Season.query.filter(
            Season.free_agency_opening
            > Season.query.filter(Season.id == season_id)
            .with_entities(Season.free_agency_opening)
            .limit(1)
            .scalar_subquery()
        )
        .order_by(Season.free_agency_opening.asc())
        .with_entities(Season.id)
        .limit(1)
        .scalar_subquery()
    )
    return (
        TransactionAsset.query.join(Asset, Asset.id == TransactionAsset.asset_id)
        .join(Contract, Contract.id == TransactionAsset.asset_id)
        .join(ContractYear, ContractYear.contract_id == Contract.id)
        .join(Person, Person.id == Contract.person_id)
        .filter(TransactionAsset.transaction_id == transaction_id)
        .filter(TransactionAsset.former_team == team_id)
        .filter(
            or_(
                ContractYear.season_id == season_id,
                and_(
                    ContractYear.season_id == next_season,
                    Contract.signing_season_id == season_id,
                ),
            )
        )
        .group_by(Person.id)
        .order_by(ContractYear.id.asc())
        .with_entities(
            TransactionAsset.asset_id,
            Person.id,
            Person.name,
            ContractYear.cap_hit,
            ContractYear.nhl_salary,
            TransactionAsset.condition,
            Asset.type,
        )
        .all()
    )


def query_transaction_rights(transaction_id, team_id):
    return (
        TransactionAsset.query.join(Asset, Asset.id == TransactionAsset.asset_id)
        .join(SigningRights, SigningRights.id == Asset.id)
        .join(Person, Person.id == SigningRights.person_id)
        .filter(TransactionAsset.transaction_id == transaction_id)
        .filter(TransactionAsset.former_team == team_id)
        .with_entities(
            TransactionAsset.asset_id,
            Person.id,
            Person.name,
            0,
            0,
            TransactionAsset.condition,
            Asset.type,
        )
        .all()
    )


def query_pick(asset_id):
    return (
        DraftPick.query.join(Asset, Asset.id == DraftPick.id)
        .join(Season, Season.id == DraftPick.season_id)
        .join(Team, Asset.originating_team == Team.id)
        .filter(DraftPick.id == asset_id)
        .with_entities(
            Team.abbreviation,
            DraftPick.round,
            Season.draft_date,
            DraftPick.position,
        )
        .first()
    )


def query_contract(asset_id):
    if Contract.query.filter(Contract.id == asset_id).count() == 0:
        return query_rights_details(asset_id)
    return (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .join(Person, Person.id == Contract.person_id)
        .join(ContractYear, ContractYear.contract_id == Contract.id)
        .filter(Asset.active == True)
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .filter(Contract.id == asset_id)
        .with_entities(
            Contract.id,
            Person.name,
            ContractYear.cap_hit,
            ContractYear.nhl_salary,
            Person.id,
            Asset.type,
        )
        .first()
    )


def query_rights_details(asset_id):
    return (
        SigningRights.query.join(Asset, Asset.id == SigningRights.id)
        .join(Person, Person.id == SigningRights.person_id)
        .filter(Asset.active == True)
        .filter(SigningRights.id == asset_id)
        .with_entities(
            SigningRights.id,
            Person.name,
            0,
            0,
            Person.id,
            Asset.type,
        )
        .first()
    )


def query_clauses(ids):
    return (
        ContractYear.query.join(Contract, ContractYear.contract_id == Contract.id)
        .join(Person, Contract.person_id == Person.id)
        .filter(ContractYear.contract_id.in_(ids))
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .with_entities(
            ContractYear.nmc,
            ContractYear.ntc,
            ContractYear.clause_limits,
            Person.name,
        )
        .all()
    )


def query_transaction_clauses(transaction_id):
    raw_ids = (
        db.session.query(TransactionAsset.asset_id)
        .filter(TransactionAsset.transaction_id == transaction_id)
        .subquery()
    )
    return query_clauses(select(raw_ids))
    # ContractYear.query\
    #    .filter(ContractYear.season_id==current_app.config["CURRENT_SEASON"])\
    #    .filter(ContractYear.contract_id.in_(raw_ids))\
    #    .with_entities(
    #        ContractYear.


def query_transaction_team_ids(transaction_id):
    raw_ids = (
        TransactionAsset.query.filter(TransactionAsset.transaction_id == transaction_id)
        .with_entities(
            TransactionAsset.former_team,
            TransactionAsset.new_team,
        )
        .distinct()
        .all()
    )
    return set([team for result in raw_ids for team in result])


def query_transaction_teams(transaction_id, season_id=None):
    if not season_id:
        season_id = current_app.config["SEASON_ID"]
    team_ids = query_transaction_team_ids(transaction_id)
    return (
        Team.query.filter(Team.id.in_(team_ids))
        .with_entities(
            Team.name,
            Team.logo,
            Team.id,
            Team.abbreviation,
        )
        .all()
    )


def query_future_considerations(transaction_id, team_id):
    return (
        TransactionAsset.query.filter(TransactionAsset.former_team == team_id)
        .filter(TransactionAsset.transaction_id == transaction_id)
        .filter(TransactionAsset.asset_id == None)
        .with_entities(TransactionAsset.condition)
        .first()
    )


def query_team(team_id):
    return (
        Team.query.filter(Team.id == team_id)
        .with_entities(Team.name, Team.logo)
        .first()
    )


def query_contract_count_by_asset(asset_ids):
    return (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .filter(Asset.active == True)
        .filter(Contract.id.in_(asset_ids))
        .count()
    )


def query_contract_count_by_team(team_id):
    return (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .count()
    )


def query_team_contracts(team_id):
    return (
        Asset.query.filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .filter(Asset.type.in_(["Contract", "Signing Rights"]))
        .with_entities(Asset.id)
        .all()
    )


def get_team_contract_count(team_id):
    return (
        Asset.query.filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .filter(Asset.type == "Contract")
        .count()
    )


def query_team_assets(team_id):
    return (
        Asset.query.filter(Asset.current_team == team_id)
        .filter(Asset.active == True)
        .order_by(Asset.id.asc())
        .with_entities(Asset.id)
        .all()
    )


def query_team_picks(team_id):
    return (
        DraftPick.query.join(Asset, Asset.id == DraftPick.id)
        .join(Team, Team.id == Asset.originating_team)
        .join(Season, DraftPick.season_id == Season.id)
        .filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .order_by(Season.draft_date.asc())
        .order_by(DraftPick.round.asc())
        .order_by(DraftPick.position.asc())
        .with_entities(
            Asset.id,
            Team.abbreviation,
            Season.draft_date,
            DraftPick.round,
            DraftPick.position,
        )
        .all()
    )


def query_contracts(team_id):
    return (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .join(Person, Person.id == Contract.person_id)
        .join(ContractYear, ContractYear.contract_id == Contract.id)
        .filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .order_by(Person.name.asc())
        .with_entities(
            Asset.id,
            Person.name,
            Contract.id,
            Contract.expiration_status,
            ContractYear.aav,
        )
        .all()
    )


def query_signing_rights(team_id):
    return (
        SigningRights.query.join(Asset, Asset.id == SigningRights.id)
        .join(Person, Person.id == SigningRights.person_id)
        .filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .order_by(Person.name.asc())
        .with_entities(
            Asset.id,
            Person.name,
            SigningRights.id,
            column("RFA"),
            0,
        )
        .all()
    )


def query_contract_years(contract_id):
    return (
        ContractYear.query.filter(ContractYear.contract_id == contract_id)
        .filter(ContractYear.season_id >= current_app.config["CURRENT_SEASON"])
        .count()
    )
