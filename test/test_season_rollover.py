import sys
import json
import random
import pathlib
import unittest
from unittest.mock import patch
from datetime import date, timedelta
from app import create_app, db
from app.models import (
    Configuration,
    Season,
    Team,
    TeamSeason,
    DraftPick,
    Asset,
    Person,
    Contract,
    ContractYear,
    Penalty,
    PenaltyYear,
    RetainedSalary,
    SkaterSeason,
    GoalieSeason,
    SigningRights,
    Note,
)

from app.admin import season_rollover as rollover

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


def sample_goalie_stats():
    cwd = pathlib.Path(__file__).resolve().parent
    file_name = cwd / "sample_goalie_stats.json"
    with open(file_name) as file:
        return json.loads(file.read())


def sample_skater_stats():
    cwd = pathlib.Path(__file__).resolve().parent
    file_name = cwd / "sample_skater_stats.json"
    with open(file_name) as file:
        return json.loads(file.read())


class SeasonRolloverTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_create_new_draft(self):
        SEASON = 8888
        teams = [
            Team(name="Test One", active=True),
            Team(name="Test Two", active=True),
            Team(name="Test 333", active=False),
            Team(name="Test For", active=True),
        ]
        for record in teams:
            db.session.add(record)
        db.session.commit()

        self.assertEqual(Asset.query.count(), 0)
        self.assertEqual(DraftPick.query.count(), 0)
        rollover.create_new_draft(SEASON)
        db.session.commit()
        self.assertEqual(Asset.query.count(), 15)
        self.assertEqual(DraftPick.query.count(), 15)
        self.assertEqual(DraftPick.query.filter(DraftPick.round == 1).count(), 3)
        self.assertEqual(DraftPick.query.filter(DraftPick.round == 2).count(), 3)
        self.assertEqual(DraftPick.query.filter(DraftPick.round == 3).count(), 3)
        self.assertEqual(DraftPick.query.filter(DraftPick.round == 4).count(), 3)
        self.assertEqual(DraftPick.query.filter(DraftPick.round == 5).count(), 3)
        self.assertEqual(
            Asset.query.filter(Asset.current_team == teams[0].id).count(), 5
        )
        self.assertEqual(
            Asset.query.filter(Asset.current_team == teams[1].id).count(), 5
        )
        self.assertEqual(
            Asset.query.filter(Asset.current_team == teams[3].id).count(), 5
        )
        self.assertEqual(
            Asset.query.filter(Asset.current_team == teams[2].id).count(), 0
        )

    def test_create_new_season(self):
        season = Season(
            name="1900-1901",
            salary_cap=100000,
            salary_floor=1000,
            minimum_salary=10,
            max_buriable_hit=15,
            bonus_overage_limit=1000,
            draft_date=date(year=1901, month=6, day=28),
            free_agency_opening=date(year=1900, month=7, day=1),
            playoffs_start=date(year=1901, month=4, day=15),
            regular_season_start=date(year=1900, month=10, day=1),
            regular_season_end=date(year=1901, month=4, day=10),
            max_elc_salary=20,
            max_elc_minor_salary=5,
            max_elc_performance_bonuses=5,
        )
        db.session.add(season)
        db.session.commit()

        season_id = rollover.create_new_season()
        db.session.commit()
        self.assertEqual(Season.query.count(), 2)
        new_season = Season.query.get(season_id)
        self.assertEqual(new_season.name, "1901-1902")
        self.assertEqual(season.salary_cap, new_season.salary_cap)
        self.assertEqual(season.salary_floor, new_season.salary_floor)
        self.assertEqual(season.minimum_salary, new_season.minimum_salary)
        self.assertEqual(season.max_buriable_hit, new_season.max_buriable_hit)
        self.assertEqual(season.bonus_overage_limit, new_season.bonus_overage_limit)
        self.assertEqual(season.max_elc_salary, new_season.max_elc_salary)
        self.assertEqual(season.max_elc_minor_salary, new_season.max_elc_minor_salary)
        self.assertEqual(
            season.max_elc_performance_bonuses, new_season.max_elc_performance_bonuses
        )
        self.assertEqual(season.draft_date.year + 1, new_season.draft_date.year)
        self.assertEqual(season.draft_date.month, new_season.draft_date.month)
        self.assertEqual(season.draft_date.day, new_season.draft_date.day)
        self.assertEqual(
            season.free_agency_opening.year + 1, new_season.free_agency_opening.year
        )
        self.assertEqual(
            season.free_agency_opening.month, new_season.free_agency_opening.month
        )
        self.assertEqual(
            season.free_agency_opening.day, new_season.free_agency_opening.day
        )
        self.assertEqual(season.playoffs_start.year + 1, new_season.playoffs_start.year)
        self.assertEqual(season.playoffs_start.month, new_season.playoffs_start.month)
        self.assertEqual(season.playoffs_start.day, new_season.playoffs_start.day)
        self.assertEqual(
            season.regular_season_start.year + 1, new_season.regular_season_start.year
        )
        self.assertEqual(
            season.regular_season_start.month, new_season.regular_season_start.month
        )
        self.assertEqual(
            season.regular_season_start.day, new_season.regular_season_start.day
        )
        self.assertEqual(
            season.regular_season_end.year + 1, new_season.regular_season_end.year
        )
        self.assertEqual(
            season.regular_season_end.month, new_season.regular_season_end.month
        )
        self.assertEqual(
            season.regular_season_end.day, new_season.regular_season_end.day
        )

    def test_activate_draft_picks(self):
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"],
                draft_date=date.today() - timedelta(days=30),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 5,
                draft_date=date.today() + timedelta(days=330),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 6,
                draft_date=date.today() - timedelta(days=330),
            ),
        ]
        assets = [
            Asset(type="Draft Pick", active=False),
            Asset(type="Draft Pick", active=False),
            Asset(type="Draft Pick", active=False),
            Asset(type="Draft Pick", active=False),
            Asset(type="Draft Pick", active=False),
            Asset(type="Draft Pick", active=False),
            Asset(type="Draft Pick", active=False),
            Asset(type="Draft Pick", active=False),
            Asset(type="Draft Pick", active=False),
            Asset(type="Draft Pick", active=False),
            Asset(type="Draft Pick", active=False),
            Asset(type="Draft Pick", active=False),
        ]
        for record in seasons + assets:
            db.session.add(record)
        db.session.flush()
        picks = [
            DraftPick(id=assets[0].id, season_id=seasons[0].id),
            DraftPick(id=assets[1].id, season_id=seasons[0].id),
            DraftPick(id=assets[2].id, season_id=seasons[0].id),
            DraftPick(id=assets[3].id, season_id=seasons[0].id),
            DraftPick(id=assets[4].id, season_id=seasons[1].id),
            DraftPick(id=assets[5].id, season_id=seasons[1].id),
            DraftPick(id=assets[6].id, season_id=seasons[1].id),
            DraftPick(id=assets[7].id, season_id=seasons[1].id),
            DraftPick(id=assets[8].id, season_id=seasons[2].id),
            DraftPick(id=assets[9].id, season_id=seasons[2].id),
            DraftPick(id=assets[10].id, season_id=seasons[2].id),
            DraftPick(id=assets[11].id, season_id=seasons[2].id),
        ]
        for record in picks:
            db.session.add(record)
        db.session.commit

        activated = rollover.activate_draft_picks()
        db.session.commit()
        self.assertEqual(activated, 4)

        self.assertFalse(assets[0].active)
        self.assertFalse(assets[1].active)
        self.assertFalse(assets[2].active)
        self.assertFalse(assets[3].active)

        self.assertFalse(assets[8].active)
        self.assertFalse(assets[9].active)
        self.assertFalse(assets[10].active)
        self.assertFalse(assets[11].active)

        self.assertTrue(assets[4].active)
        self.assertTrue(assets[5].active)
        self.assertTrue(assets[6].active)
        self.assertTrue(assets[7].active)

    def test_deactivate_expiring(self):
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"] - 1,
                name="Pre-test",
                draft_date=date(year=2010, month=1, day=1),
                free_agency_opening=date.today().replace(year=2010),
                regular_season_start=date.today().replace(year=2010),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"],
                name="Test",
                draft_date=date(year=2015, month=1, day=1),
                free_agency_opening=date.today(),
                regular_season_start=date.today(),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                name="Post-test",
                draft_date=date(year=2020, month=1, day=1),
                free_agency_opening=date.today().replace(year=2020),
                regular_season_start=date.today().replace(year=2020),
            ),
        ]
        people = [
            Person(
                id=88,
                name="Test UFA",
                current_status="Drafted,SOIR,Under Contract",
                birthdate=date(year=1992, month=11, day=1),
            ),
            Person(
                id=4,
                name="Test RFA",
                current_status="Drafted,IR,Under Contract",
                birthdate=date(year=1994, month=11, day=1),
            ),
            Person(
                id=24,
                name="Test Extended",
                current_status="Drafted,Under Contract,UFA",
                birthdate=date(year=1993, month=11, day=1),
            ),
            Person(
                id=55555,
                name="Test Other",
                current_status="UFA",
                birthdate=date(year=1995, month=11, day=1),
            ),
            Person(
                id=71,
                name="Test Unchanged",
                current_status="Under Contract,RFA",
                birthdate=date(year=1996, month=11, day=1),
            ),
        ]
        assets = [
            Asset(
                id=1, active=True, current_team=1, originating_team=1, type="Contract"
            ),
            Asset(
                id=2, active=True, current_team=1, originating_team=1, type="Contract"
            ),
            Asset(
                id=3, active=True, current_team=1, originating_team=1, type="Contract"
            ),
            Asset(
                id=4, active=True, current_team=1, originating_team=1, type="Contract"
            ),
            Asset(
                id=5, active=False, current_team=1, originating_team=1, type="Contract"
            ),
            Asset(
                id=6, active=True, current_team=1, originating_team=1, type="Contract"
            ),
        ]
        contracts = [
            Contract(
                id=assets[0].id,
                person_id=people[0].id,
                expiration_status="UFA",
                extension=False,
            ),
            Contract(
                id=assets[1].id,
                person_id=people[1].id,
                expiration_status="RFA",
                extension=False,
            ),
            Contract(
                id=assets[2].id,
                person_id=people[2].id,
                expiration_status="UFA",
                extension=False,
            ),
            Contract(
                id=assets[3].id,
                person_id=people[2].id,
                expiration_status="UFA",
                extension=True,
            ),
            Contract(
                id=assets[4].id,
                person_id=people[3].id,
                expiration_status="UFA",
                extension=False,
            ),
            Contract(
                id=assets[5].id,
                person_id=people[4].id,
                expiration_status="RFA",
                extension=False,
            ),
        ]
        contract_years = [
            ContractYear(
                contract_id=contracts[0].id,
                season_id=self.app.config["CURRENT_SEASON"] - 1,
                aav=1200000,
                nhl_salary=1000000,
                minors_salary=90000,
            ),
            ContractYear(
                contract_id=contracts[0].id,
                season_id=self.app.config["CURRENT_SEASON"],
                aav=1200000,
                nhl_salary=1000000,
                minors_salary=90000,
            ),
            ContractYear(
                contract_id=contracts[1].id,
                season_id=self.app.config["CURRENT_SEASON"] - 1,
                aav=1200000,
                nhl_salary=1000000,
                minors_salary=90000,
            ),
            ContractYear(
                contract_id=contracts[1].id,
                season_id=self.app.config["CURRENT_SEASON"],
                aav=1200000,
                nhl_salary=1000000,
                minors_salary=90000,
            ),
            ContractYear(
                contract_id=contracts[2].id,
                season_id=self.app.config["CURRENT_SEASON"] - 1,
                aav=1200000,
                nhl_salary=1000000,
                minors_salary=90000,
            ),
            ContractYear(
                contract_id=contracts[2].id,
                season_id=self.app.config["CURRENT_SEASON"],
                aav=1200000,
                nhl_salary=1000000,
                minors_salary=90000,
            ),
            ContractYear(
                contract_id=contracts[3].id,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
                aav=1200000,
                nhl_salary=1000000,
                minors_salary=90000,
            ),
            ContractYear(
                contract_id=contracts[4].id,
                season_id=self.app.config["CURRENT_SEASON"] - 1,
                aav=1200000,
                nhl_salary=1000000,
                minors_salary=90000,
            ),
            ContractYear(
                contract_id=contracts[5].id,
                season_id=self.app.config["CURRENT_SEASON"] - 1,
                aav=1200000,
                nhl_salary=1000000,
                minors_salary=90000,
            ),
            ContractYear(
                contract_id=contracts[5].id,
                season_id=self.app.config["CURRENT_SEASON"],
                aav=1200000,
                nhl_salary=1000000,
                minors_salary=90000,
            ),
            ContractYear(
                contract_id=contracts[5].id,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
                aav=1200000,
                nhl_salary=1000000,
                minors_salary=90000,
            ),
        ]
        for record in seasons + assets + people + contracts + contract_years:
            db.session.add(record)
        db.session.commit()
        deactivated = rollover.deactivate_expiring(seasons[-1].id)
        self.assertEqual(deactivated, 3)
        db.session.commit()

        self.assertEqual(people[0].name, "Test UFA")
        self.assertTrue("Drafted" in people[0].current_status)
        self.assertTrue("SOIR" in people[0].current_status)
        self.assertTrue("UFA" in people[0].current_status)
        self.assertTrue("Under Contract" not in people[0].current_status)

        self.assertEqual(people[1].name, "Test RFA")
        self.assertTrue("Drafted" in people[1].current_status)
        self.assertTrue("IR" in people[1].current_status)
        self.assertTrue("RFA" in people[1].current_status)
        self.assertTrue("Under Contract" not in people[1].current_status)

        self.assertEqual(people[2].name, "Test Extended")
        self.assertTrue("Drafted" in people[2].current_status)
        self.assertTrue("RFA" not in people[2].current_status)
        self.assertTrue("UFA" not in people[2].current_status)
        self.assertTrue("Under Contract" in people[2].current_status)

        self.assertEqual(people[3].name, "Test Other")
        self.assertEqual(people[3].current_status, "UFA")

        self.assertEqual(people[4].name, "Test Unchanged")
        self.assertEqual(people[4].current_status, "Under Contract")

        self.assertFalse(assets[0].active)
        self.assertFalse(assets[1].active)
        self.assertFalse(assets[2].active)
        self.assertTrue(assets[3].active)
        self.assertFalse(assets[4].active)
        self.assertTrue(assets[5].active)

    @patch(
        "app.admin.update_person.get_person_stats", new=lambda x: sample_skater_stats()
    )
    def test_update_season_stats_skater(self):
        # Create three people (inactive, skater, and goalie. Real ones)
        #    Ryan Reaves: 8471817
        #    Patrick Marleau: 8466139
        records = [
            Person(
                id=1, name="Ryan Reaves", nhl_id=8471817, position="RW", active=True
            ),
            Person(
                id=2, name="Patrick Marleau", nhl_id=8466139, position="C", active=False
            ),
            Season(id=self.app.config["CURRENT_SEASON"], name="2019-2020"),
            Team(id=28, abbreviation="SJS"),
            Team(id=30, abbreviation="MIN"),
            SkaterSeason(
                id=1,
                team_id=None,
                person_id=1,
                season_id=self.app.config["CURRENT_SEASON"],
                games_played=10,
                goals=5,
            ),
            SkaterSeason(
                id=8,
                team_id=None,
                person_id=1,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
                games_played=20,
                goals=4,
            ),
            SkaterSeason(
                id=4,
                team_id=5,
                person_id=1,
                season_id=self.app.config["CURRENT_SEASON"],
                games_played=30,
                goals=3,
            ),
            SkaterSeason(
                id=9,
                team_id=28,
                person_id=2,
                season_id=self.app.config["CURRENT_SEASON"] - 1,
                games_played=56,
                goals=4,
            ),
            SkaterSeason(
                id=7,
                team_id=28,
                person_id=2,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
                games_played=66,
                goals=11,
            ),
        ]
        for record in records:
            db.session.add(record)
        db.session.commit()
        rollover.update_season_stats()
        inactive_one = SkaterSeason.query.get(9)
        inactive_two = SkaterSeason.query.get(7)
        self.assertEqual(inactive_one.person_id, records[1].id)
        self.assertEqual(inactive_two.person_id, records[1].id)
        self.assertEqual(inactive_one.team_id, 28)
        self.assertEqual(inactive_two.team_id, 28)
        self.assertEqual(inactive_one.games_played, 56)
        self.assertEqual(inactive_two.games_played, 66)
        self.assertEqual(inactive_one.goals, 4)
        self.assertEqual(inactive_two.goals, 11)
        self.assertEqual(inactive_one.assists, 0)
        self.assertEqual(inactive_two.assists, 0)

        # Verify team_ids not blank
        self.assertEqual(
            SkaterSeason.query.filter(
                SkaterSeason.season_id == self.app.config["CURRENT_SEASON"]
            )
            .filter(SkaterSeason.team_id == None)
            .count(),
            0,
        )

        # Verify active peoples' other records remain
        old_skater = SkaterSeason.query.get(8)
        self.assertEqual(old_skater.person_id, records[0].id)
        self.assertEqual(old_skater.team_id, None)
        self.assertEqual(old_skater.games_played, 20)
        self.assertEqual(old_skater.goals, 4)
        self.assertNotEqual(old_skater.season_id, self.app.config["CURRENT_SEASON"])

        # Verify stats
        skater_stats = (
            SkaterSeason.query.filter(SkaterSeason.person_id == records[0].id)
            .filter(SkaterSeason.season_id == self.app.config["CURRENT_SEASON"])
            .order_by(SkaterSeason.team_id.asc())
            .all()
        )
        self.assertEqual(len(skater_stats), 2)
        stats = skater_stats.pop()
        self.assertEqual(stats.team_id, 30)
        self.assertEqual(stats.games_played, 12)
        self.assertEqual(stats.goals, 0)
        self.assertEqual(stats.assists, 5)
        self.assertEqual(stats.shots, 18)
        self.assertEqual(stats.points, 5)
        self.assertEqual(stats.penalty_minutes, 6)
        stats = skater_stats.pop()
        self.assertEqual(stats.team_id, 28)
        self.assertEqual(stats.games_played, 2)
        self.assertEqual(stats.goals, 0)
        self.assertEqual(stats.assists, 0)
        self.assertEqual(stats.shots, 4)
        self.assertEqual(stats.points, 0)
        self.assertEqual(stats.penalty_minutes, 0)

    @patch(
        "app.admin.update_person.get_person_stats", new=lambda x: sample_goalie_stats()
    )
    def test_update_season_stats_goalie(self):
        records = [
            Person(
                id=3, name="James Reimer", nhl_id=8473503, position="G", active=True
            ),
            Season(id=self.app.config["CURRENT_SEASON"], name="2019-2020"),
            Team(id=54, abbreviation="VGK"),
            GoalieSeason(
                team_id=None,
                person_id=3,
                season_id=self.app.config["CURRENT_SEASON"],
                games_played=22,
                goals_against=59,
            ),
            GoalieSeason(
                id=8,
                team_id=None,
                person_id=3,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
                games_played=48,
                goals_against=129,
            ),
        ]
        for record in records:
            db.session.add(record)
        db.session.commit()
        rollover.update_season_stats()

        # Verify team_ids not blank
        self.assertEqual(
            GoalieSeason.query.filter(
                GoalieSeason.season_id == self.app.config["CURRENT_SEASON"]
            )
            .filter(GoalieSeason.team_id == None)
            .count(),
            0,
        )

        # Verify active peoples' other records remain
        old_goalie = GoalieSeason.query.get(8)
        self.assertEqual(old_goalie.person_id, records[0].id)
        self.assertEqual(old_goalie.team_id, None)
        self.assertEqual(old_goalie.games_played, 48)
        self.assertEqual(old_goalie.goals_against, 129)
        self.assertNotEqual(old_goalie.season_id, self.app.config["CURRENT_SEASON"])

        # Verify stats
        goalie_stats = (
            GoalieSeason.query.filter(GoalieSeason.person_id == records[0].id)
            .filter(GoalieSeason.season_id == self.app.config["CURRENT_SEASON"])
            .all()
        )
        self.assertEqual(len(goalie_stats), 1)
        goalie_stats = goalie_stats.pop()
        self.assertEqual(goalie_stats.games_played, 9)
        self.assertEqual(goalie_stats.wins, 7)
        self.assertEqual(goalie_stats.losses, 1)
        self.assertEqual(goalie_stats.shots_against, 262)
        self.assertEqual(goalie_stats.starts, 9)

    def test_create_signing_rights(self):
        records = [
            Person(id=99, name="Test Human", position="G"),
            Season(
                id=self.app.config["CURRENT_SEASON"],
                name="Test Season",
                regular_season_start=date.today(),
                free_agency_opening=date.today(),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] - 1,
                name="Previous Test Season",
                regular_season_start=date.today() - timedelta(days=14),
                free_agency_opening=date.today() - timedelta(days=14),
            ),
            Asset(
                id=23, active=True, current_team=28, originating_team=3, type="Contract"
            ),
            Contract(id=23, signing_date=date.today(), person_id=99),
            ContractYear(
                contract_id=23,
                season_id=self.app.config["CURRENT_SEASON"],
                nhl_salary=1000000,
                minors_salary=90000,
                aav=1200000,
            ),
            GoalieSeason(
                person_id=99,
                season_id=39,
                team_id=28,
                games_played=7,
                wins=4,
                losses=3,
                shutouts=0,
                shots_against=392,
                goals_against=30,
            ),
            GoalieSeason(
                person_id=99,
                season_id=39,
                team_id=3,
                games_played=5,
                wins=2,
                losses=3,
                shutouts=1,
                shots_against=392,
                goals_against=30,
            ),
        ]
        for record in records:
            db.session.add(record)
        db.session.commit()

        self.assertEqual(Asset.query.count(), 1)
        self.assertEqual(SigningRights.query.count(), 0)
        rollover.create_signing_rights(records[0])
        db.session.commit()
        self.assertEqual(Asset.query.count(), 2)
        self.assertEqual(SigningRights.query.count(), 1)
        asset = Asset.query.filter(Asset.type == "Signing Rights").first()
        self.assertEqual(asset.active, True)
        self.assertEqual(asset.current_team, 28)
        rights = SigningRights.query.get(asset.id)
        self.assertIsNotNone(rights)
        self.assertEqual(rights.person_id, 99)
        self.assertEqual(rights.expiration_date, date.today().replace(month=7, day=15))
        self.assertEqual(rights.qualifying_offer, True)
        self.assertEqual(rights.qualifying_salary, 1000000)
        self.assertEqual(rights.qualifying_minors, 90000)

    def test_slide_contract(self):
        person = Person(
            name="William Eklund",
            birthdate=date(year=2002, month=10, day=12),
            birthplace="Stockholm, SWE",
            nationality="SWE",
            position="C",
            height=71,
            weight=181,
            elc_signing_age=18,
            waivers_signing_age=18,
            current_status="Drafted,Under Contract",
            active=True,
            non_roster=True,
            rookie=True,
            number=72,
            ep_id=394719,
        )
        db.session.add(person)
        db.session.flush()

        asset = Asset(type="Contract", active=True)
        season = Season(
            name="2022-2023",
            salary_cap=82500000,
            salary_floor=61000000,
            minimum_salary=750000,
            max_buriable_hit=1125000,
            bonus_overage_limit=6112500,
            draft_date=date(year=2023, month=6, day=23),
            free_agency_opening=date(year=2022, month=7, day=13),
            playoffs_start=date(year=2023, month=4, day=1),
            regular_season_start=date(year=2022, month=10, day=11),
            regular_season_end=date(year=2023, month=4, day=13),
        )
        next_season = Season(
            name="2023-2024",
            salary_cap=83500000,
            salary_floor=62000000,
            minimum_salary=775000,
            max_buriable_hit=1125000,
            bonus_overage_limit=6112500,
            draft_date=date(year=2024, month=6, day=23),
            free_agency_opening=date(year=2023, month=7, day=13),
            playoffs_start=date(year=2024, month=4, day=1),
            regular_season_start=date(year=2023, month=10, day=11),
            regular_season_end=date(year=2024, month=4, day=13),
        )
        other_seasons = [
            Season(
                name="2021-2022",
                minimum_salary=775000,
                free_agency_opening=date(year=2021, month=7, day=1),
            ),
            Season(
                name="2024-2025",
                minimum_salary=775000,
                free_agency_opening=date(year=2024, month=7, day=1),
            ),
            Season(
                name="2025-2026",
                minimum_salary=775000,
                free_agency_opening=date(year=2025, month=7, day=1),
            ),
        ]
        for record in other_seasons + [season, next_season, asset]:
            db.session.add(record)
        db.session.flush()

        contract = Contract(
            id=asset.id,
            person_id=person.id,
            signing_date=date(year=2021, month=8, day=14),
            total_value=5325000,
            expiration_status="RFA",
            entry_level=True,
            years=3,
            non_roster=True,
        )
        contract_years = [
            ContractYear(
                contract_id=asset.id,
                season_id=other_seasons[0].id,
                nhl_salary=0,
                minors_salary=80000,
                performance_bonuses=0,
                earned_performance_bonuses=0,
                signing_bonus=92500,
                cap_hit=925000,
                aav=1775000,
                two_way=True,
                entry_level_slide=True,
            ),
            ContractYear(
                contract_id=asset.id,
                season_id=season.id,
                nhl_salary=750000,
                minors_salary=80000,
                performance_bonuses=850000,
                earned_performance_bonuses=0,
                signing_bonus=175000,
                cap_hit=894166,
                aav=1744166,
                two_way=True,
                entry_level_slide=False,
            ),
            ContractYear(
                contract_id=asset.id,
                season_id=next_season.id,
                nhl_salary=832500,
                minors_salary=80000,
                performance_bonuses=850000,
                earned_performance_bonuses=0,
                signing_bonus=92500,
                cap_hit=894166,
                aav=1744166,
                two_way=True,
                entry_level_slide=False,
            ),
            ContractYear(
                contract_id=asset.id,
                season_id=other_seasons[1].id,
                nhl_salary=832500,
                minors_salary=80000,
                performance_bonuses=850000,
                earned_performance_bonuses=0,
                signing_bonus=0,
                cap_hit=894166,
                aav=1744166,
                two_way=True,
                entry_level_slide=False,
            ),
        ]
        for year in [contract] + contract_years:
            db.session.add(year)
        db.session.flush()
        db.session.commit()

        rollover.slide_contract(contract.id)
        db.session.commit()

        years = (
            ContractYear.query.filter(ContractYear.contract_id == contract.id)
            .order_by(ContractYear.id)
            .all()
        )
        self.assertEqual(len(years), 5)
        self.assertEqual(years[0].aav, 1775000)
        self.assertEqual(years[1].aav, 1744166)
        self.assertEqual(
            years[2].aav, 1694166
        )  # not 1713333 because messed with NHL salary
        self.assertEqual(
            years[3].aav, 1694166
        )  # and signing bonuses to also test minimum
        self.assertEqual(years[4].aav, 1694166)  # salary bump
        self.assertEqual(years[0].cap_hit, 925000)
        self.assertEqual(years[1].cap_hit, 894166)
        self.assertEqual(years[2].cap_hit, 844166)  # not 863333 for same reason as AAV
        self.assertEqual(years[3].cap_hit, 844166)
        self.assertEqual(years[4].cap_hit, 844166)
        self.assertEqual(years[0].performance_bonuses, 0)
        self.assertEqual(years[1].performance_bonuses, 0)
        self.assertEqual(years[2].performance_bonuses, 850000)
        self.assertEqual(years[3].performance_bonuses, 850000)
        self.assertEqual(years[4].performance_bonuses, 850000)
        self.assertEqual(years[0].signing_bonus, 92500)
        self.assertEqual(years[1].signing_bonus, 175000)
        self.assertEqual(years[2].signing_bonus, 92500)
        self.assertEqual(years[3].signing_bonus, 0)
        self.assertEqual(years[4].signing_bonus, 0)
        self.assertEqual(years[0].nhl_salary, 0)
        self.assertEqual(years[1].nhl_salary, 0)
        self.assertEqual(years[2].nhl_salary, 775000)
        self.assertEqual(years[3].nhl_salary, 832500)
        self.assertEqual(years[4].nhl_salary, 832500)
        note = Note.query.first()
        self.assertEqual(
            note.note,
            "2023-2024: NHL salary increased from 750000 to 775000 (league minimum)",
        )

    def test_get_slide_candidates(self):
        people = [
            Person(  # Two slide eligible (one slide)
                id=652,
                name="William Eklund",
                birthdate=date(year=2002, month=10, day=12),
                birthplace="Stockholm, SWE",
                nationality="SWE",
                position="C",
                height=71,
                weight=181,
                elc_signing_age=18,
                waivers_signing_age=18,
                current_status="Drafted,Under Contract",
                active=True,
                non_roster=True,
                rookie=True,
                number=72,
                ep_id=394719,
            ),
            Person(  # One slide eligible (19yo)
                id=646,
                name="Xavier Bourgault",
                birthdate=date(year=2002, month=10, day=22),
                birthplace="L'Islet, QC, CAN",
                nationality="CAN",
                position="C",
                height=72,
                weight=172,
                elc_signing_age=19,
                waivers_signing_age=20,
                current_status="Drafted,Under Contract",
                active=True,
                non_roster=True,
                rookie=True,
                number=54,
                ep_id=341629,
            ),
            Person(  # No slide eligible (20yo)
                id=763,
                name="Calle Clang",
                birthdate=date(year=2002, month=5, day=20),
                birthplace="Olofstrom, SWE",
                nationality="SWE",
                position="G",
                height=74,
                weight=194,
                elc_signing_age=20,
                waivers_signing_age=20,
                current_status="Drafted,Under Contract",
                active=True,
                non_roster=True,
                rookie=True,
                ep_id=387511,
            ),
            Person(  # Two slide eligibile (no slides so far)
                id=98,
                name="Denton Mateychuk",
                birthdate=date(year=2004, month=7, day=12),
                birthplace="Winnipeg, MB, CAN",
                nationality="CAN",
                position="D",
                height=71,
                weight=188,
                elc_signing_age=18,
                waivers_signing_age=18,
                current_status="Drafted,Under Contract",
                active=True,
                non_roster=True,
                rookie=True,
                ep_id=570931,
            ),
            Person(  # No slide eligible (already slid once)
                id=1753,
                name="Dawson Barteaux",
                birthdate=date(year=2000, month=1, day=12),
                birthplace="Foxwarren, MB, CAN",
                nationality="CAN",
                position="D",
                height=73,
                weight=195,
                elc_signing_age=19,
                waivers_signing_age=19,
                current_status="Drafted,Under Contract",
                active=True,
                non_roster=True,
                rookie=True,
                number=65,
                ep_id=285046,
            ),
        ]
        assets = [
            Asset(
                id=5288,
                active=True,
                type="Contract",
            ),
            Asset(
                id=6714,
                active=True,
                type="Contract",
            ),
            Asset(
                id=7034,
                active=True,
                type="Contract",
            ),
            Asset(
                id=8691,
                active=True,
                type="Contract",
            ),
            Asset(
                id=10780,
                active=True,
                type="Contract",
            ),
        ]
        contracts = [
            Contract(
                id=5288,
                person_id=763,
                signing_date=date(year=2022, month=5, day=5),
                entry_level=True,
            ),
            Contract(
                id=6714,
                person_id=1753,
                signing_date=date(year=2019, month=9, day=26),
                entry_level=True,
            ),
            Contract(
                id=7034,
                person_id=646,
                signing_date=date(year=2022, month=3, day=31),
                entry_level=True,
            ),
            Contract(
                id=8691,
                person_id=652,
                signing_date=date(year=2021, month=8, day=14),
                entry_level=True,
            ),
            Contract(
                id=10780,
                person_id=98,
                signing_date=date(year=2022, month=7, day=13),
                entry_level=True,
            ),
        ]
        contract_years = [
            ContractYear(
                id=208,
                contract_id=5288,
                season_id=self.app.config["CURRENT_SEASON"],
                entry_level_slide=False,
            ),
            ContractYear(
                id=3323,
                contract_id=6714,
                entry_level_slide=True,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            ContractYear(
                id=3326,
                contract_id=6714,
                entry_level_slide=False,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            ContractYear(
                id=4126,
                contract_id=7034,
                entry_level_slide=False,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            ContractYear(
                id=8586,
                contract_id=8691,
                entry_level_slide=True,
                season_id=self.app.config["CURRENT_SEASON"] - 1,
            ),
            ContractYear(
                id=8587,
                contract_id=8691,
                entry_level_slide=False,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            ContractYear(
                id=11924,
                contract_id=10780,
                entry_level_slide=False,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
        ]
        games_played = [
            SkaterSeason(person_id=652, team_id=28, season_id=17, games_played=9),
            SkaterSeason(
                person_id=652,
                team_id=28,
                season_id=self.app.config["CURRENT_SEASON"],
                games_played=9,
            ),
        ]
        season = Season(
            id=self.app.config["CURRENT_SEASON"],
            draft_date=date(year=2023, month=6, day=22),
        )

        for record in (
            people + assets + contracts + contract_years + games_played + [season]
        ):
            db.session.add(record)
        db.session.commit()
        candidates = rollover.get_slide_candidates()
        candidate_contract_ids = [data[1] for data in candidates]
        candidate_contract_ids.sort()
        self.assertEqual(candidate_contract_ids, [7034, 8691, 10780])

    def test_deactivate_penalties(self):
        seasons = [
            Season(id=self.app.config["CURRENT_SEASON"] - 1),
            Season(id=self.app.config["CURRENT_SEASON"]),
            Season(id=self.app.config["CURRENT_SEASON"] + 1),
        ]
        assets = [
            Asset(active=True, type="Penalty", id=55),
            Asset(active=True, type="Penalty", id=66),
            Asset(active=False, type="Penalty", id=77),
            Asset(active=False, type="Contract", id=11),
            Asset(active=False, type="Contract", id=22),
            Asset(active=False, type="Contract", id=33),
        ]
        penalties = [
            Penalty(
                id=assets[0].id,
                contract_id=assets[3].id,
                type="Fees",
                years=2,
                total_value=1000000,
            ),
            Penalty(
                id=assets[1].id,
                contract_id=assets[4].id,
                type="Cap Recapture",
                years=2,
                total_value=1000000,
            ),
            Penalty(
                id=assets[2].id,
                contract_id=assets[5].id,
                type="Termination Settlement",
                years=2,
                total_value=1000000,
            ),
        ]
        penalty_years = [
            PenaltyYear(
                penalty_id=penalties[0].id, season_id=seasons[0].id, cap_hit=500000
            ),
            PenaltyYear(
                penalty_id=penalties[0].id, season_id=seasons[1].id, cap_hit=500000
            ),
            PenaltyYear(
                penalty_id=penalties[1].id, season_id=seasons[1].id, cap_hit=500000
            ),
            PenaltyYear(
                penalty_id=penalties[1].id, season_id=seasons[2].id, cap_hit=500000
            ),
            PenaltyYear(
                penalty_id=penalties[2].id, season_id=seasons[0].id, cap_hit=500000
            ),
            PenaltyYear(
                penalty_id=penalties[2].id, season_id=seasons[0].id, cap_hit=500000
            ),
        ]
        for record in seasons + assets + penalties + penalty_years:
            db.session.add(record)
        db.session.commit()

        self.assertTrue(assets[0].active)
        self.assertTrue(assets[1].active)
        self.assertFalse(assets[2].active)
        self.assertEqual(assets[0].type, "Penalty")
        self.assertEqual(assets[1].type, "Penalty")
        self.assertEqual(assets[2].type, "Penalty")
        rollover.deactivate_penalties(seasons[-1].id)
        self.assertFalse(assets[0].active)
        self.assertTrue(assets[1].active)
        self.assertFalse(assets[2].active)

    def test_calculate_bonus_overage(self):
        teams = [
            Team(id=1, name="Test One"),
            Team(id=2, name="Test Two"),
        ]
        seasons = [
            Season(id=self.app.config["CURRENT_SEASON"], salary_cap=10000000),
            Season(id=3, salary_cap=10000000),
        ]
        team_seasons = [
            TeamSeason(
                team_id=teams[0].id,
                season_id=seasons[0].id,
                cap_hit=5000000,
                max_ltir_relief=1000000,
            ),
            TeamSeason(
                team_id=teams[0].id,
                season_id=seasons[1].id,
                cap_hit=5000000,
                max_ltir_relief=1000000,
            ),
            TeamSeason(
                team_id=teams[1].id,
                season_id=seasons[0].id,
                cap_hit=7000000,
                max_ltir_relief=1000000,
            ),
            TeamSeason(
                team_id=teams[1].id,
                season_id=seasons[1].id,
                cap_hit=5000000,
                max_ltir_relief=1000000,
            ),
        ]
        assets = [  # No overage, inactive, next season, overage
            Asset(
                id=4,
                current_team=teams[0].id,
                originating_team=teams[0].id,
                active=True,
            ),
            Asset(
                id=5,
                current_team=teams[0].id,
                originating_team=teams[0].id,
                active=False,
            ),
            Asset(
                id=6,
                current_team=teams[1].id,
                originating_team=teams[1].id,
                active=True,
            ),
            Asset(
                id=7,
                current_team=teams[1].id,
                originating_team=teams[1].id,
                active=True,
            ),
        ]
        contract_years = [
            ContractYear(
                contract_id=assets[0].id,
                season_id=seasons[0].id,
                earned_performance_bonuses=0,
            ),
            ContractYear(
                contract_id=assets[1].id,
                season_id=seasons[0].id,
                earned_performance_bonuses=0,
            ),
            ContractYear(
                contract_id=assets[2].id,
                season_id=seasons[1].id,
                earned_performance_bonuses=0,
            ),
            ContractYear(
                contract_id=assets[3].id,
                season_id=seasons[0].id,
                earned_performance_bonuses=5000000,
            ),
        ]
        for record in teams + seasons + team_seasons + assets + contract_years:
            db.session.add(record)
        db.session.commit()
        rollover.calculate_bonus_overage(seasons[1].id)
        ts_records = (
            TeamSeason.query.filter(TeamSeason.season_id == seasons[1].id)
            .order_by(TeamSeason.team_id)
            .all()
        )
        self.assertEqual(ts_records[0].team_id, teams[0].id)
        self.assertEqual(ts_records[0].bonus_overage, 0)
        self.assertEqual(ts_records[1].team_id, teams[1].id)
        self.assertEqual(ts_records[1].bonus_overage, 1000000)

    def test_set_performance_bonuses(self):
        year = ContractYear(
            contract_id=55,
            season_id=self.app.config["CURRENT_SEASON"],
            earned_performance_bonuses=0,
        )
        db.session.add(year)
        db.session.commit()
        self.assertEqual(ContractYear.query.count(), 1)
        output = rollover.set_performance_bonuses(55, 344)
        self.assertTrue("error" not in output)
        self.assertEqual(year.earned_performance_bonuses, 344)
        output = rollover.set_performance_bonuses(56, 344)
        self.assertTrue("error" in output)
        self.assertTrue(output["error"], "0 records would be updated. Expected 1")
        db.session.add(
            ContractYear(
                contract_id=55,
                season_id=self.app.config["CURRENT_SEASON"],
                earned_performance_bonuses=0,
            )
        )
        db.session.commit()
        output = rollover.set_performance_bonuses(55, 123)
        self.assertTrue("error" in output)
        self.assertTrue(output["error"], "2 records would be updated. Expected 1")

    def test_set_group_six(self):
        contract = Contract(
            id=55,
            expiration_status="RFA",
        )
        db.session.add(contract)
        db.session.commit()
        output = rollover.set_group_six(11)
        self.assertTrue("error" in output)
        self.assertEqual(output["error"], "Contract ID 11 does not exist")
        self.assertEqual(Note.query.count(), 0)
        output = rollover.set_group_six(55)
        self.assertTrue("error" not in output)
        self.assertEqual(contract.expiration_status, "UFA")
        self.assertEqual(Note.query.count(), 1)
        note = Note.query.first()
        self.assertEqual(note.record_id, 55)
        self.assertEqual(note.record_table, "contract")
        self.assertEqual(
            note.note,
            "RFA -> G6 UFA (Age >= 25; 3+ pro seasons on NHL deal; <80/<28 skater/goalie NHL games)",
        )

    def test_rollover(self):
        configs = [
            Configuration(
                key="CURRENT_SEASON", value=self.app.config["CURRENT_SEASON"]
            ),
            Configuration(
                key="DRAFT_ROUNDS",
                value=5,
            ),
        ]
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"] - 1,
                name="2021-2022",
                free_agency_opening=date(year=2022, month=12, day=1)
                - timedelta(days=365),
                regular_season_start=date(year=2022, month=12, day=1)
                - timedelta(days=364),
                regular_season_end=date(year=2022, month=12, day=1)
                - timedelta(days=363),
                playoffs_start=date(year=2022, month=12, day=1) - timedelta(days=362),
                draft_date=date(year=2022, month=12, day=1) - timedelta(days=361),
                minimum_salary=775000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"],
                name="2022-2023",
                free_agency_opening=date(year=2022, month=12, day=1),
                regular_season_start=date(year=2022, month=12, day=1)
                + timedelta(days=2),
                regular_season_end=date(year=2022, month=12, day=1) + timedelta(days=3),
                playoffs_start=date(year=2022, month=12, day=1) + timedelta(days=4),
                draft_date=date(year=2022, month=12, day=1) + timedelta(days=35),
                minimum_salary=775000,
                salary_cap=1000000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                name="2023-2024",
                free_agency_opening=date(year=2022, month=12, day=1)
                + timedelta(days=365),
                regular_season_start=date(year=2022, month=12, day=1)
                + timedelta(days=366),
                regular_season_end=date(year=2022, month=12, day=1)
                + timedelta(days=367),
                playoffs_start=date(year=2022, month=12, day=1) + timedelta(days=368),
                draft_date=date(year=2022, month=12, day=1) + timedelta(days=399),
                minimum_salary=775000,
                salary_cap=1000000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 2,
                name="2024-2025",
                free_agency_opening=date(year=2022, month=12, day=1)
                + timedelta(days=730),
                regular_season_start=date(year=2022, month=12, day=1)
                + timedelta(days=731),
                regular_season_end=date(year=2022, month=12, day=1)
                + timedelta(days=732),
                playoffs_start=date(year=2022, month=12, day=1) + timedelta(days=733),
                draft_date=date(year=2022, month=12, day=1) + timedelta(days=764),
                minimum_salary=775000,
                salary_cap=1000000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 3,
                name="2025-2026",
                free_agency_opening=date(year=2022, month=12, day=1)
                + timedelta(days=1095),
                regular_season_start=date(year=2022, month=12, day=1)
                + timedelta(days=1096),
                regular_season_end=date(year=2022, month=12, day=1)
                + timedelta(days=1097),
                playoffs_start=date(year=2022, month=12, day=1) + timedelta(days=1098),
                draft_date=date(year=2022, month=12, day=1) + timedelta(days=1129),
                minimum_salary=775000,
                salary_cap=1000000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 4,
                name="2026-2027",
                free_agency_opening=date(year=2022, month=12, day=1)
                + timedelta(days=1460),
                regular_season_start=date(year=2022, month=12, day=1)
                + timedelta(days=1461),
                regular_season_end=date(year=2022, month=12, day=1)
                + timedelta(days=1462),
                playoffs_start=date(year=2022, month=12, day=1) + timedelta(days=1463),
                draft_date=date(year=2022, month=12, day=1) + timedelta(days=1494),
                minimum_salary=775000,
                salary_cap=1000000,
            ),
        ]
        teams = [
            Team(
                id=1,
                active=True,
                name="Test One",
            ),
            Team(
                id=2,
                active=True,
                name="Test Two",
            ),
            Team(
                id=3,
                active=True,
                name="Test Three",
            ),
            Team(
                id=4,
                active=True,
                name="Test Four",
            ),
        ]
        team_seasons = [
            TeamSeason(
                team_id=1,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=1000000,
                bonus_overage=0,
                max_ltir_relief=500000,
            ),
            TeamSeason(
                team_id=2,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=1000000,
                bonus_overage=0,
                max_ltir_relief=250000,
            ),
            TeamSeason(
                team_id=3,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=1000000,
                bonus_overage=0,
                max_ltir_relief=750000,
            ),
            TeamSeason(
                team_id=4,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=1000000,
                bonus_overage=0,
                max_ltir_relief=750000,
            ),
            TeamSeason(
                team_id=1,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
                cap_hit=1000000,
                bonus_overage=0,
                max_ltir_relief=0,
            ),
            TeamSeason(
                team_id=2,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
                cap_hit=1000000,
                bonus_overage=0,
                max_ltir_relief=0,
            ),
            TeamSeason(
                team_id=3,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
                cap_hit=1000000,
                bonus_overage=123456,
                max_ltir_relief=0,
            ),
            TeamSeason(
                team_id=4,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
                cap_hit=1000000,
                bonus_overage=0,
                max_ltir_relief=0,
            ),
        ]
        stats = [
            SkaterSeason(
                id=123,
                person_id=555,
                team_id=22,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            SkaterSeason(
                id=234,
                person_id=555,
                team_id=44,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
        ]
        people = [
            Person(
                name="Timo Meier",
                id=555,
                position="RW",
                nhl_id=8478414,
            ),
            Person(
                name="Jacob Trouba",
                id=777,
                position="D",
                nhl_id=8476855,
            ),
            Person(
                name="Marek Alscher",
                id=888,
                position="D",
                elc_signing_age=18,
                nhl_id=8483687,
                birthdate=date(year=2004, month=4, day=7),
                current_status="Drafted,Under Contract",
                active=True,
            ),
            Person(
                name="Alex DeBrincat",
                id=999,
                position="LW",
                nhl_id=8479337,
                birthdate=date(year=1997, month=12, day=18),
                current_status="Drafted,Under Contract",
                active=True,
            ),
            Person(
                name="Dmitry Orlov",
                id=1111,
                position="D",
                nhl_id=8475200,
                birthdate=date(year=1991, month=7, day=23),
                current_status="Drafted,Under Contract",
                active=True,
            ),
            Person(
                name="Jett Woo",
                id=2222,
                position="C",
                nhl_id=8480808,
                birthdate=date(year=2000, month=7, day=27),
                current_status="Drafted,Under Contract",
                active=True,
            ),
        ]
        assets = [
            Asset(
                id=135,
                type="Contract",
                current_team=4,
                active=True,
            ),
            Asset(
                id=6378,
                type="Contract",
                current_team=4,
                active=True,
            ),
            Asset(
                id=9632,
                type="Contract",
                current_team=4,
                active=True,
            ),
            Asset(
                id=9405,
                type="Contract",
                current_team=4,
                active=True,
            ),
            Asset(
                id=24234,
                type="Contract",
                current_team=4,
                active=True,
            ),
            Asset(
                id=7277,
                type="Penalty",
                active=True,
            ),
            Asset(
                id=7278,
                type="Penalty",
                active=True,
            ),
            Asset(
                id=24,
                type="Draft Pick",
                current_team=1,
                active=False,
            ),
            Asset(
                id=46,
                type="Draft Pick",
                current_team=1,
                active=False,
            ),
            Asset(
                id=68,
                type="Draft Pick",
                current_team=1,
                active=False,
            ),
            Asset(
                id=82,
                type="Draft Pick",
                current_team=1,
                active=False,
            ),
            Asset(
                id=1256,
                type="Contract",
                active=True,
                current_team=1,
            ),
            Asset(
                id=2367,
                type="Contract",
                active=True,
                current_team=2,
            ),
            Asset(
                id=3478,
                type="Contract",
                active=True,
                current_team=3,
            ),
        ]
        draft_picks = [
            DraftPick(
                id=24,
                season_id=self.app.config["CURRENT_SEASON"] + 3,
                round=1,
            ),
            DraftPick(
                id=46,
                season_id=self.app.config["CURRENT_SEASON"] + 3,
                round=2,
            ),
            DraftPick(
                id=68,
                season_id=self.app.config["CURRENT_SEASON"] + 3,
                round=3,
            ),
            DraftPick(
                id=82,
                season_id=self.app.config["CURRENT_SEASON"] + 3,
                round=4,
            ),
        ]
        contracts = [
            Contract(
                id=135,
                person_id=888,
                entry_level=True,
                expiration_status="RFA",
                years=3,
                signing_season_id=self.app.config["CURRENT_SEASON"],
                total_value=2500000,
            ),
            Contract(
                id=6378,
                person_id=999,
                entry_level=False,
                expiration_status="RFA",
                years=1,
                total_value=9000000,
            ),
            Contract(
                id=9632,
                person_id=1111,
                entry_level=False,
                expiration_status="UFA",
                years=1,
                total_value=3300000,
            ),
            Contract(
                id=9405,
                person_id=2222,
                entry_level=False,
                expiration_status="RFA",
                years=3,
                total_value=2755000,
            ),
            Contract(
                id=24234,
                person_id=2222,
                entry_level=False,
                expiration_status="RFA",
                years=1,
                total_value=775000,
            ),
            Contract(
                id=1256,
                person_id=1256,
            ),
            Contract(
                id=2367,
                person_id=2367,
            ),
            Contract(
                id=3478,
                person_id=3478,
            ),
        ]
        contract_years = [
            ContractYear(
                contract_id=135,
                season_id=self.app.config["CURRENT_SEASON"],
                minors_salary=82500,
                performance_bonuses=80000,
                signing_bonus=95000,
                nhl_salary=775000,
                cap_hit=896666,
                aav=950000,
                two_way=True,
                earned_performance_bonuses=0,
            ),
            ContractYear(
                contract_id=135,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
                minors_salary=82500,
                performance_bonuses=80000,
                signing_bonus=95000,
                nhl_salary=775000,
                cap_hit=896666,
                aav=950000,
                two_way=True,
                earned_performance_bonuses=0,
            ),
            ContractYear(
                contract_id=135,
                season_id=self.app.config["CURRENT_SEASON"] + 2,
                minors_salary=82500,
                performance_bonuses=0,
                signing_bonus=95000,
                nhl_salary=855000,
                cap_hit=896666,
                aav=950000,
                two_way=True,
                earned_performance_bonuses=0,
            ),
            ContractYear(
                contract_id=6378,
                season_id=self.app.config["CURRENT_SEASON"],
                minors_salary=9000000,
                performance_bonuses=0,
                signing_bonus=0,
                nhl_salary=9000000,
                cap_hit=9000000,
                aav=9000000,
                two_way=False,
                earned_performance_bonuses=0,
            ),
            ContractYear(
                contract_id=9632,
                season_id=self.app.config["CURRENT_SEASON"],
                minors_salary=1300000,
                nhl_salary=1300000,
                signing_bonus=2000000,
                performance_bonuses=0,
                aav=3300000,
                cap_hit=3300000,
                earned_performance_bonuses=0,
            ),
            ContractYear(
                contract_id=9405,
                season_id=self.app.config["CURRENT_SEASON"],
                minors_salary=70000,
                nhl_salary=775000,
                signing_bonus=0,
                performance_bonuses=0,
                aav=860833,
                cap_hit=1044166,
                earned_performance_bonuses=0,
            ),
            ContractYear(
                contract_id=24234,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
                minors_salary=100000,
                nhl_salary=775000,
                signing_bonus=0,
                performance_bonuses=0,
                aav=775000,
                cap_hit=775000,
                earned_performance_bonuses=0,
            ),
            ContractYear(
                contract_id=1256,
                season_id=self.app.config["CURRENT_SEASON"],
                earned_performance_bonuses=250000,
            ),
            ContractYear(
                contract_id=2367,
                season_id=self.app.config["CURRENT_SEASON"],
                earned_performance_bonuses=500000,
            ),
            ContractYear(
                contract_id=3478,
                season_id=self.app.config["CURRENT_SEASON"],
                earned_performance_bonuses=750000,
            ),
        ]
        penalty_years = [
            PenaltyYear(
                penalty_id=7277,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            PenaltyYear(
                penalty_id=7278,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            PenaltyYear(
                penalty_id=7278,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
            ),
        ]
        for record in (
            configs
            + people
            + seasons
            + teams
            + stats
            + assets
            + contracts
            + contract_years
            + penalty_years
            + draft_picks
            + team_seasons
        ):
            db.session.add(record)
        db.session.commit()
        rollover.rollover(True)

        # Slid contract
        years = (
            ContractYear.query.filter(ContractYear.contract_id == 135)
            .order_by(ContractYear.season_id.asc())
            .all()
        )
        self.assertTrue(years[0].entry_level_slide)
        self.assertEqual(len(years), 4)
        self.assertEqual(years[0].cap_hit, 896666)
        self.assertEqual(years[0].aav, 950000)
        self.assertEqual(years[0].signing_bonus, 95000)
        self.assertEqual(years[0].performance_bonuses, 0)
        self.assertEqual(years[0].season_id, self.app.config["CURRENT_SEASON"] - 1)
        self.assertEqual(years[1].cap_hit, 865000)
        self.assertEqual(years[1].aav, 918333)
        self.assertEqual(years[1].signing_bonus, 95000)
        self.assertEqual(years[1].performance_bonuses, 80000)
        self.assertEqual(years[1].season_id, self.app.config["CURRENT_SEASON"] + 0)
        self.assertEqual(years[2].cap_hit, 865000)
        self.assertEqual(years[2].aav, 918333)
        self.assertEqual(years[2].signing_bonus, 95000)
        self.assertEqual(years[2].performance_bonuses, 80000)
        self.assertEqual(years[2].season_id, self.app.config["CURRENT_SEASON"] + 1)
        self.assertEqual(years[2].performance_bonuses, 80000)
        self.assertEqual(years[3].cap_hit, 865000)
        self.assertEqual(years[3].aav, 918333)
        self.assertEqual(years[3].season_id, self.app.config["CURRENT_SEASON"] + 2)
        self.assertEqual(years[3].signing_bonus, 0)
        self.assertEqual(years[3].performance_bonuses, 0)

        # Expiring contracts
        marek = Person.query.get(888)
        self.assertTrue("Under Contract" in marek.current_status)
        self.assertTrue("UFA" not in marek.current_status)
        self.assertTrue("RFA" not in marek.current_status)

        debrincat = Person.query.get(999)
        self.assertTrue("Under Contract" not in debrincat.current_status)
        self.assertTrue("RFA" in debrincat.current_status)
        asset = Asset.query.get(6378)
        self.assertFalse(asset.active)
        rights = Asset.query.filter(Asset.type == "Signing Rights").first()
        self.assertIsNotNone(rights)
        self.assertTrue(rights.active)
        rights_record = SigningRights.query.get(rights.id)
        self.assertIsNotNone(rights_record)
        self.assertEqual(rights_record.person_id, debrincat.id)
        self.assertTrue(rights_record.qualifying_offer)
        self.assertEqual(
            rights_record.expiration_date, date(year=2024, month=7, day=15)
        )
        self.assertEqual(rights_record.qualifying_salary, 9000000)
        self.assertEqual(rights_record.qualifying_minors, 9000000)
        orlov = Person.query.get(1111)
        self.assertTrue("Under Contract" not in orlov.current_status)
        self.assertTrue("UFA" in orlov.current_status)
        asset = Asset.query.get(9632)
        self.assertFalse(asset.active)
        self.assertIsNone(
            SigningRights.query.filter(SigningRights.person_id == 1111).first()
        )
        jett = Person.query.get(2222)
        self.assertTrue("Under Contract" in jett.current_status)
        self.assertTrue("RFA" not in jett.current_status)
        old_asset = Asset.query.get(9405)
        self.assertFalse(old_asset.active)
        new_asset = Asset.query.get(24234)
        self.assertTrue(new_asset.active)
        self.assertIsNone(
            SigningRights.query.filter(SigningRights.person_id == 2222).first()
        )

        # Expiring penalties
        asset = Asset.query.get(7277)
        self.assertFalse(asset.active)
        asset = Asset.query.get(7278)
        self.assertTrue(asset.active)

        # New season
        new_season = Season.query.filter(Season.name == "2027-2028").first()
        self.assertIsNotNone(new_season)
        self.assertEqual(
            DraftPick.query.filter(DraftPick.season_id == new_season.id).count(), 20
        )
        self.assertEqual(
            DraftPick.query.filter(DraftPick.season_id == new_season.id)
            .filter(DraftPick.round == 1)
            .count(),
            4,
        )

        # Re-activating a year of draft picks
        activated = (
            Asset.query.filter(Asset.active == True)
            .filter(Asset.type == "Draft Pick")
            .filter(Asset.id < 100)
            .order_by(Asset.id.asc())
            .all()
        )
        self.assertEqual(len(activated), 4)
        self.assertEqual(activated[0].id, 24)
        self.assertEqual(activated[1].id, 46)
        self.assertEqual(activated[2].id, 68)
        self.assertEqual(activated[3].id, 82)

        # Bonus overages
        next_one = (
            TeamSeason.query.filter(TeamSeason.team_id == 1)
            .filter(TeamSeason.season_id == self.app.config["CURRENT_SEASON"])
            .first()
        )
        self.assertIsNotNone(next_one)
        self.assertEqual(next_one.bonus_overage, 0)
        next_two = (
            TeamSeason.query.filter(TeamSeason.team_id == 2)
            .filter(TeamSeason.season_id == self.app.config["CURRENT_SEASON"])
            .first()
        )
        self.assertIsNotNone(next_two)
        self.assertEqual(next_two.bonus_overage, 250000)
        next_three = (
            TeamSeason.query.filter(TeamSeason.team_id == 3)
            .filter(TeamSeason.season_id == self.app.config["CURRENT_SEASON"])
            .first()
        )
        self.assertIsNotNone(next_three)
        self.assertEqual(next_three.bonus_overage, 123456)

        # Updated configs
        # self.app.config["CURRENT_SEASON"] was increased by 1 during rollover.
        #   tested multiple times in the above checks
        config = Configuration.query.filter(
            Configuration.key == "CURRENT_SEASON"
        ).first()
        self.assertEqual(int(config.value), 2)

    def test_deactivate_retention(self):
        assets = [
            Asset(
                id=1,
                type="Contract",
                active=False,
            ),
            Asset(
                id=2,
                type="Contract",
                active=True,
            ),
            Asset(
                id=3,
                type="Retained Salary",
                active=True,
            ),
            Asset(
                id=4,
                type="Retained Salary",
                active=True,
            ),
        ]
        retention = [
            RetainedSalary(
                id=3,
                contract_id=1,
                retention=50,
            ),
            RetainedSalary(
                id=4,
                contract_id=2,
                retention=50,
            ),
        ]
        for record in assets + retention:
            db.session.add(record)
        db.session.commit()
        self.assertTrue(assets[3].active)
        self.assertTrue(assets[2].active)
        rollover.deactivate_retention()
        self.assertTrue(assets[3].active)
        self.assertFalse(assets[2].active)

    def test_clear_emergency_callups(self):
        records = [
            Person(name="Test One", current_status="Emergency Callup,RFA"),
            Person(name="Test Two", current_status="Emergency Callup,UFA"),
            Person(name="Test Three", current_status="Under Contract,Emergency Callup"),
            Person(name="Test Four", current_status="Under Contract"),
            Person(name="Test Five", current_status="Retired"),
        ]
        for record in records:
            db.session.add(record)
        db.session.commit()
        emergency_callups = Person.query.filter(
            Person.current_status.like("%Emergency Callup%")
        ).count()
        self.assertEqual(emergency_callups, 3)
        rollover.clear_emergency_callups()
        emergency_callups = Person.query.filter(
            Person.current_status.like("%Emergency Callup%")
        ).count()
        self.assertEqual(emergency_callups, 0)
        self.assertEqual(records[0].current_status, "RFA")
        self.assertEqual(records[1].current_status, "UFA")
        self.assertEqual(records[2].current_status, "Under Contract")
        self.assertEqual(records[3].current_status, "Under Contract")
        self.assertEqual(records[4].current_status, "Retired")

    def test_clear_nonroster_statuses(self):
        records = [
            Asset(id=1, type="Contract", active=False),
            Asset(id=2, type="Contract", active=True),
            Asset(id=3, type="Contract", active=True),
            Asset(id=4, type="Contract", active=True),
            Asset(id=5, type="Signing Rights", active=True),
            Asset(id=6, type="Contract", active=True),
            Contract(id=1, non_roster=True, on_loan=True, in_juniors=True),  # idx=6
            Contract(id=2, non_roster=True, on_loan=True, in_juniors=True),
            Contract(id=3, non_roster=True, on_loan=False, in_juniors=True),
            Contract(id=4, non_roster=True, on_loan=True, in_juniors=False),
            Contract(id=5, non_roster=True, on_loan=False, in_juniors=False),
            Contract(id=6, non_roster=False, on_loan=False, in_juniors=False),
        ]
        for record in records:
            db.session.add(record)
        db.session.commit()
        rollover.clear_nonroster_statuses()
        self.assertTrue(records[6].non_roster)
        self.assertTrue(records[6].on_loan)
        self.assertTrue(records[6].in_juniors)
        self.assertTrue(records[10].non_roster)
        self.assertFalse(records[10].on_loan)
        self.assertFalse(records[10].in_juniors)

        self.assertFalse(records[7].non_roster)
        self.assertFalse(records[7].on_loan)
        self.assertFalse(records[7].in_juniors)
        self.assertFalse(records[8].non_roster)
        self.assertFalse(records[8].on_loan)
        self.assertFalse(records[8].in_juniors)
        self.assertFalse(records[9].non_roster)
        self.assertFalse(records[9].on_loan)
        self.assertFalse(records[9].in_juniors)
        self.assertFalse(records[11].non_roster)
        self.assertFalse(records[11].on_loan)
        self.assertFalse(records[11].in_juniors)

    def test_get_performance_bonuses(self):
        records = [
            Team(id=1, name="Test Team", abbreviation="TST"),
            Person(id=2, name="Test Person"),
            Asset(id=3, type="Contract", active=True, current_team=1),
            Contract(id=3, person_id=2, entry_level=True),
            ContractYear(
                contract_id=3,
                performance_bonuses=100,
                earned_performance_bonuses=10,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            Asset(id=4, type="Contract", active=False, current_team=1),
            Contract(id=4, person_id=2, entry_level=True),
            ContractYear(
                contract_id=4,
                performance_bonuses=100,
                earned_performance_bonuses=0,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            Asset(id=5, type="Contract", active=True, current_team=1),
            Contract(id=5, person_id=2, entry_level=True),
            ContractYear(
                contract_id=5,
                performance_bonuses=0,
                earned_performance_bonuses=0,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
        ]
        for record in records:
            db.session.add(record)
        db.session.commit()
        bonus = rollover.get_performance_bonuses()
        self.assertEqual(len(bonus), 1)
        self.assertEqual(bonus[0]["name"], "Test Person")
        self.assertEqual(bonus[0]["person_id"], 2)
        self.assertEqual(bonus[0]["contract_id"], 3)
        self.assertEqual(bonus[0]["performance_bonuses"], 100)
        self.assertEqual(bonus[0]["earned_bonuses"], 10)
        self.assertTrue(bonus[0]["entry_level"])
        self.assertEqual(bonus[0]["team"], "TST")

    def test_get_group_six_candidates(self):
        initial_records = [
            Season(
                id=self.app.config["CURRENT_SEASON"],
                free_agency_opening=date(year=2024, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                free_agency_opening=date(year=2025, month=7, day=1),
            ),
        ]
        for record in initial_records:
            db.session.add(record)
        db.session.commit()
        self.assertEqual(rollover.get_group_six_candidates(), [])

        personnell_records = [
            Season(
                id=self.app.config["CURRENT_SEASON"] - 1,
                free_agency_opening=date(year=2023, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] - 2,
                free_agency_opening=date(year=2022, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] - 3,
                free_agency_opening=date(year=2021, month=7, day=1),
            ),
            Person(
                id=10,
                position="LW",
                name="Test Ten",
                birthdate=date(year=1999, month=1, day=1),
            ),
            Person(
                id=11,
                position="D",
                name="Test Eleven",
                birthdate=date(year=1999, month=1, day=1),
            ),
            Person(
                id=12,
                position="C",
                name="Test Twelve",
                birthdate=date(year=1999, month=1, day=1),
            ),
            Asset(
                id=100, type="Contract", originating_team=1, current_team=1, active=True
            ),
            Asset(
                id=101, type="Contract", originating_team=1, current_team=1, active=True
            ),
            Asset(
                id=102, type="Contract", originating_team=1, current_team=1, active=True
            ),
            Asset(
                id=103, type="Contract", originating_team=1, current_team=1, active=True
            ),
            Contract(id=100, person_id=10, expiration_status="RFA"),  # Not expiring
            Contract(id=101, person_id=11, expiration_status="RFA"),  # Expiring
            Contract(id=102, person_id=11, expiration_status="UFA"),  # Extension
            Contract(
                id=103, person_id=12, expiration_status="RFA"
            ),  # Expiring, no extension
            ContractYear(
                contract_id=100, season_id=self.app.config["CURRENT_SEASON"] - 1
            ),
            ContractYear(contract_id=100, season_id=self.app.config["CURRENT_SEASON"]),
            ContractYear(
                contract_id=100, season_id=self.app.config["CURRENT_SEASON"] + 1
            ),
            ContractYear(
                contract_id=101, season_id=self.app.config["CURRENT_SEASON"] - 2
            ),
            ContractYear(
                contract_id=101, season_id=self.app.config["CURRENT_SEASON"] - 1
            ),
            ContractYear(contract_id=101, season_id=self.app.config["CURRENT_SEASON"]),
            ContractYear(
                contract_id=102, season_id=self.app.config["CURRENT_SEASON"] + 1
            ),
            ContractYear(
                contract_id=103, season_id=self.app.config["CURRENT_SEASON"] - 3
            ),
            ContractYear(
                contract_id=103, season_id=self.app.config["CURRENT_SEASON"] - 2
            ),
            ContractYear(
                contract_id=103, season_id=self.app.config["CURRENT_SEASON"] - 1
            ),
            ContractYear(contract_id=103, season_id=self.app.config["CURRENT_SEASON"]),
        ]
        for record in personnell_records:
            db.session.add(record)
        db.session.commit()
        candidates = rollover.get_group_six_candidates()
        self.assertEqual(len(candidates), 1)
        candidate = candidates.pop()
        self.assertEqual(candidate["contract_id"], 103)
        self.assertEqual(candidate["name"], "Test Twelve")
        self.assertEqual(candidate["nhl_games"], None)
        self.assertEqual(candidate["person_id"], 12)
        self.assertEqual(candidate["position"], "C")
        self.assertEqual(candidate["season_count"], 4)

        games_played = [
            SkaterSeason(
                person_id=10,
                season_id=self.app.config["CURRENT_SEASON"] - 2,
                games_played=1,
            ),
            SkaterSeason(
                person_id=10,
                season_id=self.app.config["CURRENT_SEASON"] - 1,
                games_played=10,
            ),
            SkaterSeason(
                person_id=10,
                season_id=self.app.config["CURRENT_SEASON"],
                games_played=50,
            ),
            SkaterSeason(
                person_id=11,
                season_id=self.app.config["CURRENT_SEASON"] - 2,
                games_played=10,
            ),
            SkaterSeason(
                person_id=11,
                season_id=self.app.config["CURRENT_SEASON"] - 1,
                games_played=15,
            ),
            SkaterSeason(
                person_id=11,
                season_id=self.app.config["CURRENT_SEASON"],
                games_played=19,
            ),
            SkaterSeason(
                person_id=12,
                season_id=self.app.config["CURRENT_SEASON"] - 3,
                games_played=20,
            ),
            SkaterSeason(
                person_id=12,
                season_id=self.app.config["CURRENT_SEASON"] - 2,
                games_played=10,
            ),
            SkaterSeason(
                person_id=12,
                season_id=self.app.config["CURRENT_SEASON"] - 1,
                games_played=10,
            ),
            SkaterSeason(
                person_id=12,
                season_id=self.app.config["CURRENT_SEASON"],
                games_played=20,
            ),
        ]
        for record in games_played:
            db.session.add(record)
        db.session.commit()

        candidates = rollover.get_group_six_candidates()
        self.assertEqual(len(candidates), 1)
        candidate = candidates.pop()
        self.assertEqual(candidate["contract_id"], 103)
        self.assertEqual(candidate["name"], "Test Twelve")
        self.assertEqual(candidate["nhl_games"], 60)
        self.assertEqual(candidate["person_id"], 12)
        self.assertEqual(candidate["position"], "C")
        self.assertEqual(candidate["season_count"], 4)
