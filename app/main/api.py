from flask import jsonify, request
from .. import db
from . import bp, team, person, buyout, trade, free_agency, transaction


@bp.route("/api/team/<team_id>")
def team_api(team_id):
    return jsonify(team.get_data(team_id))


@bp.route("/api/person/<person_id>")
def person_api(person_id):
    data = person.get_data(person_id)
    if "error" in data:
        return jsonify(data), 404
    return jsonify(data)


@bp.route("/api/buyout/<person_id>")
def buyout_api(person_id):
    season = request.args.get("after_season")
    data = buyout.get_data(person_id, season)
    if "error" in data:
        return jsonify(data), 404
    return jsonify(data)


@bp.route("/api/buyout_candidates/<team_id>")
def buyout_candidates(team_id):
    if not team_id:
        return jsonify({"error": "No team specified"}), 400
    return jsonify(buyout.get_buyout_candidates(team_id))


@bp.route("/api/buyout_seasons/")
@bp.route("/api/buyout_seasons/<person_id>")
def buyout_seasons(person_id=None):
    if not person_id:
        return jsonify([])
    return jsonify(buyout.get_buyout_form_seasons(person_id))


@bp.route("/api/pending_rfa/<team_id>")
def pending_rfas(team_id):
    if not team_id:
        return jsonify({"error": "No team specified"}), 400
    return jsonify(free_agency.get_pending_rfas(team_id))


@bp.route("/api/qualifying_offer/<person_id>")
@bp.route("/api/qualifying_offer/")
def qualifying_details(person_id=None):
    if not person_id:
        return "<h3 class='error'>No player specified</h3>"
    return free_agency.get_qualifying_offer_details(person_id)


@bp.route("/api/trade_assets/<team_id>")
def trade_assets(team_id):
    assets = trade.team_assets(team_id)
    return jsonify(assets[0]), assets[1]


@bp.route("/api/trade_picks/<team_id>")
def trade_picks(team_id):
    picks = trade.team_picks(team_id)
    return jsonify(picks[0]), picks[1]


@bp.route("/api/trade_contracts/<team_id>")
def trade_contracts(team_id):
    contracts = trade.team_contracts(team_id)
    return jsonify(contracts[0]), contracts[1]


@bp.route("/api/load_trades/<offset>")
def load_trades(offset):
    from_date = request.args.get("from")
    to_date = request.args.get("to")
    data = trade.load_additional_trades(offset, from_date, to_date)
    return jsonify(data)


@bp.route("/api/load_signings/<offset>")
def load_singings(offset):
    from_date = request.args.get("from")
    to_date = request.args.get("to")
    data = free_agency.get_signings(offset, from_date, to_date)
    return jsonify(data)


@bp.route("/api/load_transactions/<offset>")
def load_transactions(offset):
    from_date = request.args.get("from")
    to_date = request.args.get("to")
    data = transaction.transaction_rows(offset, from_date, to_date)
    return data
