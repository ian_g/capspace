import sys
import json
import urllib.request
from urllib.error import HTTPError, URLError
from time import sleep
from datetime import date
from app import create_app, db
from app.models import Person, Season, SkaterSeason, GoalieSeason


def main(args):
    app = create_app("default")
    app_context = app.app_context()
    app_context.push()

    if args:
        app.config["SQLALCHEMY_DATABASE_URI"] = args[0]
    for person in (
        Person.query.filter(Person.nhl_id != None)
        .filter(Person.active == True)
        .order_by(Person.nhl_id.asc())
        .all()
    ):
        print(f"{person.id} - {person.nhl_id} - {person.name}")
        data = download_nhl_stats(person.nhl_id)
        if not data:
            print(f"  Skipping (no stats)")
            continue
        if person.position == "G":
            create_stats_record(
                person.position,
                person_id=person.id,
                season_id=app.config["CURRENT_SEASON"],
                # team_id=data["team"]["id"],
                starts=data["stat"]["gamesStarted"],
                wins=data["stat"]["wins"],
                losses=data["stat"]["losses"],
                overtime_losses=data["stat"]["ot"] if "ot" in data["stat"] else None,
                games_played=data["stat"]["games"],
                shutouts=data["stat"]["shutouts"],
                shots_against=data["stat"]["shotsAgainst"],
                goals_against=data["stat"]["goalsAgainst"],
            )
        else:
            create_stats_record(
                person.position,
                person_id=person.id,
                season_id=app.config["CURRENT_SEASON"],
                # team_id=data["team"]["id"],
                goals=data["stat"]["goals"],
                assists=data["stat"]["assists"],
                points=data["stat"]["points"],
                games_played=data["stat"]["games"],
                hits=data["stat"]["hits"],
                shots=data["stat"]["shots"],
                penalty_minutes=data["stat"]["pim"],
            )
        sleep(1)
    db.session.commit()

    app_context.pop()


def create_stats_record(position, **kwargs):
    table = SkaterSeason
    if position == "G":
        table = GoalieSeason

    check = (
        table.query.filter(table.person_id == kwargs["person_id"])
        .filter(table.season_id == kwargs["season_id"])
        .filter(table.team_id == (kwargs["team_id"] if "team_id" in kwargs else None))
        .count()
    )
    if check > 0:
        print(f"  Skipping", file=sys.stderr)
        return

    # Make sure all the kwargs are valid columns
    attributes = dir(table)
    invalid = list(filter(lambda x: x not in attributes, kwargs.keys()))
    for key in invalid:
        del kwargs[key]
    db.session.add(table(**kwargs))


def get_or_create_season(season_name):
    season_name = f"{season_name[:4]}-{season_name[4:]}"
    season = Season.query.filter(Season.name == season_name).first()
    if season:
        return season.id
    print(f"Creating season {season_name}")
    season = Season(name=season_name)
    db.session.add(season)
    db.session.flush()
    return season.id


def download_nhl_stats(nhl_id, retry=False):
    try:
        data = json.loads(
            urllib.request.urlopen(
                f"https://statsapi.web.nhl.com/api/v1/people/{nhl_id}/stats?stats=statsSingleSeason&season=20212022"
            ).read()
        )
    except HTTPError as e:
        print(f"{nhl_id}: {e}", file=sys.stderr)
        return None
    except URLError as e:
        print(f"{nhl_id}: {e}", file=sys.stderr)
        if not retry:
            sleep(5)
            data = download_player(nhl_id, True)
        else:
            return None
    if nhl_id % 1000 == 0:
        print(nhl_id)

    if len(data["stats"][0]["splits"]) == 0:
        return None
    return data["stats"][0]["splits"][0]


if __name__ == "__main__":
    main(sys.argv[1:])
