fetch(`/admin/update-person`)
    .then(response => {
        if (response.redirected) {
            return;
        }
        document.querySelectorAll(".edit").forEach(element => element.style.display = "block");
    });
