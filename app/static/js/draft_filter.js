let teamFilter = document.getElementById("team-filter");
teamFilter.value = "";
teamFilter.onchange = filterPicks;
shadeRows();

function filterPicks() {
    let filterSelector = document.getElementById("team-filter");
    let visibleTeam = filterSelector.value;
    console.log(`Filter to: '${visibleTeam}'`);
    const SELECTOR = ".picks tr:not(.header)";
    let allRows = document.querySelectorAll(SELECTOR);
    if (visibleTeam == "") {
        allRows.forEach(makeVisible);
        return;
    }
    for (let idx = 0; idx < allRows.length; idx++) {
        let teamLink = allRows[idx].querySelector("a");
        if (!teamLink) {
            continue;
        }
        console.log(`Team link: ${teamLink.href} -- visibleTeam: ${visibleTeam}`);
        if (teamLink.href.includes(visibleTeam)) {
            makeVisible(allRows[idx]);
        } else {
            makeInvisible(allRows[idx]);
        }
    }
    return;
}

function makeVisible(element) {
    element.style.display = null;
    return;
}

function makeInvisible(element) {
    element.style.display = "none";
    return;
}

function shadeRows() {
    let rows = document.querySelector("table").querySelectorAll("tr");
    let offset = 0;
    rows.forEach((row, idx) => {
        if (row.parentElement.parentElement.id != "draft") {
            offset++;
            return;
        }
        if ((idx - offset) % 2 == 0) {
            return;
        }
        row.classList.add("shaded");
    })
}
