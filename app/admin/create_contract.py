from datetime import date, datetime
from flask import current_app
from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    DateField,
    IntegerField,
    SelectField,
    BooleanField,
    SubmitField,
)
from wtforms.validators import DataRequired, Optional
from sqlalchemy import select, func, text

from .. import db
from ..models import (
    Person,
    Team,
    Asset,
    Contract,
    ContractYear,
    ProfessionalTryout,
    Season,
    TeamSeason,
    SigningRights,
    Transaction,
    TransactionAsset,
    Note,
)
from . import team_season


def get_unsigned():
    signed_people = (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .filter(Asset.active == True)
        .with_entities(Contract.person_id)
        .subquery()
    )
    signing_rights = (
        SigningRights.query.join(Asset, Asset.id == SigningRights.id)
        .filter(Asset.active == True)
        .with_entities(SigningRights.person_id)
        .subquery()
    )

    people = (
        Person.query.filter(Person.active == True)
        .filter(Person.id.not_in(select(signed_people)))
        .filter(Person.id.not_in(select(signing_rights)))
        .order_by(Person.name.asc())
        .with_entities(
            Person.id,
            Person.name,
        )
        .all()
    )
    return [tuple(person) for person in people]
    # Function returns 'sqlalchemy.engine.row.Row', not 'tuple'.
    # Not sure how to cast to tuple by default


def get_inactive():
    sql_query = """
    SELECT
        person.id,
        person.name
    FROM person
    WHERE person.birthdate > :date
        AND person.id IN (
            SELECT person_id
            FROM contract
        )
        AND person.death_date IS NULL
        AND person.id NOT IN (
            SELECT 
                active_players.person_id
            FROM (
                SELECT id, person_id FROM contract
                UNION
                SELECT id, person_id FROM signing_rights
                UNION
                SELECT id, person_id FROM pto
            ) AS active_players
            JOIN asset ON asset.id=active_players.id
            JOIN person ON person.id=active_players.person_id
            WHERE asset.active=1
        )
    ORDER BY person.name ASC;
    """
    sql_query = """
    SELECT
        person.id,
        person.name
    FROM person
    WHERE person.birthdate > :date
        AND person.death_date IS NULL
        AND person.id NOT IN (
            SELECT 
                active_players.person_id
            FROM (
                SELECT id, person_id FROM contract
                UNION
                SELECT id, person_id FROM signing_rights
                UNION
                SELECT id, person_id FROM pto
            ) AS active_players
            JOIN asset ON asset.id=active_players.id
            JOIN person ON person.id=active_players.person_id
            WHERE asset.active=1
        )
    ORDER BY person.name ASC;
    """

    fifty_years_ago = date.today().replace(year=(date.today().year - 50))
    query_data = (
        Person.query.from_statement(text(sql_query))
        .params(date=fifty_years_ago)
        .with_entities(
            Person.id,
            Person.name,
        )
        .all()
    )
    return [tuple(person) for person in query_data]


def get_rights(team_id):
    """
    SELECT person.id, person.name
    FROM signing_rights
        JOIN asset ON asset.id=signing_rights.id
        JOIN person ON person.id=signing_rights.person_id
    WHERE asset.active=1
    """
    query_data = (
        SigningRights.query.join(Asset, Asset.id == SigningRights.id)
        .join(Person, Person.id == SigningRights.person_id)
        .filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .order_by(Person.name.asc())
        .with_entities(Person.id, Person.name)
    )
    if team_id == "ALL":
        query_data = (
            SigningRights.query.join(Asset, Asset.id == SigningRights.id)
            .join(Person, Person.id == SigningRights.person_id)
            .filter(Asset.active == True)
            .filter(SigningRights.qualifying_offer == True)
            .order_by(Person.name.asc())
            .with_entities(Person.id, Person.name)
        )
    return [tuple(person) for person in query_data.all()]


def get_extendable(team_id):
    """
    SELECT person.id, person.name
    FROM contract
        JOIN contract_year ON contract.id=contract_year.contract_id
        JOIN person ON contract.person_id=person.id
        JOIN asset on asset.id=contract.id
    WHERE asset.active=1
        AND contract_year.season_id=16
        AND contract.id NOT IN (
            SELECT contract.id
            FROM contract
                JOIN contract_year ON contract.id==contract_year.contract_id
                JOIN asset ON contract.id=asset.id
            WHERE contract_year.season_id=17
                AND asset.active=1
        );
    """
    current_free_agency = (
        Season.query.filter(Season.id == current_app.config["CURRENT_SEASON"])
        .with_entities(Season.free_agency_opening)
        .limit(1)
        .scalar_subquery()
    )
    next_season_id = (
        Season.query.filter(Season.free_agency_opening > current_free_agency)
        .order_by(Season.free_agency_opening.asc())
        .with_entities(Season.id)
        .limit(1)
        .scalar_subquery()
    )
    next_season_contracts = (
        db.session.query(Contract.id)
        .join(Asset, Asset.id == Contract.id)
        .join(ContractYear, ContractYear.contract_id == Contract.id)
        .filter(Asset.active == True)
        .filter(ContractYear.season_id == next_season_id)
        .subquery()
    )

    people = (
        Contract.query.join(Person, Contract.person_id == Person.id)
        .join(Asset, Contract.id == Asset.id)
        .join(ContractYear, Contract.id == ContractYear.contract_id)
        .filter(Asset.active == True)
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .filter(Contract.id.not_in(select(next_season_contracts)))
        .filter(Asset.current_team == team_id)
        .order_by(Person.name)
        .with_entities(
            Person.id,
            Person.name,
        )
        .all()
    )

    return [tuple(person) for person in people]


class CreatePTO(FlaskForm):
    def __init__(self):
        super().__init__()
        self.team.choices = self.get_teams()
        self.person.choices = self.get_free_agents()
        self.validation_errors = []

    def get_teams(self):
        raw_teams = (
            Team.query.with_entities(Team.id, Team.name)
            .filter(Team.active == True)
            .order_by(Team.name.asc())
            .all()
        )
        return [tuple(team) for team in raw_teams]

    def get_free_agents(self):
        actively_contracted = (
            Contract.query.join(Asset, Asset.id == Contract.id)
            .filter(Asset.active == True)
            .with_entities(Contract.person_id)
            .subquery()
        )
        active_pto = (
            ProfessionalTryout.query.join(Asset, Asset.id == ProfessionalTryout.id)
            .filter(Asset.active == True)
            .with_entities(ProfessionalTryout.person_id)
            .subquery()
        )
        active_signing_rights = (
            SigningRights.query.join(Asset, Asset.id == SigningRights.id)
            .filter(Asset.active == True)
            .with_entities(SigningRights.person_id)
            .subquery()
        )

        people = (
            Person.query.filter(Person.active == True)
            .filter(Person.id.not_in(select(actively_contracted)))
            .filter(Person.id.not_in(select(active_pto)))
            .filter(Person.id.not_in(select(active_signing_rights)))
            .order_by(Person.name.asc())
            .with_entities(
                Person.id,
                Person.name,
            )
            .all()
        )
        return [tuple(person) for person in people]
        # Function returns 'sqlalchemy.engine.row.Row', not 'tuple'.

    def create_pto(self):
        asset = Asset(
            current_team=self.team.data,
            originating_team=self.team.data,
            active=True,
            type="Professional Tryout",
        )
        transaction = Transaction(
            date=self.signing_date.data,
            season_id=current_app.config["CURRENT_SEASON"],
            type="Signed professional tryout (PTO)",
        )
        db.session.add(asset)
        db.session.add(transaction)
        db.session.flush()
        db.session.add(
            ProfessionalTryout(
                id=asset.id,
                person_id=self.person.data,
                season_id=current_app.config["CURRENT_SEASON"],
                signing_date=self.signing_date.data,
            )
        )
        db.session.add(
            TransactionAsset(
                transaction_id=transaction.id,
                asset_id=asset.id,
                former_team=self.team.data,
                new_team=self.team.data,
            )
        )
        db.session.commit()
        return self.person.data

    person = SelectField(
        "Player", default="", validate_choice=False, validators=[DataRequired()]
    )
    team = SelectField("Team", default="", validators=[DataRequired()])
    signing_date = DateField("Signing Date", validators=[DataRequired()])
    submit = SubmitField("Create PTO")


class CreateContract(FlaskForm):
    def __init__(self):
        super().__init__()
        self.team.choices = self.get_teams()
        self.person.choices = self.get_free_agents()
        self.first_season.choices = self.get_seasons()
        self.validation_errors = []

    def get_teams(self):
        raw_teams = (
            Team.query.with_entities(Team.id, Team.name)
            .filter(Team.active == True)
            .order_by(Team.name.asc())
            .all()
        )
        return [tuple(team) for team in raw_teams]

    def get_free_agents(self):
        return get_unsigned()

    def get_seasons(self):
        seasons = (
            Season.query.order_by(
                Season.free_agency_opening.asc().nullslast(),
                Season.id.asc(),
            )
            .with_entities(
                Season.id,
                Season.name,
            )
            .all()
        )
        return [tuple(season) for season in seasons]

    def validate(self, extra_validators=None):
        if not super().validate(extra_validators=extra_validators):
            current_app.logger.error(
                "Create Contract: WTForms validators not fulfilled"
            )
            return False
        if not self.check_empty_years():
            current_app.logger.error(
                "Create Contract: Empty year(s) in middle of contract submission"
            )
            return False
        if not self.check_nhl_salary_provided():
            current_app.logger.error(
                "Create Contract: NHL Salary not provided for all years"
            )
            return False
        variance = self.check_yearly_variance()
        first_season = self.check_first_season()
        self.load_seasons()
        if first_season < current_app.config["CURRENT_SEASON"]:
            return True
        if not variance:
            current_app.logger.error("Create Contract: yearly variance limits exceeded")
            return False
        if not first_season:
            current_app.logger.error("Create Contract: first season is invalid")
            return False
        if not self.check_maximum_pay():
            current_app.logger.error("Create Contract: maximum pay limit exceeded")
            return False
        if not self.check_minor_league_minimum_pay():
            current_app.logger.error(
                "Create Contract: minor league minimum pay limit exceeded"
            )
            return False
        if not self.check_minimum_salary():
            current_app.logger.error("Create Contract: minimum NHL salary not met")
            return False
        if not self.check_contract_count():
            current_app.logger.error("Create Contract: contract limit exceeded")
            return False
        if not self.check_contract_length():
            current_app.logger.error("Create Contract: contract year limit exceeded")
            return False
        if not self.check_entry_level():
            current_app.logger.error(
                "Create Contract: entry level restrictions not met"
            )
            return False
        if not self.check_performance_bonuses():
            current_app.logger.error(
                "Create contract: performance bonus rules not followed"
            )
            return False
        return True

    def load_seasons(self):
        free_agency = (
            Season.query.filter(Season.id == self.first_season.data)
            .with_entities(
                Season.free_agency_opening,
            )
            .limit(1)
            .scalar_subquery()
        )
        self.seasons = (
            Season.query.filter(Season.free_agency_opening >= free_agency)
            .order_by(Season.free_agency_opening.asc())
            .limit(8)
            .all()
        )

    def check_empty_years(self):
        years_selected = [
            self.second_year.data,
            self.third_year.data,
            self.fourth_year.data,
            self.fifth_year.data,
            self.sixth_year.data,
            self.seventh_year.data,
            self.eighth_year.data,
        ]
        sorted_years = sorted(years_selected, reverse=True)
        if years_selected == sorted_years:
            return True
        self.second_year.errors.append(
            "Contract years must all be added in order, second year first without gaps in the middle."
        )
        return False

    def check_nhl_salary_provided(self):
        if self.first_nhl_salary.data is None:
            self.first_nhl_salary.errors.append(
                "NHL Salary required for each selected contract year"
            )
            return False
        if self.second_year.data and self.second_nhl_salary.data is None:
            self.second_nhl_salary.errors.append(
                "NHL Salary required for each selected contract year"
            )
            return False
        if self.third_year.data and self.third_nhl_salary.data is None:
            self.third_nhl_salary.errors.append(
                "NHL Salary required for each selected contract year"
            )
            return False
        if self.fourth_year.data and self.fourth_nhl_salary.data is None:
            self.fourth_nhl_salary.errors.append(
                "NHL Salary required for each selected contract year"
            )
            return False
        if self.fifth_year.data and self.fifth_nhl_salary.data is None:
            self.fifth_nhl_salary.errors.append(
                "NHL Salary required for each selected contract year"
            )
            return False
        if self.sixth_year.data and self.sixth_nhl_salary.data is None:
            self.sixth_nhl_salary.errors.append(
                "NHL Salary required for each selected contract year"
            )
            return False
        if self.seventh_year.data and self.seventh_nhl_salary.data is None:
            self.seventh_nhl_salary.errors.append(
                "NHL Salary required for each selected contract year"
            )
            return False
        if self.eighth_year.data and self.eighth_nhl_salary.data is None:
            self.eighth_nhl_salary.errors.append(
                "NHL Salary required for each selected contract year"
            )
            return False
        return True

    def check_yearly_variance(self):
        self.years = 1 + sum(
            [
                self.second_year.data,
                self.third_year.data,
                self.fourth_year.data,
                self.fifth_year.data,
                self.sixth_year.data,
                self.seventh_year.data,
                self.eighth_year.data,
            ]
        )
        pay = [
            (self.first_nhl_salary.data or 0)
            + (self.first_signing_bonus.data or 0)
            + (self.first_performance_bonuses.data or 0),
            (self.second_nhl_salary.data or 0)
            + (self.second_signing_bonus.data or 0)
            + (self.second_performance_bonuses.data or 0),
            (self.third_nhl_salary.data or 0)
            + (self.third_signing_bonus.data or 0)
            + (self.third_performance_bonuses.data or 0),
            (self.fourth_nhl_salary.data or 0)
            + (self.fourth_signing_bonus.data or 0)
            + (self.fourth_performance_bonuses.data or 0),
            (self.fifth_nhl_salary.data or 0)
            + (self.fifth_signing_bonus.data or 0)
            + (self.fifth_performance_bonuses.data or 0),
            (self.sixth_nhl_salary.data or 0)
            + (self.sixth_signing_bonus.data or 0)
            + (self.sixth_performance_bonuses.data or 0),
            (self.seventh_nhl_salary.data or 0)
            + (self.seventh_signing_bonus.data or 0)
            + (self.seventh_performance_bonuses.data or 0),
            (self.eighth_nhl_salary.data or 0)
            + (self.eighth_signing_bonus.data or 0)
            + (self.eighth_performance_bonuses.data or 0),
        ]
        self.pay = pay[: self.years]
        if not self.second_year.data:
            return True
        if self.is_front_loaded():
            max_delta = 0.25 * self.pay[0]
            min_pay = 0.6 * max(self.pay)
            if self.pay[0] < min_pay:
                self.validation_errors.append(
                    f"Year 1: Pay below front loaded minimum ({int(min_pay)})"
                )
                return False
            for idx in range(1, len(self.pay)):
                if self.pay[idx] < min_pay:
                    message = (
                        f"Year {idx+1}: Pay below front loaded minimum ({int(min_pay)})"
                    )
                    self.validation_errors.append(message)
                    return False
                if abs(self.pay[idx] - self.pay[idx - 1]) > max_delta:
                    message = f"Year {idx}->{idx+1}: Pay variance above front loaded maximum ({int(max_delta)})"
                    self.validation_errors.append(message)
                    return False
        else:
            max_delta = min(self.pay[:2])
            if 2 * self.pay[0] < self.pay[1] or 2 * self.pay[1] < self.pay[0]:
                message = f"Pay cannot decrease by more than half or increase to more than double in year 2"
                self.validation_errors.append(message)
                return False
            for idx in range(2, self.years):
                max_increase = max_delta
                max_decrease = 0.5 * max_delta
                # range(3, 2) just returns nothing
                if self.pay[idx] - self.pay[idx - 1] > max_increase:
                    message = f"Year {idx}->{idx+1}: Pay increase above maximum ({int(max_increase)})"
                    self.validation_errors.append(message)
                    return False
                if self.pay[idx - 1] - self.pay[idx] > max_decrease:
                    message = f"Year {idx}->{idx+1}: Pay decrease above maximum ({int(max_decrease)})"
                    self.validation_errors.append(message)
                    return False
        return True

    def is_front_loaded(self):
        try:
            aav = sum(self.pay) / self.years
        except AttributeError:
            return False

        half = self.years // 2
        first_half_aav = sum(self.pay[:half]) / (half)
        if self.years % 2 == 1:
            # Odd number of years: include half of middle year pay
            first_half_aav = (sum(self.pay[:half]) + (self.pay[half] / 2)) / (
                half + 0.5
            )

        return first_half_aav > aav

    def check_maximum_pay(self):
        # cap_ceiling = Season.query.filter(Season.id==current_app.config["CURRENT_SEASON"]).first().salary_cap
        # maximum_pay = 0.2*cap_ceiling
        for idx in range(self.years):
            maximum_pay = self.seasons[idx].salary_cap * 0.2
            if self.pay[idx] > maximum_pay:
                message = f"Year {idx+1}: Pay exceeds maximum single-person limit ({int(maximum_pay)})"
                self.validation_errors.append(message)
                return False
        return True

    def check_minor_league_minimum_pay(self):
        minors = [
            self.first_minors_salary.data or 35000,
            self.second_minors_salary.data or 35000,
            self.third_minors_salary.data or 35000,
            self.fourth_minors_salary.data or 35000,
            self.fifth_minors_salary.data or 35000,
            self.sixth_minors_salary.data or 35000,
            self.seventh_minors_salary.data or 35000,
            self.eighth_minors_salary.data or 35000,
        ]
        for idx in range(self.years):
            if minors[idx] < 35000:
                self.validation_errors.append(
                    f"Year {idx+1}: Minor league minimum pay limit exceeded"
                )
                return False
        return True

    def check_minimum_salary(self):
        # min_salary = Season.query.filter(Season.id==current_app.config["CURRENT_SEASON"]).first().minimum_salary
        salaries = [
            self.first_nhl_salary.data,
            self.second_nhl_salary.data,
            self.third_nhl_salary.data,
            self.fourth_nhl_salary.data,
            self.fifth_nhl_salary.data,
            self.sixth_nhl_salary.data,
            self.seventh_nhl_salary.data,
            self.eighth_nhl_salary.data,
        ]
        salaries = list(filter(lambda salary: salary is not None, salaries))
        for idx in range(self.years):
            min_salary = self.seasons[idx].minimum_salary
            if salaries[idx] < min_salary:
                self.validation_errors.append(
                    f"Year {idx+1}: NHL salary below league minimum ({min_salary})"
                )
                return False
        return True

    def check_first_season(self):
        if self.first_season.data is None:
            last_contracted = (
                ContractYear.query.join(
                    Contract, Contract.id == ContractYear.contract_id
                )
                .join(Season, Season.id == ContractYear.season_id)
                .filter(Contract.person_id == self.person.data)
                .filter(ContractYear.bought_out == False)
                .filter(ContractYear.terminated == False)
                .filter(
                    Season.free_agency_opening
                    >= (
                        Season.query.filter(
                            Season.id == current_app.config["CURRENT_SEASON"]
                        )
                        .with_entities(Season.free_agency_opening)
                        .scalar_subquery()
                    )
                )
                .order_by(Season.free_agency_opening.desc())
                .with_entities(
                    Season.free_agency_opening,
                )
                .first()
            )
            if last_contracted and last_contracted[0]:
                next_season = (
                    Season.query.filter(Season.free_agency_opening > last_contracted[0])
                    .order_by(Season.free_agency_opening.asc())
                    .with_entities(Season.id)
                    .first()
                )
                first_season = next_season[0]
            else:
                first_season = current_app.config["CURRENT_SEASON"]
            self.first_season.data = first_season
        self.first_season.data = int(self.first_season.data)

        first_season = (
            Season.query.filter(Season.id == self.first_season.data)
            .with_entities(Season.free_agency_opening)
            .scalar_subquery()
        )
        season_ids = (
            Season.query.filter(Season.free_agency_opening >= first_season)
            .with_entities(Season.id)
            .limit(self.years)
            .subquery()
        )
        overlap_count = (
            ContractYear.query.join(Contract, Contract.id == ContractYear.contract_id)
            .filter(Contract.person_id == self.person.data)
            .filter(ContractYear.season_id.in_(select(season_ids)))
            .filter(ContractYear.bought_out == False)
            .count()
        )

        if overlap_count > 0:
            self.first_season.errors.append(
                "Contract as input overlaps a pre-existing contract"
            )
            return False
        return True

    def check_contract_count(self):
        contract_count = (
            TeamSeason.query.filter(TeamSeason.team_id == self.team.data)
            .filter(TeamSeason.season_id == self.first_season.data)
            .first()
            .contract_count
        )

        if contract_count >= 50:
            self.team.errors.append("Team selected is at the contract maximum")
            return False
        return True

    def check_contract_length(self):
        if self.years < 8:
            return True

        # 1 if extending. 0 otherwise
        extending = (
            Asset.query.join(Contract, Contract.id == Asset.id)
            .filter(Asset.active == True)
            .filter(Contract.person_id == self.person.data)
            .filter(Asset.current_team == self.team.data)
            .count()
        )

        if extending and self.years <= 8:
            return True
        return False

    def check_entry_level(self):
        previous_contracts = Contract.query.filter(
            Contract.person_id == self.person.data
        ).count()
        if previous_contracts > 0:
            return True

        person = Person.query.filter(Person.id == self.person.data).first()
        elc = date.today().replace(month=9, day=15)
        if self.signing_date.data:
            elc = self.signing_date.data.replace(month=9, day=15)
        birth = person.birthdate
        elc_age = (
            elc.year - birth.year - ((elc.month, elc.day) < (birth.month, birth.day))
        )
        if elc_age < 25:
            self.entry_level.data = True
        elif elc_age > 27:
            self.entry_level.data = False
        if not self.entry_level.data:
            return True

        if elc_age < 22 and self.years > 3:
            self.validation_errors.append("ELC length under 22 limited to 3 years")
            return False
        elif elc_age in [22, 23] and self.years > 2:
            self.validation_errors.append("ELC length at 22/23 limited to 2 years")
            return False
        elif elc_age < 28 and elc_age >= 24 and self.years > 1:
            self.validation_errors.append(
                "ELC length at 24 (24-27 for European players) limited to 1 year"
            )
            return False

        if not self.first_minors_salary.data:
            self.first_minors_salary.data = 35000
        if not self.second_minors_salary.data:
            self.second_minors_salary.data = 35000
        if not self.third_minors_salary.data:
            self.third_minors_salary.data = 35000
        # season_data = Season.query\
        #    .filter(Season.id==self.first_season.data)\
        #    .with_entities(
        #        Season.max_elc_salary,
        #        Season.max_elc_minor_salary,
        #    )\
        #    .first()
        # elc_maximum = season_data[0]
        # elc_minors_maxiumum = season_data[1]
        nhl_pay = [
            (self.first_nhl_salary.data or 0) + (self.first_signing_bonus.data or 0),
            (self.second_nhl_salary.data or 0) + (self.second_signing_bonus.data or 0),
            (self.third_nhl_salary.data or 0) + (self.third_signing_bonus.data or 0),
        ]
        for idx in range(self.years):
            elc_maximum = self.seasons[idx].max_elc_salary
            if nhl_pay[idx] > elc_maximum:
                message = (
                    f"Year {idx+1}: Salary + signing bonus exceeds entry level maximum"
                )
                self.validation_errors.append(message)
                return False
        minors_pay = [
            self.first_minors_salary.data,
            self.second_minors_salary.data,
            self.third_minors_salary.data,
        ]
        for idx in range(self.years):
            elc_minors_maximum = self.seasons[idx].max_elc_minor_salary
            if minors_pay[idx] > elc_minors_maximum:
                message = f"Year {idx+1}: Minors salary exceeds entry level maximum"
                self.validation_errors.append(message)
                return False
        # Signing bonus limited to 10% NHL salary + signing bonuses
        # a.k.a. 9*signing bonus <= nhl salary
        bonus_vs_salary = [
            9 * (self.first_signing_bonus.data or 0),
            self.first_nhl_salary.data or 0,
            9 * (self.second_signing_bonus.data or 0),
            self.second_nhl_salary.data or 0,
            9 * (self.third_signing_bonus.data or 0),
            self.third_nhl_salary.data or 0,
        ]
        for idx in range(self.years):
            if bonus_vs_salary[2 * idx] > bonus_vs_salary[2 * idx + 1]:
                message = f"Year {idx+1}: Signing bonus exceeds 10% of NHL salary + signing bonus"
                self.validation_errors.append(message)
                return False

        performance_bonuses = [
            self.first_performance_bonuses.data or 0,
            self.second_performance_bonuses.data or 0,
            self.third_performance_bonuses.data or 0,
        ]
        maximum = self.seasons[0].max_elc_performance_bonuses
        for idx in range(self.years):
            if performance_bonuses[idx] > maximum:
                message = f"Year {idx+1}: Performance bonuses exceed entry level limits"
                self.validation_errors.append(message)
                return False
        return True

    def check_performance_bonuses(self):
        performance_bonuses = [
            self.first_performance_bonuses.data is not None,
            self.second_performance_bonuses.data is not None,
            self.third_performance_bonuses.data is not None,
            self.fourth_performance_bonuses.data is not None,
            self.fifth_performance_bonuses.data is not None,
            self.sixth_performance_bonuses.data is not None,
            self.seventh_performance_bonuses.data is not None,
            self.eighth_performance_bonuses.data is not None,
        ]
        performance_bonuses = performance_bonuses[: self.years]
        # No bonuses, no problem
        if sum(performance_bonuses) == 0:
            return True

        # From here: there are bonuses
        # ELC check handles this
        if self.entry_level.data == True:
            return True

        # Non ELC performance bonuses only on one year contracts
        if self.years > 1:
            message = "SPC performance bonuses disallowed on multi-year contracts"
            self.validation_errors.append(message)
            return False

        birth = (
            Person.query.filter(Person.id == self.person.data)
            .with_entities(Person.birthdate)
            .first()[0]
        )
        cutoff = (
            Season.query.filter(Season.id == self.first_season.data)
            .with_entities(
                Season.free_agency_opening,
            )
            .first()[0]
            .replace(month=6, day=30)
        )
        cutoff_age = (
            cutoff.year
            - birth.year
            - ((cutoff.month, cutoff.day) < (birth.month, birth.day))
        )
        # 35+ contracts only. 400GP player and injury can be done manually
        if cutoff_age < 35:
            message = "Players under 35 are ineligible for performance bonuses"
            self.validation_errors.append(message)
            return False

        team_performance_bonuses = (
            ContractYear.query.join(Asset, Asset.id == ContractYear.contract_id)
            .filter(Asset.active == True)
            .filter(Asset.current_team == self.team.data)
            .filter(ContractYear.season_id == self.first_season.data)
            .with_entities(func.sum(ContractYear.performance_bonuses))
            .first()[0]
        )
        # cap_ceiling = Season.query\
        #    .filter(Season.id==self.first_season.data)\
        #    .with_entities(
        #        Season.salary_cap,
        #    )\
        #    .first()[0]
        cap_ceiling = self.seasons[0].salary_cap
        if cap_ceiling * 0.075 < (
            team_performance_bonuses + self.first_performance_bonuses.data
        ):
            message = "Performance bonuses exceed 7.5% of the cap with this contract"
            self.validation_errors.append(message)
            return False

        return True

        # Check if group 3 UFA eligible. No NMC/NTC until then

    def deactivate_signing_rights(self):
        rights_id = (
            SigningRights.query.join(Asset, Asset.id == SigningRights.id)
            .filter(SigningRights.person_id == self.person.data)
            .filter(Asset.active == True)
            .limit(1)
            .with_entities(
                SigningRights.id,
            )
            .first()
        )
        if not rights_id:
            return

        rights_id = rights_id[0]
        Asset.query.filter(Asset.id == rights_id).filter(Asset.active == True).update(
            {Asset.active: False}
        )

    def deactivate_professional_tryouts(self):
        pto_id = (
            ProfessionalTryout.query.join(Asset, Asset.id == ProfessionalTryout.id)
            .filter(ProfessionalTryout.person_id == self.person.data)
            .filter(Asset.active == True)
            .order_by(
                ProfessionalTryout.signing_date.desc(),
            )
            .limit(1)
            .with_entities(
                ProfessionalTryout.id,
            )
            .first()
        )
        if not pto_id:
            return
        pto_id = pto_id[0]
        Asset.query.filter(Asset.id == pto_id).filter(Asset.active == True).update(
            {Asset.active: False}
        )

    def create_contract(self):
        self.deactivate_signing_rights()
        self.deactivate_professional_tryouts()
        asset = Asset(
            current_team=self.team.data,
            originating_team=self.team.data,
            active=True,
            type="Contract",
        )
        current_season = Season.query.get(current_app.config["CURRENT_SEASON"])
        first_season = Season.query.get(self.first_season.data)
        if (
            self.seasons[self.years - 1].free_agency_opening
            < current_season.free_agency_opening
        ):
            asset.active = False
        signing_season = (
            Season.query.filter(Season.free_agency_opening <= self.signing_date.data)
            .order_by(Season.free_agency_opening.desc())
            .with_entities(Season.id)
            .first()
        )
        if not signing_season:
            signing_season = current_app.config["CURRENT_SEASON"]
        else:
            signing_season = signing_season[0]

        person = Person.query.get(self.person.data)
        if signing_season == current_app.config["CURRENT_SEASON"]:
            person.active = True
        if not person:
            return None
        status = person.get_status()
        if asset.active:
            status.append("Under Contract")
        if "RFA" in status:
            status.remove("RFA")
        if "UFA" in status:
            status.remove("UFA")
        person.set_status(status)
        birth = person.birthdate
        if self.entry_level.data == True:
            elc = self.signing_date.data.replace(month=9, day=15)
            elc_age = (
                elc.year
                - birth.year
                - ((elc.month, elc.day) < (birth.month, birth.day))
            )
            person.elc_signing_age = elc_age
            db.session.add(person)
            db.session.flush()
        today = date.today()
        age = (
            today.year
            - birth.year
            - ((today.month, today.day) < (birth.month, birth.day))
        )

        # Waivers age (as I understand it)
        # CBA 13.4 (page 71)
        #   18 if turns 18 between 1 January and 15 September year of draft before contract's first season
        #   19 if turns 19 the calendar year of the draft before first season
        #   20 if turns 20 the calendar year of the draft before first season
        #   etc...
        if self.entry_level.data == True and not person.waivers_signing_age:
            cutoff = (
                Season.query.filter(
                    Season.free_agency_opening < self.seasons[0].free_agency_opening
                )
                .with_entities(
                    Season.draft_date,
                )
                .order_by(Season.free_agency_opening.desc())
                .first()[0]
                .replace(month=9, day=15)
            )
            age = (
                cutoff.year
                - birth.year
                - ((cutoff.month, cutoff.day) < (birth.month, birth.day))
            )
            if age == 18:
                person.waivers_signing_age = 18
            else:
                cutoff = cutoff.replace(month=12, day=31)
                age = (
                    cutoff.year
                    - birth.year
                    - ((cutoff.month, cutoff.day) < (birth.month, birth.day))
                )
                person.waivers_signing_age = age

        cutoff_35 = Season.query.get(self.first_season.data).free_agency_opening
        age_35 = (
            cutoff_35.year
            - birth.year
            - ((cutoff_35.month, cutoff_35.day) < (birth.month, birth.day))
        )
        # if age_35 >= 35:
        #    contract.thirty_five_plus = True
        transaction = Transaction(
            season_id=signing_season,
            type="Signed contract",
            date=datetime.combine(self.signing_date.data, datetime.now().time()),
        )
        if self.entry_level.data:
            transaction.type += " (ELC)"
        elif age_35 >= 35:
            transaction.type += " (35+)"
        elif self.offer_sheet.data:
            transaction.type = "Signed offer sheet"

        active_contract_count = (
            Asset.query.join(Contract, Contract.id == Asset.id)
            .filter(Asset.active == True)
            .filter(Contract.person_id == self.person.data)
            .count()
        )
        db.session.add(transaction)
        db.session.add(asset)
        db.session.flush()
        contract = Contract(
            id=asset.id,
            person_id=self.person.data,
            years=self.years,
            signing_date=self.signing_date.data,
            total_value=self.total_value.data,
            entry_level=self.entry_level.data,
            offer_sheet=self.offer_sheet.data,
            expiration_status=self.expiration_status.data,
            non_roster=self.non_roster.data,
            extension=self.extension.data
            or (
                (first_season.free_agency_opening > current_season.free_agency_opening)
                and (active_contract_count != 0)
            ),
            signing_season_id=signing_season,
            thirty_five_plus=(age_35 >= 35),
        )
        transaction_asset = TransactionAsset(
            transaction_id=transaction.id,
            asset_id=contract.id,
            former_team=asset.originating_team,
            new_team=asset.originating_team,
        )
        db.session.add(transaction_asset)

        # Contract limit exempt requires all of: (per CBA article 1, "Reserve List")
        #   1. 18 or 19 yo
        #   2. In juniors/europe
        #   3. Has played 11 or fewer NHL games this season
        # Fudged a bit because we don't have a stats lookup
        if age < 20 and self.entry_level.data == True and self.non_roster.data == True:
            contract.contract_limit_exempt = True
        db.session.add(contract)
        db.session.add(person)

        cap_hit = [
            (self.first_nhl_salary.data or 0) + (self.first_signing_bonus.data or 0),
            (self.second_nhl_salary.data or 0) + (self.second_signing_bonus.data or 0),
            (self.third_nhl_salary.data or 0) + (self.third_signing_bonus.data or 0),
            (self.fourth_nhl_salary.data or 0) + (self.fourth_signing_bonus.data or 0),
            (self.fifth_nhl_salary.data or 0) + (self.fifth_signing_bonus.data or 0),
            (self.sixth_nhl_salary.data or 0) + (self.sixth_signing_bonus.data or 0),
            (self.seventh_nhl_salary.data or 0)
            + (self.seventh_signing_bonus.data or 0),
            (self.eighth_nhl_salary.data or 0) + (self.eighth_signing_bonus.data or 0),
        ]
        cap_hit = cap_hit[: self.years]
        cap_hit_total = sum(cap_hit)
        cap_hit = cap_hit_total // self.years
        aav = sum(self.pay) // self.years

        first = ContractYear(
            contract_id=asset.id,
            season_id=self.seasons[0].id,
            nhl_salary=self.first_nhl_salary.data,
            minors_salary=self.first_minors_salary.data or self.first_nhl_salary.data,
            signing_bonus=self.first_signing_bonus.data or 0,
            performance_bonuses=self.first_performance_bonuses.data or 0,
            earned_performance_bonuses=0,
            two_way=(
                self.first_minors_salary.data is not None
                and self.first_nhl_salary.data != self.first_minors_salary.data
            ),
            ntc=self.first_ntc.data,
            nmc=self.first_nmc.data,
            clause_limits=self.first_clause_limits.data,
            aav=aav,
            cap_hit=cap_hit,
        )
        current_season = Season.query.get(current_app.config["CURRENT_SEASON"])
        if (
            first.season_id == current_season.id
            and contract.years == 1
            and contract.signing_date > current_season.regular_season_start
            and first.signing_bonus != 0
        ):
            season_length = (
                current_season.regular_season_end - current_season.regular_season_start
            ).days
            remaining_season = (
                current_season.regular_season_end - contract.signing_date
            ).days
            first.cap_hit = int(
                first.nhl_salary
                + first.signing_bonus * (season_length / remaining_season)
            )
            print(
                f"{first.nhl_salary} + {first.signing_bonus}*({season_length}/{remaining_season})"
            )
            db.session.add(
                Note(
                    record_table="contract",
                    record_id=asset.id,
                    note="One year contracts with a signing bonus signed after the start of the season have an elevated cap hit because the entire signing bonus needs to be accounted for by the end of the regular season",
                )
            )
        if self.entry_level.data is True and int(self.entry_level_slide.data) > 0:
            first.nhl_salary = 0
            first_minors_salary = 0
            first.two_way = True
            first.performance_bonuses = 0
            first.entry_level_slide = True
            cap_hit = (
                cap_hit_total - (self.first_signing_bonus.data or 0)
            ) // self.years
            aav = (sum(self.pay) - (self.first_signing_bonus.data or 0)) // self.years
            self.fourth_year.data = self.third_year.data
            self.fourth_nhl_salary.data = self.third_nhl_salary.data
            self.fourth_minors_salary.data = self.third_minors_salary.data
            self.fourth_performance_bonuses.data = self.third_performance_bonuses.data
            self.third_year.data = self.second_year.data
            self.third_nhl_salary.data = self.second_nhl_salary.data
            self.third_minors_salary.data = self.second_minors_salary.data
            self.third_performance_bonuses.data = self.second_performance_bonuses.data
            self.second_year.data = True
            self.second_nhl_salary.data = self.first_nhl_salary.data
            self.second_minors_salary.data = self.first_minors_salary.data
            self.second_performance_bonuses.data = self.first_performance_bonuses.data
        db.session.add(first)

        if self.second_year.data == True:
            second = ContractYear(
                contract_id=asset.id,
                season_id=self.seasons[1].id,
                nhl_salary=self.second_nhl_salary.data,
                minors_salary=self.second_minors_salary.data
                or self.second_nhl_salary.data,
                signing_bonus=self.second_signing_bonus.data or 0,
                performance_bonuses=self.second_performance_bonuses.data or 0,
                earned_performance_bonuses=0,
                two_way=(
                    self.second_minors_salary.data is not None
                    and self.second_nhl_salary.data != self.second_minors_salary.data
                ),
                ntc=self.second_ntc.data,
                nmc=self.second_nmc.data,
                clause_limits=self.second_clause_limits.data,
                aav=aav,
                cap_hit=cap_hit,
            )
            if self.entry_level.data is True and int(self.entry_level_slide.data) > 1:
                second.nhl_salary = 0
                second_minors_salary = 0
                second.two_way = True
                second.performance_bonuses = 0
                second.entry_level_slide = True
                cap_hit = (
                    cap_hit_total
                    - (self.second_signing_bonus.data or 0)
                    - (self.first_signing_bonus.data or 0)
                ) // self.years
                aav = (
                    sum(self.pay)
                    - (self.first_signing_bonus.data or 0)
                    - (self.second_signing_bonus.data or 0)
                ) // self.years
                self.fifth_year.data = self.fourth_year.data
                self.fifth_nhl_salary.data = self.fourth_nhl_salary.data
                self.fifth_minors_salary.data = self.fourth_minors_salary.data
                self.fifth_performance_bonuses.data = (
                    self.fourth_performance_bonuses.data
                )
                self.fourth_year.data = self.third_year.data
                self.fourth_nhl_salary.data = self.third_nhl_salary.data
                self.fourth_minors_salary.data = self.third_minors_salary.data
                self.fourth_performance_bonuses.data = (
                    self.third_performance_bonuses.data
                )
                self.third_year.data = self.second_year.data
                self.third_nhl_salary.data = self.second_nhl_salary.data
                self.third_minors_salary.data = self.second_minors_salary.data
                self.third_performance_bonuses.data = (
                    self.second_performance_bonuses.data
                )
            db.session.add(second)

        if self.third_year.data == True:
            third = ContractYear(
                contract_id=asset.id,
                season_id=self.seasons[2].id,
                nhl_salary=self.third_nhl_salary.data,
                minors_salary=self.third_minors_salary.data
                or self.third_nhl_salary.data,
                signing_bonus=self.third_signing_bonus.data or 0,
                performance_bonuses=self.third_performance_bonuses.data or 0,
                earned_performance_bonuses=0,
                two_way=(
                    self.third_minors_salary.data is not None
                    and self.third_nhl_salary.data != self.third_minors_salary.data
                ),
                ntc=self.third_ntc.data,
                nmc=self.third_nmc.data,
                clause_limits=self.third_clause_limits.data,
                aav=aav,
                cap_hit=cap_hit,
            )
            db.session.add(third)

        if self.fourth_year.data == True:
            fourth = ContractYear(
                contract_id=asset.id,
                season_id=self.seasons[3].id,
                nhl_salary=self.fourth_nhl_salary.data,
                minors_salary=self.fourth_minors_salary.data
                or self.fourth_nhl_salary.data,
                signing_bonus=self.fourth_signing_bonus.data or 0,
                performance_bonuses=self.fourth_performance_bonuses.data or 0,
                earned_performance_bonuses=0,
                two_way=(
                    self.fourth_minors_salary.data is not None
                    and self.fourth_nhl_salary.data != self.fourth_minors_salary.data
                ),
                ntc=self.fourth_ntc.data,
                nmc=self.fourth_nmc.data,
                clause_limits=self.fourth_clause_limits.data,
                aav=aav,
                cap_hit=cap_hit,
            )
            db.session.add(fourth)

        if self.fifth_year.data == True:
            fifth = ContractYear(
                contract_id=asset.id,
                season_id=self.seasons[4].id,
                nhl_salary=self.fifth_nhl_salary.data,
                minors_salary=self.fifth_minors_salary.data
                or self.fifth_nhl_salary.data,
                signing_bonus=self.fifth_signing_bonus.data or 0,
                performance_bonuses=self.fifth_performance_bonuses.data or 0,
                earned_performance_bonuses=0,
                two_way=(
                    self.fifth_minors_salary.data is not None
                    and self.fifth_nhl_salary.data != self.fifth_minors_salary.data
                ),
                ntc=self.fifth_ntc.data,
                nmc=self.fifth_nmc.data,
                clause_limits=self.fifth_clause_limits.data,
                aav=aav,
                cap_hit=cap_hit,
            )
            db.session.add(fifth)

        if self.sixth_year.data == True:
            sixth = ContractYear(
                contract_id=asset.id,
                season_id=self.seasons[5].id,
                nhl_salary=self.sixth_nhl_salary.data,
                minors_salary=self.sixth_minors_salary.data
                or self.sixth_nhl_salary.data,
                signing_bonus=self.sixth_signing_bonus.data or 0,
                performance_bonuses=self.sixth_performance_bonuses.data or 0,
                earned_performance_bonuses=0,
                two_way=(
                    self.sixth_minors_salary.data is not None
                    and self.sixth_nhl_salary.data != self.sixth_minors_salary.data
                ),
                ntc=self.sixth_ntc.data,
                nmc=self.sixth_nmc.data,
                clause_limits=self.sixth_clause_limits.data,
                aav=aav,
                cap_hit=cap_hit,
            )
            db.session.add(sixth)

        if self.seventh_year.data == True:
            seventh = ContractYear(
                contract_id=asset.id,
                season_id=self.seasons[6].id,
                nhl_salary=self.seventh_nhl_salary.data,
                minors_salary=self.seventh_minors_salary.data
                or self.seventh_nhl_salary.data,
                signing_bonus=self.seventh_signing_bonus.data or 0,
                performance_bonuses=self.seventh_performance_bonuses.data or 0,
                earned_performance_bonuses=0,
                two_way=(
                    self.seventh_minors_salary.data is not None
                    and self.seventh_nhl_salary.data != self.seventh_minors_salary.data
                ),
                ntc=self.seventh_ntc.data,
                nmc=self.seventh_nmc.data,
                clause_limits=self.seventh_clause_limits.data,
                aav=aav,
                cap_hit=cap_hit,
            )
            db.session.add(seventh)

        if self.eighth_year.data == True:
            eighth = ContractYear(
                contract_id=asset.id,
                season_id=self.seasons[7].id,
                nhl_salary=self.eighth_nhl_salary.data,
                minors_salary=self.eighth_minors_salary.data
                or self.eighth_nhl_salary.data,
                signing_bonus=self.eighth_signing_bonus.data or 0,
                performance_bonuses=self.eighth_performance_bonuses.data or 0,
                earned_performance_bonuses=0,
                two_way=(
                    self.eighth_minors_salary.data is not None
                    and self.eighth_nhl_salary.data != self.eighth_minors_salary.data
                ),
                ntc=self.eighth_ntc.data,
                nmc=self.eighth_nmc.data,
                clause_limits=self.eighth_clause_limits.data,
                aav=aav,
                cap_hit=cap_hit,
            )
            db.session.add(eighth)
        for offset in range(self.years):
            team_season.update_team_season(
                self.team.data, self.first_season.data + offset
            )
        db.session.commit()
        return person.id

    person = SelectField(
        "Player", default="", validate_choice=False, validators=[DataRequired()]
    )
    team = SelectField("Team", default="", validators=[DataRequired()])
    signing_date = DateField("Signing Date", validators=[DataRequired()])
    total_value = IntegerField("Total Value")
    entry_level = BooleanField("Entry Level", default=False)
    offer_sheet = BooleanField("Offer Sheet", default=False)
    entry_level_slide = SelectField(
        "Entry Level Slide", default=0, choices=[(0, "None"), (1, "One"), (2, "Two")]
    )
    non_roster = BooleanField("Non Roster", default=False)
    extension = BooleanField("Extension", default=False)
    expiration_status = SelectField(
        "Expiration Status", choices=["UFA", "RFA"], validators=[DataRequired()]
    )
    first_season = SelectField("First Season", default=None, validators=[Optional()])

    # First Year
    first_nhl_salary = IntegerField("NHL Salary")
    first_performance_bonuses = IntegerField(
        "Performance Bonuses", validators=[Optional()]
    )
    first_signing_bonus = IntegerField("Signing Bonus", validators=[Optional()])
    first_minors_salary = IntegerField("Minors Salary", validators=[Optional()])
    first_ntc = BooleanField("NTC", default=False)
    first_nmc = BooleanField("NMC", default=False)
    first_clause_limits = StringField("NTC/NMC Limits")

    # Second Year
    second_year = BooleanField("Year Two", default=False)
    second_performance_bonuses = IntegerField(
        "Performance Bonuses", validators=[Optional()]
    )
    second_signing_bonus = IntegerField("Signing Bonus", validators=[Optional()])
    second_nhl_salary = IntegerField("NHL Salary", validators=[Optional()])
    second_minors_salary = IntegerField("Minors Salary", validators=[Optional()])
    second_ntc = BooleanField("NTC", default=False)
    second_nmc = BooleanField("NMC", default=False)
    second_clause_limits = StringField("NTC/NMC Limits")

    # Third Year
    third_year = BooleanField("Year Two", default=False)
    third_performance_bonuses = IntegerField(
        "Performance Bonuses", validators=[Optional()]
    )
    third_signing_bonus = IntegerField("Signing Bonus", validators=[Optional()])
    third_nhl_salary = IntegerField("NHL Salary", validators=[Optional()])
    third_minors_salary = IntegerField("Minors Salary", validators=[Optional()])
    third_ntc = BooleanField("NTC", default=False)
    third_nmc = BooleanField("NMC", default=False)
    third_clause_limits = StringField("NTC/NMC Limits")

    # Fourth Year
    fourth_year = BooleanField("Year Two", default=False)
    fourth_performance_bonuses = IntegerField(
        "Performance Bonuses", validators=[Optional()]
    )
    fourth_signing_bonus = IntegerField("Signing Bonus", validators=[Optional()])
    fourth_nhl_salary = IntegerField("NHL Salary", validators=[Optional()])
    fourth_minors_salary = IntegerField("Minors Salary", validators=[Optional()])
    fourth_ntc = BooleanField("NTC", default=False)
    fourth_nmc = BooleanField("NMC", default=False)
    fourth_clause_limits = StringField("NTC/NMC Limits")

    # Fifth Year
    fifth_year = BooleanField("Year Two", default=False)
    fifth_performance_bonuses = IntegerField(
        "Performance Bonuses", validators=[Optional()]
    )
    fifth_signing_bonus = IntegerField("Signing Bonus", validators=[Optional()])
    fifth_nhl_salary = IntegerField("NHL Salary", validators=[Optional()])
    fifth_minors_salary = IntegerField("Minors Salary", validators=[Optional()])
    fifth_ntc = BooleanField("NTC", default=False)
    fifth_nmc = BooleanField("NMC", default=False)
    fifth_clause_limits = StringField("NTC/NMC Limits")

    # Sixth Year
    sixth_year = BooleanField("Year Two", default=False)
    sixth_performance_bonuses = IntegerField(
        "Performance Bonuses", validators=[Optional()]
    )
    sixth_signing_bonus = IntegerField("Signing Bonus", validators=[Optional()])
    sixth_nhl_salary = IntegerField("NHL Salary", validators=[Optional()])
    sixth_minors_salary = IntegerField("Minors Salary", validators=[Optional()])
    sixth_ntc = BooleanField("NTC", default=False)
    sixth_nmc = BooleanField("NMC", default=False)
    sixth_clause_limits = StringField("NTC/NMC Limits")

    # Seventh Year
    seventh_year = BooleanField("Year Two", default=False)
    seventh_performance_bonuses = IntegerField(
        "Performance Bonuses", validators=[Optional()]
    )
    seventh_signing_bonus = IntegerField("Signing Bonus", validators=[Optional()])
    seventh_nhl_salary = IntegerField("NHL Salary", validators=[Optional()])
    seventh_minors_salary = IntegerField("Minors Salary", validators=[Optional()])
    seventh_ntc = BooleanField("NTC", default=False)
    seventh_nmc = BooleanField("NMC", default=False)
    seventh_clause_limits = StringField("NTC/NMC Limits")

    # Eighth Year
    eighth_year = BooleanField("Year Two", default=False)
    eighth_performance_bonuses = IntegerField(
        "Performance Bonuses", validators=[Optional()]
    )
    eighth_signing_bonus = IntegerField("Signing Bonus", validators=[Optional()])
    eighth_nhl_salary = IntegerField("NHL Salary", validators=[Optional()])
    eighth_minors_salary = IntegerField("Minors Salary", validators=[Optional()])
    eighth_ntc = BooleanField("NTC", default=False)
    eighth_nmc = BooleanField("NMC", default=False)
    eighth_clause_limits = StringField("NTC/NMC Limits")

    submit = SubmitField("Create Contract")
