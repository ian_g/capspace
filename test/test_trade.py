import sys
import json
import random
import unittest
from datetime import datetime, date
from werkzeug.exceptions import InternalServerError
from app import create_app, db
from app.models import (
    Team,
    Season,
    Person,
    Asset,
    DraftPick,
    Contract,
    ContractYear,
    Transaction,
    TransactionAsset,
    RetainedSalary,
    TeamSeason,
    SigningRights,
)

from app.main import trade
from app.main.forms import TradeProposal

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class TradeTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def create_dummy_assets(self, trade_data):
        """
        trade_data = {
            "team_one": TEAM_ONE,
            "team_two": TEAM_TWO,
            "future_considerations_one": False,
            "future_considerations_two": False,
            "contracts_one": [{"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(1, 10)],
            "contracts_two": [{"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(11, 20)],
            "picks_one": [{"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(21, 30)],
            "picks_two": [{"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(31, 40)],
        }
        """
        season_id = self.app.config["CURRENT_SEASON"]
        db.session.add(Season(id=season_id, salary_cap=1000, salary_floor=10))
        db.session.add(
            Team(
                id=trade_data["team_one"],
                name="Test One",
                abbreviation="T01",
                logo="lol",
            )
        )
        db.session.add(
            Team(
                id=trade_data["team_two"],
                name="Test Two",
                abbreviation="T02",
                logo="lol2",
            )
        )
        db.session.add(
            TeamSeason(
                team_id=trade_data["team_one"],
                season_id=season_id,
                cap_hit=10,
                projected_cap_hit=10,
                logo="lol",
            )
        )
        db.session.add(
            TeamSeason(
                team_id=trade_data["team_two"],
                season_id=season_id,
                cap_hit=10,
                projected_cap_hit=10,
                logo="lol2",
            )
        )
        db.session.add(Season(id=0, name="Test Szn", draft_date=date.today()))
        db.session.add(Person(id=0, name="Test Hmn"))
        for pick in trade_data["picks_one"]:
            db.session.add(
                Asset(
                    id=pick["id"],
                    originating_team=pick["fmr"],
                    current_team=pick["fmr"],
                )
            )
            db.session.add(DraftPick(id=pick["id"], season_id=0, round=5))
        for pick in trade_data["picks_two"]:
            db.session.add(
                Asset(
                    id=pick["id"],
                    originating_team=pick["fmr"],
                    current_team=pick["fmr"],
                )
            )
            db.session.add(DraftPick(id=pick["id"], season_id=0, round=8))
        for contract in trade_data["contracts_one"]:
            db.session.add(
                Asset(
                    id=contract["id"],
                    originating_team=contract["fmr"],
                    current_team=contract["fmr"],
                )
            )
            db.session.add(Contract(id=contract["id"], person_id=0))
            db.session.add(
                ContractYear(
                    contract_id=contract["id"],
                    season_id=season_id,
                    aav=12,
                    cap_hit=13,
                    nhl_salary=14,
                )
            )
        for contract in trade_data["contracts_two"]:
            db.session.add(
                Asset(
                    id=contract["id"],
                    originating_team=contract["fmr"],
                    current_team=contract["fmr"],
                )
            )
            db.session.add(Contract(id=contract["id"], person_id=0))
            db.session.add(
                ContractYear(
                    contract_id=contract["id"],
                    season_id=season_id,
                    aav=121,
                    cap_hit=113,
                    nhl_salary=142,
                )
            )
        db.session.commit()

    def create_team_and_assets(
        self, team_name, team_abbreviation, contracts=True, picks=True
    ):
        seasons = [
            Season(name="AAAAA"),
            Season(name="BBBBB"),
            Season(name="CCCCC"),
            Season(name="DDDDD"),
            Season(name="EEEEE"),
            Season(name="FFFFF"),
            Season(name="GGGGG"),
            Season(name="HHHHH"),
        ]
        for season in seasons:
            db.session.add(season)

        team = Team(name=team_name, abbreviation=team_abbreviation)
        db.session.add(team)
        db.session.commit()

        data = {
            "team": team.id,
        }
        if contracts:
            data["contracts"] = [
                self.create_team_contract(team.id) for _ in range(random.randint(5, 20))
            ]
            data["contracts"].sort(key=lambda contract: contract["name"])
        if picks:
            draft_year = 2000
            for season in seasons:
                draft_year = random.randint(draft_year, 100 + draft_year)
                season.draft_date = date(
                    draft_year,
                    random.randint(
                        1,
                        12,
                    ),
                    random.randint(1, 28),
                )
                db.session.add(season)
            db.session.commit()
            data["picks"] = [
                self.create_team_pick(
                    team.id, team_abbreviation, seasons[idx % len(seasons)]
                )
                for idx in range(random.randint(5, 20))
            ]
            data["picks"].sort(key=lambda pick: (pick["year"], pick["round"]))

        return data

    def create_team_contract(self, team_id):
        # Create person
        person_name = "".join(random.choices(LETTERS, k=16))
        person = Person(name=person_name)
        db.session.add(person)
        asset = Asset(
            originating_team=team_id,
            current_team=team_id,
            type="Contract",
        )
        db.session.add(asset)
        db.session.commit()

        # Create contract
        #   person_id, team_id, expires_as
        contract = Contract(
            id=asset.id,
            person_id=person.id,
            expiration_status=random.choice(["UFA", "RFA"]),
        )
        db.session.add(contract)
        db.session.commit()

        # Create contract years
        #   contract_id, cap_hit, nhl_salary, season_id
        years_remaining = random.randint(1, 8)
        cap_hit = random.randint(750000, 10000000)
        nhl_salary = random.randint(750000, 10000000)
        aav = random.randint(750000, 10000000)
        for season_id in range(1, years_remaining + 1):
            contract_year = ContractYear(
                contract_id=contract.id,
                cap_hit=cap_hit,
                nhl_salary=nhl_salary,
                season_id=season_id,
                aav=aav,
            )
            db.session.add(contract_year)
        db.session.commit()

        data = {
            "id": asset.id,
            "name": person.name,
            "expires_as": contract.expiration_status,
            "years": years_remaining,
            "cap_hit": cap_hit,
            "nhl_salary": nhl_salary,
            "aav": aav,
        }
        return data

    def create_team_pick(self, team_id, team_abb, season):
        draft_round = random.randint(1, 7)
        position = random.choice([True, False, False, False, False, False])
        draft_position = None
        if position:
            draft_position = random.randint(1, 32) + draft_round * 32

        asset = Asset(originating_team=team_id, current_team=team_id)
        db.session.add(asset)
        db.session.commit()

        draft_pick = DraftPick(
            id=asset.id, season_id=season.id, round=draft_round, position=draft_position
        )
        db.session.add(draft_pick)
        db.session.commit()

        return {
            "id": asset.id,
            "team_id": team_id,
            "year": season.draft_date.year,
            "round": draft_round,
            "position": draft_position,
            "team": team_abb,
        }

    # Test asset lookups
    def test_team_contracts_valid(self):
        data = self.create_team_and_assets("Test Team", "TEM")
        test_data = trade.team_contracts(data["team"])
        for idx in range(len(test_data[0])):
            test = test_data[0][idx]
            check = data["contracts"][idx]
            self.assertTrue(test["expires_as"] == check["expires_as"])
            self.assertTrue(test["id"] == check["id"])
            self.assertTrue(test["name"] == check["name"])
            self.assertTrue(test["years"] == check["years"])

    def test_team_contracts_no_contracts(self):
        data = self.create_team_and_assets("Test Team", "TEM", False, False)
        test_data = trade.team_contracts(data["team"])
        self.assertTrue(test_data[0] == [])
        self.assertTrue(test_data[1] == 200)

    def test_team_contracts_invalid_team(self):
        team_name = "".join(random.choices(LETTERS, k=16))
        team_id = 1
        test_data = trade.team_contracts(team_id)
        self.assertTrue(test_data[0] == f"Team {team_id} not found")
        self.assertTrue(test_data[1] == 404)

    def test_team_picks_valid(self):
        data = self.create_team_and_assets("Test Team", "TEM")
        test_data = trade.team_picks(data["team"])
        for idx in range(len(test_data[0])):
            test = test_data[0][idx]
            check = data["picks"][idx]
            self.assertTrue(test["id"] == check["id"])
            self.assertTrue(test["team"] == check["team"])
            self.assertTrue(test["year"] == check["year"])
            self.assertTrue(test["round"] == check["round"])
            self.assertTrue(test["position"] == check["position"])

    def test_team_picks_no_picks(self):
        data = self.create_team_and_assets("Test Team", "TEM", False, False)
        test_data = trade.team_contracts(data["team"])
        self.assertTrue(test_data[0] == [])
        self.assertTrue(test_data[1] == 200)

    def test_team_picks_invalid_team(self):
        team_name = "".join(random.choices(LETTERS, k=16))
        team_id = 1
        test_data = trade.team_picks(team_id)
        self.assertTrue(test_data[0] == f"Team {team_id} not found")
        self.assertTrue(test_data[1] == 404)

    # Form checks
    def test_valid_json_valid(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()
        form.contracts_one.data = json.dumps({})
        form.contracts_two.data = json.dumps({"test_key": "test_value"})
        form.picks_one.data = json.dumps({5: "number key"})
        form.picks_two.data = json.dumps({"text_key": 5})

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.contracts_two.errors = list(form.contracts_two.errors)
        form.picks_one.errors = list(form.picks_one.errors)
        form.picks_two.errors = list(form.picks_two.errors)

        self.assertTrue(form.valid_json(form.contracts_one))
        self.assertTrue(form.valid_json(form.contracts_two))
        self.assertTrue(form.valid_json(form.picks_one))
        self.assertTrue(form.valid_json(form.picks_two))
        self.assertTrue(form.contracts_one.errors == [])
        self.assertTrue(form.contracts_two.errors == [])
        self.assertTrue(form.picks_one.errors == [])
        self.assertTrue(form.picks_two.errors == [])

        ctx.pop()

    def test_valid_json_invalid(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()
        form.contracts_one.data = "{'test:' 1}"
        form.contracts_two.data = "{\"test\": '5'}"
        form.picks_one.data = "[one, two]"
        form.picks_two.data = "[None]"

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.contracts_two.errors = list(form.contracts_two.errors)
        form.picks_one.errors = list(form.picks_one.errors)
        form.picks_two.errors = list(form.picks_two.errors)

        self.assertTrue(not form.valid_json(form.contracts_one))
        self.assertTrue(not form.valid_json(form.contracts_two))
        self.assertTrue(not form.valid_json(form.picks_one))
        self.assertTrue(not form.valid_json(form.picks_two))
        self.assertTrue(form.contracts_one.errors == ["Incorrectly formed JSON object"])
        self.assertTrue(form.contracts_two.errors == ["Incorrectly formed JSON object"])
        self.assertTrue(form.picks_one.errors == ["Incorrectly formed JSON object"])
        self.assertTrue(form.picks_two.errors == ["Incorrectly formed JSON object"])

        form2 = TradeProposal()
        form2.contracts_one.data = 5
        form2.contracts_two.data = ("An", "Tuple")
        form2.picks_one.data = json.loads
        form2.picks_two.data = form

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form2.contracts_one.errors = list(form2.contracts_one.errors)
        form2.contracts_two.errors = list(form2.contracts_two.errors)
        form2.picks_one.errors = list(form2.picks_one.errors)
        form2.picks_two.errors = list(form2.picks_two.errors)

        self.assertFalse(form2.valid_json(form2.contracts_one))
        self.assertFalse(form2.valid_json(form2.contracts_two))
        self.assertFalse(form2.valid_json(form2.picks_one))
        self.assertFalse(form2.valid_json(form2.picks_two))
        self.assertTrue(
            form2.contracts_one.errors
            == ["JSON variable type must be str, bytes or bytearray"]
        )
        self.assertTrue(
            form2.contracts_two.errors
            == ["JSON variable type must be str, bytes or bytearray"]
        )
        self.assertTrue(
            form2.picks_one.errors
            == ["JSON variable type must be str, bytes or bytearray"]
        )
        self.assertTrue(
            form2.picks_two.errors
            == ["JSON variable type must be str, bytes or bytearray"]
        )

        ctx.pop()

    def test_check_contracts_valid(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        assets = self.create_team_and_assets("Test Team", "TST")
        contract_ids = [{"id": contract["id"]} for contract in assets["contracts"]]
        trade_contracts_one = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        trade_contracts_two = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )

        # Set form.contracts_{one,two}
        form.contracts_one.data = json.dumps(trade_contracts_one)
        form.contracts_two.data = json.dumps(trade_contracts_two)

        self.assertTrue(form.check_contracts(assets["team"], form.contracts_one))
        self.assertTrue(form.check_contracts(assets["team"], form.contracts_two))

        ctx.pop()

    def test_check_contracts_invalid(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        assets = self.create_team_and_assets("Test Team", "TST")
        contract_ids = [{"id": contract["id"]} for contract in assets["contracts"]]
        trade_contracts_one = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        trade_contracts_two = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        trade_contracts_two.append({"id": 10000})
        trade_contracts_one.sort(key=lambda contract: str(contract["id"]))
        trade_contracts_two.sort(key=lambda contract: str(contract["id"]))

        # Set form.contracts_{one,two}
        form.contracts_one.data = json.dumps(trade_contracts_one)
        form.contracts_two.data = json.dumps(trade_contracts_two)

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.contracts_two.errors = list(form.contracts_two.errors)

        self.assertFalse(form.check_contracts(2, form.contracts_one))
        self.assertFalse(form.check_contracts(assets["team"], form.contracts_two))

        form.contracts_one.errors.sort()
        form.contracts_two.errors.sort()
        for idx in range(len(trade_contracts_one)):
            error = form.contracts_one.errors[idx]
            message = f"Contract {trade_contracts_one[idx]['id']} not owned by team 2"
            self.assertTrue(error == message)
        self.assertTrue(
            form.contracts_two.errors == ["Contract 10000 not owned by team 1"]
        )

        ctx.pop()

    def test_check_picks_valid(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        assets = self.create_team_and_assets("Test Team", "TST")
        pick_ids = [{"id": pick["id"]} for pick in assets["picks"]]
        trade_picks_one = random.sample(pick_ids, random.randint(1, len(pick_ids)))
        trade_picks_two = random.sample(pick_ids, random.randint(1, len(pick_ids)))

        # Set form.picks_{one,two}
        form.picks_one.data = json.dumps(trade_picks_one)
        form.picks_two.data = json.dumps(trade_picks_two)

        self.assertTrue(form.check_picks(assets["team"], form.picks_one))
        self.assertTrue(form.check_picks(assets["team"], form.picks_two))

        ctx.pop()

    def test_check_picks_invalid(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        assets = self.create_team_and_assets("Test Team", "TST")
        pick_ids = [{"id": pick["id"]} for pick in assets["picks"]]
        trade_picks_one = random.sample(pick_ids, random.randint(1, len(pick_ids)))
        trade_picks_two = random.sample(pick_ids, random.randint(1, len(pick_ids)))
        trade_picks_two.append({"id": 10000})
        trade_picks_one.sort(key=lambda pick: str(pick["id"]))
        trade_picks_two.sort(key=lambda pick: str(pick["id"]))

        # Set form.picks_{one,two}
        form.picks_one.data = json.dumps(trade_picks_one)
        form.picks_two.data = json.dumps(trade_picks_two)

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form.picks_one.errors = list(form.picks_one.errors)
        form.picks_two.errors = list(form.picks_two.errors)

        self.assertFalse(form.check_picks(2, form.picks_one))
        self.assertFalse(form.check_picks(assets["team"], form.picks_two))

        form.picks_one.errors.sort()
        form.picks_two.errors.sort()
        for idx in range(len(trade_picks_one)):
            error = form.picks_one.errors[idx]
            message = f"Pick {trade_picks_one[idx]['id']} not owned by team 2"
            self.assertTrue(error == message)
        self.assertTrue(form.picks_two.errors == ["Pick 10000 not owned by team 1"])

        ctx.pop()

    def test_check_contract_count_valid_below_limit(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        data = self.create_team_and_assets("Test Team 1", "TT1")

        # Set contracts to trade
        contract_ids = [{"id": contract["id"]} for contract in data["contracts"]]
        trade_contracts_one = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        trade_contracts_two = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        form.contracts_one.data = json.dumps(trade_contracts_one)
        form.contracts_two.data = json.dumps(trade_contracts_two)
        form.team_one.data = data["team"]
        form.team_two.data = data["team"]

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.contracts_two.errors = list(form.contracts_two.errors)

        self.assertTrue(form.check_contract_count())
        self.assertTrue(form.contracts_one.errors == [])
        self.assertTrue(form.contracts_two.errors == [])

        ctx.pop()

    def test_check_contract_count_valid_at_limit(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        data = self.create_team_and_assets("Test Team", "TST")
        while len(data["contracts"]) < 50:
            data["contracts"].append(self.create_team_contract(data["team"]))
        data["contracts"].sort(key=lambda contract: contract["name"])

        # Contracts to trade
        contract_ids = [{"id": contract["id"]} for contract in data["contracts"]]
        sample_size = random.randint(1, len(contract_ids))
        contracts_one = random.sample(contract_ids, sample_size)
        contracts_two = random.sample(contract_ids, sample_size)
        form.contracts_one.data = json.dumps(contracts_one)
        form.contracts_two.data = json.dumps(contracts_two)
        form.team_one.data = data["team"]
        form.team_two.data = data["team"]

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.contracts_two.errors = list(form.contracts_two.errors)

        self.assertTrue(form.check_contract_count())
        self.assertTrue(form.contracts_one.errors == [])
        self.assertTrue(form.contracts_two.errors == [])

        ctx.pop()

    def test_check_contract_count_invalid(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        data = self.create_team_and_assets("Test Team", "TST")
        while len(data["contracts"]) < 50:
            data["contracts"].append(self.create_team_contract(data["team"]))
        data["contracts"].sort(key=lambda contract: contract["name"])

        # Contracts to trade
        contract_ids = [{"id": contract["id"]} for contract in data["contracts"]]
        sample_size = random.randint(1, len(contract_ids) - 2)
        contracts_one = random.sample(contract_ids, sample_size)
        contracts_two = random.sample(contract_ids, sample_size + 2)

        form.team_one.data = data["team"]
        form.team_two.data = data["team"]
        form.contracts_one.data = json.dumps(contracts_one)
        form.contracts_two.data = json.dumps(contracts_two)

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.contracts_two.errors = list(form.contracts_two.errors)

        self.assertFalse(form.check_contract_count())
        self.assertTrue(form.contracts_two.errors == [])
        self.assertTrue(
            form.contracts_one.errors
            == ["Exceeded 50 contract count. More contracts need to be traded away"]
        )

        # two forms. each side exceeds 50 at one point or another
        form2 = TradeProposal()
        form2.team_one.data = data["team"]
        form2.team_two.data = data["team"]
        form2.contracts_two.data = json.dumps(contracts_one)
        form2.contracts_one.data = json.dumps(contracts_two)

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form2.contracts_one.errors = list(form2.contracts_one.errors)
        form2.contracts_two.errors = list(form2.contracts_two.errors)

        self.assertFalse(form2.check_contract_count())
        self.assertTrue(form2.contracts_one.errors == [])
        self.assertTrue(
            form2.contracts_two.errors
            == ["Exceeded 50 contract count. More contracts need to be traded away"]
        )

        ctx.pop()

    def test_check_retained(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        team_one = 7
        season_id = self.app.config["CURRENT_SEASON"]

        db.session.add(
            TeamSeason(
                team_id=team_one,
                season_id=season_id,
                retained_contracts=2,
            )
        )

        form.team_one.data = str(team_one)
        form.contracts_one.data = json.dumps(
            [{"id": 444, "retention": 0}, {"id": 234, "retention": 33}]
        )
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.check_retained(form.contracts_one, form.team_one)
        self.assertEqual(form.contracts_one.errors, [])

        form.contracts_one.data = json.dumps(
            [
                {"id": 444, "retention": 0},
                {"id": 234, "retention": 33},
                {"id": 1, "retention": 3},
            ]
        )
        form.check_retained(form.contracts_one, form.team_one)
        self.assertEqual(
            form.contracts_one.errors,
            ["Team 7 cannot retain more than three contracts"],
        )

        ctx.pop()

    def test_check_previously_retained_form(self):
        team_one = 5
        team_two = 8
        assets = [2, 3, 4, 5, 6, 7]

        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()
        form.team_one.data = str(team_one)
        form.team_two.data = str(team_two)
        form.contracts_one.data = json.dumps([{"id": 2}, {"id": 3}, {"id": 4}])
        form.contracts_two.data = json.dumps([{"id": 5}, {"id": 6}, {"id": 7}])
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.contracts_two.errors = list(form.contracts_two.errors)

        self.assertTrue(form.check_previously_retained())
        db.session.add(Asset(id=1))
        db.session.add(Asset(id=2))
        db.session.add(Asset(id=3))
        db.session.add(Asset(id=4))
        db.session.add(Asset(id=5))
        db.session.add(Asset(id=6))
        db.session.add(
            RetainedSalary(
                id=1,
                retained_by=team_two,
                contract_id=assets[0],
            )
        )
        db.session.add(
            RetainedSalary(
                id=2,
                retained_by=team_two,
                contract_id=assets[1],
            )
        )
        db.session.add(
            RetainedSalary(
                id=3,
                retained_by=team_two,
                contract_id=assets[2],
            )
        )
        db.session.add(
            RetainedSalary(
                id=4,
                retained_by=team_one,
                contract_id=assets[3],
            )
        )
        db.session.add(
            RetainedSalary(
                id=5,
                retained_by=team_one,
                contract_id=assets[4],
            )
        )
        db.session.add(
            RetainedSalary(
                id=6,
                retained_by=team_one,
                contract_id=assets[5],
            )
        )
        db.session.commit()
        self.assertFalse(form.check_previously_retained())
        self.assertEqual(len(form.contracts_one.errors), 1)
        self.assertEqual(
            form.contracts_one.errors[0],
            f"Team {team_one} cannot reacquire a previously retained contract",
        )
        self.assertEqual(len(form.contracts_two.errors), 1)
        self.assertEqual(
            form.contracts_two.errors[0],
            f"Team {team_two} cannot reacquire a previously retained contract",
        )

        ctx.pop()

    # Form checks just from trade.py
    def test_inner_check_contracts_valid(self):
        data = self.create_team_and_assets("Test Team", "TST")
        contract_ids = [{"id": contract["id"]} for contract in data["contracts"]]
        contracts_one = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        contracts_two = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        self.assertTrue(trade.check_contracts(data["team"], contracts_one) == [])
        self.assertTrue(trade.check_contracts(data["team"], contracts_two) == [])

    def test_inner_check_contracts_invalid(self):
        data = self.create_team_and_assets("Test Team", "TST")
        contract_ids = [{"id": contract["id"]} for contract in data["contracts"]]
        contracts_one = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        contracts_two = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )

        contracts_one.append({"id": 120})
        contracts_two.append({"id": 104})
        contracts_two.append({"id": 107})
        contracts_one_errors = [f"Contract 120 not owned by team {data['team']}"]
        contracts_two_errors = [
            f"Contract 104 not owned by team {data['team']}",
            f"Contract 107 not owned by team {data['team']}",
        ]
        self.assertTrue(
            sorted(trade.check_contracts(data["team"], contracts_one))
            == contracts_one_errors
        )
        self.assertTrue(
            sorted(trade.check_contracts(data["team"], contracts_two))
            == contracts_two_errors
        )

    def test_inner_check_picks_valid(self):
        data = self.create_team_and_assets("Test Team", "TST")
        pick_ids = [{"id": pick["id"]} for pick in data["picks"]]
        picks_one = random.sample(pick_ids, random.randint(1, len(pick_ids)))
        picks_two = random.sample(pick_ids, random.randint(1, len(pick_ids)))
        self.assertTrue(trade.check_picks(data["team"], picks_one) == [])
        self.assertTrue(trade.check_picks(data["team"], picks_two) == [])

    def test_inner_check_picks_invalid(self):
        data = self.create_team_and_assets("Test Team", "TST")
        pick_ids = [{"id": pick["id"]} for pick in data["picks"]]
        picks_one = random.sample(pick_ids, random.randint(1, len(pick_ids)))
        picks_two = random.sample(pick_ids, random.randint(1, len(pick_ids)))

        picks_one.append({"id": 120})
        picks_two.append({"id": 104})
        picks_two.append({"id": 107})
        picks_one_errors = [f"Pick 120 not owned by team {data['team']}"]
        picks_two_errors = [
            f"Pick 104 not owned by team {data['team']}",
            f"Pick 107 not owned by team {data['team']}",
        ]
        self.assertTrue(
            sorted(trade.check_picks(data["team"], picks_one)) == picks_one_errors
        )
        self.assertTrue(
            sorted(trade.check_picks(data["team"], picks_two)) == picks_two_errors
        )

    # Save and load trade
    def test_create_transaction_valid(self):
        trade_data_one = {
            "date": date.today(),
            "season_id": 0,
            "type": "Trade Proposal",
            "user_id": 0,
        }
        trade_data_two = {
            "date": date.today(),
            "season_id": 0,
            "type": "Marriage Proposal",
            "user_id": None,
        }
        trade_data_three = {
            "date": date.today(),
            "season_id": 0,
            "type": "Marriage Proposal",
        }
        trade_id_one = trade.create_transaction(**trade_data_one)
        trade_id_two = trade.create_transaction(**trade_data_two)
        trade_id_three = trade.create_transaction(**trade_data_three)
        queried_one = trade.get_transaction(trade_id_one)
        queried_two = trade.get_transaction(trade_id_two)
        queried_three = trade.get_transaction(trade_id_three)
        trade_data_one["date"] = datetime.combine(
            trade_data_one["date"], datetime.min.time()
        )
        trade_data_two["date"] = datetime.combine(
            trade_data_two["date"], datetime.min.time()
        )
        trade_data_three["date"] = datetime.combine(
            trade_data_three["date"], datetime.min.time()
        )

        self.assertEqual(
            trade_data_one["date"].strftime(self.app.config["DATE_FMT"]),
            queried_one["date"],
        )
        self.assertEqual(trade_data_one["type"], queried_one["type"])
        self.assertEqual(trade_data_one["season_id"], queried_one["season_id"])
        self.assertEqual(trade_data_one["user_id"], queried_one["user_id"])
        self.assertEqual(
            trade_data_two["date"].strftime(self.app.config["DATE_FMT"]),
            queried_two["date"],
        )
        self.assertEqual(trade_data_two["type"], queried_two["type"])
        self.assertEqual(trade_data_two["season_id"], queried_two["season_id"])
        self.assertEqual(trade_data_two["user_id"], queried_two["user_id"])
        self.assertEqual(
            trade_data_three["date"].strftime(self.app.config["DATE_FMT"]),
            queried_three["date"],
        )
        self.assertEqual(trade_data_three["type"], queried_three["type"])
        self.assertEqual(trade_data_three["season_id"], queried_three["season_id"])
        self.assertEqual(None, queried_three["user_id"])

    def test_create_transaction_picks(self):
        TRANSACTION_ID = 7
        TEAM_ONE = 1
        TEAM_TWO = 2
        picks_one = [
            {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(21, 30)
        ]
        picks_two = [
            {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(31, 40)
        ]
        for idx in range(len(picks_one)):
            if random.choice([True, False, False, False]):
                picks_one[idx]["condition"] = "".join(random.choices(LETTERS, k=16))
                picks_two[idx]["condition"] = "".join(random.choices(LETTERS, k=16))
        trade.create_transaction_picks(TRANSACTION_ID, picks_one, TEAM_ONE, TEAM_TWO)
        trade.create_transaction_picks(TRANSACTION_ID, picks_two, TEAM_TWO, TEAM_ONE)
        # Create dummy data around picks
        queried_one = (
            TransactionAsset.query.filter(
                TransactionAsset.transaction_id == TRANSACTION_ID
            )
            .filter(TransactionAsset.former_team == TEAM_ONE)
            .order_by(TransactionAsset.asset_id)
            .all()
        )
        queried_two = (
            TransactionAsset.query.filter(
                TransactionAsset.transaction_id == TRANSACTION_ID
            )
            .filter(TransactionAsset.former_team == TEAM_TWO)
            .order_by(TransactionAsset.asset_id)
            .all()
        )
        for idx in range(len(picks_one)):
            self.assertEqual(picks_one[idx]["id"], queried_one[idx].asset_id)
            self.assertEqual(picks_one[idx]["fmr"], queried_one[idx].former_team)
            self.assertEqual(picks_one[idx]["new"], queried_one[idx].new_team)
            self.assertEqual(TRANSACTION_ID, queried_one[idx].transaction_id)
            if "condition" in picks_one[idx]:
                self.assertEqual(
                    picks_one[idx]["condition"], queried_one[idx].condition
                )
        for idx in range(len(picks_two)):
            self.assertEqual(picks_two[idx]["id"], queried_two[idx].asset_id)
            self.assertEqual(picks_two[idx]["fmr"], queried_two[idx].former_team)
            self.assertEqual(picks_two[idx]["new"], queried_two[idx].new_team)
            self.assertEqual(TRANSACTION_ID, queried_two[idx].transaction_id)
            if "condition" in picks_two[idx]:
                self.assertEqual(
                    picks_two[idx]["condition"], queried_two[idx].condition
                )

    def test_create_transaction_assets_contracts(self):
        TRANSACTION_ID = 7
        TEAM_ONE = 1
        TEAM_TWO = 2
        picks_one = [
            {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(21, 30)
        ]
        picks_two = [
            {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(31, 40)
        ]
        for idx in range(len(picks_one)):
            if random.choice([True, False, False, False]):
                picks_one[idx]["retention"] = random.randint(1, 50)
                picks_two[idx]["retention"] = random.randint(1, 50)
        trade.create_transaction_contracts(
            TRANSACTION_ID, picks_one, TEAM_ONE, TEAM_TWO
        )
        trade.create_transaction_contracts(
            TRANSACTION_ID, picks_two, TEAM_TWO, TEAM_ONE
        )
        # Create dummy data around picks
        queried_one = (
            TransactionAsset.query.filter(
                TransactionAsset.transaction_id == TRANSACTION_ID
            )
            .filter(TransactionAsset.former_team == TEAM_ONE)
            .order_by(TransactionAsset.asset_id)
            .all()
        )
        queried_two = (
            TransactionAsset.query.filter(
                TransactionAsset.transaction_id == TRANSACTION_ID
            )
            .filter(TransactionAsset.former_team == TEAM_TWO)
            .order_by(TransactionAsset.asset_id)
            .all()
        )
        for idx in range(len(picks_one)):
            self.assertEqual(picks_one[idx]["id"], queried_one[idx].asset_id)
            self.assertEqual(picks_one[idx]["fmr"], queried_one[idx].former_team)
            self.assertEqual(picks_one[idx]["new"], queried_one[idx].new_team)
            self.assertEqual(TRANSACTION_ID, queried_one[idx].transaction_id)
            if "retention" in picks_one[idx]:
                self.assertEqual(
                    f"Retention: {picks_one[idx]['retention']}%",
                    queried_one[idx].condition,
                )
        for idx in range(len(picks_two)):
            self.assertEqual(picks_two[idx]["id"], queried_two[idx].asset_id)
            self.assertEqual(picks_two[idx]["fmr"], queried_two[idx].former_team)
            self.assertEqual(picks_two[idx]["new"], queried_two[idx].new_team)
            self.assertEqual(TRANSACTION_ID, queried_two[idx].transaction_id)
            if "retention" in picks_two[idx]:
                self.assertEqual(
                    f"Retention: {picks_two[idx]['retention']}%",
                    queried_two[idx].condition,
                )

    def test_create_transaction_assets_future_considerations(self):
        TRANSACTION_ID = 7
        TEAM_ONE = 1
        TEAM_TWO = 2
        fut_con = "Random condition nobody cares about"

        trade.create_future_considerations(TRANSACTION_ID, fut_con, TEAM_ONE, TEAM_TWO)
        trade.create_future_considerations(TRANSACTION_ID, fut_con, TEAM_TWO, TEAM_ONE)
        trade.create_future_considerations(TRANSACTION_ID, "", TEAM_ONE, TEAM_TWO)
        trade.create_future_considerations(TRANSACTION_ID, "", TEAM_TWO, TEAM_ONE)

        queried = (
            TransactionAsset.query.filter(
                TransactionAsset.transaction_id == TRANSACTION_ID
            )
            .order_by(TransactionAsset.id)
            .all()
        )

        self.assertEqual(queried[0].transaction_id, TRANSACTION_ID)
        self.assertEqual(queried[1].transaction_id, TRANSACTION_ID)
        self.assertEqual(queried[2].transaction_id, TRANSACTION_ID)
        self.assertEqual(queried[3].transaction_id, TRANSACTION_ID)
        self.assertEqual(queried[0].former_team, TEAM_ONE)
        self.assertEqual(queried[1].former_team, TEAM_TWO)
        self.assertEqual(queried[2].former_team, TEAM_ONE)
        self.assertEqual(queried[3].former_team, TEAM_TWO)
        self.assertEqual(queried[0].new_team, TEAM_TWO)
        self.assertEqual(queried[1].new_team, TEAM_ONE)
        self.assertEqual(queried[2].new_team, TEAM_TWO)
        self.assertEqual(queried[3].new_team, TEAM_ONE)
        self.assertEqual(queried[0].condition, f"Future Considerations:\n{fut_con}")
        self.assertEqual(queried[1].condition, f"Future Considerations:\n{fut_con}")
        self.assertEqual(queried[2].condition, "Future Considerations")
        self.assertEqual(queried[3].condition, "Future Considerations")

    def test_check_data(self):
        trade_data = {}
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(
                e.description,
                "Missing team one. Please make sure two teams are selected to trade",
            )
        trade_data["team_one"] = 1
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(
                e.description,
                "Missing team two. Please make sure two teams are selected to trade",
            )
        trade_data["team_two"] = 2
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing team one picks data")
        trade_data["picks_one"] = "asdf"
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing team two picks data")
        trade_data["picks_two"] = "asdf"
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing pick ID for team one. Pick a")
        trade_data["picks_one"] = []
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing pick ID for team two. Pick a")
        trade_data["picks_one"] = [{"id": "idk"}]
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing pick ID for team two. Pick a")
        trade_data["picks_two"] = []
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing team one contracts data")
        trade_data["picks_two"] = [{"id": "idk"}]
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing team one contracts data")
        trade_data["contracts_one"] = "asdf"
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing team two contracts data")
        trade_data["contracts_two"] = "asdf"
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing contract ID for team one. Pick a")
        trade_data["contracts_one"] = []
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing contract ID for team two. Pick a")
        trade_data["contracts_one"] = [{"id": "asdf"}]
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing contract ID for team two. Pick a")
        trade_data["contracts_two"] = []
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(
                e.description, "Future considerations not indicated for team one"
            )
        trade_data["contracts_two"] = [{"id": "asdf"}]
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(
                e.description, "Future considerations not indicated for team one"
            )
        trade_data["future_considerations_one"] = False
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(
                e.description, "Future considerations not indicated for team two"
            )
        trade_data["future_considerations_two"] = True
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(
                e.description, "Future considerations details not provided"
            )
        trade_data["future_considerations"] = ""
        self.assertIsNone(trade.check_data(trade_data))

    def test_save_proposal_valid(self):
        TEAM_ONE = 1
        TEAM_TWO = 2
        trade_data = {
            "team_one": TEAM_ONE,
            "team_two": TEAM_TWO,
            "picks_one": [
                {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(21, 30)
            ],
            "picks_two": [
                {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(31, 40)
            ],
            "contracts_one": [
                {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(41, 50)
            ],
            "contracts_two": [
                {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(51, 60)
            ],
            "future_considerations_one": True,
            "future_considerations_two": True,
            "future_considerations": "Test condition",
        }
        transaction_id = trade.save_proposal(trade_data, 0)  # dummy user ID

        transaction = Transaction.query.filter(Transaction.id == transaction_id).first()
        self.assertEqual(transaction.id, transaction_id)
        queried_assets = (
            TransactionAsset.query.filter(
                TransactionAsset.transaction_id == transaction_id
            )
            .order_by(TransactionAsset.id)
            .all()
        )
        created_assets = (
            trade_data["picks_one"]
            + trade_data["picks_two"]
            + trade_data["contracts_one"]
            + trade_data["contracts_two"]
        )
        created_assets.sort(key=lambda asset: asset["id"])
        for idx in range(len(created_assets)):
            self.assertEqual(queried_assets[idx].transaction_id, transaction_id)
            self.assertEqual(queried_assets[idx].asset_id, created_assets[idx]["id"])
            self.assertEqual(
                queried_assets[idx].former_team, created_assets[idx]["fmr"]
            )
            self.assertEqual(queried_assets[idx].new_team, created_assets[idx]["new"])
        # Future considerations
        self.assertEqual(queried_assets[-2].transaction_id, transaction_id)
        self.assertEqual(queried_assets[-2].former_team, TEAM_ONE)
        self.assertEqual(queried_assets[-2].new_team, TEAM_TWO)
        self.assertEqual(
            queried_assets[-2].condition, f"Future Considerations:\nTest condition"
        )
        self.assertEqual(queried_assets[-1].transaction_id, transaction_id)
        self.assertEqual(queried_assets[-1].new_team, TEAM_ONE)
        self.assertEqual(queried_assets[-1].former_team, TEAM_TWO)
        self.assertEqual(
            queried_assets[-1].condition, f"Future Considerations:\nTest condition"
        )

    def test_get_transaction_teams(self):
        TEAM_ONE = 1
        TEAM_TWO = 2
        trade_data = {
            "team_one": TEAM_ONE,
            "team_two": TEAM_TWO,
            "picks_one": [],
            "picks_two": [],
            "contracts_one": [],
            "contracts_two": [],
            "future_considerations_one": True,
            "future_considerations_two": True,
            "future_considerations": "Test condition",
        }
        transaction_id = trade.save_proposal(trade_data, 0)  # dummy user id
        teams = trade.get_transaction_teams(transaction_id)
        self.assertEqual(teams, [{}, {}])

        self.create_dummy_assets(trade_data)
        teams = trade.get_transaction_teams(transaction_id)
        self.assertEqual(teams[0]["id"], TEAM_ONE)
        self.assertEqual(teams[1]["id"], TEAM_TWO)
        self.assertEqual(teams[0]["name"], "Test One")
        self.assertEqual(teams[1]["name"], "Test Two")
        self.assertEqual(teams[0]["logo"], "lol")
        self.assertEqual(teams[1]["logo"], "lol2")

    def test_get_transaction_picks(self):
        TEAM_ONE = 1
        TEAM_TWO = 2
        trade_data = {
            "team_one": TEAM_ONE,
            "team_two": TEAM_TWO,
            "picks_one": [
                {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(21, 30)
            ],
            "picks_two": [
                {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(31, 40)
            ],
            "contracts_one": [],
            "contracts_two": [],
            "future_considerations_one": True,
            "future_considerations_two": True,
            "future_considerations": "Test condition",
        }
        transaction_id = trade.save_proposal(trade_data, 0)
        picks_one = trade.get_transaction_picks(transaction_id, TEAM_ONE)
        picks_two = trade.get_transaction_picks(transaction_id, TEAM_TWO)
        self.assertEqual(picks_one, [])
        self.assertEqual(picks_two, [])

        self.create_dummy_assets(trade_data)
        # Looks up picks going to a team
        picks_one = trade.get_transaction_picks(transaction_id, TEAM_ONE)
        picks_two = trade.get_transaction_picks(transaction_id, TEAM_TWO)
        for idx in range(len(picks_one)):
            self.assertEqual(picks_one[idx]["id"], trade_data["picks_one"][idx]["id"])
            self.assertEqual(picks_one[idx]["originating_team"], "T01")
            self.assertEqual(picks_one[idx]["round"], 5)
            self.assertEqual(picks_one[idx]["season"], date.today().year)
        for idx in range(len(picks_two)):
            self.assertEqual(picks_two[idx]["id"], trade_data["picks_two"][idx]["id"])
            self.assertEqual(picks_two[idx]["originating_team"], "T02")
            self.assertEqual(picks_two[idx]["round"], 8)
            self.assertEqual(picks_two[idx]["season"], date.today().year)

    def test_get_transaction_contracts(self):
        TEAM_ONE = 1
        TEAM_TWO = 2
        trade_data = {
            "team_one": TEAM_ONE,
            "team_two": TEAM_TWO,
            "picks_one": [],
            "picks_two": [],
            "contracts_one": [
                {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(41, 50)
            ],
            "contracts_two": [
                {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(51, 60)
            ],
            "future_considerations_one": True,
            "future_considerations_two": True,
            "future_considerations": "Test condition",
        }
        transaction_id = trade.save_proposal(trade_data, 0)
        contracts_one = trade.get_transaction_contracts(transaction_id, TEAM_ONE)
        contracts_two = trade.get_transaction_contracts(transaction_id, TEAM_TWO)
        self.assertEqual(contracts_one, [])
        self.assertEqual(contracts_two, [])

        self.create_dummy_assets(trade_data)
        contracts_one = trade.get_transaction_contracts(transaction_id, TEAM_ONE)
        contracts_two = trade.get_transaction_contracts(transaction_id, TEAM_TWO)
        for idx in range(len(contracts_one)):
            self.assertEqual(contracts_one[idx]["id"], 0)
            self.assertEqual(contracts_one[idx]["name"], "Test Hmn")
            self.assertEqual(contracts_one[idx]["nhl_salary"], 14)
            self.assertEqual(contracts_one[idx]["cap_hit"], 13)
        for idx in range(len(contracts_two)):
            self.assertEqual(contracts_two[idx]["id"], 0)
            self.assertEqual(contracts_two[idx]["name"], "Test Hmn")
            self.assertEqual(contracts_two[idx]["nhl_salary"], 142)
            self.assertEqual(contracts_two[idx]["cap_hit"], 113)

    def test_get_future_considerations(self):
        TEAM_ONE = 1
        TEAM_TWO = 2
        trade_data = {
            "team_one": TEAM_ONE,
            "team_two": TEAM_TWO,
            "picks_one": [],
            "picks_two": [],
            "contracts_one": [],
            "contracts_two": [],
            "future_considerations_one": True,
            "future_considerations_two": False,
            "future_considerations": "Test condition",
        }
        transaction_id = trade.save_proposal(trade_data, 0)
        future_considerations_one = trade.get_future_considerations(
            transaction_id, TEAM_ONE
        )
        future_considerations_two = trade.get_future_considerations(
            transaction_id, TEAM_TWO
        )
        self.assertEqual(
            future_considerations_one, "Future Considerations:\nTest condition"
        )
        self.assertIsNone(future_considerations_two)

        trade_data["future_considerations_one"] = False
        trade_data["future_considerations_two"] = True
        trade_data["future_considerations"] = ""
        transaction_id = trade.save_proposal(trade_data, 0)
        future_considerations_one = trade.get_future_considerations(
            transaction_id, TEAM_ONE
        )
        future_considerations_two = trade.get_future_considerations(
            transaction_id, TEAM_TWO
        )
        self.assertEqual(future_considerations_two, "Future Considerations")
        self.assertIsNone(future_considerations_one)

    def test_get_transaction_details(self):
        TEAM_ONE = 1
        TEAM_TWO = 2
        trade_data = {
            "team_one": TEAM_ONE,
            "team_two": TEAM_TWO,
            "picks_one": [
                {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(21, 30)
            ],
            "picks_two": [
                {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(31, 40)
            ],
            "contracts_one": [
                {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(41, 50)
            ],
            "contracts_two": [
                {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(51, 60)
            ],
            "future_considerations_one": True,
            "future_considerations_two": True,
            "future_considerations": "Test condition",
        }
        transaction_id = trade.save_proposal(trade_data, 0)
        self.create_dummy_assets(trade_data)
        queried_data = trade.get_transaction_details(transaction_id)
        self.assertEqual(queried_data["clause_warning"], [])
        picks_one = queried_data["picks_one"]
        picks_two = queried_data["picks_two"]
        for idx in range(len(picks_one)):
            self.assertEqual(picks_one[idx]["id"], trade_data["picks_one"][idx]["id"])
            self.assertEqual(picks_one[idx]["originating_team"], "T01")
            self.assertEqual(picks_one[idx]["round"], 5)
            self.assertEqual(picks_one[idx]["season"], date.today().year)
        for idx in range(len(picks_two)):
            self.assertEqual(picks_two[idx]["id"], trade_data["picks_two"][idx]["id"])
            self.assertEqual(picks_two[idx]["originating_team"], "T02")
            self.assertEqual(picks_two[idx]["round"], 8)
            self.assertEqual(picks_two[idx]["season"], date.today().year)
        contracts_one = queried_data["contracts_one"]
        contracts_two = queried_data["contracts_two"]
        for idx in range(len(contracts_one)):
            self.assertEqual(contracts_one[idx]["id"], 0)
            self.assertEqual(contracts_one[idx]["name"], "Test Hmn")
            self.assertEqual(contracts_one[idx]["nhl_salary"], 14)
            self.assertEqual(contracts_one[idx]["cap_hit"], 13)
        for idx in range(len(contracts_two)):
            self.assertEqual(contracts_two[idx]["id"], 0)
            self.assertEqual(contracts_two[idx]["name"], "Test Hmn")
            self.assertEqual(contracts_two[idx]["nhl_salary"], 142)
            self.assertEqual(contracts_two[idx]["cap_hit"], 113)
        self.assertTrue(queried_data["future_considerations_one"])
        self.assertTrue(queried_data["future_considerations_two"])
        self.assertEqual(
            queried_data["future_considerations"],
            "Future Considerations:\nTest condition",
        )
        ContractYear.query.filter(
            ContractYear.contract_id == trade_data["contracts_one"][0]["id"]
        ).filter(ContractYear.season_id == self.app.config["CURRENT_SEASON"]).update(
            {
                "nmc": True,
                "clause_limits": "Not sure",
            }
        )
        queried_data = trade.get_transaction_details(transaction_id)
        self.assertEqual(queried_data["clause_warning"], ["Test Hmn: NMC (Not sure)"])

    def test_get_form_team(self):
        team = Team(id=5, name="Test Team", abbreviation="TT", logo="no")
        db.session.add(team)
        db.session.commit()
        queried = trade.get_form_team(team.id)
        self.assertEqual(team.name, queried["name"])
        self.assertEqual(team.logo, queried["logo"])

    def test_get_form_picks(self):
        TEAM_ONE = 1
        TEAM_TWO = 2
        trade_data = {
            "team_one": TEAM_ONE,
            "team_two": TEAM_TWO,
            "picks_one": [
                {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(21, 30)
            ],
            "picks_two": [
                {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(31, 40)
            ],
            "contracts_one": [],
            "contracts_two": [],
            "future_considerations_one": True,
            "future_considerations_two": True,
            "future_considerations": "Test condition",
        }
        self.create_dummy_assets(trade_data)
        picks_one = trade.get_form_picks(trade_data["picks_one"])
        picks_two = trade.get_form_picks(trade_data["picks_two"])
        picks_three = trade.get_form_picks(trade_data["contracts_one"])
        self.assertEqual(picks_three, [])
        for idx in range(len(picks_one)):
            self.assertEqual(picks_one[idx]["id"], trade_data["picks_one"][idx]["id"])
            self.assertEqual(picks_one[idx]["originating_team"], "T01")
            self.assertEqual(picks_one[idx]["round"], 5)
            self.assertEqual(picks_one[idx]["season"], date.today().year)
        for idx in range(len(picks_two)):
            self.assertEqual(picks_two[idx]["id"], trade_data["picks_two"][idx]["id"])
            self.assertEqual(picks_two[idx]["originating_team"], "T02")
            self.assertEqual(picks_two[idx]["round"], 8)
            self.assertEqual(picks_two[idx]["season"], date.today().year)

    def test_get_form_contracts(self):
        TEAM_ONE = 1
        TEAM_TWO = 2
        trade_data = {
            "team_one": TEAM_ONE,
            "team_two": TEAM_TWO,
            "picks_one": [],
            "picks_two": [],
            "contracts_one": [
                {"id": idx, "retention": "", "fmr": TEAM_ONE, "new": TEAM_TWO}
                for idx in range(41, 50)
            ],
            "contracts_two": [
                {
                    "id": idx,
                    "retention": random.randint(1, 25),
                    "fmr": TEAM_TWO,
                    "new": TEAM_ONE,
                }
                for idx in range(51, 60)
            ],
            "future_considerations_one": True,
            "future_considerations_two": True,
            "future_considerations": "Test condition",
        }
        self.create_dummy_assets(trade_data)
        contracts_one = trade.get_form_contracts(trade_data["contracts_one"])
        contracts_two = trade.get_form_contracts(trade_data["contracts_two"])
        contracts_three = trade.get_form_contracts(trade_data["picks_one"])
        self.assertEqual(contracts_three, [])
        for idx in range(len(contracts_one)):
            self.assertEqual(contracts_one[idx]["id"], 0)
            self.assertEqual(contracts_one[idx]["name"], "Test Hmn")
            self.assertEqual(contracts_one[idx]["nhl_salary"], 14)
            self.assertEqual(contracts_one[idx]["cap_hit"], 13)
        for idx in range(len(contracts_two)):
            self.assertEqual(contracts_two[idx]["id"], 0)
            self.assertEqual(contracts_two[idx]["name"], "Test Hmn")
            self.assertEqual(contracts_two[idx]["nhl_salary"], 142)
            self.assertEqual(contracts_two[idx]["cap_hit"], 113)

    def test_get_form_details(self):
        TEAM_ONE = 1
        TEAM_TWO = 2
        trade_data = {
            "team_one": TEAM_ONE,
            "team_two": TEAM_TWO,
            "picks_one": [
                {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(21, 30)
            ],
            "picks_two": [
                {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(31, 40)
            ],
            "contracts_one": [
                {"id": idx, "retention": "", "fmr": TEAM_ONE, "new": TEAM_TWO}
                for idx in range(41, 50)
            ],
            "contracts_two": [
                {
                    "id": idx,
                    "retention": random.randint(1, 25),
                    "fmr": TEAM_TWO,
                    "new": TEAM_ONE,
                }
                for idx in range(51, 60)
            ],
            "future_considerations_one": True,
            "future_considerations_two": True,
            "future_considerations": "Test condition",
        }
        self.create_dummy_assets(trade_data)
        queried_data = trade.get_form_details(trade_data)
        self.assertEqual(queried_data["clause_warning"], [])
        self.assertEqual(queried_data["team_one"]["name"], "Test One")
        self.assertEqual(queried_data["team_one"]["logo"], "lol")
        self.assertEqual(queried_data["team_two"]["name"], "Test Two")
        self.assertEqual(queried_data["team_two"]["logo"], "lol2")
        contracts_one = queried_data["contracts_one"]
        contracts_two = queried_data["contracts_two"]
        for idx in range(len(contracts_one)):
            self.assertEqual(contracts_one[idx]["id"], 0)
            self.assertEqual(contracts_one[idx]["name"], "Test Hmn")
            self.assertEqual(contracts_one[idx]["nhl_salary"], 14)
            self.assertEqual(contracts_one[idx]["cap_hit"], 13)
        for idx in range(len(contracts_two)):
            self.assertEqual(contracts_two[idx]["id"], 0)
            self.assertEqual(contracts_two[idx]["name"], "Test Hmn")
            self.assertEqual(contracts_two[idx]["nhl_salary"], 142)
            self.assertEqual(contracts_two[idx]["cap_hit"], 113)
        picks_one = queried_data["picks_one"]
        picks_two = queried_data["picks_two"]
        for idx in range(len(picks_one)):
            self.assertEqual(picks_one[idx]["id"], trade_data["picks_one"][idx]["id"])
            self.assertEqual(picks_one[idx]["originating_team"], "T01")
            self.assertEqual(picks_one[idx]["round"], 5)
            self.assertEqual(picks_one[idx]["season"], date.today().year)
        for idx in range(len(picks_two)):
            self.assertEqual(picks_two[idx]["id"], trade_data["picks_two"][idx]["id"])
            self.assertEqual(picks_two[idx]["originating_team"], "T02")
            self.assertEqual(picks_two[idx]["round"], 8)
            self.assertEqual(picks_two[idx]["season"], date.today().year)
        ContractYear.query.filter(
            ContractYear.contract_id == trade_data["contracts_one"][0]["id"]
        ).filter(ContractYear.season_id == self.app.config["CURRENT_SEASON"]).update(
            {"ntc": True}
        )
        queried_data = trade.get_form_details(trade_data)
        self.assertEqual(queried_data["clause_warning"][0], "Test Hmn: NTC")

    def test_query_transaction_team_ids(self):
        transaction_id = 7
        team_one = 9
        team_two = 42
        team_three = 3

        empty = trade.query_transaction_team_ids(transaction_id)
        self.assertEqual(empty, set())

        transaction_asset_one = TransactionAsset(
            transaction_id=transaction_id,
            asset_id=0,
            former_team=team_one,
            new_team=team_two,
        )
        transaction_asset_two = TransactionAsset(
            transaction_id=transaction_id,
            asset_id=1,
            former_team=team_two,
            new_team=team_one,
        )
        db.session.add(transaction_asset_one)
        db.session.add(transaction_asset_two)
        db.session.commit()
        simple_trade = trade.query_transaction_team_ids(transaction_id)
        simple_trade = trade.query_transaction_team_ids(transaction_id)
        self.assertEqual(simple_trade, {team_one, team_two})

        transaction_asset_three = TransactionAsset(
            transaction_id=transaction_id,
            asset_id=2,
            former_team=team_three,
            new_team=team_one,
        )
        db.session.add(transaction_asset_three)
        db.session.commit()
        three_way_trade = trade.query_transaction_team_ids(transaction_id)
        self.assertEqual(three_way_trade, {team_one, team_two, team_three})

    def test_query_retained_assets(self):
        transaction_id = 7
        empty = trade.query_retained_assets(transaction_id, 1)
        self.assertEqual([], empty)

        retained = "Retention: 25%"
        other = "Some other Retention condition"
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=8,
                former_team=1,
                new_team=2,
            )
        )
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=6,
                former_team=1,
                new_team=2,
                condition=retained,
            )
        )
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=4,
                former_team=1,
                new_team=2,
                condition=other,
            )
        )
        queried = trade.query_retained_assets(transaction_id, 1)
        self.assertEqual(1, len(queried))
        self.assertEqual(6, queried[0][0])
        self.assertEqual(retained, queried[0][1])

    def test_get_retained_assets(self):
        transaction_id = 7
        empty = trade.get_retained_assets(transaction_id, 1)
        self.assertEqual([], empty)

        retained = "Retention: 25%"
        other = "Some other Retention condition"
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=8,
                former_team=1,
                new_team=2,
            )
        )
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=6,
                former_team=1,
                new_team=2,
                condition=retained,
            )
        )
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=4,
                former_team=1,
                new_team=2,
                condition=other,
            )
        )
        queried = trade.get_retained_assets(transaction_id, 1)
        self.assertEqual(1, len(queried))
        self.assertEqual(6, queried[0]["id"])
        self.assertEqual(retained, queried[0]["retention"])

    def test_query_get_asset_data(self):
        transaction_id = 7
        assets = [
            {
                "id": random.randint(0, 2000),
                "current_team": random.randint(0, 32),
                "originating_team": random.randint(0, 32),
                "active": True,
            }
            for _ in range(3)
        ]
        transaction_assets = [
            {
                "transaction_id": transaction_id,
                "asset_id": asset["id"],
                "former_team": asset["current_team"],
                "new_team": random.randint(0, 32),
                "condition": "".join(random.choices(LETTERS, k=16)),
            }
            for asset in assets
        ]
        for asset in assets:
            db.session.add(Asset(**asset))
        for transaction_asset in transaction_assets:
            db.session.add(TransactionAsset(**transaction_asset))
        db.session.commit()
        assets.sort(key=lambda x: x["id"])
        transaction_assets.sort(key=lambda x: x["asset_id"])

        queried = trade.query_asset_data(transaction_id)
        self.assertEqual(len(queried), 3)
        for idx in range(len(assets)):
            self.assertEqual(assets[idx]["id"], queried[idx][0])
            self.assertEqual(transaction_assets[idx]["asset_id"], queried[idx][0])
            self.assertEqual(transaction_assets[idx]["former_team"], queried[idx][1])
            self.assertEqual(transaction_assets[idx]["new_team"], queried[idx][2])
            self.assertEqual(assets[idx]["current_team"], queried[idx][3])
            self.assertEqual(assets[idx]["active"], queried[idx][4])
            self.assertEqual(transaction_assets[idx]["condition"], queried[idx][5])

        queried = trade.get_asset_data(transaction_id)
        self.assertEqual(len(queried), 3)
        for idx in range(len(assets)):
            self.assertEqual(assets[idx]["id"], queried[idx]["id"])
            self.assertEqual(transaction_assets[idx]["asset_id"], queried[idx]["id"])
            self.assertEqual(
                transaction_assets[idx]["former_team"], queried[idx]["former_team"]
            )
            self.assertEqual(
                transaction_assets[idx]["new_team"], queried[idx]["new_team"]
            )
            self.assertEqual(assets[idx]["current_team"], queried[idx]["current_team"])
            self.assertEqual(assets[idx]["active"], queried[idx]["active"])
            self.assertEqual(
                transaction_assets[idx]["condition"], queried[idx]["condition"]
            )

    def test_clause_warnings(self):
        self.assertIsNone(
            trade.get_clause_warning(
                [{"id": "1"}, {"id": "2"}, {"id": "3"}, {"id": "4"}, {"id": "5"}]
            )
        )
        years = [
            ContractYear(
                contract_id=1,
                season_id=self.app.config["CURRENT_SEASON"],
                ntc=False,
                nmc=True,
            ),
            ContractYear(
                contract_id=2,
                season_id=self.app.config["CURRENT_SEASON"],
                ntc=True,
                nmc=False,
            ),
            ContractYear(
                contract_id=3,
                season_id=self.app.config["CURRENT_SEASON"],
                ntc=False,
                nmc=False,
            ),
            ContractYear(
                contract_id=4,
                season_id=self.app.config["CURRENT_SEASON"],
                clause_limits="testing",
            ),
            ContractYear(
                contract_id=5,
                season_id=self.app.config["CURRENT_SEASON"],
                ntc=True,
                clause_limits="testing forreal",
            ),
        ]
        contracts = [
            Contract(id=1, person_id=1),
            Contract(id=2, person_id=2),
            Contract(id=3, person_id=3),
            Contract(id=4, person_id=4),
            Contract(id=5, person_id=5),
        ]
        assets = [
            Asset(id=1),
            Asset(id=2),
            Asset(id=3),
            Asset(id=4),
            Asset(id=5),
        ]
        people = [
            Person(id=1, name="Test-1"),
            Person(id=2, name="Test-2"),
            Person(id=3, name="Test-3"),
            Person(id=4, name="Test-4"),
            Person(id=5, name="Test-5"),
        ]
        for year in years:
            db.session.add(year)
        for contract in contracts:
            db.session.add(contract)
        for asset in assets:
            db.session.add(asset)
        for person in people:
            db.session.add(person)
        db.session.commit()
        warning = trade.get_clause_warning(
            [{"id": "1"}, {"id": "2"}, {"id": "3"}, {"id": "4"}, {"id": "5"}]
        )
        self.assertEqual(len(warning), 3)
        self.assertEqual(warning[0], "Test-1: NMC")
        self.assertEqual(warning[1], "Test-2: NTC")
        self.assertEqual(warning[2], "Test-5: NTC (testing forreal)")

        self.assertIsNone(trade.get_transaction_clause_warning("7"))
        transaction_assets = [
            TransactionAsset(transaction_id=7, asset_id=1),
            TransactionAsset(transaction_id=7, asset_id=2),
            TransactionAsset(transaction_id=7, asset_id=3),
            TransactionAsset(transaction_id=7, asset_id=4),
            TransactionAsset(transaction_id=7, asset_id=5),
        ]
        for asset in transaction_assets:
            db.session.add(asset)
        db.session.commit()
        warning = trade.get_transaction_clause_warning("7")
        self.assertEqual(len(warning), 3)
        self.assertEqual(warning[0], "Test-1: NMC")
        self.assertEqual(warning[1], "Test-2: NTC")
        self.assertEqual(warning[2], "Test-5: NTC (testing forreal)")

    def test_get_form_cap_warning(self):
        season_id = self.app.config["CURRENT_SEASON"]
        db.session.add(
            Season(
                id=season_id,
                salary_cap=1000,
                salary_floor=100,
            )
        )
        team_one = 4
        team_two = 5
        db.session.add(Team(id=team_one, name="Test Team One"))
        db.session.add(Team(id=team_two, name="Test Team Two"))

        team_season_one = TeamSeason(
            team_id=team_one,
            season_id=season_id,
            cap_hit=1,
            projected_cap_hit=1,
        )
        team_season_two = TeamSeason(
            team_id=team_two,
            season_id=season_id,
            cap_hit=999,
            projected_cap_hit=999,
        )
        db.session.add(team_season_one)
        db.session.add(team_season_two)

        # asset, person, contract, contract_year
        db.session.add(Asset(id=6, active=True))
        db.session.add(Person(id=7, name="Test One"))
        db.session.add(Contract(id=6, person_id=7))
        contract_year_one = ContractYear(
            contract_id=6, season_id=season_id, cap_hit=5, nhl_salary=5
        )
        db.session.add(contract_year_one)

        db.session.add(Asset(id=8, active=True))
        db.session.add(Person(id=9, name="Test Two"))
        db.session.add(Contract(id=8, person_id=9))
        contract_year_two = ContractYear(
            contract_id=8, season_id=season_id, cap_hit=1, nhl_salary=5
        )
        db.session.add(contract_year_two)
        db.session.commit()

        data = {
            "team_one": team_one,
            "team_two": team_two,
            "contracts_one": [{"id": 6, "retention": 0}],
            "contracts_two": [{"id": 8, "retention": 0}],
        }
        warnings = trade.get_form_cap_warning(data)
        warnings.sort()
        self.assertEqual(len(warnings), 2)
        self.assertEqual(
            warnings[0], "Test Team One: under the cap floor with this trade"
        )
        self.assertEqual(
            warnings[1], "Test Team Two: over the salary cap with this trade"
        )

    def test_get_transaction_cap_warning(self):
        season_id = self.app.config["CURRENT_SEASON"]
        db.session.add(
            Season(
                id=season_id,
                salary_cap=1000,
                salary_floor=100,
                free_agency_opening=date(year=2020, month=6, day=1),
            )
        )
        team_one = 4
        team_two = 5
        db.session.add(Team(id=team_one, name="Test Team One"))
        db.session.add(Team(id=team_two, name="Test Team Two"))

        team_season_one = TeamSeason(
            team_id=team_one,
            season_id=season_id,
            cap_hit=999,
            projected_cap_hit=999,
        )
        team_season_two = TeamSeason(
            team_id=team_two,
            season_id=season_id,
            cap_hit=1,
            projected_cap_hit=1,
        )
        db.session.add(team_season_one)
        db.session.add(team_season_two)

        # asset, person, contract, contract_year
        db.session.add(Asset(id=6, active=True))
        db.session.add(Person(id=7, name="Test One"))
        db.session.add(Contract(id=6, person_id=7))
        contract_year_one = ContractYear(
            contract_id=6, season_id=season_id, cap_hit=1, nhl_salary=5
        )
        db.session.add(contract_year_one)

        db.session.add(Asset(id=8, active=True))
        db.session.add(Person(id=9, name="Test Two"))
        db.session.add(Contract(id=8, person_id=9))
        contract_year_two = ContractYear(
            contract_id=8, season_id=season_id, cap_hit=4, nhl_salary=5
        )
        db.session.add(contract_year_two)

        # transaction
        transaction = Transaction(id=10, date=date(year=2020, month=7, day=1))
        db.session.add(transaction)
        db.session.add(
            TransactionAsset(
                transaction_id=10, asset_id=8, former_team=team_two, new_team=team_one
            )
        )
        db.session.add(
            TransactionAsset(
                transaction_id=10, asset_id=6, former_team=team_one, new_team=team_two
            )
        )
        db.session.commit()

        warnings = trade.get_transaction_cap_warning(transaction.id)
        warnings.sort()
        self.assertEqual(len(warnings), 2)
        self.assertEqual(
            warnings[0], "Test Team One: over the salary cap with this trade"
        )
        self.assertEqual(
            warnings[1], "Test Team Two: under the cap floor with this trade"
        )

    def test_check_previously_retained(self):
        team_id = 5
        assets = [2, 3, 4]
        self.assertEqual(trade.check_previously_retained(team_id, assets), [])
        db.session.add(Asset(id=1))
        db.session.add(Asset(id=2))
        db.session.add(Asset(id=3))
        db.session.add(
            RetainedSalary(
                id=1,
                retained_by=team_id,
                contract_id=2,
            )
        )
        db.session.add(
            RetainedSalary(
                id=2,
                retained_by=team_id,
                contract_id=3,
            )
        )
        db.session.add(
            RetainedSalary(
                id=3,
                retained_by=team_id,
                contract_id=4,
            )
        )
        db.session.commit()
        self.assertEqual(
            trade.check_previously_retained(team_id, assets), [(2,), (3,), (4,)]
        )

    def test_query_contract_data(self):
        asset_id = 847
        team_id = 44
        person_id = 27
        self.assertIsNone(trade.query_contract(asset_id))
        self.assertIsNone(trade.contract_data({"id": asset_id}))

        asset = Asset(
            id=asset_id,
            originating_team=team_id,
            current_team=team_id,
            type="Contract",
            active=True,
        )
        person = Person(
            id=person_id,
            name="Test Person",
        )
        contract = Contract(
            id=asset_id,
            person_id=person.id,
            years=1,
        )
        contract_year = ContractYear(
            contract_id=asset.id,
            season_id=self.app.config["CURRENT_SEASON"],
            cap_hit=123,
            nhl_salary=654,
        )
        db.session.add(asset)
        db.session.add(person)
        db.session.add(contract)
        db.session.add(contract_year)
        db.session.commit()

        raw_data = trade.query_contract(asset_id)
        self.assertEqual(raw_data[0], asset.id)
        self.assertEqual(raw_data[1], person.name)
        self.assertEqual(raw_data[2], contract_year.cap_hit)
        self.assertEqual(raw_data[3], contract_year.nhl_salary)
        self.assertEqual(raw_data[4], person.id)
        self.assertEqual(raw_data[5], asset.type)

        data = trade.contract_data({"id": asset_id, "retention": 0})
        self.assertEqual(data["name"], person.name)
        self.assertEqual(data["cap_hit"], contract_year.cap_hit)
        self.assertEqual(data["nhl_salary"], contract_year.nhl_salary)
        self.assertEqual(data["id"], person.id)
        self.assertEqual(data["type"], asset.type)
        self.assertTrue("retention" not in data)

    def test_query_rights_details(self):
        asset_id = 847
        team_id = 44
        person_id = 88
        self.assertIsNone(trade.query_rights_details(asset_id))

        asset = Asset(
            id=asset_id,
            originating_team=team_id,
            current_team=team_id,
            type="Signing Rights",
            active=True,
        )
        person = Person(
            name="Test Person",
            id=person_id,
        )
        rights = SigningRights(
            id=asset.id,
            person_id=person.id,
        )
        db.session.add(asset)
        db.session.add(person)
        db.session.add(rights)
        db.session.commit()

        raw_data = trade.query_rights_details(asset_id)
        self.assertEqual(raw_data[0], asset.id)
        self.assertEqual(raw_data[1], person.name)
        self.assertEqual(raw_data[2], 0)
        self.assertEqual(raw_data[3], 0)
        self.assertEqual(raw_data[4], person.id)
        self.assertEqual(raw_data[5], asset.type)

        raw_data2 = trade.query_contract(asset_id)
        self.assertEqual(raw_data2[0], asset.id)
        self.assertEqual(raw_data2[1], person.name)
        self.assertEqual(raw_data2[2], 0)
        self.assertEqual(raw_data2[3], 0)
        self.assertEqual(raw_data2[4], person.id)
        self.assertEqual(raw_data2[5], asset.type)

    def test_query_team_contracts(self):
        team_id = 88
        self.assertEqual(trade.query_team_contracts(team_id), [])

        assets = [
            Asset(current_team=team_id, active=True, type="Signing Rights"),
            Asset(current_team=team_id, active=True, type="Contract"),
            Asset(current_team=team_id, active=True, type="Draft Pick"),
            Asset(current_team=team_id, active=False, type="Signing Rights"),
            Asset(current_team=team_id, active=False, type="Contract"),
            Asset(current_team=team_id, active=False, type="Draft Pick"),
        ]
        for asset in assets:
            db.session.add(asset)
        db.session.commit()

        raw_data = trade.query_team_contracts(team_id)
        self.assertTrue((assets[0].id,) in raw_data)
        self.assertTrue((assets[1].id,) in raw_data)

    def test_query_transaction_contracts(self):
        transaction_id = 837
        team_id = 18
        asset_id = 271
        person_id = 2134
        current = Season(
            id=self.app.config["CURRENT_SEASON"],
            free_agency_opening=date(year=2022, month=6, day=1),
        )
        db.session.add(current)
        previous = Season(
            id=self.app.config["CURRENT_SEASON"] + 1,
            free_agency_opening=date(year=2022, month=5, day=1),
        )
        db.session.add(previous)
        transaction = Transaction(
            id=transaction_id,
            date=date(year=2022, month=5, day=15),
        )
        db.session.add(transaction)
        transaction_asset = TransactionAsset(
            transaction_id=transaction_id,
            asset_id=asset_id,
            former_team=team_id,
            new_team=team_id + 1,
        )
        db.session.add(transaction_asset)
        contract = Contract(
            id=asset_id,
            person_id=person_id,
        )
        db.session.add(contract)
        contract_year = ContractYear(
            contract_id=asset_id,
            season_id=previous.id,
            cap_hit=123,
            nhl_salary=654,
        )
        db.session.add(contract_year)
        asset = Asset(
            active=True,
            id=asset_id,
            current_team=18,
            originating_team=18,
            type="Contract",
        )
        db.session.add(asset)
        person = Person(
            id=person_id,
            name="Test Person",
        )
        db.session.add(person)
        db.session.commit()

        raw_data = trade.query_transaction_contracts(transaction_id, team_id)
        self.assertEqual(len(raw_data), 1)
        self.assertEqual(raw_data[0][0], asset.id)
        self.assertEqual(raw_data[0][1], person.id)
        self.assertEqual(raw_data[0][2], person.name)
        self.assertEqual(raw_data[0][3], contract_year.cap_hit)
        self.assertEqual(raw_data[0][4], contract_year.nhl_salary)
        self.assertEqual(raw_data[0][5], transaction_asset.condition)
        self.assertEqual(raw_data[0][6], asset.type)

        contract_year.season_id = current.id
        db.session.add(contract_year)
        db.session.commit()
        raw_data = trade.query_transaction_contracts(transaction_id, team_id)
        self.assertEqual(raw_data, [])

        contract.signing_season_id = previous.id
        raw_data = trade.query_transaction_contracts(transaction_id, team_id)
        self.assertEqual(len(raw_data), 1)
        self.assertEqual(raw_data[0][0], asset.id)
        self.assertEqual(raw_data[0][1], person.id)
        self.assertEqual(raw_data[0][2], person.name)
        self.assertEqual(raw_data[0][3], contract_year.cap_hit)
        self.assertEqual(raw_data[0][4], contract_year.nhl_salary)
        self.assertEqual(raw_data[0][5], transaction_asset.condition)

        asset.active = False
        db.session.add(asset)
        previous_previous = Season(
            id=self.app.config["CURRENT_SEASON"] + 2,
            free_agency_opening=date(year=2022, month=4, day=1),
        )
        db.session.add(previous_previous)
        contract_year.season_id = previous.id
        db.session.add(contract_year)
        contract.signing_season_id = previous_previous.id
        db.session.add(contract)
        transaction.date = date(year=2022, month=4, day=15)
        db.session.add(transaction)
        db.session.commit()
        raw_data = trade.query_transaction_contracts(transaction_id, team_id)
        self.assertEqual(len(raw_data), 1)
        self.assertEqual(raw_data[0][0], asset.id)
        self.assertEqual(raw_data[0][1], person.id)
        self.assertEqual(raw_data[0][2], person.name)
        self.assertEqual(raw_data[0][3], contract_year.cap_hit)
        self.assertEqual(raw_data[0][4], contract_year.nhl_salary)
        self.assertEqual(raw_data[0][5], transaction_asset.condition)

    def test_get_team_contract_count(self):
        TEAM = 123
        db.session.add(Asset(id=1, current_team=TEAM, type="Contract", active=True))
        db.session.add(Asset(id=2, current_team=TEAM, type="Contract", active=True))
        db.session.add(Asset(id=3, current_team=TEAM, type="Contract", active=False))
        db.session.add(
            Asset(id=4, current_team=TEAM, type="Signing Rights", active=True)
        )
        db.session.add(
            Asset(id=5, current_team=TEAM, type="Signing Rights", active=True)
        )
        db.session.commit()
        self.assertEqual(trade.get_team_contract_count(TEAM), 2)


if __name__ == "__main__":
    unittest.main(verbosity=2)
