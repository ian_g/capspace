import sys
import random
import unittest
from datetime import datetime, date
from app import create_app, db
from app.models import Season

from app.admin import season

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class SeasonTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_season_ids(self):
        db.session.add(
            Season(
                id=0,
                name="Test Zero",
                free_agency_opening=date(year=2022, month=1, day=1),
            )
        )
        db.session.add(
            Season(
                id=1,
                name="Test Zero1",
                free_agency_opening=date(year=2022, month=5, day=1),
            )
        )
        db.session.add(
            Season(
                id=2,
                name="Test Zero2",
                free_agency_opening=date(year=2022, month=3, day=1),
            )
        )
        db.session.add(
            Season(
                id=3,
                name="Test One",
                free_agency_opening=date(year=2022, month=9, day=1),
            )
        )
        db.session.add(
            Season(
                id=4,
                name="Test Seven",
                free_agency_opening=date(year=2021, month=1, day=1),
            )
        )
        db.session.add(
            Season(
                id=5,
                name="Test Two",
                free_agency_opening=date(year=2023, month=1, day=1),
            )
        )
        db.session.commit()

        data = season.season_ids()
        self.assertEqual(len(data), 6)
        self.assertEqual(data[0][0], 4)
        self.assertEqual(data[0][1], "Test Seven")
        self.assertEqual(data[1][0], 0)
        self.assertEqual(data[1][1], "Test Zero")
        self.assertEqual(data[2][0], 2)
        self.assertEqual(data[2][1], "Test Zero2")
        self.assertEqual(data[3][0], 1)
        self.assertEqual(data[3][1], "Test Zero1")
        self.assertEqual(data[4][0], 3)
        self.assertEqual(data[4][1], "Test One")
        self.assertEqual(data[5][0], 5)
        self.assertEqual(data[5][1], "Test Two")

    def test_valid_id(self):
        db.session.add(
            Season(
                id=0,
                name="Test Zero",
                free_agency_opening=date(year=2022, month=1, day=1),
            )
        )
        db.session.add(
            Season(
                id=1,
                name="Test Zero1",
                free_agency_opening=date(year=2022, month=5, day=1),
            )
        )
        db.session.add(
            Season(
                id=2,
                name="Test Zero2",
                free_agency_opening=date(year=2022, month=3, day=1),
            )
        )
        db.session.add(
            Season(
                id=3,
                name="Test One",
                free_agency_opening=date(year=2022, month=9, day=1),
            )
        )
        db.session.add(
            Season(
                id=4,
                name="Test Seven",
                free_agency_opening=date(year=2021, month=1, day=1),
            )
        )
        db.session.add(
            Season(
                id=5,
                name="Test Two",
                free_agency_opening=date(year=2023, month=1, day=1),
            )
        )
        db.session.commit()

        self.assertFalse(season.valid_id(-1))
        self.assertFalse(season.valid_id(6))
        self.assertFalse(season.valid_id("Zero"))
        self.assertFalse(season.valid_id("Test Zero"))
        self.assertFalse(season.valid_id("idk"))
        self.assertFalse(season.valid_id(3.14159))
        self.assertTrue(season.valid_id(0))
        self.assertTrue(season.valid_id(1))
        self.assertTrue(season.valid_id(2))
        self.assertTrue(season.valid_id(3))
        self.assertTrue(season.valid_id(4))
        self.assertTrue(season.valid_id(5))

    def test_set_data(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = season.SeasonForm()
        season_record = Season(
            id=8,
            name="2012-2013",
            salary_cap=70200000,
            salary_floor=54200000,
            minimum_salary=525000,
            max_buriable_hit=900000,
            bonus_overage_limit=5625000,
            draft_date=date(year=2013, month=6, day=30),
            free_agency_opening=date(year=2012, month=7, day=1),
            regular_season_start=date(year=2013, month=1, day=19),
            playoffs_start=date(year=2013, month=4, day=30),
            max_elc_salary=925000,
            max_elc_minor_salary=70000,
            max_elc_performance_bonuses=2850000,
        )
        db.session.add(season_record)
        db.session.commit()

        form.set_data(season_record.id)
        self.assertEqual(season_record.id, form.id.data)
        self.assertEqual(season_record.name, form.name.data)
        self.assertEqual(season_record.salary_cap, form.salary_cap.data)
        self.assertEqual(season_record.salary_floor, form.salary_floor.data)
        self.assertEqual(season_record.minimum_salary, form.minimum_salary.data)
        self.assertEqual(season_record.max_buriable_hit, form.max_buriable.data)
        self.assertEqual(season_record.bonus_overage_limit, form.bonus_overage.data)
        self.assertEqual(season_record.draft_date, form.draft_date.data)
        self.assertEqual(
            season_record.free_agency_opening, form.free_agency_opening.data
        )
        self.assertEqual(season_record.regular_season_start, form.regular_season.data)
        self.assertEqual(season_record.playoffs_start, form.playoffs.data)
        self.assertEqual(season_record.max_elc_salary, form.max_elc_salary.data)
        self.assertEqual(season_record.max_elc_minor_salary, form.max_elc_minors.data)
        self.assertEqual(
            season_record.max_elc_performance_bonuses, form.max_elc_performance.data
        )

        ctx.pop()

    def test_update_season(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = season.SeasonForm()
        season_record = Season(
            id=8,
            name="2012-2013",
            salary_cap=70200000,
            salary_floor=54200000,
            minimum_salary=525000,
            max_buriable_hit=900000,
            bonus_overage_limit=5625000,
            draft_date=date(year=2013, month=6, day=30),
            free_agency_opening=date(year=2012, month=7, day=1),
            regular_season_start=date(year=2013, month=1, day=19),
            playoffs_start=date(year=2013, month=4, day=30),
            max_elc_salary=925000,
            max_elc_minor_salary=70000,
            max_elc_performance_bonuses=2850000,
        )
        db.session.add(season_record)
        db.session.commit()

        form.set_data(season_record.id)
        form.id.data = 18
        form.name.data = "2018-2019"
        form.salary_cap.data = 64300000
        form.salary_floor.data = 4400000
        form.minimum_salary.data = 550000
        form.max_buriable.data = 925000
        form.bonus_overage.data = 4822500
        form.draft_date.data = date(year=2014, month=6, day=27)
        form.free_agency_opening.data = date(year=2013, month=7, day=1)
        form.regular_season.data = date(year=2013, month=10, day=1)
        form.playoffs.data = date(year=2014, month=4, day=16)
        form.max_elc_salary.data = 925000
        form.max_elc_minors.data = 70000
        form.max_elc_performance.data = 2850000

        try:
            form.update_season()
        except AttributeError:
            pass
        form.id.data = 8
        form.update_season()
        self.assertEqual(season_record.id, 8)
        self.assertEqual(season_record.name, "2012-2013")
        self.assertEqual(season_record.salary_cap, form.salary_cap.data)
        self.assertEqual(season_record.salary_floor, form.salary_floor.data)
        self.assertEqual(season_record.minimum_salary, form.minimum_salary.data)
        self.assertEqual(season_record.max_buriable_hit, form.max_buriable.data)
        self.assertEqual(season_record.bonus_overage_limit, form.bonus_overage.data)
        self.assertEqual(season_record.draft_date, form.draft_date.data)
        self.assertEqual(
            season_record.free_agency_opening, form.free_agency_opening.data
        )
        self.assertEqual(season_record.regular_season_start, form.regular_season.data)
        self.assertEqual(season_record.playoffs_start, form.playoffs.data)
        self.assertEqual(season_record.max_elc_salary, form.max_elc_salary.data)
        self.assertEqual(season_record.max_elc_minor_salary, form.max_elc_minors.data)


if __name__ == "__main__":
    unittest.main(verbosity=2)
