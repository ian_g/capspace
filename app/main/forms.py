import json
from flask import current_app, request
from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    SubmitField,
    SelectField,
    SelectMultipleField,
    TextAreaField,
    BooleanField,
    HiddenField,
)

# from wtforms.ext.sqlalchemy.fields import QuerySelectField
#   Unused in BuyoutForm because I wanted an "All" choice for team
#   Also, most of the code to just use SelectField was already present
#   Also, it's been deprecated
from wtforms.validators import DataRequired, Length
from ..models import User, Team
from .team import get_teams
from .season import get_draft_years, get_current_draft_year
from .free_agency import get_pending_rfas
from . import buyout, trade, team_season


class BuyoutForm(FlaskForm):
    def __init__(self):
        super().__init__()
        self.team.choices = [("", "All")] + get_teams()
        self.person.choices = buyout.get_buyout_candidates()

    # Swap for Buyout After season
    team = SelectField("Team", default="", id="team")
    person = SelectField("Player", validators=[DataRequired()], id="players")
    buyout_after = SelectField(
        "Buyout After", validate_choice=False, choices=[], validators=[DataRequired()]
    )
    submit = SubmitField("Buy Out")


class QualifyingOffer(FlaskForm):
    def __init__(self):
        super().__init__()
        self.team.choices = [("", "All")] + get_teams()
        self.person.choices = get_pending_rfas()

    team = SelectField("Team", default="", id="team")
    person = SelectField("Player", validators=[DataRequired()], id="players")
    submit = SubmitField("Qualify", id="qualify")


class TradeProposal(FlaskForm):
    def __init__(self):
        super().__init__()
        self.team_one.choices = get_teams()
        self.team_two.choices = get_teams()

    def validate(self, extra_validators=None):
        if not super().validate(extra_validators=extra_validators):
            return False
        if self.team_one.data == self.team_two.data:
            self.team_one.errors.append("One team cannot trade with itself")
            self.team_two.errors.append("One team cannot trade with itself")
        if not self.valid_json(self.picks_one):
            current_app.logger.error(
                "TradeProposal.validate.valid_json - Picks One - Invalid JSON"
            )
            return False
        if not self.valid_json(self.picks_two):
            current_app.logger.error(
                "TradeProposal.validate.valid_json - Picks Two - Invalid JSON"
            )
            return False
        if not self.valid_json(self.contracts_one):
            current_app.logger.error(
                "TradeProposal.validate.valid_json - Contracts One - Invalid JSON"
            )
            return False
        if not self.valid_json(self.contracts_two):
            current_app.logger.error(
                "TradeProposal.validate.valid_json - Contracts Two - Invalid JSON"
            )
            return False
        if not self.check_contracts(self.team_one.data, self.contracts_one):
            current_app.logger.error(
                "TradeProposal.validate.check_contracts - Team One - Invalid Contracts"
            )
            return False
        if not self.check_contracts(self.team_two.data, self.contracts_two):
            current_app.logger.error(
                "TradeProposal.validate.check_contracts - Team Two - Invalid Contracts"
            )
            return False
        if not self.check_picks(self.team_one.data, self.picks_one):
            current_app.logger.error(
                "TradeProposal.validate.check_picks - Team One - Invalid Picks"
            )
            return False
        if not self.check_picks(self.team_two.data, self.picks_two):
            current_app.logger.error(
                "TradeProposal.validate.check_picks - Team Two - Invalid Picks"
            )
            return False
        if not self.check_contract_count():
            current_app.logger.error(
                "TradeProposal.validate.check_contract_count - Invalid contract count"
            )
            return False
        if not self.check_retention():
            current_app.logger.error(
                "TradeProposal.validate.check_retention - Invalid retention amount"
            )
            return False
        if not self.check_retained(self.contracts_one, self.team_one):
            current_app.logger.error(
                "TradeProposal.validate.check_retained - Team One - Too many retained"
            )
            return False
        if not self.check_retained(self.contracts_two, self.team_two):
            current_app.logger.error(
                "TradeProposal.validate.check_retained - Team Two - Too many retained"
            )
            return False
        if not self.check_previously_retained():
            current_app.logger.error(
                "TradeProposal.validate.check_previously_retained - Cannot re-acquire a previously retained contract"
            )
            return False
        return True

    def valid_json(self, form_field):
        try:
            json.loads(form_field.data)
            return True
        except TypeError:
            form_field.errors.append(
                "JSON variable type must be str, bytes or bytearray"
            )
        except json.decoder.JSONDecodeError:
            form_field.errors.append("Incorrectly formed JSON object")
        return False

    def check_contracts(self, team_id, contracts_field):
        data = json.loads(contracts_field.data)
        errors = trade.check_contracts(team_id, data)
        for error in errors:
            contracts_field.errors.append(error)
        return errors == []

    def check_picks(self, team_id, picks_field):
        data = json.loads(picks_field.data)
        errors = trade.check_picks(team_id, data)
        for error in errors:
            picks_field.errors.append(error)
        return errors == []

    def check_contract_count(self):
        data_one = json.loads(self.contracts_one.data)
        data_two = json.loads(self.contracts_two.data)
        if len(data_one) == len(data_two):
            return True
        count_one = trade.get_team_contract_count(self.team_one.data)
        count_two = trade.get_team_contract_count(self.team_two.data)
        current_app.logger.debug(
            f"New contract count 1: {count_one - len(data_one) + len(data_two)}"
        )
        current_app.logger.debug(
            f"New contract count 2: {count_two - len(data_two) + len(data_one)}"
        )
        if count_one - len(data_one) + len(data_two) > 50:
            current_app.logger.debug(f"Team 1: 50 contract count exceeded")
            self.contracts_one.errors.append(
                "Exceeded 50 contract count. More contracts need to be traded away"
            )
            return False
        if count_two - len(data_two) + len(data_one) > 50:
            current_app.logger.debug(f"Team 2: 50 contract count exceeded")
            self.contracts_two.errors.append(
                "Exceeded 50 contract count. More contracts need to be traded away"
            )
            return False
        return True

    def check_retention(self):
        data_one = json.loads(self.contracts_one.data)
        data_two = json.loads(self.contracts_two.data)
        errors = False
        for contract in data_one:
            if "retention" not in contract:
                continue
            retention = int(contract["retention"])
            if retention > 50 or retention < 0:
                errors = True
                message = (
                    f"Contract {contract['id']}: Retention outside 1% - 50% limits"
                )
                self.contracts_one.errors.append(message)
        for contract in data_two:
            if "retention" not in contract:
                continue
            retention = int(contract["retention"])
            if retention > 50 or retention < 0:
                errors = True
                message = (
                    f"Contract {contract['id']}: Retention outside 1% - 50% limits"
                )
                self.contracts_two.errors.append(message)
        return errors == False

    def check_retained(self, contracts_field, team):
        data = json.loads(contracts_field.data)
        retained_count = 0
        for contract in data:
            if contract["retention"] == 0:
                continue
            retained_count += 1
        season_data = team_season.get_team_season(team.data, None)
        total_retained = season_data["retained_contracts"] + retained_count
        if total_retained > 3:
            contracts_field.errors.append(
                f"Team {team.data} cannot retain more than three contracts"
            )
            return False
        return True

    def check_previously_retained(self):
        team_one = self.team_one.data
        team_two = self.team_two.data
        contracts_one = json.loads(self.contracts_one.data)
        contracts_two = json.loads(self.contracts_two.data)
        ids_one = [contract["id"] for contract in contracts_one]
        ids_two = [contract["id"] for contract in contracts_two]
        no_errors = True
        if trade.check_previously_retained(team_one, ids_two) != []:
            no_errors = False
            self.contracts_one.errors.append(
                f"Team {team_one} cannot reacquire a previously retained contract"
            )
        if trade.check_previously_retained(team_two, ids_one) != []:
            no_errors = False
            self.contracts_two.errors.append(
                f"Team {team_two} cannot reacquire a previously retained contract"
            )
        return no_errors

    team_one = SelectField(
        "Team", default="", validators=[DataRequired()], id="team_one"
    )
    team_two = SelectField(
        "Team", default="", validators=[DataRequired()], id="team_two"
    )
    picks_one = HiddenField(default="", id="picks_one")
    picks_two = HiddenField(default="", id="picks_two")
    contracts_one = HiddenField(default="", id="contracts_one")
    contracts_two = HiddenField(default="", id="contracts_two")
    future_considerations_one = BooleanField(
        "Future Considerations", default="", id="fut_con_one"
    )
    future_considerations_two = BooleanField(
        "Future Considerations", default="", id="fut_con_two"
    )
    future_considerations = TextAreaField(
        "Future Considerations Details", default="", validators=[Length(min=0, max=512)]
    )
    submit = SubmitField("Validate Trade", id="submit")


class SaveTradeProposal(FlaskForm):
    submit = SubmitField("Save Trade Proposal")


class SelectDraftSeason(FlaskForm):
    def __init__(self):
        super().__init__()
        self.season.choices = get_draft_years()

    season = SelectField("Season", validators=[DataRequired()], id="season")
    submit = SubmitField("Select Draft Year")


class SearchForm(FlaskForm):
    def __init__(self, *args, **kwargs):
        if "formdata" not in kwargs:
            kwargs["formdata"] = request.args
        if "meta" not in kwargs:
            kwargs["meta"] = {"csrf": False}
        super(SearchForm, self).__init__(*args, **kwargs)

    search = StringField("🔎", validators=[DataRequired()])
