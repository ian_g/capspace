from app import create_app, db
from app.models import Person, Alias


def main():
    normalize_map = {
        "Á": "A",
        "À": "A",
        "Â": "A",
        "Ä": "A",
        "Ã": "A",
        "Å": "A",
        "Ā": "A",
        "Ç": "C",
        "Č": "C",
        "Ð": "D",
        "Ď": "D",
        "É": "E",
        "È": "E",
        "Ë": "E",
        "Ê": "E",
        "Í": "I",
        "Ì": "I",
        "Ï": "I",
        "Î": "I",
        "Ī": "I",
        "Ĺ": "L",
        "Ļ": "L",
        "Ñ": "N",
        "Ň": "N",
        "Ń": "N",
        "Ņ": "N",
        "Ó": "O",
        "Ò": "O",
        "Ö": "O",
        "Ô": "O",
        "Õ": "O",
        "Ø": "O",
        "Ř": "R",
        "Ŕ": "R",
        "Š": "S",
        "Ť": "T",
        "Ú": "U",
        "Ù": "U",
        "Ü": "U",
        "Û": "U",
        "Ů": "U",
        "Ū": "U",
        "Ý": "Y",
        "Ÿ": "Y",
        "Ž": "Z",
        "Ź": "Z",
        "Ż": "Z",
        "á": "a",
        "à": "a",
        "â": "a",
        "ä": "a",
        "ã": "a",
        "å": "a",
        "ā": "a",
        "ç": "c",
        "č": "c",
        "ð": "d",
        "ď": "d",
        "é": "e",
        "è": "e",
        "ë": "e",
        "ê": "e",
        "í": "i",
        "ì": "i",
        "ï": "i",
        "î": "i",
        "ī": "i",
        "ĺ": "l",
        "ļ": "l",
        "ñ": "n",
        "ň": "n",
        "ń": "n",
        "ņ": "n",
        "ó": "o",
        "ò": "o",
        "ö": "o",
        "ô": "o",
        "õ": "o",
        "ø": "o",
        "ř": "r",
        "ŕ": "r",
        "š": "s",
        "ť": "t",
        "ú": "u",
        "ù": "u",
        "ü": "u",
        "û": "u",
        "ů": "u",
        "ū": "u",
        "ý": "y",
        "ÿ": "y",
        "ž": "z",
        "ź": "z",
        "ż": "z",
        "Æ": "AE",
        "æ": "ae",
        "Œ": "OE",
        "œ": "oe",
        "Þ": "P",
        "þ": "p",
        "ß": "ss",
    }
    normalize = str.maketrans(normalize_map)

    app = create_app("default")
    app_context = app.app_context()
    app_context.push()

    for person in Person.query.all():
        person.normalized_name = person.name.translate(normalize)
        db.session.add(
            Alias(
                person_id=person.id,
                value=person.name,
                search=True,
            )
        )
        if person.name != person.name.translate(normalize):
            db.session.add(
                Alias(
                    person_id=person.id,
                    search=True,
                    value=person.name.translate(normalize),
                )
            )
    db.session.commit()


if __name__ == "__main__":
    main()
