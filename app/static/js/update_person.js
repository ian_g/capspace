document.getElementById("add-status").onclick = addSelectedStatus;
document.getElementById("add-alias").onclick = addAlias;
setDate();

let validatePerson = document.getElementById("validate");
validatePerson.onclick = validate;

let person = document.getElementById("person");
person.onchange = removeIcon;

document.querySelectorAll("option").forEach(node => node.selected = false);

if (document.getElementById("person").value != "") {
    validate();
}

function setDate() {
    let date = (new URLSearchParams(window.location.search)).get('date');
    if (date) {
        document.getElementById('date').value = date;
    }
}

function addSelectedStatus() {
    let statusValue = document.getElementById("new-status").value;
    let statusSelector = document.getElementById("current_status");
    let newStatus = document.createElement("option");
    newStatus.value = statusValue;
    newStatus.innerText = statusValue;
    newStatus.selected = true;
    statusSelector.appendChild(newStatus);
}

function addAlias() {
    let aliasValue = document.getElementById("new-alias").value;
    let aliasSelector = document.getElementById("aliases");
    let newAlias = document.createElement("option");
    newAlias.value = aliasValue;
    newAlias.innerText = aliasValue;
    newAlias.selected = true;
    aliasSelector.appendChild(newAlias);
}

async function validate() {
    let name = document.getElementById("person").value;
    let response = await fetch(`/admin/api/validate_person/${name}`);
    let data = await response.json();
    console.log(data);
    let iconText = "✅";
    if (data.error) {
        alert(`Invalid name: ${data.error}`);
        iconText = "❌";
    }
    let icon = document.createElement("span");
    icon.classList.add("validation");
    icon.innerText = iconText;
    document.getElementById("person").parentElement.appendChild(icon);
    if (iconText != "✅") {
        return;
    }

    //Set values
    document.getElementById("weight").value = data.weight;
    document.getElementById("number").value = data.number;
    document.getElementById("active").checked = data.active;
    document.getElementById("captain").checked = data.captain;
    document.getElementById("alternate_captain").checked = data.alternate_captain;
    document.getElementById("rookie").checked = data.rookie;
    document.getElementById("non_roster").checked = data.non_roster;
    document.getElementById("nhl_id").value = data.nhl_id;
    document.getElementById("id").value = data.id;
    document.getElementById("person").value = data.name;
    document.getElementById("ep_id").value = data.ep_id;
    let statusBox = document.getElementById("current_status");
    let currentStatus = data.current_status.split(",");
    for (let state of currentStatus) {
        if (!state) {
            continue;
        }
        let option = statusBox.querySelector(`option[value='${state}']`);
        if (!option) {
            option = document.createElement("option");
            option.value = state;
            option.innerText = state;
            option.selected = true;
            statusBox.appendChild(option);
        } else {
            option.selected = true;
        }
    }
    let aliasBox = document.getElementById("aliases");
    console.log(data.aliases);
    for (let alias of data.aliases) {
        if (!alias) {
            continue;
        }
        let option = aliasBox.querySelector(`option[value='${alias}']`);
        if (!option) {
            option = document.createElement("option");
            option.value = alias;
            option.innerText = alias;
            option.selected = true;
            aliasBox.appendChild(option);
        } else {
            option.selected = true;
        }
    }

    //Display static information
    let staticData = document.createElement("p");
    staticData.appendChild(createNode("Birthdate", data.birthdate));
    staticData.appendChild(createNode("Birthplace", data.birthplace));
    staticData.appendChild(createNode("Nationality", data.nationality));
    staticData.appendChild(createNode("Position", data.position));
    document.getElementById("static-data").appendChild(staticData);
}

function createNode(title, value) {
    let span = document.createElement("span");
    let titleElement = document.createElement("strong");
    titleElement.innerText = title;
    span.appendChild(titleElement);
    span.append(`: ${value}`);
    span.append(document.createElement("br"));
    return span;
}

function removeIcon() {
    let iconParent = document.getElementById("person").parentElement;
    iconParent.querySelectorAll(".validation").forEach(node => node.remove());
}
