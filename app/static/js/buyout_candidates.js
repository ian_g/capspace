let teamSelector = document.getElementById("team");
teamSelector.value = "";
teamSelector.onchange=(event => updatePlayers(event.srcElement.value));

async function updatePlayers(teamId) {
    if (teamId == "") {
        displayAllPlayers();
    } else {
        let buyoutCandidates = await getBuyoutCandidates(teamId);
        updatePlayerSelector(buyoutCandidates);
    }
}

async function getBuyoutCandidates(teamId) {
    let response = await fetch(`/api/buyout_candidates/${teamId}`);
    let userIDs = await response.json();
    return userIDs;
}

function updatePlayerSelector(playerIDs) {
    let playerSelector = document.getElementById("players");
    playerSelector.childNodes.forEach(
        player => playerIDs.includes(player.value) ? player.style.display = "block" : player.style.display = "none"
    );
}

function displayAllPlayers() {
    let playerSelector = document.getElementById("players");
    playerSelector.childNodes.forEach(player => player.style.display = "block");
}
