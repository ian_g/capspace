import sys
import random
import unittest
from datetime import datetime, date
from app import create_app, db
from app.models import (
    Asset,
    Contract,
    ContractYear,
    Note,
    Person,
    Team,
    Transaction,
    TransactionAsset,
    Season,
    SigningRights,
)

from app.admin import adjust_roster

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class AdjustRosterTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app.config.update(SERVER_NAME="127.0.0.1:5000")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_get_teams(self):
        self.assertEqual(adjust_roster.get_teams(), [])

        team = Team(name="Test Zebra")
        db.session.add(team)
        db.session.commit()
        self.assertEqual(adjust_roster.get_teams(), [(1, "Test Zebra")])

        team = Team(name="Test Puppy")
        db.session.add(team)
        db.session.commit()
        self.assertEqual(
            adjust_roster.get_teams(), [(2, "Test Puppy"), (1, "Test Zebra")]
        )

    def test_waive(self):
        PERSON = 97
        CONTRACT = 274
        TEAM = 382
        self.assertEqual(
            adjust_roster.reassign(PERSON, None, "Waived", None, "On Waivers"),
            {"code": 500, "error": "Unused person ID", "result": "error"},
        )

        db.session.add(Person(id=PERSON, name="Test"))
        db.session.add(Asset(id=CONTRACT, active=True, current_team=TEAM))
        db.session.add(Contract(id=CONTRACT, person_id=PERSON))
        db.session.commit()

        self.assertEqual(Transaction.query.count(), 0)
        self.assertEqual(TransactionAsset.query.count(), 0)
        self.assertEqual(
            adjust_roster.reassign(PERSON, None, "Waived", None, "On Waivers"),
            {"code": 200, "result": "success"},
        )
        self.assertEqual(Transaction.query.count(), 1)
        self.assertEqual(TransactionAsset.query.count(), 1)
        transaction = Transaction.query.first()
        tr_asset = TransactionAsset.query.first()
        self.assertEqual(transaction.season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(transaction.type, "Waived")
        self.assertEqual(tr_asset.asset_id, CONTRACT)
        self.assertEqual(tr_asset.transaction_id, transaction.id)
        self.assertEqual(tr_asset.former_team, TEAM)

        PERSON += 1
        CONTRACT += 1
        db.session.add(Person(id=PERSON, name="Test 2"))
        db.session.add(Asset(id=CONTRACT, active=True, current_team=TEAM))
        db.session.add(Contract(id=CONTRACT, person_id=PERSON))
        db.session.commit()

        self.assertEqual(Transaction.query.count(), 1)
        self.assertEqual(TransactionAsset.query.count(), 1)
        self.assertEqual(
            adjust_roster.reassign(PERSON, None, "Waived", "2020-08-18", "On Waivers"),
            {"code": 200, "result": "success"},
        )
        self.assertEqual(Transaction.query.count(), 2)
        self.assertEqual(TransactionAsset.query.count(), 2)
        tr_asset = TransactionAsset.query.filter(
            TransactionAsset.asset_id == CONTRACT
        ).first()
        transaction = Transaction.query.get(tr_asset.transaction_id)
        self.assertEqual(transaction.season_id, self.app.config["CURRENT_SEASON"])
        self.assertEqual(transaction.type, "Waived")
        self.assertEqual(tr_asset.asset_id, CONTRACT)
        self.assertEqual(tr_asset.transaction_id, transaction.id)
        self.assertEqual(tr_asset.former_team, TEAM)
        self.assertEqual(transaction.date, datetime(year=2020, month=8, day=18))

    def test_loan(self):
        PERSON = 9
        CONTRACT = 74
        TEAM = 38
        self.assertEqual(
            adjust_roster.reassign(PERSON, "on_loan", "Loaned"),
            {"code": 500, "result": "error", "error": "Unused person ID"},
        )

        db.session.add(
            Person(id=PERSON, name="Test", birthdate=date(year=2003, month=1, day=2))
        )
        db.session.add(Asset(id=CONTRACT, active=True, current_team=TEAM))
        db.session.add(Team(id=TEAM, abbreviation="TST", name="Test Team"))
        db.session.add(
            Season(
                id=self.app.config["CURRENT_SEASON"],
                regular_season_start=date(year=2022, month=1, day=1),
            )
        )
        db.session.add(
            contract := Contract(
                id=CONTRACT, non_roster=False, on_loan=False, person_id=PERSON
            )
        )
        db.session.commit()

        self.assertEqual(Transaction.query.count(), 0)
        self.assertFalse(contract.non_roster)
        self.assertFalse(contract.on_loan)
        self.assertEqual(adjust_roster.loan(PERSON), {"code": 200, "result": "success"})
        self.assertTrue(contract.on_loan)
        self.assertEqual(Transaction.query.count(), 1)
        transaction = Transaction.query.first()
        transaction_asset = TransactionAsset.query.first()
        self.assertEqual(transaction.type, "Loaned")
        self.assertEqual(transaction_asset.asset_id, CONTRACT)
        self.assertEqual(transaction_asset.new_team, TEAM)
        self.assertEqual(transaction_asset.former_team, TEAM)

    def test_reassign(self):
        PERSON = 9
        CONTRACT = 74
        TEAM = 38
        self.assertEqual(
            adjust_roster.reassign(PERSON, "non_roster", "Reassigned to NHL"),
            {"code": 500, "result": "error", "error": "Unused person ID"},
        )

        db.session.add(
            Person(id=PERSON, name="Test", birthdate=(date.today().replace(year=1993)))
        )
        db.session.add(Asset(id=CONTRACT, active=True, current_team=TEAM))
        db.session.add(Team(id=TEAM, abbreviation="TST", name="Test Team"))
        db.session.add(
            contract := Contract(id=CONTRACT, non_roster=False, person_id=PERSON)
        )
        db.session.add(
            Season(
                id=self.app.config["CURRENT_SEASON"], regular_season_start=date.today()
            )
        )
        db.session.commit()

        self.assertEqual(Transaction.query.count(), 0)
        self.assertFalse(contract.non_roster)
        self.assertEqual(
            adjust_roster.reassign(PERSON, "non_roster", "Reassigned to Minors"),
            {"code": 200, "result": "success"},
        )
        self.assertTrue(contract.non_roster)
        self.assertFalse(contract.on_loan)
        self.assertEqual(Transaction.query.count(), 1)
        transaction = Transaction.query.first()
        transaction_asset = TransactionAsset.query.first()
        self.assertEqual(transaction.type, "Reassigned to Minors")
        self.assertEqual(transaction_asset.asset_id, CONTRACT)
        self.assertEqual(transaction_asset.new_team, TEAM)
        self.assertEqual(transaction_asset.former_team, TEAM)
        contract.on_loan = True
        db.session.add(contract)
        db.session.commit()
        self.assertEqual(
            adjust_roster.reassign(PERSON, "non_roster", "Reassigned to NHL"),
            {"code": 200, "result": "success"},
        )
        self.assertFalse(contract.non_roster)
        self.assertFalse(contract.on_loan)
        self.assertEqual(Transaction.query.count(), 2)
        self.assertEqual(TransactionAsset.query.count(), 2)
        transaction = Transaction.query.order_by(Transaction.id.desc()).first()
        transaction_asset = TransactionAsset.query.order_by(
            TransactionAsset.id.desc()
        ).first()
        self.assertEqual(transaction.type, "Reassigned to NHL")
        self.assertEqual(transaction_asset.asset_id, CONTRACT)
        self.assertEqual(transaction_asset.new_team, TEAM)
        self.assertEqual(transaction_asset.former_team, TEAM)

        self.assertEqual(
            adjust_roster.reassign(
                PERSON, "non_roster", "Reassigned to Minors", "2020-10-10"
            ),
            {"code": 200, "result": "success"},
        )
        self.assertTrue(contract.non_roster)
        self.assertFalse(contract.on_loan)
        self.assertEqual(Transaction.query.count(), 3)
        self.assertEqual(TransactionAsset.query.count(), 3)
        transaction = Transaction.query.order_by(Transaction.id.desc()).first()
        self.assertEqual(transaction.date, datetime(year=2020, month=10, day=10))

    def test_clear(self):
        ASSET = 23
        PERSON = 128
        TRANSACTION_ASSET = 237
        TRANSACTION = 238
        TEAM = 38

        self.assertEqual(
            adjust_roster.clear(TRANSACTION_ASSET),
            {"code": 500, "result": "error", "error": "Unused transaction asset ID"},
        )

        asset = Asset(id=ASSET, current_team=TEAM, active=True)
        person = Person(
            id=PERSON,
            name="Test",
            current_status="Drafted,On Waivers",
            birthdate=date.today().replace(year=1993),
        )
        contract = Contract(id=ASSET, person_id=PERSON, non_roster=False)
        season = Season(
            id=self.app.config["CURRENT_SEASON"], regular_season_start=date.today()
        )
        transaction = Transaction(id=TRANSACTION, type="Waivers")
        transaction_asset = TransactionAsset(
            id=TRANSACTION_ASSET,
            transaction_id=TRANSACTION,
            asset_id=ASSET,
            former_team=TEAM,
        )

        db.session.add(person)
        db.session.add(asset)
        db.session.add(contract)
        db.session.add(season)
        db.session.add(transaction)
        db.session.add(transaction_asset)
        db.session.commit()

        self.assertEqual(
            adjust_roster.clear(TRANSACTION_ASSET), {"code": 200, "result": "success"}
        )
        clear = Transaction.query.filter(Transaction.type == "Cleared Waivers").all()
        self.assertEqual(len(clear), 1)
        clear_asset = TransactionAsset.query.filter(
            TransactionAsset.transaction_id == clear[0].id
        ).first()
        minors = Transaction.query.filter(
            Transaction.type.like("Reassigned to Minors")
        ).all()
        self.assertEqual(len(minors), 1)
        minors_asset = TransactionAsset.query.filter(
            TransactionAsset.transaction_id == minors[0].id
        ).first()
        self.assertEqual(transaction_asset.former_team, transaction_asset.new_team)
        self.assertEqual(contract.non_roster, True)
        self.assertTrue("On Waivers" not in person.current_status)
        self.assertEqual(clear_asset.former_team, TEAM)
        self.assertEqual(clear_asset.new_team, TEAM)
        self.assertEqual(clear_asset.asset_id, ASSET)
        self.assertEqual(minors_asset.former_team, TEAM)
        self.assertEqual(minors_asset.new_team, TEAM)
        self.assertEqual(minors_asset.asset_id, ASSET)

        transaction_asset.new_team = None
        contract.non_roster = False
        person.current_status += ",On Waivers"
        db.session.delete(clear[0])
        db.session.delete(clear_asset)
        db.session.add(transaction_asset)
        db.session.add(contract)
        db.session.add(person)
        db.session.commit()
        self.assertEqual(
            adjust_roster.clear(TRANSACTION_ASSET, False),
            {"code": 200, "result": "success"},
        )
        clear = Transaction.query.filter(Transaction.type == "Cleared Waivers").all()
        self.assertEqual(len(clear), 1)
        clear_asset = TransactionAsset.query.filter(
            TransactionAsset.transaction_id == clear[0].id
        ).first()
        minors = Transaction.query.filter(
            Transaction.type == "Reassigned to minors"
        ).all()
        self.assertEqual(len(minors), 0)
        self.assertEqual(transaction_asset.former_team, transaction_asset.new_team)
        self.assertEqual(contract.non_roster, False)
        self.assertTrue("On Waivers" not in person.current_status)
        self.assertEqual(clear_asset.former_team, TEAM)
        self.assertEqual(clear_asset.new_team, TEAM)
        self.assertEqual(clear_asset.asset_id, ASSET)

    def test_claim(self):
        ASSET = 23
        PERSON = 128
        TRANSACTION_ASSET = 237
        TRANSACTION = 238
        TEAM = 38
        NEW_TEAM = 2387

        self.assertEqual(
            adjust_roster.claim(TRANSACTION_ASSET, NEW_TEAM),
            {"code": 500, "result": "error", "error": "Unused transaction asset ID"},
        )

        asset = Asset(id=ASSET, current_team=TEAM, active=True)
        person = Person(id=PERSON, name="Test", current_status="Drafted,On Waivers")
        contract = Contract(id=ASSET, person_id=PERSON, non_roster=False)
        new_team = Team(id=NEW_TEAM, name="Test Team", abbreviation="TST", logo="logo")
        transaction = Transaction(id=TRANSACTION, type="Waivers")
        transaction_asset = TransactionAsset(
            id=TRANSACTION_ASSET,
            transaction_id=TRANSACTION,
            asset_id=ASSET,
            former_team=TEAM,
        )

        db.session.add(person)
        db.session.add(asset)
        db.session.add(new_team)
        db.session.add(contract)
        db.session.add(transaction)
        db.session.add(transaction_asset)
        db.session.commit()

        self.assertEqual(
            adjust_roster.claim(TRANSACTION_ASSET, NEW_TEAM),
            {"code": 200, "result": "success"},
        )
        self.assertEqual(transaction_asset.former_team, TEAM)
        self.assertEqual(transaction_asset.new_team, NEW_TEAM)
        self.assertEqual(contract.non_roster, False)
        self.assertTrue("On Waivers" not in person.current_status)
        self.assertEqual(Note.query.count(), 1)
        note = Note.query.first()
        self.assertTrue(Note.safe)
        self.assertEqual(
            note.note,
            f"Claimed by <a href=\"http://{self.app.config['SERVER_NAME']}/team/TST\" alt=\"Test Team logo\"><img class=\"logo\" src=\"http://{self.app.config['SERVER_NAME']}/static/logo\">Test Team </a>",
        )
        self.assertEqual(Transaction.query.count(), 2)
        claim = Transaction.query.filter(
            Transaction.type == "Claimed on waivers"
        ).first()
        claim_asset = TransactionAsset.query.filter(
            TransactionAsset.transaction_id == claim.id
        ).first()
        self.assertEqual(claim_asset.asset_id, ASSET)
        self.assertEqual(claim_asset.former_team, NEW_TEAM)
        self.assertEqual(claim_asset.new_team, NEW_TEAM)

    def test_get_roster(self):
        TEAM = 478
        self.assertEqual(adjust_roster.get_roster(TEAM), [])
        people = [
            Person(
                id=238,
                position="F",
                waivers_exempt=False,
                current_status="Drafted,Under Contract",
                name="Test 238",
            ),
            Person(
                id=248,
                position="F",
                waivers_exempt=False,
                current_status="Under Contract",
                name="Test 248",
            ),
            Person(
                id=258,
                position="D",
                waivers_exempt=False,
                current_status="RFA",
                name="Test 258",
            ),
            Person(
                id=268,
                position="D",
                waivers_exempt=False,
                current_status="Under Contract",
                name="Test 268",
            ),
            Person(
                id=278,
                position="G",
                waivers_exempt=True,
                current_status="Drafted",
                name="Test 278",
            ),
            Person(
                id=288,
                position="G",
                waivers_exempt=True,
                current_status="IDK",
                name="Test 288",
            ),
            Person(
                id=298,
                position="F",
                waivers_exempt=True,
                current_status="Floundering",
                name="Test 298",
            ),
            Person(
                id=338,
                position="F",
                waivers_exempt=True,
                current_status="KHL",
                name="Test 338",
            ),
        ]
        assets = [
            Asset(
                id=112,
                current_team=TEAM,
                originating_team=TEAM,
                type="Contract",
                active=True,
            ),
            Asset(
                id=122,
                current_team=TEAM,
                originating_team=TEAM,
                type="Contract",
                active=True,
            ),
            Asset(
                id=132,
                current_team=TEAM,
                originating_team=TEAM,
                type="Contract",
                active=True,
            ),
            Asset(
                id=142,
                current_team=TEAM,
                originating_team=TEAM,
                type="Contract",
                active=True,
            ),
            Asset(
                id=152,
                current_team=TEAM,
                originating_team=TEAM,
                type="Contract",
                active=True,
            ),
            Asset(
                id=162,
                current_team=TEAM,
                originating_team=TEAM,
                type="Contract",
                active=True,
            ),
            Asset(
                id=172,
                current_team=TEAM,
                originating_team=TEAM,
                type="Contract",
                active=True,
            ),
            Asset(
                id=182,
                current_team=TEAM,
                originating_team=TEAM,
                type="Contract",
                active=True,
            ),
        ]
        contracts = [
            Contract(
                id=assets[0].id,
                person_id=people[0].id,
                total_value=1000000,
                years=4,
                non_roster=False,
            ),
            Contract(
                id=assets[1].id,
                person_id=people[1].id,
                total_value=2000000,
                years=4,
                non_roster=False,
            ),
            Contract(
                id=assets[2].id,
                person_id=people[2].id,
                total_value=3000000,
                years=4,
                non_roster=False,
            ),
            Contract(
                id=assets[3].id,
                person_id=people[3].id,
                total_value=4000000,
                years=4,
                non_roster=False,
            ),
            Contract(
                id=assets[4].id,
                person_id=people[4].id,
                total_value=3000000,
                years=4,
                non_roster=False,
            ),
            Contract(
                id=assets[5].id,
                person_id=people[5].id,
                total_value=3000000,
                years=4,
                non_roster=False,
            ),
            Contract(
                id=assets[6].id,
                person_id=people[6].id,
                total_value=3000000,
                years=2,
                non_roster=True,
            ),
            Contract(
                id=assets[7].id,
                person_id=people[7].id,
                total_value=3000000,
                years=4,
                non_roster=True,
            ),
        ]
        contract_years = [
            ContractYear(
                contract_id=contracts[0].id,
                season_id=self.app.config["CURRENT_SEASON"],
                nmc=False,
                ntc=False,
                cap_hit=1000000,
            ),
            ContractYear(
                contract_id=contracts[1].id,
                season_id=self.app.config["CURRENT_SEASON"],
                nmc=False,
                ntc=False,
                cap_hit=2000000,
            ),
            ContractYear(
                contract_id=contracts[2].id,
                season_id=self.app.config["CURRENT_SEASON"],
                nmc=False,
                ntc=False,
                cap_hit=3000000,
            ),
            ContractYear(
                contract_id=contracts[3].id,
                season_id=self.app.config["CURRENT_SEASON"],
                nmc=False,
                ntc=False,
                cap_hit=3000000,
            ),
            ContractYear(
                contract_id=contracts[4].id,
                season_id=self.app.config["CURRENT_SEASON"],
                nmc=False,
                ntc=False,
                cap_hit=3000000,
            ),
            ContractYear(
                contract_id=contracts[5].id,
                season_id=self.app.config["CURRENT_SEASON"],
                nmc=False,
                ntc=False,
                cap_hit=3000000,
            ),
            ContractYear(
                contract_id=contracts[6].id,
                season_id=self.app.config["CURRENT_SEASON"],
                nmc=False,
                ntc=False,
                cap_hit=3000000,
            ),
            ContractYear(
                contract_id=contracts[7].id,
                season_id=self.app.config["CURRENT_SEASON"],
                nmc=False,
                ntc=False,
                cap_hit=3000000,
            ),
        ]
        for record in people + assets + contracts + contract_years:
            db.session.add(record)
        db.session.commit()

        roster = adjust_roster.get_roster(TEAM)
        self.assertEqual(len(roster), 8)
        self.assertEqual(roster[1]["name"], "Test 248")
        self.assertEqual(roster[1]["position"], "F")
        self.assertEqual(roster[1]["id"], 248)
        self.assertEqual(roster[1]["waivers_exempt"], False)
        self.assertEqual(roster[1]["nmc"], False)
        self.assertEqual(roster[1]["ntc"], False)
        self.assertEqual(roster[1]["clause_limits"], None)
        self.assertEqual(roster[1]["current_status"], "Under Contract")
        self.assertEqual(roster[1]["non_roster"], False)
        self.assertEqual(roster[0]["name"], "Test 238")
        self.assertEqual(roster[0]["position"], "F")
        self.assertEqual(roster[0]["id"], 238)
        self.assertEqual(roster[0]["waivers_exempt"], False)
        self.assertEqual(roster[0]["nmc"], False)
        self.assertEqual(roster[0]["ntc"], False)
        self.assertEqual(roster[0]["clause_limits"], None)
        self.assertEqual(roster[0]["current_status"], "Drafted,Under Contract")
        self.assertEqual(roster[0]["non_roster"], False)
        self.assertEqual(roster[3]["name"], "Test 268")
        self.assertEqual(roster[3]["position"], "D")
        self.assertEqual(roster[3]["id"], 268)
        self.assertEqual(roster[3]["waivers_exempt"], False)
        self.assertEqual(roster[3]["nmc"], False)
        self.assertEqual(roster[3]["ntc"], False)
        self.assertEqual(roster[3]["clause_limits"], None)
        self.assertEqual(roster[3]["current_status"], "Under Contract")
        self.assertEqual(roster[3]["non_roster"], False)
        self.assertEqual(roster[2]["name"], "Test 258")
        self.assertEqual(roster[2]["position"], "D")
        self.assertEqual(roster[2]["id"], 258)
        self.assertEqual(roster[2]["waivers_exempt"], False)
        self.assertEqual(roster[2]["nmc"], False)
        self.assertEqual(roster[2]["ntc"], False)
        self.assertEqual(roster[2]["clause_limits"], None)
        self.assertEqual(roster[2]["current_status"], "RFA")
        self.assertEqual(roster[2]["non_roster"], False)
        self.assertEqual(roster[4]["name"], "Test 278")
        self.assertEqual(roster[4]["position"], "G")
        self.assertEqual(roster[4]["id"], 278)
        self.assertEqual(roster[4]["waivers_exempt"], True)
        self.assertEqual(roster[4]["nmc"], False)
        self.assertEqual(roster[4]["ntc"], False)
        self.assertEqual(roster[4]["clause_limits"], None)
        self.assertEqual(roster[4]["current_status"], "Drafted")
        self.assertEqual(roster[4]["non_roster"], False)
        self.assertEqual(roster[5]["name"], "Test 288")
        self.assertEqual(roster[5]["position"], "G")
        self.assertEqual(roster[5]["id"], 288)
        self.assertEqual(roster[5]["waivers_exempt"], True)
        self.assertEqual(roster[5]["nmc"], False)
        self.assertEqual(roster[5]["ntc"], False)
        self.assertEqual(roster[5]["clause_limits"], None)
        self.assertEqual(roster[5]["current_status"], "IDK")
        self.assertEqual(roster[5]["non_roster"], False)
        self.assertEqual(roster[7]["name"], "Test 338")
        self.assertEqual(roster[7]["position"], "F")
        self.assertEqual(roster[7]["id"], 338)
        self.assertEqual(roster[7]["waivers_exempt"], True)
        self.assertEqual(roster[7]["nmc"], False)
        self.assertEqual(roster[7]["ntc"], False)
        self.assertEqual(roster[7]["clause_limits"], None)
        self.assertEqual(roster[7]["current_status"], "KHL")
        self.assertEqual(roster[7]["non_roster"], True)
        self.assertEqual(roster[6]["name"], "Test 298")
        self.assertEqual(roster[6]["position"], "F")
        self.assertEqual(roster[6]["id"], 298)
        self.assertEqual(roster[6]["waivers_exempt"], True)
        self.assertEqual(roster[6]["nmc"], False)
        self.assertEqual(roster[6]["ntc"], False)
        self.assertEqual(roster[6]["clause_limits"], None)
        self.assertEqual(roster[6]["current_status"], "Floundering")
        self.assertEqual(roster[6]["non_roster"], True)

    def test_get_waivers(self):
        TEAM_ONE = 37
        TEAM_TWO = 2

        self.assertEqual(adjust_roster.get_waivers(), [])

        transactions = [
            Transaction(
                id=234,
                type="Waived",
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            Transaction(
                id=334,
                type="Waived",
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            Transaction(
                id=434,
                type="Waived",
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            Transaction(
                id=534,
                type="Waived",
                season_id=self.app.config["CURRENT_SEASON"],
            ),
        ]
        assets = [
            Asset(id=2, current_team=TEAM_ONE, type="Contract", active=True),
            Asset(id=4, current_team=TEAM_ONE, type="Contract", active=True),
            Asset(id=6, current_team=TEAM_TWO, type="Contract", active=True),
            Asset(id=8, current_team=TEAM_TWO, type="Contract", active=True),
        ]
        transaction_assets = [
            TransactionAsset(
                id=48,
                transaction_id=transactions[0].id,
                asset_id=assets[0].id,
                former_team=assets[0].current_team,
            ),
            TransactionAsset(
                id=97,
                transaction_id=transactions[1].id,
                asset_id=assets[1].id,
                former_team=assets[1].current_team,
            ),
            TransactionAsset(
                id=27,
                transaction_id=transactions[2].id,
                asset_id=assets[2].id,
                former_team=assets[2].current_team,
            ),
            TransactionAsset(
                id=28,
                transaction_id=transactions[3].id,
                asset_id=assets[3].id,
                former_team=assets[3].current_team,
            ),
        ]
        teams = [
            Team(id=TEAM_ONE, name="Team ONE"),
            Team(id=TEAM_TWO, name="Team A"),
        ]
        people = [
            Person(
                id=87,
                name="Test One",
                position="F",
            ),
            Person(
                id=11,
                name="Test Two",
                position="D",
            ),
            Person(
                id=63,
                name="Test Three",
                position="D",
            ),
            Person(
                id=32,
                name="Test Four",
                position="G",
            ),
        ]
        contracts = [
            Contract(id=assets[0].id, person_id=people[0].id, total_value=4000000),
            Contract(id=assets[1].id, person_id=people[1].id, total_value=2000000),
            Contract(id=assets[2].id, person_id=people[2].id, total_value=4000000),
            Contract(id=assets[3].id, person_id=people[3].id, total_value=8000000),
        ]
        for record in (
            contracts + people + teams + transaction_assets + assets + transactions
        ):
            db.session.add(record)
        db.session.commit()

        waivers = adjust_roster.get_waivers()
        self.assertEqual(len(waivers), 4)
        self.assertEqual(waivers[0]["transaction_asset"], 28)
        self.assertEqual(waivers[1]["transaction_asset"], 27)
        self.assertEqual(waivers[2]["transaction_asset"], 48)
        self.assertEqual(waivers[3]["transaction_asset"], 97)
        self.assertEqual(waivers[0]["contract_id"], 8)
        self.assertEqual(waivers[1]["contract_id"], 6)
        self.assertEqual(waivers[2]["contract_id"], 2)
        self.assertEqual(waivers[3]["contract_id"], 4)
        self.assertEqual(waivers[0]["team_id"], TEAM_TWO)
        self.assertEqual(waivers[1]["team_id"], TEAM_TWO)
        self.assertEqual(waivers[2]["team_id"], TEAM_ONE)
        self.assertEqual(waivers[3]["team_id"], TEAM_ONE)
        self.assertEqual(waivers[0]["team_name"], "Team A")
        self.assertEqual(waivers[1]["team_name"], "Team A")
        self.assertEqual(waivers[2]["team_name"], "Team ONE")
        self.assertEqual(waivers[3]["team_name"], "Team ONE")
        self.assertEqual(waivers[0]["person_id"], 32)
        self.assertEqual(waivers[1]["person_id"], 63)
        self.assertEqual(waivers[2]["person_id"], 87)
        self.assertEqual(waivers[3]["person_id"], 11)
        self.assertEqual(waivers[0]["name"], "Test Four")
        self.assertEqual(waivers[1]["name"], "Test Three")
        self.assertEqual(waivers[2]["name"], "Test One")
        self.assertEqual(waivers[3]["name"], "Test Two")
        self.assertEqual(waivers[0]["position"], "G")
        self.assertEqual(waivers[1]["position"], "D")
        self.assertEqual(waivers[2]["position"], "F")
        self.assertEqual(waivers[3]["position"], "D")

    def test_terminate(self):
        PERSON = 387
        ASSET = 237
        TEAM = 98
        TR_ASSET = 22
        person = Person(
            name="Test User",
            id=PERSON,
            current_status="Drafted,Under Contract,On Waivers",
        )
        asset = Asset(
            id=ASSET,
            type="Contract",
            current_team=TEAM,
            originating_team=TEAM,
            active=True,
        )
        contract = Contract(id=ASSET, person_id=PERSON)
        seasons = [
            Season(id=28, free_agency_opening=date(year=2001, month=1, day=1)),
            Season(id=38, free_agency_opening=date(year=2002, month=1, day=1)),
            Season(
                id=self.app.config["CURRENT_SEASON"],
                free_agency_opening=date(year=2003, month=1, day=1),
            ),
            Season(id=68, free_agency_opening=date(year=2004, month=1, day=1)),
            Season(id=98, free_agency_opening=date(year=2005, month=1, day=1)),
            Season(id=128, free_agency_opening=date(year=2006, month=1, day=1)),
        ]
        contract_years = [
            ContractYear(contract_id=ASSET, season_id=seasons[0].id),
            ContractYear(contract_id=ASSET, season_id=seasons[1].id),
            ContractYear(contract_id=ASSET, season_id=seasons[2].id),
            ContractYear(contract_id=ASSET, season_id=seasons[3].id),
            ContractYear(contract_id=ASSET, season_id=seasons[4].id),
            ContractYear(contract_id=ASSET, season_id=seasons[5].id),
        ]
        transaction = Transaction(
            id=338,
            type="Waivers",
            season_id=self.app.config["CURRENT_SEASON"],
            date=date.today(),
        )
        transaction_asset = TransactionAsset(
            id=TR_ASSET,
            transaction_id=transaction.id,
            asset_id=ASSET,
            former_team=TEAM,
        )

        self.assertEqual(
            adjust_roster.terminate(TR_ASSET),
            {"code": 500, "result": "error", "error": "Unused transaction asset ID"},
        )
        db.session.add(person)
        db.session.add(asset)
        db.session.add(contract)
        db.session.add(transaction)
        db.session.add(transaction_asset)
        for record in seasons + contract_years:
            db.session.add(record)
        db.session.commit()

        self.assertEqual(Note.query.count(), 0)
        self.assertEqual(
            adjust_roster.terminate(TR_ASSET), {"code": 200, "result": "success"}
        )
        self.assertTrue("On Waivers" not in person.current_status)
        self.assertTrue("Under Contract" not in person.current_status)
        self.assertTrue("UFA" in person.current_status)
        self.assertFalse(asset.active)
        self.assertEqual(contract.termination_date, date.today())
        self.assertEqual(transaction_asset.former_team, transaction_asset.new_team)
        self.assertFalse(contract_years[0].terminated)
        self.assertFalse(contract_years[1].terminated)
        self.assertTrue(contract_years[2].terminated)
        self.assertTrue(contract_years[3].terminated)
        self.assertTrue(contract_years[4].terminated)
        self.assertTrue(contract_years[5].terminated)
        self.assertEqual(transaction.type, "Unconditional Waivers")
        self.assertEqual(Note.query.count(), 1)
        note = Note.query.first()
        self.assertEqual(note.note, "Contract terminated once waviers were cleared")
        self.assertFalse(note.safe)
        self.assertEqual(note.record_id, transaction_asset.id)
        self.assertEqual(note.record_table, "transaction_asset")

        transactions = Transaction.query.order_by(Transaction.id).all()
        self.assertEqual(len(transactions), 3)
        self.assertEqual(transactions[2].type, "Cleared Waivers")
        self.assertEqual(transactions[1].type, "Contract terminated")
        asset_clear = TransactionAsset.query.filter(
            TransactionAsset.transaction_id == transactions[2].id
        ).first()
        self.assertEqual(asset_clear.asset_id, transaction_asset.asset_id)
        asset_term = TransactionAsset.query.filter(
            TransactionAsset.transaction_id == transactions[1].id
        ).first()
        self.assertEqual(asset_term.asset_id, transaction_asset.asset_id)

    def test_get_rights(self):
        TEAM_ID = 5
        assets = [
            Asset(
                id=55,
                type="Signing Rights",
                current_team=TEAM_ID,
                active=True,
            ),
            Asset(
                id=65,
                type="Signing Rights",
                current_team=TEAM_ID,
                active=False,
            ),
            Asset(
                id=56,
                type="Signing Rights",
                current_team=TEAM_ID + 1,
                active=True,
            ),
        ]
        people = [
            Person(
                id=333,
                name="Test Goalie",
                position="G",
                waivers_exempt=False,
                current_status="Hungry",
            ),
            Person(
                id=33,
                name="Test Forward",
                position="F",
                waivers_exempt=True,
                current_status="Horny",
            ),
            Person(
                id=3,
                name="Test Defense",
                position="D",
                waivers_exempt=False,
                current_status="Hippo",
            ),
        ]
        rights = [
            SigningRights(
                id=assets[0].id,
                person_id=people[0].id,
                qualifying_offer=True,
                qualifying_salary=900000,
                qualifying_minors=300000,
                expiration_date=date.today(),
            ),
            SigningRights(
                id=assets[1].id,
                person_id=people[1].id,
                qualifying_offer=False,
            ),
            SigningRights(
                id=assets[2].id,
                person_id=people[2].id,
                qualifying_offer=False,
                expiration_date=date.today(),
            ),
        ]
        for record in assets + people + rights:
            db.session.add(record)
        db.session.commit()

        rights_data = adjust_roster.get_rights(TEAM_ID)
        self.assertEqual(len(rights_data), 1)
        data = rights_data[0]
        self.assertEqual(data["name"], "Test Goalie"),
        self.assertEqual(data["position"], "G"),
        self.assertEqual(data["current_status"], "Hungry"),
        self.assertTrue(data["is_qualifying_offer"])
        self.assertEqual(data["qualifying_salary"], 900000)
        self.assertEqual(data["qualifying_minors"], 300000)
        self.assertEqual(
            data["rights_expiration"],
            datetime.strftime(date.today(), self.app.config["DATE_FMT"]),
        )

    def test_deactivate_rights(self):
        asset = Asset(
            id=24242,
            type="Signing Rights",
            originating_team=28,
            current_team=28,
            active=True,
        )
        person = Person(
            id=590,
            name="Benjamin Gaudreau",
        )
        signing_rights = SigningRights(
            id=24242,
            person_id=590,
            expiration_date=date(year=2023, month=6, day=1),
            qualifying_offer=False,
        )
        db.session.add(asset)
        db.session.add(person)
        db.session.add(signing_rights)
        db.session.commit()
        self.assertTrue(asset.active)
        adjust_roster.deactivate_rights(person.id)
        self.assertFalse(asset.active)

    def test_dequalify(self):
        rights_asset = Asset(
            id=24322,
            type="Signing Rights",
            active=True,
            current_team=28,
            originating_team=28,
        )
        rights = SigningRights(
            id=24322,
            person_id=19,
            expiration_date=date(year=2023, month=7, day=15),
            qualifying_salary=787500,
            qualifying_minors=80000,
        )
        contract_asset = Asset(
            id=8702,
            type="Contract",
            active=False,
            current_team=28,
            originating_team=28,
        )
        contract = Contract(
            id=8702,
            person_id=19,
        )
        person = Person(
            id=19,
            name="Strauss Mann",
            current_status="RFA",
        )
        db.session.add(rights_asset)
        db.session.add(rights)
        db.session.add(contract_asset)
        db.session.add(contract)
        db.session.add(person)
        db.session.commit()

        self.assertEqual(Note.query.count(), 0)
        self.assertTrue(rights_asset.active)
        self.assertFalse(contract_asset.active)
        self.assertEqual(person.current_status, "RFA")

        adjust_roster.dequalify(person.id)
        self.assertEqual(Note.query.count(), 1)
        self.assertFalse(rights_asset.active)
        self.assertFalse(contract_asset.active)
        self.assertEqual(person.current_status, "UFA")
        note = Note.query.first()
        self.assertEqual(note.record_id, 8702)
        self.assertEqual(
            note.note,
            "Player reacted unrestricted free agency after not being tendered a qualifying offer",
        )


if __name__ == "__main__":
    unittest.main(verbosity=2)
