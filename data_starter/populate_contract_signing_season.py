from datetime import date

from app import create_app, db
from app.models import Asset, Contract, Season


def main():
    contracts = (
        Contract.query.filter(Contract.signing_season_id == None)
        .filter(Contract.signing_date > date(year=2005, month=8, day=1))
        .order_by(Contract.signing_date.asc())
        .all()
    )

    for contract in contracts:
        signing_season = (
            Season.query.filter(Season.free_agency_opening <= contract.signing_date)
            .order_by(Season.free_agency_opening.desc())
            .with_entities(Season.id)
            .first()
        )
        # print(f"{contract.id} - {signing_season} - {contract.signing_date}")
        contract.signing_season_id = signing_season[0]
        db.session.add(contract)


if __name__ == "__main__":
    app = create_app("default")
    app_context = app.app_context()
    app_context.push()

    main()
    db.session.commit()

    app_context.pop()
