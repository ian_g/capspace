"""rename ltir relief to max ltir relief

Revision ID: ecff4dcfef52
Revises: a37338e5a783
Create Date: 2022-05-12 14:27:36.922668

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "ecff4dcfef52"
down_revision = "a37338e5a783"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "team_season", sa.Column("max_ltir_relief", sa.Integer(), nullable=True)
    )
    op.drop_column("team_season", "ltir_relief")
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("team_season", sa.Column("ltir_relief", sa.INTEGER(), nullable=True))
    op.drop_column("team_season", "max_ltir_relief")
    # ### end Alembic commands ###
