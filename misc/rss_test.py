import sys
import urllib.request

from defusedxml.ElementTree import fromstring


def request_rss(ep_id):
    if not ep_id:
        return True
    try:
        url = f"https://www.eliteprospects.com/rss_player_stats2.php?player={ep_id}"
        data = urllib.request.urlopen(url).read()
    except urllib.error.HTTPError as e:
        print("Error querying EliteProspects player stats", file=sys.stderr)
        current_app.logger.error(f"EliteProspects stats query failed: {e}")
        return ""
    return data


def pull_player_stats(ep_rss):
    rss_data = fromstring(ep_rss)
    channel = rss_data.find("channel")
    item = channel.find("item")
    html = item.find("description").text

    ns = {"atom": "http://www.w3.org/2005/Atom"}
    atom_link = rss_data.find("channel").find("atom:link", ns)
    try:
        print(f"Source: {atom_link.attrib['href']}")
    except AttributeError as e:
        print(f"Element {atom_link} attribute not found: {e}")
    except KeyError as e:
        print(f"Attribute key not found for {atom_link}: {e}")
    return html


if __name__ == "__main__":
    data = pull_player_stats(request_rss(56038))
    # print(data)
