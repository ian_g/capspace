import sys
import json
import random
import unittest
from datetime import datetime, date
from app import create_app, db
from app.models import Team, Season, Person, Asset, DraftPick

from app.main import draft
from app.main.forms import SelectDraftSeason

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class DraftTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def create_seasons(self):
        to_create = random.randint(5, 20)
        season_data = []
        for _ in range(to_create):
            season_data.append(self.create_season())
        return season_data

    def create_season(self):
        data = {
            "name": "".join(random.choices(LETTERS, k=9)),
            "salary_cap": random.randint(1, 100000000),
            "salary_floor": random.randint(1, 50000000),
            "minimum_salary": random.randint(1, 1500000),
            "max_buriable_hit": random.randint(1, 2000000),
            "bonus_overage_limit": random.randint(1, 10000000),
            "draft_date": datetime.strptime(
                f"{random.randint(1950,2100)}-{random.randint(1,12):02}-{random.randint(1,28):02}",
                self.app.config["DATE_FMT"],
            ).date(),
        }
        if random.randint(1, 100) > 90:
            del data["draft_date"]
        season = Season(**data)
        db.session.add(season)
        db.session.commit()
        data["id"] = season.id
        return data

    def create_teams(self):
        to_create = random.randint(5, 20)
        team_data = []
        for _ in range(to_create):
            data = {
                "name": "".join(random.choices(LETTERS, k=16)),
                "general_manager": "".join(random.choices(LETTERS, k=16)),
                "head_coach": "".join(random.choices(LETTERS, k=16)),
                "abbreviation": "".join(random.choices(LETTERS, k=3)),
                "team_name": "".join(random.choices(LETTERS, k=8)),
                "team_location": "".join(random.choices(LETTERS, k=8)),
                "first_year": random.randint(1890, 2100),
                "logo": "".join(random.choices(LETTERS, k=500)),
            }
            while (
                Team.query.filter(Team.abbreviation == data["abbreviation"]).count()
                != 0
            ):
                data["abbreviation"] = "".join(random.choices(LETTERS, k=3))
            team_row = Team(**data)
            db.session.add(team_row)
            db.session.commit()
            data["id"] = team_row.id
            team_data.append(data)
        return team_data

    def create_draft_picks(self, season_id):
        to_create = random.randint(50, 100)
        pick_data = []
        team_data = self.create_teams()
        for _ in range(to_create):
            asset_data = {
                "current_team": random.choice(team_data)["id"],
                "originating_team": random.choice(team_data)["id"],
            }
            asset = Asset(**asset_data)
            db.session.add(asset)
            db.session.flush()
            person_data = None
            if random.randint(1, 10) > 5:
                person_data = {
                    "name": "".join(random.choices(LETTERS, k=16)),
                    "draft_pick": asset.id,
                }
                person = Person(**person_data)
                db.session.add(person)
                db.session.flush()
                person_data["id"] = person.id
            draft_pick_data = {
                "id": asset.id,
                "season_id": season_id,
                "round": random.randint(1, 7),
                "position": random.randint(1, 250),
            }
            if random.randint(1, 10) > 5:
                del draft_pick_data["position"]
            if person_data:
                draft_pick_data["person_id"] = person.id
            else:
                draft_pick_data["person_id"] = None
            draft_pick = DraftPick(**draft_pick_data)
            db.session.add(draft_pick)
            db.session.flush()
            draft_pick_data["player"] = None
            if person_data:
                draft_pick_data["player"] = person_data["name"]
            team_abbreviation = ""
            original_abbreviation = ""
            for team in team_data:
                if team["id"] == asset.current_team:
                    team_abbreviation = team["abbreviation"]
                if team["id"] == asset.originating_team:
                    original_abbreviation = team["abbreviation"]
            if team_abbreviation == original_abbreviation:
                original_abbreviation = None
            draft_pick_data["team"] = team_abbreviation
            draft_pick_data["original"] = original_abbreviation
            if "position" not in draft_pick_data:
                draft_pick_data["position"] = None
            pick_data.append(draft_pick_data)
        db.session.commit()
        pick_data.sort(
            key=lambda pick: (
                pick["round"],
                pick["position"] or 1000,
                pick["original"] or pick["team"],
            )
        )
        # Round, Position (nulls last), Originating team's abbreviation
        return pick_data

    # draft.py tests
    def test_get_draft_picks_current_season(self):
        season = self.create_season()
        self.assertEqual(self.app.config["CURRENT_SEASON"], season["id"])
        picks = self.create_draft_picks(self.app.config["CURRENT_SEASON"])
        query_picks = draft.get_draft_picks()
        self.assertEqual(len(picks), len(query_picks))
        for idx in range(len(picks)):
            self.assertEqual(picks[idx]["round"], query_picks[idx]["round"])
            self.assertEqual(picks[idx]["position"], query_picks[idx]["position"])
            self.assertEqual(picks[idx]["team"], query_picks[idx]["team"])
            self.assertEqual(picks[idx]["original"], query_picks[idx]["original"])
            self.assertEqual(picks[idx]["player"], query_picks[idx]["player"])
            self.assertEqual(picks[idx]["person_id"], query_picks[idx]["person_id"])

    def test_get_draft_picks_specified_year(self):
        self.assertIsNone(draft.get_draft_picks(2005))
        self.create_season()
        self.create_draft_picks(self.app.config["CURRENT_SEASON"])
        season = self.create_season()
        while "draft_date" not in season:
            season = self.create_season()
        self.assertFalse(season["id"] == self.app.config["CURRENT_SEASON"])
        picks = self.create_draft_picks(season["id"])
        query_picks = draft.get_draft_picks(season["draft_date"].year)
        self.assertEqual(len(picks), len(query_picks))
        for idx in range(len(picks)):
            self.assertEqual(picks[idx]["round"], query_picks[idx]["round"])
            self.assertEqual(picks[idx]["position"], query_picks[idx]["position"])
            self.assertEqual(picks[idx]["team"], query_picks[idx]["team"])
            self.assertEqual(picks[idx]["original"], query_picks[idx]["original"])
            self.assertEqual(picks[idx]["player"], query_picks[idx]["player"])
            self.assertEqual(picks[idx]["person_id"], query_picks[idx]["person_id"])

    def test_get_logos(self):
        data = self.create_teams()
        team_logos = {team["abbreviation"]: team["logo"] for team in data}
        queried_team_logos = draft.get_logos()
        self.assertEqual(len(team_logos.keys()), len(queried_team_logos.keys()))
        abbrs = team_logos.keys()
        for team in abbrs:
            self.assertEqual(team_logos[team], queried_team_logos[team])


if __name__ == "__main__":
    unittest.main(verbosity=2)
