document.getElementById("add-alias").onclick = addAlias;

const nhl_id = document.getElementById("nhl_id");
nhl_id.onchange = (event => populatePersonData(event, previousData));

const previousData = {};

function addAlias() {
    let aliasValue = document.getElementById("new-alias").value;
    let aliasSelector = document.getElementById("aliases");
    let newAlias = document.createElement("option");
    newAlias.value = aliasValue;
    newAlias.innerText = aliasValue;
    newAlias.selected = true;
    aliasSelector.appendChild(newAlias);
    document.getElementById("new-alias").value = "";
}

async function populatePersonData(event, existing) {
    let id = event.target.value;
    console.log(`NHL ID: ${id}`);
    if (!id) {
        return;
    }

    let response = await fetch(`/admin/proxy/api-web.nhle.com/v1/player/${id}/landing`);
    if (!response.ok) {
        alert("Invalid NHL ID: form will not submit");
        return;
    }
    let personData = await response.json();
    //let personData = data.people[0];
    //nationality, active, rookie, captain, alternateCaptain
    let name = document.getElementById("name");
    existing.name = name.value;
    name.value = `${personData.firstName.default} ${personData.lastName.default}`;

    let birthdate = document.getElementById("birthdate");
    existing.birthdate = birthdate.value;
    birthdate.value = personData.birthDate;

    let birthplace = document.getElementById("birthplace");
    existing.birthplace = birthplace.value;
    let birthplaceValue = personData.birthCity.default;
    if (Object.keys(personData).includes("birthStateProvince")) {
        birthplaceValue += `, ${personData.birthStateProvince.default}`;
    }
    birthplaceValue += `, ${personData.birthCountry}`;
    birthplace.value = birthplaceValue;

    let nationality = document.getElementById("nationality");
    existing.nationality = nationality.value;
    nationality.value = personData.nationality || "";

    let number = document.getElementById("number");
    existing.number = number.value;
    number.value = personData.sweaterNumber || "";

    let feet = document.getElementById("feet");
    existing.feet = feet.value;
    feet.value = Math.round(personData.heightInInches/12);

    let inches = document.getElementById("inches");
    existing.inches = inches.value;
    inches.value = personData.heightInInches % 12;

    let weight = document.getElementById("weight");
    existing.weight = weight.value;
    weight.value = personData.weightInPounds;

    let shoots = document.getElementById("shoots");
    existing.shoots = shoots.value;
    shoots.value = personData.shootsCatches;

    let position = document.getElementById("position");
    existing.position = position.value;
    position.value = personData.position;

    let nonRoster = document.getElementById("non_roster");
    existing.nonRoster = nonRoster.checked;
    nonRoster.checked = (!!personData.currentTeamRoster.find(person => person.playerId == id));

    if (!document.getElementById("undo")) {
        let undo = document.createElement("input");
        undo.id = "undo";
        undo.type = "button";
        undo.value = "Undo Auto-populated Values";
        undo.onclick = (() => undoChanges());

        document.getElementById("submit").parentElement.appendChild(undo);
    }
}

function undoChanges() {
    console.log(previousData);
    let name = document.getElementById("name");
    name.value = previousData.name;
    let birthdate = document.getElementById("birthdate");
    birthdate.value = previousData.birthdate;
    let birthplace = document.getElementById("birthplace");
    birthplace.value = previousData.birthplace;
    let nationality = document.getElementById("nationality");
    nationality.value = previousData.nationality;
    let number = document.getElementById("number");
    number.value = previousData.number;
    let feet = document.getElementById("feet");
    feet.value = previousData.feet;
    let inches = document.getElementById("inches");
    inches.value = previousData.inches;
    let weight = document.getElementById("weight");
    weight.value = previousData.weight;
    let shoots = document.getElementById("shoots");
    shoots.value = previousData.shoots;
    let position = document.getElementById("position")
    position.value = previousData.position;
    let active = document.getElementById("active");
    active.checked = previousData.active;
    let rookie = document.getElementById("rookie");
    rookie.checked = previousData.rookie;
    let captain = document.getElementById("captain");
    captain.checked = previousData.captain;
    let alternateCaptain = document.getElementById("alternate_captain");
    alternateCaptain.checked = previousData.alternateCaptain;
    let nonRoster = document.getElementById("non_roster");
    nonRoster.checked = previousData.nonRoster;

    let undo = document.getElementById("undo");
    undo.remove();
}
