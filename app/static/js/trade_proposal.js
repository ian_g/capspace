//Use this as a basis for trade updates
let teamOne = document.getElementById("team_one");
teamOne.value = "";
let teamTwo = document.getElementById("team_two");
teamTwo.value = "";

let picksOne = document.getElementById("picks_one");
picksOne.value = "";
let picksTwo = document.getElementById("picks_two");
picksTwo.value = "";
let contractsOne = document.getElementById("contracts_one");
contractsOne.value = "";
let contractsTwo = document.getElementById("contracts_two");
contractsTwo.value = "";

//Update this for picks_{one,two} and contracts_{one,two}
teamOne.onchange = (event => {
    updatePicks(event.srcElement.value, "select_picks_one");
    updateContracts(event.srcElement.value, "select_contracts_one")
});
teamTwo.onchange = (event => {
    updatePicks(event.srcElement.value, "select_picks_two");
    updateContracts(event.srcElement.value, "select_contracts_two")
});

let futConOne = document.getElementById("fut_con_one");
let futConTwo = document.getElementById("fut_con_two");
futConOne.onchange = (() => showHideDetails());
futConTwo.onchange = (() => showHideDetails());
shadeRows();

async function updatePicks(teamId, picksTable) {
    let picks = document.getElementById(picksTable);
    while (picks.firstChild) {
        picks.removeChild(picks.lastChild);
    }
    if (teamId == "") {
        return;
    }

    let teamPicks = await getTeamPicks(teamId);
    for (let pick of teamPicks) {
        picks.appendChild(createPickRow(pick));
    }
    shadeRows();
}

async function updateContracts(teamId, contractsTable) {
    let contracts = document.getElementById(contractsTable);
    while (contracts.firstChild) {
        contracts.removeChild(contracts.lastChild);
    }
    if (teamId == "") {
        return; }

    let teamContracts = await getTeamContracts(teamId);
    for (let contract of teamContracts) {
        contracts.appendChild(createContractRow(contract));
    }
    shadeRows();
}

function createPickRow(pick) {
    //ID, Team, Year, Round, Condition
    let row = document.createElement("tr");
    let cellOne = document.createElement("td");
    row.appendChild(cellOne);

    let checkBox = document.createElement("input");
    checkBox.setAttribute("type", "checkbox");
    checkBox.setAttribute("asset_id", pick.id);
    cellOne.appendChild(checkBox);

    let cellTwo = document.createElement("td");
    cellTwo.innerText = pick.team;
    row.appendChild(cellTwo);

    let cellThree = document.createElement("td");
    cellThree.innerText = pick.year;
    row.appendChild(cellThree);

    let cellFour = document.createElement("td");
    row.appendChild(cellFour);
    cellFour.innerText = pick.round;
    if (pick.position) {
        cellFour.innerText += ` (${pick.position})`;
    }

    let cellFive = document.createElement("td");
    row.appendChild(cellFive);
    let condition = document.createElement("input");
    cellFive.appendChild(condition);

    return row;
}

function createContractRow(contract) {
    //ID, Name, Expires, Years, AAV, Retention
    let row = document.createElement("tr");
    let cellCheck = document.createElement("td");
    row.appendChild(cellCheck);

    let checkBox = document.createElement("input");
    checkBox.setAttribute("type", "checkbox");
    checkBox.setAttribute("asset_id", contract.id);
    cellCheck.appendChild(checkBox);

    let cellName = document.createElement("td");
    cellName.innerText = contract.name;
    row.appendChild(cellName);

    let cellExpires = document.createElement("td");
    cellExpires.innerText = contract.expires_as;
    row.appendChild(cellExpires);

    let cellYears = document.createElement("td");
    cellYears.innerHTML = `<span>&nbsp;</span>${contract.years}`;
    row.appendChild(cellYears);

    let cellAAV = document.createElement("td");
    cellAAV.innerText = new Intl.NumberFormat("en-US", {
        "currency": "USD",
        "style": "currency",
        "maximumFractionDigits": 0,
        "minimumFractionDigits": 0,
    }).format(contract.aav);
    row.appendChild(cellAAV);

    let cellRetain = document.createElement("td");
    row.appendChild(cellRetain);
    let retention = document.createElement("input");
    retention.setAttribute("type", "number");
    retention.setAttribute("min", 0);
    retention.setAttribute("max", 50);
    retention.setAttribute("value", 0);
    cellRetain.appendChild(retention);
    cellRetain.append("%");

    return row;
}

async function getTeamPicks(teamId) {
    let response = await fetch(`/api/trade_picks/${teamId}`);
    let assets = await response.json();
    return assets;
}

async function getTeamContracts(teamId) {
    let response = await fetch(`/api/trade_contracts/${teamId}`);
    let assets = await response.json();
    return assets;
}

function showHideDetails() {
    let details = document.getElementById("considerations");
    let display = "block";
    if (!futConOne.checked && !futConTwo.checked) {
        display = "none";
    }
    details.style.setProperty("display", display);
    details.value = "";
}

function collectSelections() {
    let success = true;
    success = success && selectedContracts("select_contracts_one", "contracts_one");
    success = success && selectedContracts("select_contracts_two", "contracts_two");

    success = success && selectedPicks("select_picks_one", "picks_one");
    success = success && selectedPicks("select_picks_two", "picks_two");
    return success;
}

function selectedContracts(selectionTable, formText) {
    let contracts = document.getElementById(selectionTable);
    let data = document.getElementById(formText);
    if (!contracts) {
        console.log("Cannot find contracts table");
        return false;
    }
    if (!data) {
        console.log("Cannot find contracts form field");
        return false;
    }

    let contractData = [];
    for (let row of contracts.childNodes) {
        if (!row.firstChild.firstChild.checked) {
            continue;
        }

        let assetId = row.firstChild.firstChild.getAttribute("asset_id");
        let retention = row.lastChild.firstChild.value;
        if (!assetId) {
            console.log("No asset ID found for contract");
            return false;
        }
        contractData.push({
            "id": assetId,
            "retention": retention,
        });
    }

    console.log(contractData);
    data.value = JSON.stringify(contractData);
    return true;
}

function selectedPicks(selectionTable, formText) {
    let picks = document.getElementById(selectionTable);
    let data = document.getElementById(formText);
    if (!picks) {
        console.log("Cannot find picks table");
        return false;
    }
    if (!data) {
        console.log("Cannot find picks form field");
        return false;
    }

    let pickData = [];
    for (let row of picks.childNodes) {
        if (!row.firstChild.firstChild.checked) {
            continue;
        }

        let assetId = row.firstChild.firstChild.getAttribute("asset_id");
        let condition = row.lastChild.firstChild.value;
        let pick = {
            "id": assetId
        }
        if (condition) {
            pick.condition = condition;
        }
        if (!assetId) {
            console.log("No asset ID found for draft pick");
            return false;
        }
        pickData.push(pick);
    }

    console.log(pickData);
    data.value = JSON.stringify(pickData);
    return true;
}

async function shadeRows() {
    for (let section of document.querySelectorAll(".team")) {
        section 
            .querySelectorAll("tr")
            .forEach((elem, idx) => {
                if (idx % 2 == 0) {
                    elem.classList.remove("shaded");
                } else {
                    elem.classList.add("shaded");
                }
            });
    }
}
