document.getElementById("load_transactions").onclick = loadTransactions;
document.getElementById("team-filter").onchange = filter;
document.getElementById("type-filter").onclick = filter;
document.getElementById("team-filter").value = "";
setTransactionTypes();
rejiggerRows();

async function rejiggerRows() {
    reshadeRows();
    relabelDates();
}

async function reshadeRows() {
    let rows = document.querySelector("table").querySelectorAll("tr:not([style*='display: none'])");
    for (let idx = 0; idx < rows.length; idx++) {
        if (idx % 2 == 0) {
            rows[idx].classList.remove("shaded");
        } else {
            rows[idx].classList.add("shaded");
        }
    }
}

function setTransactionTypes() {
    let idx = getTransactionIndex();
    if (!idx) {
        return;
    }
    let rows = document.querySelectorAll("tbody tr");
    let types = [];
    rows.forEach(row => {
        types.push(row.querySelectorAll("td")[idx].innerText);
    });
    types = [...(new Set(types))].sort();
    types.unshift("");
    let filter = document.getElementById("type-filter");
    let filterValue = filter.value;
    filter.querySelectorAll("option").forEach(el => el.remove());
    for (let type of types) {
        let option = document.createElement("option");
        option.value = type;
        option.innerText = type;
        if (filterValue == type) {
            option.selected = true;
        }
        filter.appendChild(option);
    }
}

function getTransactionIndex() {
    let header = document.querySelector("thead tr");
    let typeIdx;
    header.querySelectorAll("td").forEach((val, idx) => {
        if (val.innerText == "Transaction") {
            typeIdx = idx;
        }
    });
    return typeIdx;
}

async function loadTransactions() {
    let body = document.querySelector("table").querySelector("tbody");
    let offset = body.rows.length;
    let loadURL = new URL(window.location.href);
    loadURL.pathname = `/api/load_transactions/${offset}`
    let response = await fetch(loadURL);
    let transactions = await response.text();
    if (!transactions.includes("tr")) {
        document.getElementById("load_transactions").style.display = "none";
    }

    body.innerHTML = body.innerHTML + transactions;
    rejiggerRows();
    setTransactionTypes();
    filter();
}

async function relabelDates() {
    let body = document.querySelector("table").querySelector("tbody");
    let date = "";
    body.querySelectorAll("tr")
        .forEach(row => {
            row.querySelector("span").classList.remove("hide");
            row.classList.remove("overbar");
        });
    body.querySelectorAll("tr:not([style*='display: none'])").forEach(row => {
        let element = row.querySelector("span");
        if (element.innerText != date) {
            date = element.innerText;
            row.classList.add("overbar");
        } else {
            element.classList.add("hide");
        }
    });
}

function filter() {
    let teamID = document.getElementById("team-filter").value;
    let typeID = document.getElementById("type-filter").value;
    let idx = getTransactionIndex();
    let body = document.querySelector("table tbody");

    if (!teamID && !typeID) {
        body.querySelectorAll("tr").forEach(row => row.style.display = "");
        rejiggerRows();
        return;
    }
    let rows = body.querySelectorAll("tr");
    rows.forEach(row => row.style.display = "none");
    if (teamID) {
        rows = body.querySelectorAll(`.team_${teamID}`);
    }
    rows.forEach(row => {
        if (!typeID) {
            row.style.display = "table-row";
        } else if (row.querySelectorAll("td")[idx].innerText == typeID) {
            row.style.display = "table-row";
        } else {
            row.style.display = "none";
        }
    });
    rejiggerRows();
}
