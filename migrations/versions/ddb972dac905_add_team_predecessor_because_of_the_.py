"""Add team.predecessor because of the Barons, Seals, North Stars, and Sharks

Revision ID: ddb972dac905
Revises: 97f35ed8dd72
Create Date: 2023-07-31 23:59:22.556410

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "ddb972dac905"
down_revision = "97f35ed8dd72"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table("team", schema=None) as batch_op:
        batch_op.add_column(sa.Column("predecessor", sa.Integer(), nullable=True))
        batch_op.create_foreign_key(
            batch_op.f("fk_team_predecessor_team"), "team", ["predecessor"], ["id"]
        )

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table("team", schema=None) as batch_op:
        batch_op.drop_constraint(
            batch_op.f("fk_team_predecessor_team"), type_="foreignkey"
        )
        batch_op.drop_column("predecessor")

    # ### end Alembic commands ###
