document.querySelectorAll(".position").forEach(element => element.value = "");
document.querySelectorAll(".name").forEach(element => element.value = "");
document.getElementById('add-pick').onclick = addCompensatory;

let picksTable = document.getElementsByTagName("table")[0]; for (let row of picksTable.rows) {
    if (!row.id.includes("pick")) {
        continue;
    }
    const POSITIONIDX = 2;
    const NAMEIDX = 3;
    
    //Set onclick for writing pick position
    let position = row.cells[POSITIONIDX];
    let positionInput = position.getElementsByClassName("position")[0];
    let setPosition = position.getElementsByClassName("set_position")[0];
    if (positionInput && setPosition) {
        setPosition.onclick = writePickPosition;
    }

    //Set onclick for name validation
    //Set onchange for vanishing validation icon
    //Set onclick for writing name to db
    let name = row.cells[NAMEIDX];
    let nameInput = name.getElementsByClassName("name")[0];
    let nameValidate = name.getElementsByClassName("check_name")[0];
    let setName = name.getElementsByClassName("set_name")[0];
    if (nameInput && nameValidate && setName) {
        nameInput.onchange = removeValidationIcon;
        nameValidate.onclick = validateName;
        setName.onclick = writeName;
    }
}

async function writePickPosition(event) {
    let pickID = event.target.parentElement.parentElement.id.split("_")[1];
    let position = event.target.previousElementSibling.value;
    console.log(`writePosition: pickID=${pickID} position=${position}`);

    //Verify position between 1 and 32 inclusive (and is a number)
    position = parseInt(position);
    if (position != position) {
        alert("Pick position is not an integer");
        return;
    }
    if (position < 1 || position > 42) {
        alert("Pick position is not between 0 and 42");
        return;
    }
    console.log("Pick position is a valid integer");

    //Set position
    let response = await fetch(`/admin/api/pick_position/${pickID}?posn=${position}`, {"method": "POST"});
    let data = await response.json();
    if (data.error) {
        alert(`Failed to set pick position: ${data.error}`);
        return;
    }

    //On success, remove input and button. Replace with `position` as text
    let cell = event.target.parentElement;
    event.target.previousElementSibling.remove();
    event.target.remove();
    cell.innerText = position;
}

async function validateName(event) {
    let nameValue = event.target.previousElementSibling.value;
    console.log(`validateName: nameValue=${nameValue}`);

    let response = await fetch(`/admin/api/validate_name/${nameValue}`);
    let data = await response.json();
    let iconText =  "✅";
    if (data.error) {
        alert(`Invalid name: ${data.error}`);
        iconText = "❌";
    }
    if (data.name) {
        iconText += ` (${data.name})`;
    }
    let icon = document.createElement("span");
    icon.classList.add("validation");
    icon.innerText = iconText;
    event.target.parentElement.appendChild(icon);
    return;
}

async function writeName (event) {
    let pickID = event.target.parentElement.parentElement.id.split("_")[1];
    let nameValue = event.target.previousElementSibling.previousElementSibling.value;
    console.log(`writeName: nameValue=${nameValue}`);

    //Ask server to write person ID as draft pick selection
    let response = await fetch(`/admin/api/pick_name/${pickID}?name=${nameValue}`, {"method": "POST"});
    let data = await response.json();
    if (data.error) {
        alert(`Invalid update: ${data.error}`);
        return;
    }
    let nameLink = document.createElement("a");
    nameLink.href = `/person/${data.id}`;
    nameLink.innerText = data.name;

    let cell = event.target.parentElement;
    while (cell.children.length > 0) {
        cell.children[0].value = "";
        cell.children[0].remove();
    }
    cell.appendChild(nameLink);
}

function removeValidationIcon(event) {
    let validationIcon = event.target.parentElement.getElementsByClassName("validation")[0];
    if (!validationIcon) {
        return;
    }
    validationIcon.remove();
}

async function addCompensatory(event) {
    const form = event.target.parentElement;
    const position = form.querySelector('#pick-position').value;
    const team = form.querySelector('#pick-team').value;
    if (!team) {
        alert('Team must be selected to add a compensatory pick');
        return;
    }
    const url = `/admin/api/add-compensatory?team=${team}&position=${position}`;
    const response = await fetch(url, {'method': 'POST'});
    if (response.ok) {
        alert('Compensatory pick successfully added. Refresh page to view');
        return;
    }
    const text = response.text();
    alert(`Error creating pick: ${text}`);
}
