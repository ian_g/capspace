import os
import logging

# from logging.handlers import SMTPHandler
from logging.handlers import RotatingFileHandler
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_admin import Admin, AdminIndexView
from flask_admin.contrib.sqla import ModelView
from sqlalchemy import MetaData, inspect
from sqlalchemy.exc import OperationalError
from flask_apscheduler import APScheduler

from config import config

convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}

metadata = MetaData(naming_convention=convention)
db = SQLAlchemy(metadata=metadata)
migrate = Migrate()
login = LoginManager()
login.login_view = "login"
scheduler = APScheduler()
flask_admin = Admin(template_mode="bootstrap4")


def create_app(config_name="default"):
    app = Flask(__name__)
    app.config.from_object(config[config_name])

    db.init_app(app)
    migrate.init_app(app, db, render_as_batch=True)
    login.init_app(app)
    flask_admin.init_app(
        app,
        index_view=AdminIndexView(
            name="Database",
            template="flask-admin/index.html",
            url="/database",
            endpoint="flask-admin",
        ),
    )
    flask_admin.index_view.extra_css = [
        "/static/css/base.css",
        "/static/css/flask-admin.css",
    ]

    scheduler.init_app(app)
    scheduler.start()

    with app.app_context():
        from .models import Configuration

        try:
            settings = Configuration.query.all()
        except OperationalError as e:
            settings = {}
        for row in settings:
            if row.value.isdigit():
                app.config[row.key] = int(row.value)
            else:
                app.config[row.key] = row.value

    from .errors import bp as errors_bp

    app.register_blueprint(errors_bp)

    from .user import bp as user_bp

    app.register_blueprint(user_bp)

    from .main import bp as main_bp

    app.register_blueprint(main_bp)

    from .admin import bp as admin_bp

    app.register_blueprint(admin_bp)

    from .database import bp as database_bp

    app.register_blueprint(database_bp)

    if not app.debug:
        #    if app.config["MAIL_SERVER"]:
        #        auth = None
        #        if app.config["MAIL_USERNAME"] or app.config["MAIL_PASSWORD"]:
        #            auth = (app.config["MAIL_USERNAME", app.config["MAIL_PASSWORD"]
        #        secure = None
        #        if app.config["MAIL_USER_TLS"]:
        #            secure = ()
        #        mail_handler = SMTPHandler(
        #            mailhost = (app.config["MAIL_SERVER"], app.config["MAIL_PORT"]),
        #            fromaddr = "no-reply@" + app.config["MAIL_SERVER"],
        #            toaddrs = app.config["ADMINS"], subject="Capsite Error",
        #            credentials = auth, secure = secure)
        #        mail_handler.setLevel(logging.ERROR)
        #        app.logger.addHandler(mail_handler)
        if not os.path.exists("log"):
            os.mkdir("log")
        file_name = app.config["LOG_FILE"] or "site.log"
        file_handler = RotatingFileHandler(
            "log/" + file_name, maxBytes=10240, backupCount=10
        )
        file_handler.setFormatter(
            logging.Formatter(
                "%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]"
            )
        )
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)
        app.logger.setLevel(logging.INFO)
        app.logger.info("Capsite start")

    return app
