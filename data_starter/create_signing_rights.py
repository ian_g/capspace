import sys
import json
import urllib.request
from urllib.error import HTTPError, URLError
from time import sleep
from datetime import date
from sqlalchemy import select
from app import create_app, db
from app.models import Person, Asset, Contract, SigningRights


def main(args):
    app = create_app("default")
    app_context = app.app_context()
    app_context.push()

    if args:
        app.config["SQLALCHEMY_DATABASE_URI"] = args[0]
    rfas = query_rfas(app.config["CURRENT_SEASON"] - 1)
    # print(rfas)
    for rfa in rfas:
        create_signing_rights(rfa)

    app_context.pop()


def query_rfas(season_id):
    active_contracts = (
        Asset.query.filter(Asset.active == True)
        .filter(Asset.type == "Contract")
        .with_entities(
            Asset.id,
        )
        .subquery()
    )
    people = (
        Person.query.join(Contract, Contract.person_id == Person.id)
        .join(ContractYear, ContractYear.contract_id == Contract.id)
        .filter(Contract.id.not_in(select(active_contracts)))
        .filter(Person.current_status.like("%RFA%"))
        .filter(ContractYear.season_id == season_id)
        .with_entities(
            Person.id,
        )
        .group_by(Person.id)
        .all()
    )
    return list(map(lambda row: row[0], people))


def create_signing_rights(person_id):
    last_team = query_last_team(person_id)
    asset = Asset(
        active=True,
        current_team=last_team,
        originating_team=last_team,
        type="Signing Rights",
    )
    db.session.add(asset)
    db.session.flush()
    rights = SigningRights(
        id=asset.id,
        person_id=person_id,
    )
    db.session.add(rights)
    db.session.commit()


def query_last_team(person_id):
    last_team = (
        Asset.query.join(Contract, Contract.id == Asset.id)
        .filter(Contract.person_id == person_id)
        .filter(Asset.active == False)
        .with_entities(
            Asset.current_team,
        )
        .order_by(
            Contract.signing_date.desc(),
        )
        .first()
    )
    return last_team[0]


if __name__ == "__main__":
    main(sys.argv[1:])
