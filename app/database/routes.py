import json
from flask import redirect, url_for, abort, request
from flask_login import current_user
from flask_admin import Admin, AdminIndexView
from flask_admin.model import typefmt
from flask_admin.contrib.sqla import ModelView
from markupsafe import Markup

from .. import db, flask_admin
from ..models import (
    Team,
    Season,
    TeamSeason,
    Transaction,
    TransactionAsset,
    Asset,
    Person,
    DraftPick,
    Contract,
    ContractYear,
    BuyoutYear,
    RetainedSalary,
    Note,
    Configuration,
    SkaterSeason,
    GoalieSeason,
    Penalty,
    PenaltyYear,
    DailyCap,
    Alias,
)


def bool_formatter(view, value):
    if value:
        return "True"
    return "False"


def currency_formatter(value):
    if not value:
        return ""
    return "${:,}".format(value)


FORMATTERS = {
    type(None): typefmt.null_formatter,
    bool: bool_formatter,
    list: typefmt.list_formatter,
    dict: typefmt.dict_formatter,
}


class ModelViewV2(ModelView):
    can_view_details = True
    column_display_pk = True
    column_hide_backrefs = False
    column_type_formatters = FORMATTERS
    can_set_page_size = True
    action_disallowed_list = ["delete"]
    extra_css = [
        "/static/css/base.css",
        "/static/css/flask-admin.css",
    ]

    def is_accessible(self):
        return current_user.is_authenticated and current_user.admin == True

    def inaccessible_callback(self, name, **kwargs):
        if not current_user.is_authenticated:
            return redirect(url_for("user.login", next=request.url))
        abort(403)

    # edit_template = None   #Custom template to ignore this arg entirely
    # list_template = #Custom template
    # create_template = #Decide if need this. If so, custom template. If not, None


class TeamView(ModelViewV2):
    column_list = [
        "id",
        "name",
        "abbreviation",
        "team_name",
        "team_location",
        "first_year",
        "general_manager",
        "head_coach",
        "logo",
    ]
    column_details_list = column_list
    column_searchable_list = ["id", "name", "abbreviation"]
    column_editable_list = [
        "name",
        "abbreviation",
        "team_name",
        "team_location",
        "general_manager",
        "head_coach",
    ]
    column_default_sort = "name"


class AliasView(ModelViewV2):
    column_list = ["id", "person", "person_id", "value", "search"]
    column_details_list = column_list
    column_searchable_list = ["value", "search", "person_id"]
    column_editable_list = ["value", "search"]
    column_labels = {
        "value": "Alias",
        "search": "Search-only alias",
    }
    form_widget_args = {
        "person": {
            "disabled": True,
        },
    }


class DraftView(ModelViewV2):
    column_list = ["id", "round", "position", "season", "person"]
    column_details_list = column_list
    column_editable_list = ["position", "person"]
    column_searchable_list = ["person.name"]


class SkaterView(ModelViewV2):
    column_list = [
        "id",
        "team",
        "person",
        "season",
        "goals",
        "assists",
        "points",
        "games_played",
        "hits",
        "shots",
        "penalty_minutes",
    ]
    column_details_list = column_list
    column_editable_list = [
        "goals",
        "assists",
        "points",
        "games_played",
        "hits",
        "shots",
        "penalty_minutes",
    ]
    column_sortable_list = column_editable_list
    form_widget_args = {
        "person": {
            "disabled": True,
        },
        "season": {
            "disabled": True,
        },
        "team": {
            "disabled": True,
        },
    }


class GoalieView(ModelViewV2):
    column_list = [
        "id",
        "team",
        "person",
        "season",
        "starts",
        "wins",
        "losses",
        "overtime_losses",
        "shutouts",
        "shots_against",
        "goals_against",
        "games_played",
    ]
    column_details_list = column_list
    column_editable_list = [
        "starts",
        "wins",
        "losses",
        "overtime_losses",
        "shutouts",
        "shots_against",
        "goals_against",
        "games_played",
    ]
    column_sortable_list = column_editable_list
    form_widget_args = {
        "person": {
            "disabled": True,
        },
        "season": {
            "disabled": True,
        },
        "team": {
            "disabled": True,
        },
    }


class PersonView(ModelViewV2):
    column_list = [
        "id",
        "nhl_id",
        "ep_id",
        "name",
        "birthdate",
        "death_date",
        "nationality",
        "position",
        "height",
        "weight",
        "shoots",
        "elc_signing_age",
        "waivers_signing_age",
        "arbitration",
        "waivers_exempt",
        "current_status",
        "active",
        "number",
    ]
    column_details_list = [
        "id",
        "nhl_id",
        "name",
        "birthdate",
        "birthplace",
        "death_date",
        "nationality",
        "position",
        "height",
        "weight",
        "shoots",
        "elc_signing_age",
        "waivers_signing_age",
        "draft_pick",
        "arbitration",
        "waivers_exempt",
        "current_status",
        "active",
        "rookie",
        "captain",
        "alternate_captain",
        "non_roster",
        "number",
        "ep_id",
        "contracts",
        "aliases",
    ]
    column_editable_list = [
        "position",
        "height",
        "weight",
        "elc_signing_age",
        "waivers_signing_age",
        "arbitration",
        "waivers_exempt",
        "current_status",
        "active",
        "number",
    ]
    column_searchable_list = [
        "name",
        "height",
        "weight",
        "position",
        "arbitration",
        "waivers_exempt",
        "current_status",
        "active",
        "number",
    ]
    column_sortable_list = [
        "name",
        "height",
        "position",
        "waivers_exempt",
        "current_status",
        "active",
        "number",
    ]
    column_labels = {
        "nhl_id": "NHL Player ID",
        "ep_id": "EliteProspects Player ID",
        "arbitration": "Arbitration Eligible",
        "elc_signing_age": "ELC Age",
        "waivers_signing_age": "Waivers Age",
    }


class ContractView(ModelViewV2):
    column_list = [
        "id",
        "person",
        "total_value",
        "years",
        "signing_date",
        "buyout_date",
        "termination_date",
        "expiration_status",
        "entry_level",
        "contract_limit_exempt",
        "thirty_five_plus",
        "extension",
    ]
    column_details_list = [
        "id",
        "person_id",
        "years",
        "signing_date",
        "buyout_date",
        "termination_date",
        "total_value",
        "expiration_status",
        "entry_level",
        "elc_slide",
        "contract_limit_exempt",
        "thirty_five_plus",
        "non_roster",
        "on_loan",
        "extension",
        "signing_season_id",
        "contract_years",
        "buyout_years",
        "signing_season",
        "person",
        "penalties",
        "retention",
    ]
    column_editable_list = [
        "total_value",
        "years",
        "signing_date",
        "buyout_date",
        "expiration_status",
        "entry_level",
        "contract_limit_exempt",
    ]
    column_searchable_list = [
        "person.name",
        "total_value",
        "years",
        "entry_level",
        "signing_season.name",
        "thirty_five_plus",
        "non_roster",
        "on_loan",
    ]
    column_sortable_list = [
        "person.name",
        "total_value",
        "years",
        "entry_level",
        "signing_season.name",
        "thirty_five_plus",
        "non_roster",
        "on_loan",
    ]
    form_widget_args = {
        "person": {
            "disabled": True,
        },
        "contract_years": {
            "disabled": True,
        },
        "buyout_years": {
            "disabled": True,
        },
        "penalties": {
            "disabled": True,
        },
        "signing_season": {
            "disabled": True,
        },
        "retention": {
            "disabled": True,
        },
    }
    column_labels = {
        "elc": "Entry-Level",
        "thirty_five_plus": "35+",
    }


class ContractYearView(ModelViewV2):
    column_list = [
        "id",
        "contract",
        "contract.person",
        "season",
        "cap_hit",
        "nhl_salary",
        "minors_salary",
        "signing_bonus",
        "two_way",
        "nmc",
        "ntc",
        "clause_limits",
    ]
    column_details_list = [
        "id",
        "contract",
        "season",
        "aav",
        "nhl_salary",
        "minors_salary",
        "performance_bonuses",
        "earned_performance_bonuses",
        "signing_bonus",
        "cap_hit",
        "bought_out",
        "terminated",
        "two_way",
    ]
    column_editable_list = [
        "cap_hit",
        "nhl_salary",
        "minors_salary",
        "signing_bonus",
        "bought_out",
        "terminated",
        "entry_level_slide",
        "nmc",
        "ntc",
        "clause_limits",
    ]
    column_searchable_list = [
        "contract.person.name",
        "season.name",
        "entry_level_slide",
        "ntc",
        "nmc",
        "bought_out",
        "terminated",
        "minors_salary",
        "nhl_salary",
    ]
    column_sortable_list = [
        "cap_hit",
        "nhl_salary",
        "minors_salary",
        "bought_out",
        "entry_level_slide",
        "season",
        "nmc",
        "ntc",
    ]
    column_formatters = {
        "aav": lambda v, c, m, p: currency_formatter(m.aav),
        "cap_hit": lambda v, c, m, p: currency_formatter(m.cap_hit),
        "signing_bonus": lambda v, c, m, p: currency_formatter(m.signing_bonus),
    }
    form_widget_args = {
        "contract": {
            "disabled": True,
        },
        "season": {
            "disabled": True,
        },
    }
    column_labels = {
        "entry_level_slide": "ELC Slide",
        "nhl_salary": "NHL Salary",
        "nmc": "NMC",
        "ntc": "NTC",
        "aav": "AAV",
    }


class ConfigurationView(ModelViewV2):
    column_list = ["id", "key", "value"]
    column_details_list = column_list
    column_editable_list = ["value"]
    column_searchable_list = ["key"]
    column_sortable_list = ["key", "value"]
    column_labels = {
        "key": "Setting Name",
        "value": "Setting Value",
    }
    form_widget_args = {
        "key": {
            "disabled": True,
        }
    }


class NoteView(ModelViewV2):
    column_list = [
        "record_table",
        "record_id",
        "note",
        "safe",
    ]
    column_details_list = column_list
    column_searchable_list = ["note"]
    column_sortable_list = [
        "record_table",
        "record_id",
        "safe",
    ]
    form_widget_args = {
        "record_table": {
            "disabled": True,
        },
        "record_id": {
            "disabled": True,
        },
    }


class AssetView(ModelViewV2):
    column_list = [
        "id",
        "active",
        "type",
        "originating",
        "current",
        "draft_pick",
        "contract",
    ]
    column_details_list = column_list
    column_editable_list = ["current"]
    column_searchable_list = ["type", "originating.name", "current.name"]
    column_sortable_list = [
        "originating",
        "current",
        "active",
        "type",
    ]
    form_widget_args = {
        "originating": {
            "disabled": True,
        },
        "type": {
            "disabled": True,
        },
    }
    column_labels = {
        "originating": "Original Team",
        "current": "Current Team",
    }


class SeasonView(ModelViewV2):
    column_list = [
        "id",
        "name",
        "salary_cap",
        "salary_floor",
        "minimum_salary",
        "max_buriable_hit",
        "free_agency_opening",
        "regular_season_start",
        "regular_season_end",
        "playoffs_start",
        "draft_date",
    ]
    column_details_list = [
        "id",
        "name",
        "salary_cap",
        "salary_floor",
        "minimum_salary",
        "max_buriable_hit",
        "bonus_overage_limit",
        "draft_date",
        "free_agency_opening",
        "regular_season_start",
        "regular_season_end",
        "playoffs_start",
        "max_elc_salary",
        "max_elc_minor_salary",
        "max_elc_performance_bonuses",
    ]
    column_editable_list = [
        "salary_cap",
        "salary_floor",
        "minimum_salary",
        "max_buriable_hit",
        "free_agency_opening",
        "regular_season_start",
        "regular_season_end",
        "playoffs_start",
        "draft_date",
    ]
    column_searchable_list = [
        "name",
    ]
    column_sortable_list = [
        "name",
        "salary_cap",
        "salary_floor",
        "draft_date",
        "free_agency_opening",
        "regular_season_start",
        "playoffs_start",
    ]
    column_labels = {
        "max_elc_salary": "Max ELC Salary",
        "max_elc_minor_salary": "Max ELC Minors Salary",
        "max_elc_performance_bonuses": "Max ELC P. Bonuses",
    }
    column_formatters = {
        "salary_cap": lambda v, c, m, p: currency_formatter(m.salary_cap),
        "salary_floor": lambda v, c, m, p: currency_formatter(m.salary_floor),
        "minimum_salary": lambda v, c, m, p: currency_formatter(m.minimum_salary),
        "max_buriable_hit": lambda v, c, m, p: currency_formatter(m.max_buriable_hit),
    }


class RetentionView(ModelViewV2):
    column_list = [
        "id",
        "retention",
        "retained_by_team",
        "contract",
        "retained_from",
    ]
    column_details_list = column_list
    column_editable_list = [
        "retention",
        "retained_from",
    ]
    column_searchable_list = [
        "retained_by_team.name",
        "contract.person.name",
        "retained_from",
    ]
    column_sortable_list = ["contract", "retained_from"]
    form_widget_args = {
        "retained_by_team": {
            "disabled": True,
        },
        "contract": {
            "disabled": True,
        },
    }
    column_labels = {
        "retained_by_team": "Retained By",
    }


class BuyoutView(ModelViewV2):
    column_list = [
        "id",
        "contract",
        "season",
        "signing_bonus",
        "nhl_salary",
        "cap_hit",
        "compliance",
    ]
    column_details_list = column_list
    column_editable_list = [
        "signing_bonus",
        "nhl_salary",
        "cap_hit",
        "compliance",
    ]
    column_searchable_list = [
        "contract.person.name",
        "nhl_salary",
        "cap_hit",
    ]
    column_sortable_list = [
        "cap_hit",
        "nhl_salary",
        "compliance",
    ]
    form_widget_args = {
        "contract": {
            "disabled": True,
        },
        "season": {
            "disabled": True,
        },
    }
    column_labels = {
        "nhl_salary": "NHL Salary",
    }
    column_formatters = {
        "nhl_salary": lambda v, c, m, p: currency_formatter(m.nhl_salary),
        "cap_hit": lambda v, c, m, p: currency_formatter(m.cap_hit),
        "signing_bonus": lambda v, c, m, p: currency_formatter(m.signing_bonus),
    }


class PenaltyView(ModelViewV2):
    column_list = [
        "id",
        "type",
        "years",
        "total_value",
        "contract",
    ]
    column_details_list = [
        "id",
        "type",
        "years",
        "total_value",
        "contract",
        "penalty_years",
    ]
    column_editable_list = [
        "type",
        "years",
        "total_value",
    ]
    column_searchable_list = [
        "contract.person.name",
        "years",
        "type",
    ]
    column_sortable_list = [
        "years",
        "type",
        "total_value",
    ]
    form_widget_args = {
        "penalty_years": {
            "disabled": True,
        },
        "contract": {
            "disabled": True,
        },
    }
    column_formatters = {
        "total_value": lambda v, c, m, p: currency_formatter(m.total_value),
    }


class PenaltyYearView(ModelViewV2):
    column_list = [
        "id",
        "penalty",
        "season",
        "cap_hit",
    ]
    column_details_list = column_list
    column_editable_list = [
        "cap_hit",
    ]
    column_sortable_list = ["cap_hit"]
    form_widget_args = {
        "penalty": {
            "disabled": True,
        },
        "season": {
            "disabled": True,
        },
    }
    column_formatters = {
        "cap_hit": lambda v, c, m, p: current_formatter(m.cap_hit),
    }


class DailyCapView(ModelViewV2):
    column_list = [
        "id",
        "team",
        "season",
        "contract",
        "date",
        "non_roster",
        "retained",
        "cap_hit",
        "ir",
        "ltir",
        "soir",
    ]
    column_details_list = column_list
    column_editable_list = [
        "non_roster",
        "retained",
        "cap_hit",
        "ir",
        "ltir",
        "soir",
    ]
    column_sortable_list = [
        "date",
        "cap_hit",
        "retained",
        "ir",
        "ltir",
        "soir",
    ]
    form_widget_args = {
        "season": {
            "disabled": True,
        },
        "contract": {
            "disabled": True,
        },
        "team": {
            "disabled": True,
        },
    }
    column_labels = {
        "non_roster": "Non-roster",
        "ir": "Injured Reserve",
        "ltir": "Long-term IR",
        "ir": "Season-Opening IR",
    }
    column_formatters = {
        "cap_hit": lambda v, c, m, p: currency_formatter(m.cap_hit),
    }


class TransactionView(ModelViewV2):
    column_list = [
        "id",
        "date",
        "season",
        "type",
    ]
    column_details_list = column_list
    column_editable_list = [
        "date",
        "type",
    ]
    column_searchable_list = [
        "type",
        "date",
    ]
    column_sortable_list = [
        "type",
        "date",
    ]
    form_widget_args = {
        "season": {
            "disabled": True,
        },
    }


class TransactionAssetView(ModelViewV2):
    column_list = [
        "id",
        "transaction",
        "asset",
        "former",
        "new",
        "condition",
        "condition_met",
    ]
    column_details_list = column_list
    column_editable_list = [
        "new",
        "condition",
        "condition_met",
    ]
    column_searchable_list = [
        "former.name",
        "new.name",
        "condition",
    ]
    column_sortable_list = [
        "former",
        "new",
        "condition",
        "id",
    ]
    form_widget_args = {
        "asset": {
            "disabled": True,
        },
        "former": {
            "disabled": True,
        },
        "new": {
            "disabled": True,
        },
        "transaction": {
            "disabled": True,
        },
    }
    column_labels = {
        "former": "Former Team",
        "new": "New Team",
    }


class TeamSeasonView(ModelViewV2):
    column_list = [
        "id",
        "season",
        "team",
        "cap_hit",
        "projected_cap_hit",
        "standings_points",
        "contract_count",
        "retained_contracts",
        "max_ltir_relief",
    ]
    column_details_list = column_list
    column_editable_list = [
        "cap_hit",
        "contract_count",
        "retained_contracts",
        "max_ltir_relief",
    ]
    column_sortable_list = ["cap_hit", "contract_count", "retained_contracts"]
    form_widget_args = {
        "team": {
            "disabled": True,
        },
        "season": {
            "disabled": True,
        },
    }
    column_labels = {
        "field": "Value",
    }
    column_formatters = {
        "cap_hit": lambda v, c, m, p: currency_formatter(m.cap_hit),
    }


flask_admin.add_views(
    TeamView(Team, db.session, category="Team"),
    DraftView(DraftPick, db.session, category="Team"),
    PersonView(Person, db.session, category="People"),
    AliasView(Alias, db.session, category="People"),
    SkaterView(SkaterSeason, db.session, category="People"),
    GoalieView(GoalieSeason, db.session, category="People"),
    ContractView(Contract, db.session, category="Contract"),
    ContractYearView(ContractYear, db.session, category="Contract"),
    BuyoutView(BuyoutYear, db.session, category="Contract"),
    RetentionView(RetainedSalary, db.session, category="Contract"),
    PenaltyView(Penalty, db.session, category="Contract"),
    PenaltyYearView(PenaltyYear, db.session, category="Contract"),
    TeamSeasonView(TeamSeason, db.session, category="Rosters"),
    TransactionView(Transaction, db.session, category="Rosters"),
    TransactionAssetView(TransactionAsset, db.session, category="Rosters"),
    DailyCapView(DailyCap, db.session, category="Rosters"),
    ConfigurationView(Configuration, db.session, category="Other"),
    AssetView(Asset, db.session, category="Other"),
    SeasonView(Season, db.session, category="Other"),
    NoteView(Note, db.session, category="Other"),
)
