import sys
import random
import unittest
from datetime import datetime, date, timedelta
from sqlalchemy import func
from app import create_app, db
from app.models import (
    TeamSeason,
    Team,
    Season,
    DailyCap,
    Person,
    Contract,
    ContractYear,
)

from app.main import team_season

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class MainTeamSeasonTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_get_team_season(self):
        team_id = 5
        self.assertEqual(team_season.get_team_season(team_id, None), {})
        db.session.add(
            TeamSeason(
                team_id=team_id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=12345,
                projected_cap_hit=5231,
                contract_count=4,
                retained_contracts=15,
                max_ltir_relief=1209,
            )
        )
        db.session.commit()
        team_szn = team_season.get_team_season(team_id, None)
        self.assertEqual(team_szn["cap_hit"], 5231)
        self.assertEqual(team_szn["contracts"], 4)
        self.assertEqual(team_szn["retained_contracts"], 15)
        self.assertEqual(team_szn["max_ltir_relief"], 1209)

    def test_get_team_season2(self):
        team_id = 5
        self.assertEqual(team_season.get_team_season(team_id, None), {})
        db.session.add(
            TeamSeason(
                team_id=team_id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=12345,
                projected_cap_hit=5231,
                contract_count=4,
                retained_contracts=15,
            )
        )
        db.session.commit()
        team_szn = team_season.get_team_season(team_id, None)
        self.assertEqual(team_szn["cap_hit"], 5231)
        self.assertEqual(team_szn["contracts"], 4)
        self.assertEqual(team_szn["retained_contracts"], 15)
        self.assertEqual(team_szn["max_ltir_relief"], 0)

    def test_homepage_data(self):
        teams = [
            Team(active=True, id=38, name="Team One", logo="no"),
            Team(active=True, id=48, name="Team Alpha", logo="yes"),
            Team(active=True, id=28, name="Team Primary", logo="color"),
            Team(active=True, id=7, name="Team Momo", logo="marble"),
        ]
        team_seasons = [
            TeamSeason(
                team_id=teams[0].id,
                season_id=self.app.config["CURRENT_SEASON"],
                contract_count=33,
                cap_hit=100000,
                projected_cap_hit=200000,
            ),
            TeamSeason(
                team_id=teams[1].id,
                season_id=self.app.config["CURRENT_SEASON"],
                contract_count=28,
                cap_hit=10000000,
                projected_cap_hit=92000000,
            ),
            TeamSeason(
                team_id=teams[2].id,
                season_id=self.app.config["CURRENT_SEASON"],
                contract_count=47,
                cap_hit=10003000,
                projected_cap_hit=22000000,
            ),
            TeamSeason(
                team_id=teams[3].id,
                season_id=self.app.config["CURRENT_SEASON"],
                contract_count=50,
                cap_hit=1080000,
                projected_cap_hit=82000000,
            ),
        ]
        season = Season(id=self.app.config["CURRENT_SEASON"], salary_cap=1000)
        for record in teams + team_seasons + [season]:
            db.session.add(record)
        db.session.commit()

        data = team_season.homepage_data()
        self.assertEqual(len(data), 4)
        self.assertEqual(data[0]["id"], 48)
        self.assertEqual(data[0]["name"], "Team Alpha")
        self.assertEqual(data[0]["logo"], "yes")
        self.assertEqual(data[0]["projected_cap"], 92000000)
        self.assertEqual(data[0]["used_cap"], 10000000)
        self.assertEqual(data[0]["contracts"], 28)
        self.assertEqual(data[1]["id"], 7)
        self.assertEqual(data[1]["name"], "Team Momo")
        self.assertEqual(data[1]["logo"], "marble")
        self.assertEqual(data[1]["projected_cap"], 82000000)
        self.assertEqual(data[1]["used_cap"], 1080000)
        self.assertEqual(data[1]["contracts"], 50)
        self.assertEqual(data[2]["id"], 28)
        self.assertEqual(data[2]["name"], "Team Primary")
        self.assertEqual(data[2]["logo"], "color")
        self.assertEqual(data[2]["projected_cap"], 22000000)
        self.assertEqual(data[2]["used_cap"], 10003000)
        self.assertEqual(data[2]["contracts"], 47)
        self.assertEqual(data[3]["id"], 38)
        self.assertEqual(data[3]["name"], "Team One")
        self.assertEqual(data[3]["logo"], "no")
        self.assertEqual(data[3]["projected_cap"], 200000)
        self.assertEqual(data[3]["used_cap"], 100000)
        self.assertEqual(data[3]["contracts"], 33)

    def test_daily_cap_data(self):
        TEAM = 38
        ABBR = "tst"
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"],
                name="Test Current",
                regular_season_start=date(year=2020, month=1, day=1),
                regular_season_end=date(year=2020, month=1, day=11),
                salary_cap=1,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                name="Test Other",
                regular_season_start=date(year=2020, month=1, day=1),
                regular_season_end=date(year=2020, month=1, day=11),
                salary_cap=1,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 2,
                name="Test Third",
                regular_season_start=date(year=2020, month=1, day=1),
                regular_season_end=date(year=2020, month=1, day=11),
                salary_cap=1,
            ),
        ]
        team = Team(id=TEAM, abbreviation="TST", name="Test Team")
        people = [
            Person(id=1, name="Test NHL"),
            Person(id=2, name="Test Minors"),
            Person(id=3, name="Test All"),
        ]
        contracts = [
            Contract(id=11, person_id=people[0].id, non_roster=False),
            Contract(id=12, person_id=people[1].id, non_roster=True),
            Contract(id=13, person_id=people[2].id, non_roster=False),
        ]
        contract_years = [
            ContractYear(
                contract_id=contracts[0].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=5000,
            ),
            ContractYear(
                contract_id=contracts[1].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=4000,
            ),
            ContractYear(
                contract_id=contracts[2].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=3000,
            ),
        ]
        daily_cap = [
            DailyCap(
                team_id=TEAM,
                season_id=self.app.config["CURRENT_SEASON"],
                contract_id=contracts[0].id,
                date=date(year=2020, month=1, day=1),
                non_roster=False,
                cap_hit=100,
                retained=False,
                ir=False,
                soir=False,
                ltir=False,
            ),
            DailyCap(
                team_id=TEAM,
                season_id=self.app.config["CURRENT_SEASON"],
                contract_id=contracts[1].id,
                date=date(year=2020, month=1, day=1),
                non_roster=True,
                cap_hit=10,
                retained=False,
                ir=False,
                soir=False,
                ltir=False,
            ),
            DailyCap(
                team_id=TEAM,
                season_id=self.app.config["CURRENT_SEASON"],
                contract_id=contracts[1].id,
                date=date(year=2020, month=1, day=2),
                non_roster=True,
                cap_hit=20,
                retained=False,
                ir=False,
                soir=False,
                ltir=False,
            ),
            DailyCap(
                team_id=TEAM,
                season_id=self.app.config["CURRENT_SEASON"],
                contract_id=contracts[2].id,
                date=date(year=2020, month=1, day=1),
                non_roster=False,
                cap_hit=100,
                retained=False,
                ir=False,
                soir=False,
                ltir=False,
            ),
            DailyCap(
                team_id=TEAM,
                season_id=self.app.config["CURRENT_SEASON"],
                contract_id=contracts[2].id,
                date=date(year=2020, month=1, day=2),
                non_roster=True,
                cap_hit=20,
                retained=False,
                ir=False,
                soir=False,
                ltir=False,
            ),
            DailyCap(
                team_id=TEAM,
                season_id=self.app.config["CURRENT_SEASON"],
                contract_id=contracts[2].id,
                date=date(year=2020, month=1, day=3),
                non_roster=False,
                cap_hit=50,
                retained=True,
                ir=False,
                soir=False,
                ltir=False,
            ),
        ]
        for record in (
            seasons + [team] + people + contracts + contract_years + daily_cap
        ):
            db.session.add(record)
        db.session.commit()

        data = team_season.daily_cap_data(ABBR)
        self.assertEqual(len(data["roster"]), 4)
        self.assertEqual(len(data["seasons"]), 3)
        self.assertEqual(data["season"], "Test Current")
        self.assertEqual(data["seasons"], ["Test Current", "Test Other", "Test Third"])

        self.assertEqual(data["roster"][0]["person_name"], "Test NHL")
        self.assertEqual(data["roster"][0]["person_id"], 1)
        self.assertEqual(data["roster"][0]["cap_hit"], 5000)
        self.assertEqual(data["roster"][0]["non_roster"], False)
        self.assertEqual(data["roster"][0]["accumulated_cap"], 100)
        self.assertEqual(data["roster"][0]["retained"], False)
        self.assertEqual(data["roster"][0]["total_days"], 1)
        self.assertEqual(data["roster"][0]["nhl_days"], 1)
        self.assertEqual(data["roster"][0]["minors_days"], 0)
        self.assertEqual(data["roster"][0]["projected_cap"], 1000)
        self.assertEqual(data["roster"][1]["person_name"], "Test All")
        self.assertEqual(data["roster"][1]["person_id"], 3)
        self.assertEqual(data["roster"][1]["cap_hit"], 3000)
        self.assertEqual(data["roster"][1]["non_roster"], False)
        self.assertEqual(data["roster"][1]["accumulated_cap"], 120)
        self.assertEqual(data["roster"][1]["retained"], False)
        self.assertEqual(data["roster"][1]["total_days"], 2)
        self.assertEqual(data["roster"][1]["nhl_days"], 1)
        self.assertEqual(data["roster"][1]["minors_days"], 1)
        self.assertEqual(data["roster"][1]["projected_cap"], 600)
        self.assertEqual(data["roster"][2]["person_name"], "Test Minors")
        self.assertEqual(data["roster"][2]["person_id"], 2)
        self.assertEqual(data["roster"][2]["cap_hit"], 4000)
        self.assertEqual(data["roster"][2]["non_roster"], True)
        self.assertEqual(data["roster"][2]["accumulated_cap"], 30)
        self.assertEqual(data["roster"][2]["retained"], False)
        self.assertEqual(data["roster"][2]["total_days"], 2)
        self.assertEqual(data["roster"][2]["nhl_days"], 0)
        self.assertEqual(data["roster"][2]["minors_days"], 2)
        self.assertEqual(data["roster"][2]["projected_cap"], 150)

        self.assertEqual(data["roster"][3]["person_name"], "Test All")
        self.assertEqual(data["roster"][3]["person_id"], 3)
        self.assertEqual(data["roster"][3]["cap_hit"], 3000)
        self.assertEqual(data["roster"][3]["non_roster"], False)
        self.assertEqual(data["roster"][3]["accumulated_cap"], 50)
        self.assertEqual(data["roster"][3]["retained"], True)
        self.assertEqual(data["roster"][3]["total_days"], 1)
        self.assertEqual(data["roster"][3]["nhl_days"], 1)
        self.assertEqual(data["roster"][3]["minors_days"], 0)
        self.assertEqual(data["roster"][3]["projected_cap"], 500)


if __name__ == "__main__":
    unittest.main(verbosity=2)
