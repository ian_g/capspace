/* Update "Player" box for extensions */
document.getElementById('team').onchange = updateCheckboxes;
document.getElementById('team').value = '';
document.getElementById('first_season').value = '';
document.getElementById('show-extendable').onclick = updatePlayers;
document.getElementById('show-inactive').onclick = updatePlayers;
document.getElementById('show-rights').onclick = updatePlayers;
document.getElementById('offer_sheet').onclick = updatePlayers;

/* Show/hide contract years */
document.querySelectorAll('[id$="_year"]').forEach(el => {
    el.onclick = (event => showHide(event, el.id.split('_').shift()));
    el.checked = false;
});

/* Sum up total contract value */
document.querySelectorAll('[id$="_performance_bonuses"]')
    .forEach(el => el.onchange = setTotalValue);
document.querySelectorAll('[id$="_signing_bonus"]')
    .forEach(el => el.onchange = setTotalValue);
document.querySelectorAll('[id$="_nhl_salary"]')
    .forEach(el => el.onchange = setTotalValue);

updateCheckboxes({target: document.getElementById('team')});

function showHide(event, dataId) {
    let fields = document.getElementById(dataId);

    if (event.target.checked) {
        fields.style.display = 'table';
    } else {
        fields.style.display = 'none';
    }
}

function setTotalValue() {
    let sum = [
        ...document.querySelectorAll('[id$="_performance_bonuses"]'),
        ...document.querySelectorAll('[id$="_signing_bonus"]'),
        ...document.querySelectorAll('[id$="_nhl_salary"]'),
    ].map(el => parseInt(el.value) || 0).reduce((x, y) => x + y, 0);
    document.getElementById('total_value').value = sum;
}

function updateCheckboxes(event) {
    let checkboxes = [
        document.getElementById('show-extendable'),
        document.getElementById('show-rights'),
    ];
    if (!event.target.value) {
        checkboxes.forEach(el => {el.disabled = true; el.checked = false});
        return;
    }
    checkboxes.forEach((el) => {
        if (el.checked) {
            updatePlayers(event);
        } else {
            el.disabled = false;
        }
    });
}

async function updatePlayers(event) {
    let url;
    let extend = document.getElementById('show-extendable');
    let inactive = document.getElementById('show-inactive');
    let rights = document.getElementById('show-rights');
    let offerSheet = document.getElementById('offer_sheet');
    if (event.target.checked && event.target.id === 'show-extendable') {
        let team = document.getElementById('team');
        url = `/admin/api/extendable/${team.value}`;
        inactive.checked = false;
        rights.checked = false;
    } else if (event.target.checked && event.target.id === 'show-inactive') {
        url = '/admin/api/inactive';
        extend.checked = false;
        rights.checked = false;
    } else if (event.target.checked && event.target.id === 'show-rights') {
        let team = document.getElementById('team');
        url = `/admin/api/signing-rights/${team.value}`;
        extend.checked = false;
        inactive.checked = false;
    } else if (event.target.checked && event.target.id === 'offer_sheet') {
        url = `/admin/api/signing-rights/ALL`;
        extend.checked = false;
        inactive.checked = false;
        rights.checked = false;
    } else {
        extend.checked = false;
        rights.checked = false;
        inactive.checked = false;
        url = '/admin/api/unsigned';
    }
    let players = document.getElementById('person');
    console.log('Updating players');
    let response = await fetch(url);
    if (!response.ok) {
        console.log('Error querying players');
        return;
    }
    let data = await response.json();
    if (data.length == 0) {
        alert('No players returned');
        return;
    }

    //Clear player box
    while (players.firstChild) {
        players.removeChild(players.lastChild);
    }

    //Add new players in
    for (let item of data) {
        let player = document.createElement('option');
        player.value = item[0];
        player.innerText = item[1];
        players.appendChild(player);
    }
}

function populateForm(data) {
    if ('date' in data) {
        document.getElementById('signing_date').value = data.date;
    }
    if ('team' in data) {
        document.getElementById('team').value = [...document.querySelectorAll('#team option')]
            .reduce((value, option) => {
                return option.innerText == data.team ? option.value : value;
            }, null);
    }
    console.log('Set team');
    if ('entryLevel' in data) {
        document.getElementById('entry_level').checked = data.entryLevel;
    }
    if ('entryLevelSlide' in data) {
        document.getElementById('entry_level_slide').value = data.entryLevelSlide;
    }
    console.log('Set entry level');
    if ('firstSeason' in data) {
        document.getElementById('first_season').value = [...document.querySelectorAll('#first_season option')]
            .reduce((value, option) => {
                return option.innerText == data.firstSeason ? option.value : value;
            }, null);
    }
    if ('expiration' in data) {
        document.getElementById('expiration_status').value = data.expiration;
    }
    console.log('Set expiration and first season');
    if (!('years' in data)) {
        console.log('No years');
        return;
    }
    let formYears = [...document.querySelectorAll('form ul li')];
    for (let idx = 0; idx < data.years.length; idx++) {
        let year = formYears[idx];
        console.log(year);
        if (idx != 0) {
            year.querySelector('h2 input').click();
        }
        let inputs = year.querySelectorAll('table input');
        if ('nhlSalary' in data.years[idx]) {
            inputs[0].value = data.years[idx].nhlSalary;
        }
        if ('minorsSalary' in data.years[idx]) {
            inputs[1].value = data.years[idx].minorsSalary;
        } else {
            inputs[1].value = null;
        }
        if ('signingBonus' in data.years[idx]) {
            inputs[2].value = data.years[idx].signingBonus;
        } else {
            inputs[2].value = null;
        }
        if ('performanceBonuses' in data.years[idx]) {
            inputs[3].value = data.years[idx].performanceBonuses;
        } else {
            inputs[3].value = null;
        }
        if ('clause' in data.years[idx]) {
            inputs[4].checked = data.years[idx].clause.includes('NTC');
            inputs[5].checked = data.years[idx].clause.includes('NMC');
        } else {
            inputs[4].checked = false;
            inputs[5].checked = false;
        }
    }
    setTotalValue();
}
