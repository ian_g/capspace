"""NHL data tables

Revision ID: 90f56a63c3a9
Revises: 27798935e4ae
Create Date: 2021-04-17 13:57:28.577873

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "90f56a63c3a9"
down_revision = "27798935e4ae"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "season",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(), nullable=True),
        sa.Column("salary_cap", sa.Integer(), nullable=True),
        sa.Column("salary_floor", sa.Integer(), nullable=True),
        sa.Column("minimum_salary", sa.Integer(), nullable=True),
        sa.Column("max_buriable_hit", sa.Integer(), nullable=True),
        sa.Column("bonus_overage_limit", sa.Integer(), nullable=True),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_index(op.f("ix_season_name"), "season", ["name"], unique=True)
    op.create_table(
        "team",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(), nullable=True),
        sa.Column("general_manager", sa.String(), nullable=True),
        sa.Column("head_coach", sa.String(), nullable=True),
        sa.Column("abbreviation", sa.String(), nullable=True),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("name"),
        sa.UniqueConstraint("abbreviation"),
    )
    op.create_table(
        "asset",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("originating_team", sa.Integer(), nullable=True),
        sa.Column("current_team", sa.Integer(), nullable=True),
        sa.Column("active", sa.Boolean(), nullable=True),
        sa.ForeignKeyConstraint(
            ["current_team"],
            ["team.id"],
        ),
        sa.ForeignKeyConstraint(
            ["originating_team"],
            ["team.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "trade",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("team_one", sa.Integer(), nullable=True),
        sa.Column("team_two", sa.Integer(), nullable=True),
        sa.Column("date", sa.DateTime(), nullable=True),
        sa.Column("season", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["season"],
            ["season.id"],
        ),
        sa.ForeignKeyConstraint(
            ["team_one"],
            ["team.id"],
        ),
        sa.ForeignKeyConstraint(
            ["team_two"],
            ["team.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_index(op.f("ix_trade_date"), "trade", ["date"], unique=False)
    op.create_table(
        "draft_pick",
        sa.Column("id", sa.Integer(), nullable=False),
        # sa.Column('asset_id', sa.Integer(), nullable=True),
        sa.Column("round", sa.Integer(), nullable=True),
        sa.Column("position", sa.Integer(), nullable=True),
        sa.Column("season_id", sa.Integer(), nullable=True),
        sa.Column("person_id", sa.Integer(), nullable=True),
        # sa.ForeignKeyConstraint(['asset_id'], ['asset.id'], ),
        sa.ForeignKeyConstraint(
            ["id"],
            ["asset.id"],
        ),
        sa.ForeignKeyConstraint(
            ["season_id"],
            ["season.id"],
        ),
        sa.ForeignKeyConstraint(
            ["person_id"],
            ["person.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
        # sa.UniqueConstraint('asset_id')
    )
    op.create_table(
        "person",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("nhl_id", sa.Integer(), nullable=True),
        sa.Column("name", sa.String(), nullable=True),
        sa.Column("birthdate", sa.Date(), nullable=True),
        sa.Column("birthplace", sa.String(), nullable=True),
        sa.Column("nationality", sa.String(), nullable=True),
        sa.Column("position", sa.String(), nullable=True),
        sa.Column("height", sa.Integer(), nullable=True),
        sa.Column("weight", sa.Integer(), nullable=True),
        sa.Column("shoots", sa.String(), nullable=True),
        sa.Column("elc_signing_age", sa.String(), nullable=True),
        sa.Column("waivers_signing_age", sa.String(), nullable=True),
        sa.Column("draft_pick", sa.Integer(), nullable=True),
        sa.Column("arbitration", sa.Boolean(), nullable=True),
        sa.Column("waivers", sa.Boolean(), nullable=True),
        sa.Column("current_status", sa.String(), nullable=True),
        sa.ForeignKeyConstraint(
            ["draft_pick"],
            ["asset.id"],
        ),
        sa.UniqueConstraint("nhl_id"),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_index(op.f("ix_person_name"), "person", ["name"], unique=False)
    op.create_table(
        "trade_asset",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("trade", sa.Integer(), nullable=True),
        sa.Column("asset", sa.Integer(), nullable=True),
        sa.Column("former_team", sa.Integer(), nullable=True),
        sa.Column("retained_salary", sa.Boolean(), nullable=True),
        sa.ForeignKeyConstraint(
            ["asset"],
            ["asset.id"],
        ),
        sa.ForeignKeyConstraint(
            ["former_team"],
            ["team.id"],
        ),
        sa.ForeignKeyConstraint(
            ["trade"],
            ["trade.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "contract",
        sa.Column("id", sa.Integer(), nullable=False),
        # sa.Column('asset_id', sa.Integer(), nullable=True),
        sa.Column("person_id", sa.Integer(), nullable=True),
        sa.Column("signing_date", sa.Date(), nullable=True),
        sa.Column("buyout_date", sa.Date(), nullable=True),
        sa.Column("total_value", sa.Integer(), nullable=True),
        sa.Column("expiration_status", sa.String(), nullable=True),
        sa.Column("entry_level", sa.Boolean(), nullable=True),
        sa.Column("elc_slide", sa.Boolean(), nullable=True),
        sa.Column("two_way", sa.Boolean(), nullable=True),
        sa.Column("non_roster", sa.Boolean(), nullable=True),
        sa.Column("contract_limit_exempt", sa.Boolean(), nullable=True),
        sa.Column("thirty_five_plus", sa.Boolean(), nullable=True),
        # sa.ForeignKeyConstraint(['asset_id'], ['asset.id'], ),
        sa.ForeignKeyConstraint(
            ["id"],
            ["asset.id"],
        ),
        sa.ForeignKeyConstraint(
            ["person_id"],
            ["person.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
        # sa.UniqueConstraint('asset_id')
    )
    op.create_table(
        "buyout_year",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("contract_id", sa.Integer(), nullable=True),
        sa.Column("season_id", sa.Integer(), nullable=True),
        sa.Column("signing_bonus", sa.Integer(), nullable=True),
        sa.Column("nhl_salary", sa.Integer(), nullable=True),
        sa.Column("cap_hit", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["contract_id"],
            ["contract.id"],
        ),
        sa.ForeignKeyConstraint(
            ["season_id"],
            ["season.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "contract_year",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("contract_id", sa.Integer(), nullable=True),
        sa.Column("season_id", sa.Integer(), nullable=True),
        sa.Column("total_compensation", sa.Integer(), nullable=True),
        sa.Column("nhl_salary", sa.Integer(), nullable=True),
        sa.Column("minors_salary", sa.Integer(), nullable=True),
        sa.Column("performance_bonuses", sa.Integer(), nullable=True),
        sa.Column("earned_performance_bonuses", sa.Integer(), nullable=True),
        sa.Column("signing_bonus", sa.Integer(), nullable=True),
        sa.Column("cap_hit", sa.Integer(), nullable=True),
        sa.Column("bought_out", sa.Boolean(), nullable=True),
        sa.ForeignKeyConstraint(
            ["contract_id"],
            ["contract.id"],
        ),
        sa.ForeignKeyConstraint(
            ["season_id"],
            ["season.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("contract_year")
    op.drop_table("buyout_year")
    op.drop_table("contract")
    op.drop_table("trade_asset")
    op.drop_index(op.f("ix_person_name"), table_name="person")
    op.drop_table("person")
    op.drop_table("draft_pick")
    op.drop_index(op.f("ix_trade_date"), table_name="trade")
    op.drop_table("trade")
    op.drop_table("asset")
    op.drop_table("team")
    op.drop_index(op.f("ix_season_name"), table_name="season")
    op.drop_table("season")
    # ### end Alembic commands ###
