import factory

from app import db, models


class UserFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.User
        sqlalchemy_session = db.session
        sqlalchemy_get_or_create = ("username",)

    username = factory.Faker("user_name")
    email = factory.Faker("free_email")
    created = factory.Faker("date_object")
    bio = factory.Faker("text")
    last_active = factory.Faker("date_object")
    admin = False
