document.querySelectorAll("[id^='team_']").forEach(element => element.onchange=updateClaim);
document.querySelectorAll("[id^='clear_']").forEach(element => element.onclick=clearWaivers);
document.querySelectorAll("[id^='claim_']").forEach(element => element.onclick=claimWaivers);
document.querySelectorAll("[id^='terminate_']").forEach(element => element.onclick=terminateContract);
document.getElementById("clear-all").onclick = clearAll;
reshadeRows();

function updateClaim(event) {
    let disabled = true;
    if (event.target.value != "") {
        disabled = false;
    }
    let claim = event.target.parentElement.querySelector("[id^='claim']");
    claim.disabled = disabled;
}

function clearAll() {
    if (!confirm("Click 'Ok' to confirm all players on waivers clear")) {
        return;
    }
    document.querySelectorAll("[id^='clear_']").forEach(element => element.click());
}

async function clearWaivers(event) {
    let transactionAsset = event.target.id.split("_")[1];
    let date = document.querySelector("#transaction-date")?.value;
    let toMinors = !document.querySelector(`#in-nhl-${transactionAsset}`).checked;
    console.log(`Transaction Asset: ${transactionAsset}`);
    let url = `/admin/api/clear/${transactionAsset}?minors=${toMinors}`;
    if (date) {
        url += `&date=${date}`;
    }
    let response = await fetch(url, {"method": "POST"});
    let data = await response.json();
    console.log(data);
    if (data.error) {
        alert(`Invalid update: ${data.error}`);
        return;
    }
    event.target.parentElement.parentElement.remove();
    reshadeRows();
}

async function claimWaivers(event) {
    let transactionAsset = event.target.id.split("_")[1];
    let date = document.querySelector("#transaction-date")?.value;
    let teamID = event.target.parentElement.querySelector("select").value;
    if (teamID == "") {
        alert("No team ID provided. Claim cannot be processed");
        return;
    }
    if (!confirm("Click 'Ok' to confirm waiver claim")) {
        return;
    }
    let url = `/admin/api/claim/${transactionAsset}?team=${teamID}`;
    if (date) {
        url += `&date=${date}`;
    }
    let response = await fetch(url, {"method": "POST"});
    let data = await response.json();
    console.log(data);
    if (data.error) {
        alert(`Invalid update: ${data.error}`);
        return;
    }
    event.target.parentElement.parentElement.remove();
    reshadeRows();
}

async function terminateContract(event) {
    if (!confirm("Click 'Ok' to confirm contract termination")) {
        return;
    }
    let transactionAsset = event.target.id.split("_")[1];
    let date = document.querySelector("#transaction-date")?.value;
    let url = `/admin/api/terminate/${transactionAsset}`;
    if (date) {
        url += `?date=${date}`;
    }
    let response = await fetch(url, {"method": "POST"});
    let data = await response.json();
    console.log(data);
    if (data.error) {
        alert(`Invalid update: ${data.error}`);
        return;
    }
    event.target.parentElement.parentElement.remove();
    reshadeRows();
}

async function reshadeRows() {
    let rows = document.querySelector("table").querySelectorAll("tr");
    for (let idx = 0; idx < rows.length; idx++) {
        if (idx % 2 == 0) {
            rows[idx].classList.remove("shaded");
        } else {
            rows[idx].classList.add("shaded");
        }
    }
}
