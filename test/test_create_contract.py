import sys
import random
import unittest
from datetime import datetime, date, timedelta
from app import create_app, db
from app.models import (
    Asset,
    Person,
    Team,
    Contract,
    ContractYear,
    Season,
    TeamSeason,
    SigningRights,
    ProfessionalTryout,
    Transaction,
    TransactionAsset,
)

from app.admin.create_contract import CreateContract, CreatePTO, get_extendable

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class AdminContractTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_get_extendable(self):
        self.assertEqual(get_extendable("idk"), [])
        self.assertEqual(get_extendable("7"), [])
        self.assertEqual(get_extendable(7), [])
        team = Team()
        db.session.add(team)
        db.session.flush()
        self.assertEqual(get_extendable(team.id), [])

        person = Person(name="Test Signed")
        asset = Asset(current_team=team.id, originating_team=team.id)
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"],
                free_agency_opening=date(year=2020, month=1, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                free_agency_opening=date(year=2021, month=1, day=1),
            ),
        ]
        db.session.add(person)
        db.session.add(asset)
        for season in seasons:
            db.session.add(season)
        db.session.flush()
        contract = Contract(id=asset.id, person_id=person.id)
        contract_years = [
            ContractYear(
                contract_id=asset.id, season_id=self.app.config["CURRENT_SEASON"]
            ),
            ContractYear(
                contract_id=asset.id, season_id=self.app.config["CURRENT_SEASON"] + 1
            ),
        ]
        db.session.add(contract)
        db.session.add(contract_years[0])
        db.session.add(contract_years[1])
        db.session.flush()
        self.assertEqual(get_extendable(team.id), [])

        person = Person(name="Test Expiring")
        asset = Asset(current_team=team.id, originating_team=team.id)
        db.session.add(person)
        db.session.add(asset)
        db.session.flush()
        contract = Contract(id=asset.id, person_id=person.id)
        contract_year = ContractYear(
            contract_id=asset.id, season_id=self.app.config["CURRENT_SEASON"]
        )
        db.session.add(contract)
        db.session.add(contract_year)
        db.session.flush()
        self.assertEqual(get_extendable(team.id), [(person.id, person.name)])
        # Tuple here. flask.jsonify() turns tuple into list when send data back to browser

    def test_get_teams(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        self.assertEqual(form.get_teams(), [])

        db.session.add(Team(name="Test Team One"))
        self.assertEqual(form.get_teams(), [(1, "Test Team One")])
        db.session.add(Team(name="Test Team Two"))
        db.session.add(Team(name="Test Team Three"))
        self.assertEqual(
            form.get_teams(),
            [(1, "Test Team One"), (3, "Test Team Three"), (2, "Test Team Two")],
        )

        ctx.pop()

    def test_get_seasons(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        self.assertEqual(form.get_seasons(), [])

        db.session.add(Season(name="Test Season"))
        db.session.commit()
        self.assertEqual(form.get_seasons(), [(1, "Test Season")])

        db.session.add(Season(name="Test Season 2", free_agency_opening=date.today()))
        db.session.commit()
        self.assertEqual(form.get_seasons(), [(2, "Test Season 2"), (1, "Test Season")])

        db.session.add(Season(name="Test 3", free_agency_opening=date.today()))
        db.session.commit()
        self.assertEqual(
            form.get_seasons(),
            [(2, "Test Season 2"), (3, "Test 3"), (1, "Test Season")],
        )

        ctx.pop()

    def test_get_free_agents(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        self.assertEqual(form.get_free_agents(), [])
        person = Person(name="Signed")
        asset = Asset(active=True)
        db.session.add(person)
        db.session.add(asset)
        db.session.flush()
        contract = Contract(id=asset.id, person_id=person.id)
        db.session.add(contract)
        self.assertEqual(form.get_free_agents(), [])

        person = Person(name="Free Agent", active=True)
        db.session.add(person)
        db.session.flush()
        self.assertEqual(form.get_free_agents(), [(person.id, person.name)])

        person2 = Person(name="An Undefined Agent", active=True)
        db.session.add(person2)
        db.session.flush()
        result = [
            (person.id, person.name),
            (person2.id, person2.name),
        ]
        result.sort(key=lambda val: val[1])
        self.assertEqual(form.get_free_agents(), result)

        asset2 = Asset(active=True)
        db.session.add(asset2)
        db.session.flush()
        contract = Contract(id=asset2.id, person_id=person.id)
        db.session.add(contract)
        db.session.flush()
        self.assertEqual(form.get_free_agents(), [(person2.id, person2.name)])
        ctx.pop()

    def test_check_empty_years(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()
        # WTForms' built in validate function does this for us
        form.second_year.errors = list(form.second_year.errors)

        self.assertTrue(form.check_empty_years())

        form.second_year.data = True
        self.assertTrue(form.check_empty_years())

        form.second_year.data = False
        form.third_year.data = True
        self.assertFalse(form.check_empty_years())

        form.second_year.data = True
        self.assertTrue(form.check_empty_years())

        form.second_year.data = False
        form.fifth_year.data = True
        self.assertFalse(form.check_empty_years())

        form.fifth_year.data = False
        form.fourth_year.data = True
        form.second_year.data = True
        self.assertTrue(form.check_empty_years())

        form.seventh_year.data = True
        self.assertFalse(form.check_empty_years())

        form.sixth_year.data = True
        form.fifth_year.data = True
        self.assertTrue(form.check_empty_years())

        form.eighth_year.data = True
        form.sixth_year.data = False
        self.assertFalse(form.check_empty_years())

        form.sixth_year.data = True
        self.assertTrue(form.check_empty_years())
        ctx.pop()

    def test_check_nhl_salary_provided(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        form.first_nhl_salary.errors = list(form.first_nhl_salary.errors)
        self.assertFalse(form.check_nhl_salary_provided())
        self.assertNotEqual(form.first_nhl_salary.errors, [])

        form.first_nhl_salary.data = 1
        self.assertTrue(form.check_nhl_salary_provided())

        form.second_nhl_salary.errors = list(form.second_nhl_salary.errors)
        form.second_year.data = True
        self.assertFalse(form.check_nhl_salary_provided())
        self.assertNotEqual(form.second_nhl_salary.errors, [])

        form.second_nhl_salary.data = 1
        self.assertTrue(form.check_nhl_salary_provided())

        form.third_nhl_salary.errors = list(form.third_nhl_salary.errors)
        form.third_year.data = True
        self.assertFalse(form.check_nhl_salary_provided())
        self.assertNotEqual(form.third_nhl_salary.errors, [])

        form.third_nhl_salary.data = 1
        self.assertTrue(form.check_nhl_salary_provided())

        form.fourth_nhl_salary.errors = list(form.fourth_nhl_salary.errors)
        form.fourth_year.data = True
        self.assertFalse(form.check_nhl_salary_provided())
        self.assertNotEqual(form.fourth_nhl_salary.errors, [])

        form.fourth_nhl_salary.data = 1
        self.assertTrue(form.check_nhl_salary_provided())

        form.fifth_nhl_salary.errors = list(form.fifth_nhl_salary.errors)
        form.fifth_year.data = True
        self.assertFalse(form.check_nhl_salary_provided())
        self.assertNotEqual(form.fifth_nhl_salary.errors, [])

        form.fifth_nhl_salary.data = 1
        self.assertTrue(form.check_nhl_salary_provided())

        form.sixth_nhl_salary.errors = list(form.sixth_nhl_salary.errors)
        form.sixth_year.data = True
        self.assertFalse(form.check_nhl_salary_provided())
        self.assertNotEqual(form.sixth_nhl_salary.errors, [])

        form.sixth_nhl_salary.data = 1
        self.assertTrue(form.check_nhl_salary_provided())

        form.seventh_nhl_salary.errors = list(form.seventh_nhl_salary.errors)
        form.seventh_year.data = True
        self.assertFalse(form.check_nhl_salary_provided())
        self.assertNotEqual(form.seventh_nhl_salary.errors, [])

        form.seventh_nhl_salary.data = 1
        self.assertTrue(form.check_nhl_salary_provided())

        form.eighth_nhl_salary.errors = list(form.eighth_nhl_salary.errors)
        form.eighth_year.data = True
        self.assertFalse(form.check_nhl_salary_provided())
        self.assertNotEqual(form.eighth_nhl_salary.errors, [])

        form.eighth_nhl_salary.data = 1
        self.assertTrue(form.check_nhl_salary_provided())

        ctx.pop()

    def test_is_front_loaded(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        self.assertFalse(form.is_front_loaded())

        # Even years
        form.years = 4
        form.pay = [
            4000000,
            3000000,
            3000000,
            3000000,
        ]
        self.assertTrue(form.is_front_loaded())
        form.pay[0] = 3000000
        self.assertFalse(form.is_front_loaded())

        # Odd years
        form.years = 5
        form.pay.append(3000000)
        self.assertFalse(form.is_front_loaded())
        form.pay[1] = 4000000
        self.assertTrue(form.is_front_loaded())

        ctx.pop()

    def test_check_yearly_variance(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        form.check_yearly_variance()
        self.assertEqual(form.years, 1)
        self.assertEqual(form.pay, [0])

        # Single year. Returns true
        form.first_nhl_salary.data = 4000000
        self.assertTrue(form.check_yearly_variance())

        # Front loaded. Pay below minimum
        form.second_year.data = True
        form.second_nhl_salary.data = 4000000
        form.third_year.data = True
        form.third_nhl_salary.data = 2000000
        form.fourth_year.data = True
        form.fourth_nhl_salary.data = 3000000
        self.assertFalse(form.check_yearly_variance())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 3: Pay below front loaded minimum (2400000)",
        )

        # Front loaded. Pay variance exceeded
        form.second_nhl_salary.data = 2900000
        self.assertFalse(form.check_yearly_variance())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 1->2: Pay variance above front loaded maximum (1000000)",
        )

        # Front loaded. Ok
        form.second_nhl_salary.data = 3000000
        form.third_nhl_salary.data = 3000000
        self.assertTrue(form.check_yearly_variance())

        # Not front loaded. First two years too swingy
        form.first_nhl_salary.data = 1000000
        form.second_nhl_salary.data = 2100000
        form.third_nhl_salary.data = 4000000
        form.fourth_nhl_salary.data = 4000000
        self.assertFalse(form.check_yearly_variance())
        self.assertEqual(
            form.validation_errors[-1],
            "Pay cannot decrease by more than half or increase to more than double in year 2",
        )

        # Not front loaded too big increase
        form.second_nhl_salary.data = 2000000
        self.assertFalse(form.check_yearly_variance())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 2->3: Pay increase above maximum (1000000)",
        )

        # Not front loaded too big decrease
        form.third_nhl_salary.data = 3000000
        form.fourth_nhl_salary.data = 2000000
        self.assertFalse(form.check_yearly_variance())
        self.assertEqual(
            form.validation_errors[-1], "Year 3->4: Pay decrease above maximum (500000)"
        )

        # Not front loaded ok
        form.fourth_nhl_salary.data = 2500000
        self.assertTrue(form.check_yearly_variance())
        ctx.pop()

    def test_cba_variance_one(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        form.first_nhl_salary.data = 2000000
        form.second_year.data = True
        form.second_nhl_salary.data = 3000000
        form.third_year.data = True
        form.third_nhl_salary.data = 1900000
        form.fourth_year.data = True
        form.fourth_nhl_salary.data = 4000000
        self.assertFalse(form.check_yearly_variance())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 2->3: Pay decrease above maximum (1000000)",
        )

        form.third_nhl_salary.data = 2000000
        self.assertTrue(form.check_yearly_variance())

        form.third_nhl_salary.data = 5100000
        self.assertFalse(form.check_yearly_variance())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 2->3: Pay increase above maximum (2000000)",
        )

        form.third_nhl_salary.data = 5000000
        self.assertTrue(form.check_yearly_variance())

        ctx.pop()

    def test_cba_variance_two(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        form.first_nhl_salary.data = 2000000
        form.second_year.data = True
        form.second_nhl_salary.data = 1400000
        form.third_year.data = True
        form.third_nhl_salary.data = 1000000
        form.fourth_year.data = True
        form.fourth_nhl_salary.data = 1000000
        self.assertFalse(form.check_yearly_variance())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 1->2: Pay variance above front loaded maximum (500000)",
        )

        form.second_nhl_salary.data = 1500000
        form.third_nhl_salary.data = 1500000
        form.fourth_nhl_salary.data = 1500000
        self.assertTrue(form.check_yearly_variance())

        form.second_nhl_salary.data = 2800000
        self.assertFalse(form.check_yearly_variance())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 1->2: Pay variance above front loaded maximum (500000)",
        )

        form.second_nhl_salary.data = 2500000
        form.third_nhl_salary.data = 3000000
        form.fourth_nhl_salary.data = 3333336
        form.fifth_year.data = True
        form.sixth_year.data = True
        form.seventh_year.data = True
        form.eighth_year.data = True
        form.fifth_nhl_salary.data = 3000000
        form.sixth_nhl_salary.data = 2500000
        form.seventh_nhl_salary.data = 2000000
        form.eighth_nhl_salary.data = 2000000
        self.assertFalse(form.check_yearly_variance())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 1: Pay below front loaded minimum (2000001)",
        )

        ctx.pop()

    def test_cba_variance_three(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        form.second_year.data = True
        form.third_year.data = True
        form.fourth_year.data = True
        form.fifth_year.data = True
        form.sixth_year.data = True
        form.seventh_year.data = True
        form.first_nhl_salary.data = 6000000
        form.second_nhl_salary.data = 9000000
        form.third_nhl_salary.data = 12000000
        form.fourth_nhl_salary.data = 9000000
        form.fifth_nhl_salary.data = 9000000
        form.sixth_nhl_salary.data = 6000000
        form.seventh_nhl_salary.data = 3000000
        self.assertFalse(form.check_yearly_variance())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 1: Pay below front loaded minimum (7200000)",
        )

        form.third_nhl_salary.data = 10000000
        self.assertFalse(form.check_yearly_variance())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 1->2: Pay variance above front loaded maximum (1500000)",
        )

        ctx.pop()

    def test_cba_variance_four(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        form.second_year.data = True
        form.third_year.data = True
        form.fourth_year.data = True
        form.fifth_year.data = True
        form.first_nhl_salary.data = 1000000
        form.second_nhl_salary.data = 2000000
        form.third_nhl_salary.data = 3000000
        form.fourth_nhl_salary.data = 4000000
        form.fifth_nhl_salary.data = 5000000
        self.assertTrue(form.check_yearly_variance())

        ctx.pop()

    def test_check_maximum_pay(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"],
                free_agency_opening=date(year=2020, month=7, day=1),
                salary_cap=100,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                free_agency_opening=date(year=2021, month=7, day=1),
                salary_cap=100,
            ),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.commit()

        form.check_yearly_variance()
        form.check_first_season()
        form.load_seasons()
        self.assertTrue(form.check_maximum_pay())

        form.first_nhl_salary.data = 25
        form.check_yearly_variance()
        self.assertFalse(form.check_maximum_pay())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 1: Pay exceeds maximum single-person limit (20)",
        )

        form.first_nhl_salary.data = 20
        form.check_yearly_variance()
        self.assertTrue(form.check_maximum_pay())

        form.first_nhl_salary.data = 10
        form.check_yearly_variance()
        self.assertTrue(form.check_maximum_pay())

        ctx.pop()

    def test_check_minor_league_minimum_pay(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        form.second_year.data = True
        form.third_year.data = True
        form.check_yearly_variance()

        form.second_minors_salary.data = 40000
        form.third_minors_salary.data = 20000
        self.assertFalse(form.check_minor_league_minimum_pay())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 3: Minor league minimum pay limit exceeded",
        )

        form.fourth_year.data = True
        form.fifth_year.data = True
        form.sixth_year.data = True
        form.seventh_year.data = True
        form.eighth_year.data = True
        form.check_yearly_variance()
        form.third_minors_salary.data = 40000
        form.fourth_minors_salary.data = 40000
        form.fifth_minors_salary.data = 40000
        form.sixth_minors_salary.data = 40000
        form.seventh_minors_salary.data = 40000
        form.eighth_minors_salary.data = 40000
        self.assertTrue(form.check_minor_league_minimum_pay())

        ctx.pop()

    def test_check_minimum_salary(self):
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"],
                minimum_salary=750000,
                free_agency_opening=date(year=2021, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                minimum_salary=750000,
                free_agency_opening=date(year=2022, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 2,
                minimum_salary=750000,
                free_agency_opening=date(year=2023, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 3,
                minimum_salary=750000,
                free_agency_opening=date(year=2024, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 4,
                minimum_salary=750000,
                free_agency_opening=date(year=2025, month=7, day=1),
            ),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.commit()

        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        form.second_year.data = True
        form.third_year.data = True
        form.fourth_year.data = True
        form.fifth_year.data = True
        form.first_nhl_salary.data = 1000000
        form.second_nhl_salary.data = 1000000
        form.third_nhl_salary.data = 1000000
        form.fourth_nhl_salary.data = 500000
        form.fifth_nhl_salary.data = 1000000

        form.check_yearly_variance()
        form.check_first_season()
        form.load_seasons()
        self.assertFalse(form.check_minimum_salary())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 4: NHL salary below league minimum (750000)",
        )

        ctx.pop()

    def test_check_first_season(self):
        TEAM_ID = 4
        PERSON_ID = 3
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()
        form.first_season.errors = list(form.first_season.errors)
        form.years = 4

        seasons = [
            Season(
                id=1, name="Test 1", free_agency_opening=date(year=2000, month=1, day=1)
            ),
            Season(
                id=2, name="Test 2", free_agency_opening=date(year=2000, month=2, day=1)
            ),
            Season(
                id=3, name="Test 3", free_agency_opening=date(year=2000, month=3, day=1)
            ),
            Season(
                id=4, name="Test 4", free_agency_opening=date(year=2000, month=4, day=1)
            ),
            Season(
                id=5, name="Test 5", free_agency_opening=date(year=2000, month=5, day=1)
            ),
            Season(
                id=6, name="Test 6", free_agency_opening=date(year=2000, month=6, day=1)
            ),
        ]
        for season in seasons:
            db.session.add(season)
        person = Person(name="Test Person")
        db.session.add(person)
        db.session.commit()

        form.person.data = person.id
        self.assertIsNone(form.first_season.data)
        self.assertTrue(form.check_first_season())
        self.assertEqual(form.first_season.data, 1)

        form.first_season.data = 2
        self.assertTrue(form.check_first_season())
        self.assertEqual(form.first_season.data, 2)

        contract = Contract(id=7, person_id=person.id)
        contract_year = ContractYear(contract_id=contract.id, season_id=5)
        db.session.add(contract)
        db.session.add(contract_year)
        db.session.commit()
        self.assertFalse(form.check_first_season())
        self.assertEqual(form.first_season.data, 2)

        form.first_season.data = 1
        self.assertTrue(form.check_first_season())
        self.assertEqual(form.first_season.data, 1)

        ctx.pop()

    def test_check_contract_count(self):
        TEAM_ID = 7
        PERSON_ID = 4
        team_season = TeamSeason(
            season_id=self.app.config["CURRENT_SEASON"],
            team_id=TEAM_ID,
            contract_count=50,
        )
        db.session.add(team_season)
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"] + 0,
                free_agency_opening=date(year=2020, month=1, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                free_agency_opening=date(year=2021, month=1, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 2,
                free_agency_opening=date(year=2022, month=1, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 3,
                free_agency_opening=date(year=2023, month=1, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 4,
                free_agency_opening=date(year=2024, month=1, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 5,
                free_agency_opening=date(year=2025, month=1, day=1),
            ),
        ]
        for season in seasons:
            db.session.add(season)

        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()
        form.team.errors = list(form.team.errors)

        form.person.data = PERSON_ID
        form.team.data = TEAM_ID
        asset = Asset()
        db.session.add(asset)
        db.session.flush()
        contract = Contract(id=asset.id, person_id=PERSON_ID)
        db.session.add(contract)
        db.session.flush()
        contract_year = ContractYear(
            contract_id=contract.id, season_id=self.app.config["CURRENT_SEASON"] + 2
        )
        # Increased to one more than the contract year because contract starts the season afterwards
        team_season.season_id = self.app.config["CURRENT_SEASON"] + 3
        team_season.contract_count = 40
        db.session.add(contract_year)
        db.session.add(team_season)
        db.session.commit()
        form.check_yearly_variance()
        form.check_first_season()
        self.assertTrue(form.check_contract_count())

        ctx.pop()

    def test_check_contract_length(self):
        TEAM_ID = 7
        PERSON_ID = 4
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        form.second_year.data = True
        form.third_year.data = True
        form.fourth_year.data = True
        form.fifth_year.data = True
        form.sixth_year.data = True
        form.seventh_year.data = True

        form.check_yearly_variance()
        self.assertTrue(form.check_contract_length())

        form.eighth_year.data = True
        form.check_yearly_variance()
        self.assertFalse(form.check_contract_length())

        form.person.data = PERSON_ID
        form.team.data = TEAM_ID
        asset = Asset(current_team=TEAM_ID, active=True)
        db.session.add(asset)
        db.session.flush()
        contract = Contract(id=asset.id, person_id=PERSON_ID)
        db.session.add(contract)
        db.session.commit()
        self.assertTrue(form.check_contract_length())

        ctx.pop()

    def test_check_entry_level(self):
        PERSON_ID = 4
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"],
                free_agency_opening=date(year=2020, month=1, day=1),
                max_elc_salary=100000,
                max_elc_minor_salary=50000,
                max_elc_performance_bonuses=3500000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                free_agency_opening=date(year=2020, month=1, day=1),
                max_elc_salary=100000,
                max_elc_minor_salary=50000,
                max_elc_performance_bonuses=3500000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 2,
                free_agency_opening=date(year=2020, month=1, day=1),
                max_elc_salary=100000,
                max_elc_minor_salary=50000,
                max_elc_performance_bonuses=3500000,
            ),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.commit()

        asset = Asset()
        db.session.add(asset)
        db.session.flush()
        contract = Contract(id=asset.id, person_id=PERSON_ID)
        db.session.add(contract)
        db.session.flush()
        form.person.data = PERSON_ID
        form.load_seasons()
        self.assertTrue(form.check_entry_level())
        db.session.rollback()

        person = Person(
            id=PERSON_ID,
            birthdate=date(year=1993, month=1, day=1),
        )
        db.session.add(person)
        db.session.flush()
        form.entry_level.data = False
        self.assertTrue(form.check_entry_level())
        self.assertFalse(form.entry_level.data)

        today = date.today()
        two_months = timedelta(days=90)
        twenty_four = today.replace(year=today.year - 24) - two_months
        person.birthdate = twenty_four
        db.session.add(person)
        db.session.flush()
        form.years = 4
        self.assertFalse(form.check_entry_level())
        self.assertEqual(
            form.validation_errors[-1],
            "ELC length at 24 (24-27 for European players) limited to 1 year",
        )
        twenty_three = today.replace(year=today.year - 23) - two_months
        person.birthdate = twenty_three
        db.session.add(person)
        db.session.flush()
        self.assertFalse(form.check_entry_level())
        self.assertEqual(
            form.validation_errors[-1], "ELC length at 22/23 limited to 2 years"
        )

        twenty_one = today.replace(year=today.year - 21) - two_months
        person.birthdate = twenty_one
        db.session.add(person)
        db.session.flush()
        self.assertFalse(form.check_entry_level())
        self.assertEqual(
            form.validation_errors[-1], "ELC length under 22 limited to 3 years"
        )

        form.first_season.data = self.app.config["CURRENT_SEASON"]
        form.years = 2
        form.second_year = True
        form.first_nhl_salary.data = 100000
        form.first_signing_bonus.data = 10000
        form.second_nhl_salary.data = 90000
        form.second_signing_bonus.data = 10000
        form.check_first_season()
        form.load_seasons()
        self.assertFalse(form.check_entry_level())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 1: Salary + signing bonus exceeds entry level maximum",
        )
        self.assertEqual(form.first_minors_salary.data, 35000)
        self.assertEqual(form.second_minors_salary.data, 35000)
        self.assertEqual(form.third_minors_salary.data, 35000)

        form.first_nhl_salary.data = 90000
        form.second_minors_salary.data = 100000
        self.assertFalse(form.check_entry_level())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 2: Minors salary exceeds entry level maximum",
        )

        form.second_minors_salary.data = 35000
        form.first_nhl_salary.data = 80000
        self.assertFalse(form.check_entry_level())
        message = "Year 1: Signing bonus exceeds 10% of NHL salary + signing bonus"
        self.assertEqual(form.validation_errors[-1], message)

        form.first_nhl_salary.data = 90000
        form.first_performance_bonuses.data = 4000000
        self.assertFalse(form.check_entry_level())
        self.assertEqual(
            form.validation_errors[-1],
            "Year 1: Performance bonuses exceed entry level limits",
        )

        form.first_performance_bonuses.data = 3500000
        self.assertTrue(form.check_entry_level())

        ctx.pop()

    def test_check_performance_bonuses(self):
        PERSON_ID = 5
        TEAM_ID = 9
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        form.check_yearly_variance()
        self.assertTrue(form.check_performance_bonuses())

        form.entry_level.data = True
        self.assertTrue(form.check_performance_bonuses())

        form.entry_level.data = False
        form.second_year.data = True
        form.first_nhl_salary.data = 1000000
        form.second_nhl_salary.data = 1000000
        form.entry_level.data = False
        form.first_performance_bonuses.data = 1
        form.check_yearly_variance()
        self.assertFalse(form.check_performance_bonuses())
        message = "SPC performance bonuses disallowed on multi-year contracts"
        self.assertEqual(form.validation_errors[-1], message)

        form.person.data = PERSON_ID
        form.team.data = TEAM_ID
        form.first_season.data = self.app.config["CURRENT_SEASON"]
        person = Person(id=PERSON_ID, birthdate=date.today())
        season = Season(
            id=form.first_season.data,
            salary_cap=10000000,
            free_agency_opening=date(year=2021, month=7, day=1),
        )
        next_season = Season(
            id=form.first_season.data + 1,
            salary_cap=10000000,
            free_agency_opening=date.today(),
        )
        asset = Asset(current_team=TEAM_ID)
        db.session.add(person)
        db.session.add(season)
        db.session.add(next_season)
        db.session.add(asset)
        db.session.flush()
        contract_year = ContractYear(
            contract_id=asset.id,
            performance_bonuses=750001,
            season_id=form.first_season.data,
        )
        db.session.add(contract_year)
        db.session.commit()

        form.second_year.data = False
        form.second_nhl_salary.data = None
        form.check_yearly_variance()
        form.load_seasons()
        message = "Players under 35 are ineligible for performance bonuses"
        self.assertFalse(form.check_performance_bonuses())
        self.assertEqual(form.validation_errors[-1], message)

        person.birthdate = date.today().replace(year=1985)
        db.session.add(person)
        db.session.commit()
        message = "Performance bonuses exceed 7.5% of the cap with this contract"
        self.assertFalse(form.check_performance_bonuses())
        self.assertEqual(form.validation_errors[-1], message)

        contract_year.performance_bonuses = 500000
        db.session.add(contract_year)
        db.session.commit()
        self.assertTrue(form.check_performance_bonuses())

        ctx.pop()

    def test_validate_grubauer2021(self):
        person = Person(
            name="Gru",
            birthdate=date(year=1991, month=11, day=25),
        )
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"],
                salary_cap=82500000,
                free_agency_opening=date(year=2021, month=7, day=1),
                minimum_salary=10,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                salary_cap=82500000,
                free_agency_opening=date(year=2022, month=7, day=1),
                minimum_salary=10,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 2,
                salary_cap=82500000,
                free_agency_opening=date(year=2023, month=7, day=1),
                minimum_salary=10,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 3,
                salary_cap=82500000,
                free_agency_opening=date(year=2024, month=7, day=1),
                minimum_salary=10,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 4,
                salary_cap=82500000,
                free_agency_opening=date(year=2025, month=7, day=1),
                minimum_salary=10,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 5,
                salary_cap=82500000,
                free_agency_opening=date(year=2026, month=7, day=1),
                minimum_salary=10,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 6,
                salary_cap=82500000,
                free_agency_opening=date(year=2027, month=7, day=1),
                minimum_salary=10,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 7,
                salary_cap=82500000,
                free_agency_opening=date(year=2028, month=7, day=1),
                minimum_salary=10,
            ),
        ]
        team = Team(
            name="Test",
        )
        db.session.add(person)
        for season in seasons:
            db.session.add(season)
        db.session.add(team)
        db.session.flush()
        team_season = TeamSeason(
            contract_count=4,
            season_id=self.app.config["CURRENT_SEASON"],
            team_id=team.id,
        )
        db.session.add(team_season)
        db.session.commit()

        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        form.person.data = str(person.id)
        form.team.data = str(team.id)
        form.expiration_status.data = "UFA"
        form.signing_date.data = date(year=2021, month=7, day=28)
        form.entry_level.data = False
        form.non_roster.data = False
        form.second_year.data = True
        form.third_year.data = True
        form.fourth_year.data = True
        form.fifth_year.data = True
        form.sixth_year.data = True
        form.first_nhl_salary.data = 5000000
        form.second_nhl_salary.data = 6000000
        form.third_nhl_salary.data = 7500000
        form.fourth_nhl_salary.data = 6800000
        form.fifth_nhl_salary.data = 5600000
        form.sixth_nhl_salary.data = 4500000
        form.total_value.data = 35400000
        self.assertFalse(form.validate())

        form.validation_errors = []
        form.second_nhl_salary.data = 6250000
        form.third_nhl_salary.data = 7250000

        self.assertTrue(form.validate())
        ctx.pop()

    def test_validate_eklund2021(self):
        person = Person(
            name="Eklund",
            birthdate=date(year=2002, month=12, day=10),
        )
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"],
                salary_cap=82500000,
                free_agency_opening=date(year=2021, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                salary_cap=82500000,
                free_agency_opening=date(year=2021, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 2,
                salary_cap=82500000,
                free_agency_opening=date(year=2021, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 3,
                salary_cap=82500000,
                free_agency_opening=date(year=2021, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 4,
                salary_cap=82500000,
                free_agency_opening=date(year=2021, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 5,
                salary_cap=82500000,
                free_agency_opening=date(year=2021, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 6,
                salary_cap=82500000,
                free_agency_opening=date(year=2021, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 7,
                salary_cap=82500000,
                free_agency_opening=date(year=2021, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
        ]
        team = Team(
            name="Test",
        )
        db.session.add(person)
        for season in seasons:
            db.session.add(season)
        db.session.add(team)
        db.session.flush()
        team_season = TeamSeason(
            contract_count=4,
            season_id=self.app.config["CURRENT_SEASON"],
            team_id=team.id,
        )
        db.session.add(team_season)
        db.session.commit()

        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        form.person.data = person.id
        form.team.data = str(team.id)
        form.expiration_status.data = "RFA"
        form.signing_date.data = date(year=2021, month=8, day=21)
        form.entry_level.data = True
        form.second_year.data = True
        form.third_year.data = True
        form.first_nhl_salary.data = 832500
        form.second_nhl_salary.data = 832500
        form.third_nhl_salary.data = 832500
        form.first_signing_bonus.data = 92500
        form.second_signing_bonus.data = 92500
        form.third_signing_bonus.data = 92500
        form.first_performance_bonuses.data = 850000
        form.second_performance_bonuses.data = 850000
        form.third_performance_bonuses.data = 850000
        form.first_minors_salary.data = 80000
        form.second_minors_salary.data = 80000
        form.third_minors_salary.data = 80000
        self.assertTrue(form.validate())

        ctx.pop()

    def test_validate_debrusk2021(self):
        person = Person(
            name="Debrusk",
            birthdate=date(year=1996, month=10, day=17),
        )
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"],
                salary_cap=82500000,
                free_agency_opening=date(year=2021, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                salary_cap=82500000,
                free_agency_opening=date(year=2022, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 2,
                salary_cap=82500000,
                free_agency_opening=date(year=2023, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 3,
                salary_cap=82500000,
                free_agency_opening=date(year=2024, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 4,
                salary_cap=82500000,
                free_agency_opening=date(year=2025, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 5,
                salary_cap=82500000,
                free_agency_opening=date(year=2026, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 6,
                salary_cap=82500000,
                free_agency_opening=date(year=2027, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 7,
                salary_cap=82500000,
                free_agency_opening=date(year=2028, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
            ),
        ]
        team = Team(
            name="Test",
        )
        db.session.add(person)
        for season in seasons:
            db.session.add(season)
        db.session.add(team)
        db.session.flush()
        team_season = TeamSeason(
            contract_count=4,
            season_id=self.app.config["CURRENT_SEASON"],
            team_id=team.id,
        )
        db.session.add(team_season)
        db.session.commit()

        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        form.person.data = person.id
        form.team.data = str(team.id)
        form.expiration_status.data = "UFA"
        form.signing_date.data = date(year=2022, month=3, day=21)
        form.entry_level.data = False
        form.second_year.data = True
        form.first_nhl_salary.data = 4000000
        form.second_nhl_salary.data = 4000000
        self.assertTrue(form.validate())

        ctx.pop()

    def test_extend_jost(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        person = Person(
            name="Tyson Jost",
            birthdate=date(year=1995, month=1, day=3),
            current_status="Drafted,RFA",
        )
        current_season = Season(
            id=self.app.config["CURRENT_SEASON"],
            salary_cap=82500000,
            free_agency_opening=date(year=2021, month=7, day=1),
            draft_date=date(year=2022, month=7, day=7),
            minimum_salary=800000,
            max_elc_salary=925000,
            max_elc_minor_salary=80000,
        )
        next_season = Season(
            id=self.app.config["CURRENT_SEASON"] + 1,
            salary_cap=82500000,
            free_agency_opening=date(year=2022, month=7, day=1),
            draft_date=date(year=2023, month=7, day=7),
            minimum_salary=800000,
            max_elc_salary=925000,
            max_elc_minor_salary=80000,
        )
        team = Team(name="San Jose Sharks")
        db.session.add(person)
        db.session.add(team)
        db.session.add(current_season)
        db.session.add(next_season)
        current_asset = Asset(
            current_team=team.id, originating_team=team.id, active=True
        )
        db.session.add(current_asset)
        db.session.flush()
        team_season = TeamSeason(
            team_id=team.id,
            season_id=self.app.config["CURRENT_SEASON"],
            contract_count=5,
        )
        team_season_two = TeamSeason(
            team_id=team.id,
            season_id=self.app.config["CURRENT_SEASON"] + 1,
            contract_count=5,
        )
        db.session.add(team_season)
        db.session.add(team_season_two)
        current_contract = Contract(
            id=current_asset.id,
            person_id=person.id,
            years=1,
            signing_date=date(year=2021, month=7, day=2),
            total_value=975000,
            entry_level=True,
            non_roster=False,
        )
        current_contract_year = ContractYear(
            contract_id=current_asset.id,
            season_id=current_season.id,
            aav=975000,
            nhl_salary=975000,
            cap_hit=975000,
            minors_salary=80000,
            two_way=True,
        )
        db.session.add(current_contract)
        db.session.add(current_contract_year)
        db.session.commit()

        form.person.data = person.id
        form.team.data = team.id
        form.entry_level.data = True
        form.signing_date.data = date(year=2022, month=4, day=16)
        form.expiration_status.data = "RFA"
        form.non_roster.data = False
        form.total_value.data = 975000

        form.first_nhl_salary.data = 975000

        form.check_yearly_variance()
        # form.check_contract_count()
        form.check_first_season()
        form.load_seasons()
        self.assertEqual(form.create_contract(), person.id)

        contract = (
            Contract.query.filter(Contract.person_id == person.id)
            .order_by(Contract.id.desc())
            .first()
        )
        self.assertIsNotNone(contract)
        self.assertTrue(contract.extension)
        self.assertTrue("RFA" not in person.current_status)

    def test_create_bordeleau21(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        person = Person(
            name="Thomas Bordeleau",
            birthdate=date(year=2002, month=1, day=3),
        )
        previous_season = Season(
            id=self.app.config["CURRENT_SEASON"] - 1,
            salary_cap=82500000,
            free_agency_opening=date(year=2020, month=7, day=28),
            draft_date=date(year=2021, month=7, day=23),
            minimum_salary=800000,
            max_elc_salary=925000,
            max_elc_minor_salary=80000,
        )
        current_season = Season(
            id=self.app.config["CURRENT_SEASON"],
            salary_cap=82500000,
            free_agency_opening=date(year=2021, month=7, day=1),
            draft_date=date(year=2022, month=7, day=7),
            minimum_salary=800000,
            max_elc_salary=925000,
            max_elc_minor_salary=80000,
        )
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                free_agency_opening=date(year=2022, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 2,
                free_agency_opening=date(year=2023, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 3,
                free_agency_opening=date(year=2024, month=7, day=1),
            ),
        ]
        team = Team(name="San Jose Sharks")
        db.session.add(person)
        db.session.add(previous_season)
        db.session.add(current_season)
        db.session.add(team)
        for season in seasons:
            db.session.add(season)
        db.session.flush()
        rights_asset = Asset(
            originating_team=team.id,
            current_team=team.id,
            active=True,
            type="Signing Rights",
        )
        db.session.add(rights_asset)
        db.session.flush()
        rights = SigningRights(
            id=rights_asset.id,
            person_id=person.id,
        )
        db.session.add(rights)
        db.session.flush()

        pto_asset = Asset(
            originating_team=team.id,
            current_team=team.id,
            active=True,
            type="Professional Tryout",
        )
        db.session.add(pto_asset)
        db.session.flush()
        pto = ProfessionalTryout(
            id=pto_asset.id,
            person_id=person.id,
            season_id=self.app.config["CURRENT_SEASON"],
        )
        db.session.add(pto)
        db.session.flush()

        team_season = TeamSeason(
            team_id=team.id,
            season_id=self.app.config["CURRENT_SEASON"],
            contract_count=5,
        )
        team_season_two = TeamSeason(
            team_id=team.id,
            season_id=self.app.config["CURRENT_SEASON"] + 1,
            contract_count=5,
        )
        team_season_three = TeamSeason(
            team_id=team.id,
            season_id=self.app.config["CURRENT_SEASON"] + 2,
            contract_count=5,
        )
        db.session.add(team_season)
        db.session.add(team_season_two)
        db.session.add(team_season_three)
        db.session.commit()

        form.person.data = person.id
        form.team.data = team.id
        form.entry_level.data = True
        form.signing_date.data = date(year=2022, month=4, day=16)
        form.expiration_status.data = "RFA"
        form.non_roster.data = False
        form.total_value.data = 2775000

        form.first_nhl_salary.data = 807500
        form.first_performance_bonuses.data = 25000
        form.first_signing_bonus.data = 92500
        form.first_minors_salary.data = 80000

        form.second_year.data = True
        form.second_nhl_salary.data = 832500
        form.second_signing_bonus.data = 92500
        form.second_minors_salary.data = 80000

        form.third_year.data = True
        form.third_nhl_salary.data = 832500
        form.third_signing_bonus.data = 92500
        form.third_minors_salary.data = 80000

        self.assertEqual(TeamSeason.query.count(), 3)
        self.assertEqual(
            TeamSeason.query.filter(TeamSeason.contract_count == 5).count(), 3
        )

        form.check_yearly_variance()
        # form.check_contract_count()
        form.check_first_season()
        form.load_seasons()
        self.assertEqual(form.create_contract(), person.id)

        self.assertFalse(rights_asset.active)
        self.assertFalse(pto_asset.active)

        contract = Contract.query.filter(Contract.person_id == person.id).first()
        self.assertTrue(contract.entry_level)
        self.assertFalse(contract.non_roster)
        self.assertEqual(contract.total_value, 2775000)
        self.assertEqual(contract.expiration_status, "RFA")
        self.assertEqual(contract.signing_date, date(year=2022, month=4, day=16))
        self.assertEqual(contract.extension, False)
        self.assertEqual(contract.thirty_five_plus, False)

        asset = Asset.query.filter(Asset.id == contract.id).first()
        self.assertEqual(asset.originating_team, team.id)
        self.assertEqual(asset.current_team, team.id)
        self.assertEqual(asset.type, "Contract")
        self.assertTrue(asset.active)

        years = (
            ContractYear.query.filter(ContractYear.contract_id == contract.id)
            .order_by(ContractYear.season_id.asc())
            .all()
        )
        self.assertEqual(len(years), 3)

        self.assertEqual(years[0].aav, 925000)
        self.assertEqual(years[0].cap_hit, 916666)
        self.assertEqual(years[0].nhl_salary, 807500)
        self.assertEqual(years[0].minors_salary, 80000)
        self.assertEqual(years[0].signing_bonus, 92500)
        self.assertEqual(years[0].performance_bonuses, 25000)
        self.assertTrue(years[0].two_way)

        self.assertEqual(years[1].aav, 925000)
        self.assertEqual(years[1].cap_hit, 916666)
        self.assertEqual(years[1].nhl_salary, 832500)
        self.assertEqual(years[1].minors_salary, 80000)
        self.assertEqual(years[1].signing_bonus, 92500)
        self.assertEqual(years[1].performance_bonuses, 0)
        self.assertTrue(years[1].two_way)

        self.assertEqual(years[2].aav, 925000)
        self.assertEqual(years[2].cap_hit, 916666)
        self.assertEqual(years[2].nhl_salary, 832500)
        self.assertEqual(years[2].minors_salary, 80000)
        self.assertEqual(years[2].signing_bonus, 92500)
        self.assertEqual(years[2].performance_bonuses, 0)
        self.assertTrue(years[2].two_way)

        self.assertTrue(person.status_is("Under Contract"))
        self.assertEqual(TeamSeason.query.count(), 3)
        self.assertEqual(
            TeamSeason.query.filter(TeamSeason.cap_hit == 916666).count(), 3
        )
        self.assertEqual(
            TeamSeason.query.filter(TeamSeason.projected_cap_hit == 916666).count(), 3
        )
        self.assertEqual(
            TeamSeason.query.filter(TeamSeason.contract_count == 6).count(), 0
        )
        self.assertEqual(
            TeamSeason.query.filter(TeamSeason.contract_count == 1).count(), 3
        )

        ctx.pop()

    def test_create_bordeleau21_2(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        person = Person(
            name="Thomas Bordeleau",
            birthdate=date(year=2002, month=1, day=3),
        )
        dummy_season = Season(
            id=self.app.config["CURRENT_SEASON"] - 1,
            salary_cap=82500000,
            free_agency_opening=date(year=2020, month=7, day=28),
            draft_date=date(year=2021, month=7, day=23),
            minimum_salary=800000,
            max_elc_salary=925000,
            max_elc_minor_salary=80000,
        )
        previous_season = Season(
            id=self.app.config["CURRENT_SEASON"],
            salary_cap=82500000,
            free_agency_opening=date(year=2021, month=7, day=28),
            draft_date=date(year=2021, month=7, day=23),
            minimum_salary=800000,
            max_elc_salary=925000,
            max_elc_minor_salary=80000,
        )
        current_season = Season(
            id=self.app.config["CURRENT_SEASON"] + 1,
            salary_cap=82500000,
            free_agency_opening=date(year=2022, month=7, day=1),
            draft_date=date(year=2022, month=7, day=7),
            minimum_salary=800000,
            max_elc_salary=925000,
            max_elc_minor_salary=80000,
        )
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"] + 2,
                free_agency_opening=date(year=2023, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 3,
                free_agency_opening=date(year=2024, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 4,
                free_agency_opening=date(year=2025, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 5,
                free_agency_opening=date(year=2026, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 6,
                free_agency_opening=date(year=2027, month=7, day=1),
            ),
        ]
        team = Team(name="San Jose Sharks")
        db.session.add(person)
        db.session.add(previous_season)
        db.session.add(current_season)
        db.session.add(team)
        for season in seasons:
            db.session.add(season)
        db.session.flush()
        team_season = TeamSeason(
            team_id=team.id,
            season_id=self.app.config["CURRENT_SEASON"] + 1,
            contract_count=5,
        )
        team_season_two = TeamSeason(
            team_id=team.id,
            season_id=self.app.config["CURRENT_SEASON"] + 2,
            contract_count=5,
        )
        team_season_three = TeamSeason(
            team_id=team.id,
            season_id=self.app.config["CURRENT_SEASON"] + 3,
            contract_count=5,
        )
        db.session.add(team_season)
        db.session.add(team_season_two)
        db.session.add(team_season_three)
        db.session.commit()

        form.person.data = person.id
        form.team.data = team.id
        form.entry_level.data = True
        form.signing_date.data = date(year=2022, month=4, day=16)
        form.expiration_status.data = "RFA"
        form.non_roster.data = False
        form.total_value.data = 2775000
        form.first_season.data = self.app.config["CURRENT_SEASON"] + 1

        form.first_nhl_salary.data = 807500
        form.first_performance_bonuses.data = 25000
        form.first_signing_bonus.data = 92500
        form.first_minors_salary.data = 80000

        form.second_year.data = True
        form.second_nhl_salary.data = 832500
        form.second_signing_bonus.data = 92500
        form.second_minors_salary.data = 80000

        form.third_year.data = True
        form.third_nhl_salary.data = 832500
        form.third_signing_bonus.data = 92500
        form.third_minors_salary.data = 80000

        self.assertEqual(TeamSeason.query.count(), 3)
        self.assertEqual(
            TeamSeason.query.filter(TeamSeason.contract_count == 5).count(), 3
        )

        form.check_yearly_variance()
        # form.check_contract_count()
        form.check_first_season()
        form.load_seasons()
        self.assertEqual(form.create_contract(), person.id)

        contract = Contract.query.filter(Contract.person_id == person.id).first()
        self.assertTrue(contract.entry_level)
        self.assertFalse(contract.non_roster)
        self.assertEqual(contract.total_value, 2775000)
        self.assertEqual(contract.expiration_status, "RFA")
        self.assertEqual(contract.signing_date, date(year=2022, month=4, day=16))
        self.assertEqual(contract.extension, False)
        self.assertEqual(contract.thirty_five_plus, False)

        asset = Asset.query.filter(Asset.id == contract.id).first()
        self.assertEqual(asset.originating_team, team.id)
        self.assertEqual(asset.current_team, team.id)
        self.assertEqual(asset.type, "Contract")
        self.assertTrue(asset.active)

        years = (
            ContractYear.query.filter(ContractYear.contract_id == contract.id)
            .order_by(ContractYear.season_id.asc())
            .all()
        )
        self.assertEqual(len(years), 3)

        self.assertEqual(years[0].season_id, self.app.config["CURRENT_SEASON"] + 1)
        self.assertEqual(years[0].aav, 925000)
        self.assertEqual(years[0].cap_hit, 916666)
        self.assertEqual(years[0].nhl_salary, 807500)
        self.assertEqual(years[0].minors_salary, 80000)
        self.assertEqual(years[0].signing_bonus, 92500)
        self.assertEqual(years[0].performance_bonuses, 25000)
        self.assertTrue(years[0].two_way)

        self.assertEqual(years[1].season_id, self.app.config["CURRENT_SEASON"] + 2)
        self.assertEqual(years[1].aav, 925000)
        self.assertEqual(years[1].cap_hit, 916666)
        self.assertEqual(years[1].nhl_salary, 832500)
        self.assertEqual(years[1].minors_salary, 80000)
        self.assertEqual(years[1].signing_bonus, 92500)
        self.assertEqual(years[1].performance_bonuses, 0)
        self.assertTrue(years[1].two_way)

        self.assertEqual(years[2].season_id, self.app.config["CURRENT_SEASON"] + 3)
        self.assertEqual(years[2].aav, 925000)
        self.assertEqual(years[2].cap_hit, 916666)
        self.assertEqual(years[2].nhl_salary, 832500)
        self.assertEqual(years[2].minors_salary, 80000)
        self.assertEqual(years[2].signing_bonus, 92500)
        self.assertEqual(years[2].performance_bonuses, 0)
        self.assertTrue(years[2].two_way)

        self.assertTrue(person.status_is("Under Contract"))
        self.assertEqual(TeamSeason.query.count(), 3)
        self.assertEqual(
            TeamSeason.query.filter(TeamSeason.cap_hit == 916666).count(), 3
        )
        self.assertEqual(
            TeamSeason.query.filter(TeamSeason.projected_cap_hit == 916666).count(), 3
        )
        self.assertEqual(
            TeamSeason.query.filter(TeamSeason.contract_count == 6).count(), 0
        )
        self.assertEqual(
            TeamSeason.query.filter(TeamSeason.contract_count == 1).count(), 3
        )

        ctx.pop()

    def test_create_karlsson19(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()
        person = Person(
            name="Erik Karlsson",
            birthdate=date(year=1990, month=5, day=31),
            elc_signing_age=19,
            waivers_signing_age=19,
            current_status="Drafted,UFA",
        )
        prev_prev = Season(
            id=self.app.config["CURRENT_SEASON"] - 2,
            salary_cap=79500000,
            free_agency_opening=date(year=2018, month=7, day=1),
            draft_date=date(year=2019, month=6, day=21),
            minimum_salary=800000,
            max_elc_salary=900000,
            max_elc_minor_salary=75000,
        )
        previous_season = Season(
            id=self.app.config["CURRENT_SEASON"] - 1,
            salary_cap=79500000,
            free_agency_opening=date(year=2019, month=7, day=1),
            draft_date=date(year=2020, month=10, day=6),
            minimum_salary=800000,
            max_elc_salary=900000,
            max_elc_minor_salary=75000,
        )
        current_season = Season(
            id=self.app.config["CURRENT_SEASON"],
            salary_cap=81500000,
            free_agency_opening=date(year=2020, month=10, day=20),
            draft_date=date(year=2021, month=7, day=23),
            minimum_salary=800000,
            max_elc_salary=925000,
            max_elc_minor_salary=75000,
        )
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                free_agency_opening=date(year=2021, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 2,
                free_agency_opening=date(year=2022, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 3,
                free_agency_opening=date(year=2023, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 4,
                free_agency_opening=date(year=2024, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 5,
                free_agency_opening=date(year=2025, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 6,
                free_agency_opening=date(year=2026, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 7,
                free_agency_opening=date(year=2027, month=7, day=1),
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 8,
                free_agency_opening=date(year=2028, month=7, day=1),
            ),
        ]
        team = Team(name="San Jose Sharks")
        db.session.add(person)
        db.session.add(prev_prev)
        db.session.add(previous_season)
        db.session.add(current_season)
        db.session.add(team)
        for season in seasons:
            db.session.add(season)
        db.session.flush()
        team_season = TeamSeason(
            team_id=team.id,
            season_id=self.app.config["CURRENT_SEASON"],
            contract_count=5,
        )
        db.session.add(team_season)
        db.session.commit()

        form.person.data = person.id
        form.team.data = team.id
        form.entry_level.data = False
        form.signing_date.data = date(year=2019, month=6, day=17)
        form.expiration_status.data = "UFA"
        form.non_roster.data = False
        form.total_value.data = 92000000

        form.first_nhl_salary.data = 3500000
        form.first_signing_bonus.data = 11000000

        form.second_year.data = True
        form.second_nhl_salary.data = 2000000
        form.second_signing_bonus.data = 10000000

        form.third_year.data = True
        form.third_nhl_salary.data = 5500000
        form.third_signing_bonus.data = 9000000

        form.fourth_year.data = True
        form.fourth_nhl_salary.data = 2000000
        form.fourth_signing_bonus.data = 10000000

        form.fifth_year.data = True
        form.fifth_nhl_salary.data = 10500000
        form.fifth_signing_bonus.data = 1000000

        form.sixth_year.data = True
        form.sixth_nhl_salary.data = 10000000
        form.sixth_signing_bonus.data = 1000000

        form.seventh_year.data = True
        form.seventh_nhl_salary.data = 4000000
        form.seventh_signing_bonus.data = 5000000

        form.eighth_year.data = True
        form.eighth_nhl_salary.data = 1500000
        form.eighth_signing_bonus.data = 6000000

        self.assertEqual(TeamSeason.query.count(), 1)

        form.check_yearly_variance()
        # form.check_contract_count()
        form.check_first_season()
        form.load_seasons()
        self.assertEqual(form.create_contract(), person.id)

        self.assertTrue("UFA" not in person.current_status)

        contract = Contract.query.filter(Contract.person_id == person.id).first()
        self.assertFalse(contract.entry_level)
        self.assertFalse(contract.non_roster)
        self.assertEqual(contract.total_value, 92000000)
        self.assertEqual(contract.expiration_status, "UFA")
        self.assertEqual(contract.signing_date, date(year=2019, month=6, day=17))
        self.assertEqual(contract.extension, False)
        self.assertEqual(contract.thirty_five_plus, False)
        tr_asset = TransactionAsset.query.filter(
            TransactionAsset.asset_id == contract.id
        ).first()
        self.assertEqual(tr_asset.former_team, tr_asset.new_team)
        self.assertEqual(tr_asset.former_team, team.id)
        transaction = Transaction.query.get(tr_asset.transaction_id)
        self.assertEqual(transaction.type, "Signed contract")
        self.assertEqual(transaction.season_id, self.app.config["CURRENT_SEASON"] - 2)

        asset = Asset.query.filter(Asset.id == contract.id).first()
        self.assertEqual(asset.originating_team, team.id)
        self.assertEqual(asset.current_team, team.id)
        self.assertTrue(asset.active)
        self.assertEqual(asset.type, "Contract")

        years = (
            ContractYear.query.filter(ContractYear.contract_id == contract.id)
            .order_by(ContractYear.season_id.asc())
            .all()
        )
        self.assertEqual(len(years), 8)

        self.assertEqual(years[0].aav, 11500000)
        self.assertEqual(years[0].cap_hit, 11500000)
        self.assertEqual(years[0].nhl_salary, 3500000)
        self.assertEqual(years[0].minors_salary, 3500000)
        self.assertEqual(years[0].signing_bonus, 11000000)
        self.assertEqual(years[0].performance_bonuses, 0)
        self.assertFalse(years[0].two_way)

        self.assertEqual(years[1].aav, 11500000)
        self.assertEqual(years[1].cap_hit, 11500000)
        self.assertEqual(years[1].nhl_salary, 2000000)
        self.assertEqual(years[1].minors_salary, 2000000)
        self.assertEqual(years[1].signing_bonus, 10000000)
        self.assertEqual(years[1].performance_bonuses, 0)
        self.assertFalse(years[1].two_way)

        self.assertEqual(years[2].aav, 11500000)
        self.assertEqual(years[2].cap_hit, 11500000)
        self.assertEqual(years[2].nhl_salary, 5500000)
        self.assertEqual(years[2].minors_salary, 5500000)
        self.assertEqual(years[2].signing_bonus, 9000000)
        self.assertEqual(years[2].performance_bonuses, 0)
        self.assertFalse(years[2].two_way)

        self.assertEqual(years[3].aav, 11500000)
        self.assertEqual(years[3].cap_hit, 11500000)
        self.assertEqual(years[3].nhl_salary, 2000000)
        self.assertEqual(years[3].minors_salary, 2000000)
        self.assertEqual(years[3].signing_bonus, 10000000)
        self.assertEqual(years[3].performance_bonuses, 0)
        self.assertFalse(years[3].two_way)

        self.assertEqual(years[4].aav, 11500000)
        self.assertEqual(years[4].cap_hit, 11500000)
        self.assertEqual(years[4].nhl_salary, 10500000)
        self.assertEqual(years[4].minors_salary, 10500000)
        self.assertEqual(years[4].signing_bonus, 1000000)
        self.assertEqual(years[4].performance_bonuses, 0)
        self.assertFalse(years[4].two_way)

        self.assertEqual(years[5].aav, 11500000)
        self.assertEqual(years[5].cap_hit, 11500000)
        self.assertEqual(years[5].nhl_salary, 10000000)
        self.assertEqual(years[5].minors_salary, 10000000)
        self.assertEqual(years[5].signing_bonus, 1000000)
        self.assertEqual(years[5].performance_bonuses, 0)
        self.assertFalse(years[5].two_way)

        self.assertEqual(years[6].aav, 11500000)
        self.assertEqual(years[6].cap_hit, 11500000)
        self.assertEqual(years[6].nhl_salary, 4000000)
        self.assertEqual(years[6].minors_salary, 4000000)
        self.assertEqual(years[6].signing_bonus, 5000000)
        self.assertEqual(years[6].performance_bonuses, 0)
        self.assertFalse(years[6].two_way)

        self.assertEqual(years[7].aav, 11500000)
        self.assertEqual(years[7].cap_hit, 11500000)
        self.assertEqual(years[7].nhl_salary, 1500000)
        self.assertEqual(years[7].minors_salary, 1500000)
        self.assertEqual(years[7].signing_bonus, 6000000)
        self.assertEqual(years[7].performance_bonuses, 0)
        self.assertFalse(years[7].two_way)

        self.assertTrue(person.status_is("Under Contract"))
        self.assertEqual(TeamSeason.query.count(), 8)
        self.assertEqual(
            TeamSeason.query.filter(TeamSeason.cap_hit == 11500000).count(), 8
        )
        self.assertEqual(
            TeamSeason.query.filter(TeamSeason.projected_cap_hit == 11500000).count(), 8
        )
        self.assertEqual(
            TeamSeason.query.filter(TeamSeason.contract_count == 1).count(), 8
        )

        ctx.pop()

    def test_create_eklund2021(self):
        person = Person(
            name="Eklund",
            birthdate=date(year=2002, month=12, day=10),
        )
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"] - 1,
                salary_cap=82500000,
                free_agency_opening=date(year=2020, month=7, day=1),
                draft_date=date(year=2020, month=7, day=23),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"],
                salary_cap=82500000,
                free_agency_opening=date(year=2021, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                salary_cap=82500000,
                free_agency_opening=date(year=2022, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 2,
                salary_cap=82500000,
                free_agency_opening=date(year=2023, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 3,
                salary_cap=82500000,
                free_agency_opening=date(year=2024, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 4,
                salary_cap=82500000,
                free_agency_opening=date(year=2025, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 5,
                salary_cap=82500000,
                free_agency_opening=date(year=2026, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 6,
                salary_cap=82500000,
                free_agency_opening=date(year=2027, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 7,
                salary_cap=82500000,
                free_agency_opening=date(year=2028, month=7, day=1),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
        ]
        team = Team(
            name="Test",
        )
        db.session.add(person)
        for season in seasons:
            db.session.add(season)
        db.session.add(team)
        db.session.flush()
        team_season = TeamSeason(
            contract_count=4,
            season_id=self.app.config["CURRENT_SEASON"],
            team_id=team.id,
        )
        db.session.add(team_season)
        db.session.commit()

        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        form.person.data = person.id
        form.team.data = str(team.id)
        form.expiration_status.data = "RFA"
        form.signing_date.data = date(year=2021, month=8, day=21)
        form.entry_level.data = True
        form.entry_level_slide.data = "2"
        form.non_roster.data = True
        form.second_year.data = True
        form.third_year.data = True
        form.first_nhl_salary.data = 832500
        form.second_nhl_salary.data = 832500
        form.third_nhl_salary.data = 832500
        form.first_signing_bonus.data = 92500
        form.second_signing_bonus.data = 92500
        form.third_signing_bonus.data = 92500
        form.first_performance_bonuses.data = 850000
        form.second_performance_bonuses.data = 850000
        form.third_performance_bonuses.data = 850000
        form.first_minors_salary.data = 80000
        form.second_minors_salary.data = 80000
        form.third_minors_salary.data = 80000
        self.assertTrue(form.validate())
        self.assertEqual(form.create_contract(), person.id)

        contract = Contract.query.filter(Contract.person_id == person.id).first()
        self.assertEqual(contract.signing_date, date(year=2021, month=8, day=21))
        self.assertEqual(contract.expiration_status, "RFA")
        self.assertEqual(contract.contract_limit_exempt, True)
        self.assertEqual(contract.thirty_five_plus, False)
        self.assertEqual(contract.years, 3)
        self.assertEqual(contract.non_roster, True)
        self.assertEqual(contract.extension, False)
        tr_asset = TransactionAsset.query.filter(
            TransactionAsset.asset_id == contract.id
        ).first()
        self.assertEqual(tr_asset.former_team, tr_asset.new_team)
        self.assertEqual(tr_asset.former_team, team.id)
        transaction = Transaction.query.get(tr_asset.transaction_id)
        self.assertEqual(transaction.type, "Signed contract (ELC)")
        self.assertEqual(transaction.season_id, self.app.config["CURRENT_SEASON"])

        contract_years = (
            ContractYear.query.join(Contract, Contract.id == ContractYear.contract_id)
            .join(Season, Season.id == ContractYear.season_id)
            .filter(Contract.person_id == person.id)
            .order_by(Season.free_agency_opening.asc())
            .with_entities(
                ContractYear.nhl_salary,
                ContractYear.minors_salary,
                ContractYear.performance_bonuses,
                ContractYear.signing_bonus,
                ContractYear.cap_hit,
                ContractYear.aav,
                ContractYear.two_way,
                ContractYear.entry_level_slide,
            )
            .all()
        )
        self.assertEqual(len(contract_years), 5)
        self.assertEqual(contract_years[0][0], 0)
        self.assertEqual(contract_years[0][1], 80000)
        self.assertEqual(contract_years[0][2], 0)
        self.assertEqual(contract_years[0][3], 92500)
        self.assertEqual(contract_years[0][4], 925000)
        self.assertEqual(contract_years[0][5], 1775000)
        self.assertEqual(contract_years[0][6], True)
        self.assertEqual(contract_years[0][7], True)
        self.assertEqual(contract_years[1][0], 0)
        self.assertEqual(contract_years[1][1], 80000)
        self.assertEqual(contract_years[1][2], 0)
        self.assertEqual(contract_years[1][3], 92500)
        self.assertEqual(contract_years[1][4], 894166)
        self.assertEqual(contract_years[1][5], 1744166)
        self.assertEqual(contract_years[1][6], True)
        self.assertEqual(contract_years[1][7], True)
        self.assertEqual(contract_years[2][0], 832500)
        self.assertEqual(contract_years[2][1], 80000)
        self.assertEqual(contract_years[2][2], 850000)
        self.assertEqual(contract_years[2][3], 92500)
        self.assertEqual(contract_years[2][4], 863333)
        self.assertEqual(contract_years[2][5], 1713333)
        self.assertEqual(contract_years[2][6], True)
        self.assertEqual(contract_years[2][7], False)
        self.assertEqual(contract_years[3][0], 832500)
        self.assertEqual(contract_years[3][1], 80000)
        self.assertEqual(contract_years[3][2], 850000)
        self.assertEqual(contract_years[3][3], 0)
        self.assertEqual(contract_years[3][4], 863333)
        self.assertEqual(contract_years[3][5], 1713333)
        self.assertEqual(contract_years[3][6], True)
        self.assertEqual(contract_years[3][7], False)
        self.assertEqual(contract_years[4][0], 832500)
        self.assertEqual(contract_years[4][1], 80000)
        self.assertEqual(contract_years[4][2], 850000)
        self.assertEqual(contract_years[4][3], 0)
        self.assertEqual(contract_years[4][4], 863333)
        self.assertEqual(contract_years[4][5], 1713333)
        self.assertEqual(contract_years[4][6], True)
        self.assertEqual(contract_years[4][7], False)

        ctx.pop()

    def test_create_pto(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreatePTO()
        person = Person(
            name="Thomas Bordeleau",
            birthdate=date(year=2002, month=1, day=3),
        )
        current_season = Season(
            id=self.app.config["CURRENT_SEASON"],
            salary_cap=82500000,
            free_agency_opening=date(year=2021, month=7, day=1),
            draft_date=date(year=2022, month=7, day=7),
            minimum_salary=800000,
            max_elc_salary=925000,
            max_elc_minor_salary=80000,
        )
        team = Team(name="San Jose Sharks")
        db.session.add(person)
        db.session.add(current_season)
        db.session.add(team)
        db.session.commit()

        self.assertEqual(Asset.query.count(), 0)
        self.assertEqual(Transaction.query.count(), 0)
        self.assertEqual(ProfessionalTryout.query.count(), 0)
        self.assertEqual(TransactionAsset.query.count(), 0)

        form.person.data = person.id
        form.team.data = team.id
        form.signing_date.data = date(year=2022, month=4, day=16)
        form.create_pto()

        self.assertEqual(Asset.query.count(), 1)
        self.assertEqual(Transaction.query.count(), 1)
        self.assertEqual(ProfessionalTryout.query.count(), 1)
        self.assertEqual(TransactionAsset.query.count(), 1)

        asset = Asset.query.first()
        self.assertEqual(asset.active, True)
        self.assertEqual(asset.type, "Professional Tryout")
        self.assertEqual(asset.current_team, team.id)

        pto = ProfessionalTryout.query.first()
        self.assertEqual(asset.id, pto.id)
        self.assertEqual(pto.person_id, person.id)
        self.assertEqual(pto.signing_date, date(year=2022, month=4, day=16))

        transaction = Transaction.query.first()
        self.assertEqual(transaction.type, "Signed professional tryout (PTO)")
        self.assertEqual(transaction.date.date(), pto.signing_date)
        self.assertEqual(transaction.season_id, self.app.config["CURRENT_SEASON"])

        tr_asset = TransactionAsset.query.first()
        self.assertEqual(tr_asset.transaction_id, transaction.id)
        self.assertEqual(tr_asset.asset_id, asset.id)
        self.assertEqual(tr_asset.former_team, tr_asset.new_team)
        self.assertEqual(tr_asset.former_team, team.id)

    def test_broberg_offer_sheet(self):
        broberg = Person(
            id=1317,
            nhl_id=8481598,
            ep_id=349467,
            name="Philip Broberg",
            birthdate=date(year=2021, month=6, day=25),
            position="D",
            height=75,
            weight=212,
            active=True,
            current_status="Drafted,RFA",
        )
        blues = Team(
            id=19,
            name="St. Louis Blues",
            abbreviation="STL",
        )
        records = [
            Season(
                id=self.app.config["CURRENT_SEASON"],
                name="2024-2025",
                salary_cap=82500000,
                free_agency_opening=date(year=2024, month=7, day=1),
                draft_date=date(year=2025, month=6, day=23),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                name="2025-2026",
                salary_cap=82500000,
                free_agency_opening=date(year=2025, month=7, day=1),
                draft_date=date(year=2026, month=6, day=23),
                minimum_salary=10,
                max_elc_salary=925000,
                max_elc_minor_salary=80000,
                max_elc_performance_bonuses=2850000,
            ),
            TeamSeason(
                team_id=19,
                season_id=self.app.config["CURRENT_SEASON"],
                contract_count=20,
            ),
            TeamSeason(
                team_id=19,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
                contract_count=40,
            ),
            Asset(
                id=1,
                type="Contract",
                current_team=blues.id,
                originating_team=blues.id,
                active=False,
            ),
            Contract(
                id=1,
                person_id=broberg.id,
            ),
        ]
        for record in records:
            db.session.add(record)
        db.session.add(broberg)
        db.session.add(blues)
        db.session.commit()

        self.assertEqual(Asset.query.count(), 1)
        self.assertEqual(Contract.query.count(), 1)
        self.assertEqual(ContractYear.query.count(), 0)
        self.assertEqual(Transaction.query.count(), 0)
        self.assertEqual(TransactionAsset.query.count(), 0)

        ctx = self.app.test_request_context()
        ctx.push()
        form = CreateContract()

        form.person.data = broberg.id
        form.team.data = str(blues.id)
        form.expiration_status.data = "RFA"
        form.signing_date.data = date(year=2024, month=8, day=13)
        form.entry_level.data = False
        form.offer_sheet.data = True
        form.non_roster.data = False
        form.second_year.data = True
        form.first_nhl_salary.data = 4580917
        form.second_nhl_salary.data = 4580917
        self.assertTrue(form.validate())
        self.assertEqual(form.create_contract(), broberg.id)

        self.assertEqual(Asset.query.count(), 2)
        self.assertEqual(Contract.query.filter(Contract.offer_sheet == True).count(), 1)
        self.assertEqual(ContractYear.query.count(), 2)
        self.assertEqual(Transaction.query.count(), 1)
        self.assertEqual(TransactionAsset.query.count(), 1)

    # def test_get_PTO_free_agents(self):


if __name__ == "__main__":
    unittest.main(verbosity=2)
