import sys
import random
import unittest
from datetime import datetime, date, timedelta
from app import create_app, db
from app.models import (
    Team,
    Season,
    Person,
    Asset,
    DraftPick,
    Contract,
    ContractYear,
    Transaction,
    TransactionAsset,
    RetainedSalary,
    BuyoutYear,
    Penalty,
    PenaltyYear,
    TeamSeason,
    ProfessionalTryout,
    SigningRights,
    SkaterSeason,
    GoalieSeason,
    DailyCap,
)

from app.main import person, team

try:
    from .create_people import create_zach_parise, create_jared_cowen
except ImportError:
    from create_people import create_zach_parise, create_jared_cowen


class TeamTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def create_draft_picks(self, team_id, team_abbr):
        test_data = []
        year = 1900
        for _ in range(3):
            season_name = "".join(random.choices("abdefghijklmnopqrstuvwzyz ", k=15))
            season = Season(
                name=season_name, draft_date=date(year=year, month=1, day=1)
            )
            year += 1
            db.session.add(season)
            db.session.commit()
            season_data = {
                "season_id": season.id,
                "season": season_name,
                "draft_date": season.draft_date,
                "picks": [],
            }
            for draft_round in range(1, 8):
                traded_away = random.choice([True, False, False])
                if traded_away:
                    season_data["picks"].append(
                        self.create_traded_away_pick(
                            team_id, team_abbr, season.id, draft_round
                        )
                    )
                else:
                    season_data["picks"].append(
                        self.create_team_draft_pick(
                            team_id, team_abbr, season.id, draft_round
                        )
                    )

                acquired_pick = random.choice([True, False, False])
                if acquired_pick:
                    season_data["picks"].append(
                        self.create_acquired_pick(
                            team_id, team_abbr, season.id, draft_round
                        )
                    )

            season_data["picks"].sort(
                key=lambda pick: (pick["round"], pick["position"] or 0, pick["team"])
            )
            test_data.append(season_data)
        test_data.sort(key=lambda x: x["draft_date"])
        self.app.config["DRAFT_SEASON"] = test_data[0]["season_id"]
        return test_data

    def create_team(self):
        team_name = "".join(random.choices("abdefghijklmnopqrstuvwzyz ", k=15))
        team_abbreviation = "".join(
            random.choices("ABCDEFGHIJLMNOPQRSTUVWXYZ01234567890", k=3)
        )
        team = Team(
            name=team_name,
            abbreviation=team_abbreviation,
            first_year=random.randint(1890, 2100),
        )
        db.session.add(team)
        db.session.commit()
        test_data = {
            "id": team.id,
            "name": team_name,
            "abbreviation": team_abbreviation,
            "first_year": team.first_year,
        }
        return test_data

    def create_acquired_pick(self, team_id, team_abbr, season_id, round_number):
        originating_team = self.create_team()
        asset = Asset(
            originating_team=originating_team["id"], current_team=team_id, active=True
        )
        db.session.add(asset)
        db.session.commit()

        test_data = self.create_pick(asset, season_id, round_number)
        test_data["team"] = originating_team["abbreviation"]
        test_data["traded_away"] = False
        test_data["traded"] = True
        return test_data

    def create_traded_away_pick(self, team_id, team_abbr, season_id, round_number):
        current_team = self.create_team()
        asset = Asset(
            originating_team=team_id, current_team=current_team["id"], active=True
        )
        db.session.add(asset)
        db.session.commit()

        test_data = self.create_pick(asset, season_id, round_number)
        test_data["team"] = team_abbr
        test_data["traded_away"] = True
        test_data["traded"] = False
        return test_data

    def create_team_draft_pick(self, team_id, team_abbr, season_id, round_number):
        asset = Asset(
            originating_team=team_id,
            current_team=team_id,
            active=True,
        )
        db.session.add(asset)
        db.session.commit()

        test_data = self.create_pick(asset, season_id, round_number)
        test_data["team"] = team_abbr
        test_data["traded_away"] = False
        test_data["traded"] = False
        return test_data

    def create_pick(self, asset, season_id, draft_round):
        draft_position = 32 * draft_round + random.randint(1, 32)
        if random.choice([True, False]):
            draft_position = None

        draft_pick = DraftPick(
            id=asset.id, season_id=season_id, round=draft_round, position=draft_position
        )
        db.session.add(draft_pick)
        db.session.commit()
        test_data = {
            "position": draft_position,
            "round": draft_round,
        }
        return test_data

    def compare_draft_picks(self, test_data, check_data):
        self.assertTrue(test_data[0]["season"] == check_data[0]["season"])
        self.assertTrue(test_data[1]["season"] == check_data[1]["season"])
        self.assertTrue(test_data[2]["season"] == check_data[2]["season"])
        for idx_s in range(3):
            test_round = test_data[idx_s]["picks"]
            check_round = check_data[idx_s]["picks"]
            for idx_p in range(len(test_round)):
                self.assertTrue(
                    (
                        test_round[idx_p]["position"] is None
                        and check_round[idx_p]["position"] is None
                    )
                    or test_round[idx_p]["position"] == check_round[idx_p]["position"]
                )
                self.assertTrue(
                    test_round[idx_p]["round"] == check_round[idx_p]["round"]
                )
                self.assertTrue(
                    test_round[idx_p]["traded"] == check_round[idx_p]["traded"]
                )
                self.assertTrue(
                    test_round[idx_p]["traded_away"]
                    == check_round[idx_p]["traded_away"]
                )
                self.assertTrue(test_round[idx_p]["team"] == check_round[idx_p]["team"])

    def create_roster(self, team_id):
        letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "
        seasons = []
        for _ in range(8):
            season_name = "".join(random.choices(letters, k=20))
            season = Season(name=season_name)
            seasons.append(season)
            db.session.add(season)
        db.session.commit()
        offense = []
        defense = []
        goaltending = []
        # for _ in range(13):
        for _ in range(2):
            position = random.choice(["C", "RW", "LW"])
            offense.append(self.create_player(team_id, position, seasons))
        # for _ in range(8):
        for _ in range(2):
            position = random.choice(["D", "LD", "RD"])
            defense.append(self.create_player(team_id, position, seasons))
        # for _ in range(3):
        for _ in range(2):
            position = "G"
            goaltending.append(self.create_player(team_id, position, seasons))
        # Contract total value desc, Contract years desc
        offense.sort(
            key=lambda person: (
                -person["years"][0]["cap_hit"],
                -person["total_value"],
                -len(person["years"]),
            )
        )
        defense.sort(
            key=lambda person: (
                -person["years"][0]["cap_hit"],
                -person["total_value"],
                -len(person["years"]),
            )
        )
        goaltending.sort(
            key=lambda person: (
                -person["years"][0]["cap_hit"],
                -person["total_value"],
                -len(person["years"]),
            )
        )
        return offense + defense + goaltending

    def create_player(self, team_id, position, seasons):
        test_data = {
            "birthdate": datetime.strptime(
                f"{random.randint(1000,9999)}-{random.randint(1,12):02}-{random.randint(1,28):02}",
                self.app.config["DATE_FMT"],
            ).date(),
            "name": "".join(random.choices(" qwertyuiopasdfghjklzxcvbnm", k=15)),
            "position": position,
        }
        person = Person(**test_data)
        db.session.add(person)
        db.session.commit()

        contract_data = self.create_contract(person.id, team_id, seasons)
        test_data["expires_as"] = contract_data["expires_as"]
        test_data["total_value"] = contract_data["total_value"]
        test_data["years"] = contract_data["years"]
        test_data["birthdate"] = test_data["birthdate"].strftime(
            self.app.config["DATE_FMT"]
        )
        test_data["id"] = person.id
        test_data["contract_id"] = contract_data["id"]
        test_data["nmc"] = contract_data["nmc"]
        test_data["ntc"] = contract_data["ntc"]
        test_data["clause_limits"] = contract_data["clause_limits"]
        return test_data

    def create_contract(self, person_id, team_id, seasons):
        asset_data = {"current_team": team_id, "active": True}
        asset = Asset(**asset_data)
        db.session.add(asset)
        db.session.flush()

        contract_data = {
            "id": asset.id,
            "person_id": person_id,
            "years": random.randint(1, 8),
            "total_value": random.randint(0, 20000000),
            "expiration_status": random.choice(["UFA", "RFA", "10.2c", "N/A"]),
            "entry_level": random.choice([True, False]),
            "thirty_five_plus": random.choice([True, False]),
        }
        contract = Contract(**contract_data)
        db.session.add(contract)
        db.session.commit()

        contract_data["seasons"] = contract_data["years"]
        contract_data["expires_as"] = contract_data["expiration_status"]
        contract_data["years"] = []
        contract_data["id"] = contract.id
        aav = contract_data["total_value"] // contract_data["seasons"]

        letters = "abcdefghijklmnopqrstuvwxyz "
        for idx in range(contract_data["seasons"]):
            contract_data["years"].append(
                self.create_contract_year(
                    contract.id, aav, seasons[idx].id, seasons[idx].name
                )
            )
        contract_data["nmc"] = contract_data["years"][0]["nmc"]
        contract_data["ntc"] = contract_data["years"][0]["ntc"]
        contract_data["clause_limits"] = contract_data["years"][0]["clause_limits"]
        contract_data["years"].sort(key=lambda year: year["season"])

        return contract_data

    def create_contract_year(self, contract_id, aav, season_id, season_name):
        data = {
            "aav": aav,
            "cap_hit": aav,
            "minors_salary": random.randint(0, 300000),
            "nhl_salary": random.randint(0, 3000000),
            "performance_bonuses": random.randint(0, 1000000),
            "signing_bonus": random.randint(0, 100000),
            "contract_id": contract_id,
            "season_id": season_id,
            "nmc": random.choice([True, False, False, False, False]),
            "ntc": random.choice([True, False, False, False, False]),
            "clause_limits": "".join(
                random.choices("abcdefghijklmnopqrstuvwxyz ", k=16)
            ),
        }

        contract_year = ContractYear(**data)
        db.session.add(contract_year)
        db.session.commit()
        data["season"] = season_name
        return data

    def compare_contract_years(self, test_data, check_data):
        for idx in range(len(test_data)):
            self.assertTrue(test_data[idx]["aav"] == check_data[idx]["aav"])
            self.assertTrue(test_data[idx]["cap_hit"] == check_data[idx]["cap_hit"])
            self.assertTrue(
                test_data[idx]["nhl_salary"] == check_data[idx]["nhl_salary"]
            )
            self.assertTrue(
                test_data[idx]["performance_bonuses"]
                == check_data[idx]["performance_bonuses"]
            )
            self.assertTrue(
                test_data[idx]["signing_bonus"] == check_data[idx]["signing_bonus"]
            )
            self.assertTrue(test_data[idx]["season"] == check_data[idx]["season"])

    def compare_roster(self, test_data, check_data):
        for idx in range(len(test_data)):
            self.assertEqual(test_data[idx]["birthdate"], check_data[idx]["birthdate"])
            self.assertEqual(
                test_data[idx]["expires_as"], check_data[idx]["expires_as"]
            )
            self.assertEqual(test_data[idx]["id"], check_data[idx]["id"])
            self.assertEqual(test_data[idx]["name"], check_data[idx]["name"])
            self.assertEqual(test_data[idx]["position"], check_data[idx]["position"])
            self.assertEqual(test_data[idx]["ntc"], check_data[idx]["ntc"])
            self.assertEqual(test_data[idx]["nmc"], check_data[idx]["nmc"])
            self.assertEqual(
                test_data[idx]["clause_limits"], check_data[idx]["clause_limits"]
            )
            self.compare_contract_years(
                test_data[idx]["years"], check_data[idx]["years"]
            )

    def create_test_team(self):
        test_data = self.create_team()
        test_data["roster"] = self.create_roster(test_data["id"])
        test_data["draft_picks"] = self.create_draft_picks(
            test_data["id"], test_data["abbreviation"]
        )
        return test_data

    def compare_team(self, test_data, check_data):
        self.assertTrue(test_data["abbreviation"] == check_data["abbreviation"])
        self.assertTrue(test_data["first_year"] == check_data["first_year"])
        self.assertTrue(test_data["id"] == check_data["id"])
        self.assertTrue(test_data["name"] == check_data["name"])
        self.compare_roster(test_data["roster"], check_data["roster"])
        self.compare_draft_picks(test_data["draft_picks"], check_data["draft_picks"])

    def test_draft_picks(self):
        test_team = self.create_team()
        test_data = self.create_draft_picks(test_team["id"], test_team["abbreviation"])
        check_data = team.get_draft_picks(test_team["id"])
        self.compare_draft_picks(test_data, check_data)

    def test_roster(self):
        test_team = self.create_team()
        test_data = self.create_roster(test_team["id"])
        check_data = team.get_roster(test_team["id"])
        self.compare_roster(test_data, check_data)

    def test_get_data(self):
        test_data = self.create_test_team()
        check_data = team.get_data(test_data["id"])
        self.compare_team(test_data, check_data)

    def test_buyout(self):
        db_data = create_zach_parise(db)
        team_id = db_data["teams"][1].id
        team_data = team.get_data(team_id)
        self.assertEqual(db_data["teams"][1].id, team_data["id"])
        self.assertEqual(db_data["teams"][1].name, team_data["name"])
        self.assertEqual(db_data["teams"][1].abbreviation, team_data["abbreviation"])
        self.assertEqual([], team_data["draft_picks"])
        self.assertEqual([], team_data["roster"])
        self.assertTrue("non_roster" in team_data)
        self.assertTrue("buyouts" in team_data["non_roster"])
        buyout = team_data["non_roster"]["buyouts"][0]
        self.assertEqual(db_data["person"].name, buyout["name"])
        self.assertEqual(db_data["person"].id, buyout["id"])
        self.assertEqual(len(db_data["buyout_years"]), len(buyout["years"]))
        for idx in range(len(buyout["years"])):
            season = [
                season.name
                for season in db_data["seasons"]
                if season.id == db_data["buyout_years"][idx].season_id
            ][0]
            self.assertEqual(season, buyout["years"][idx]["season"])
            self.assertEqual(
                db_data["buyout_years"][idx].cap_hit, buyout["years"][idx]["cap_hit"]
            )
            self.assertEqual(
                db_data["buyout_years"][idx].nhl_salary,
                buyout["years"][idx]["nhl_salary"],
            )
            self.assertEqual(
                db_data["buyout_years"][idx].signing_bonus,
                buyout["years"][idx]["signing_bonus"],
            )

    def test_set_titles(self):
        data = [
            {
                "picks": [
                    {
                        "condition": None,
                        "traded_away": False,
                    },
                    {
                        "condition": None,
                        "traded_away": True,
                    },
                ],
            },
            {
                "picks": [
                    {
                        "condition": ["test one"],
                        "traded_away": True,
                    },
                    {
                        "condition": ["test one", "test_two"],
                        "traded_away": True,
                    },
                ],
            },
            {
                "picks": [
                    {
                        "condition": ["test one"],
                        "traded_away": False,
                    },
                    {
                        "condition": ["test one", "test_two"],
                        "traded_away": False,
                    },
                ],
            },
        ]
        team.set_titles(data)
        self.assertFalse("title" in data[0]["picks"][0])
        self.assertEqual(data[0]["picks"][1]["title"], "Traded Away")
        self.assertEqual(data[1]["picks"][0]["title"], "Traded Away\ntest one")
        self.assertEqual(
            data[1]["picks"][1]["title"], "Traded Away\ntest one\ntest_two"
        )
        self.assertEqual(data[2]["picks"][0]["title"], "test one")
        self.assertEqual(data[2]["picks"][1]["title"], "test one\ntest_two")

    def test_limit_years(self):
        data = {"idk": [{"years": [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]}]}
        team.limit_years(data)
        self.assertEqual(data["idk"][0]["years"], [1, 2, 3, 4, 5, 6, 7, 8, 9, 0])

        data = {
            "roster": [
                {"years": [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]},
                {"years": [1, 2, 3, 4]},
                {"years": [1, 2, 3, 4, 5, 6, 7]},
                {"years": [1, 2, 3, 4, 5, 6]},
            ],
        }
        team.limit_years(data)
        self.assertTrue(data["roster"][0]["display_overflow"])
        self.assertTrue(data["roster"][2]["display_overflow"])
        self.assertFalse("display_overflow" in data["roster"][1])
        self.assertFalse("display_overflow" in data["roster"][3])

        self.assertEqual(data["roster"][0]["years"], [1, 2, 3, 4, 5, 6])
        self.assertEqual(data["roster"][1]["years"], [1, 2, 3, 4])
        self.assertEqual(data["roster"][2]["years"], [1, 2, 3, 4, 5, 6])
        self.assertEqual(data["roster"][3]["years"], [1, 2, 3, 4, 5, 6])

        data["non_roster"] = {
            "test": [
                {"years": [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]},
                {"years": [1, 2, 3, 4]},
            ],
            "test2": [{"years": [1, 2, 3, 4, 5, 6, 7]}, {"years": [1, 2, 3, 4, 5, 6]}],
        }
        team.limit_years(data)
        self.assertTrue(data["non_roster"]["test"][0]["display_overflow"])
        self.assertTrue(data["non_roster"]["test2"][0]["display_overflow"])
        self.assertFalse("display_overflow" in data["non_roster"]["test"][1])
        self.assertFalse("display_overflow" in data["non_roster"]["test2"][1])

        self.assertEqual(data["non_roster"]["test"][0]["years"], [1, 2, 3, 4, 5, 6])
        self.assertEqual(data["non_roster"]["test"][1]["years"], [1, 2, 3, 4])
        self.assertEqual(data["non_roster"]["test2"][0]["years"], [1, 2, 3, 4, 5, 6])
        self.assertEqual(data["non_roster"]["test2"][1]["years"], [1, 2, 3, 4, 5, 6])

        del data["roster"]
        data["non_roster"] = {
            "test": [
                {"years": [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]},
                {"years": [1, 2, 3, 4]},
            ],
            "test2": [{"years": [1, 2, 3, 4, 5, 6, 7]}, {"years": [1, 2, 3, 4, 5, 6]}],
        }
        self.assertFalse("display_overflow" in data["non_roster"]["test"][0])
        self.assertFalse("display_overflow" in data["non_roster"]["test"][1])
        self.assertFalse("display_overflow" in data["non_roster"]["test2"][0])
        self.assertFalse("display_overflow" in data["non_roster"]["test2"][1])

    def test_age_from_birthdate(self):
        test = (date.today() - timedelta(days=300)).strftime(
            self.app.config["DATE_FMT"]
        )
        self.assertEqual(team.age_from_birthdate(test), 0)

        test = (date.today() - timedelta(days=380)).strftime(
            self.app.config["DATE_FMT"]
        )
        self.assertEqual(team.age_from_birthdate(test), 1)

        test = (date.today() - timedelta(days=(365 * 12 + 40))).strftime(
            self.app.config["DATE_FMT"]
        )
        self.assertEqual(team.age_from_birthdate(test), 12)

    def test_get_teams(self):
        teams = [
            Team(id=1, name="rTest 1"),
            Team(id=2, name="Test 2"),
            Team(id=3, name="ATest 3"),
            Team(id=0, name="ZTest 0"),
            Team(id=4, name="aTest 4"),
        ]
        for tm in teams:
            db.session.add(tm)
        db.session.commit()
        queried = team.get_teams()
        self.assertEqual(queried[0], (3, "ATest 3"))
        self.assertEqual(queried[1], (2, "Test 2"))
        self.assertEqual(queried[2], (0, "ZTest 0"))
        self.assertEqual(queried[3], (4, "aTest 4"))
        self.assertEqual(queried[4], (1, "rTest 1"))

    def test_get_pick_conditions(self):
        transactions = [
            Transaction(id=1, date=date.today()),
            Transaction(id=2, date=date.today() - timedelta(days=2)),
        ]
        assets = [
            TransactionAsset(transaction_id=1, asset_id=1, condition="test one"),
            TransactionAsset(transaction_id=2, asset_id=1, condition="test_two"),
        ]
        for tr in transactions:
            db.session.add(tr)
        for asset in assets:
            db.session.add(asset)
        db.session.commit()
        self.assertEqual(team.get_pick_conditions(1), ["test_two", "test one"])

    def test_get_season_names(self):
        seasons = [
            Season(
                id=1, name="Test-1", free_agency_opening=date(year=1999, month=1, day=1)
            ),
            Season(
                id=2, name="Test-2", free_agency_opening=date(year=2002, month=1, day=1)
            ),
            Season(
                id=3, name="Test-3", free_agency_opening=date(year=2012, month=1, day=1)
            ),
            Season(
                id=4, name="Test-4", free_agency_opening=date(year=2007, month=1, day=1)
            ),
            Season(
                id=5, name="Test-5", free_agency_opening=date(year=2017, month=1, day=1)
            ),
            Season(
                id=6, name="Test-6", free_agency_opening=date(year=2019, month=1, day=1)
            ),
            Season(
                id=7, name="Test-7", free_agency_opening=date(year=2004, month=1, day=1)
            ),
            Season(
                id=8, name="Test-8", free_agency_opening=date(year=2010, month=1, day=1)
            ),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.commit()
        names = team.get_season_names()
        self.assertEqual(
            names,
            [
                ("Test-1",),
                ("Test-2",),
                ("Test-7",),
                ("Test-4",),
                ("Test-8",),
                ("Test-3",),
            ],
        )

    def test_get_total_retention(self):
        contract = Contract(id=1, person_id=7)
        db.session.add(contract)
        db.session.commit()
        self.assertEqual(team.get_total_retention(7), 0)
        asset = Asset(id=1)
        retained = RetainedSalary(id=1, contract_id=1, retention=25)
        db.session.add(asset)
        db.session.add(retained)
        db.session.commit()
        self.assertEqual(team.get_total_retention(7), 25)
        asset = Asset(id=2)
        retained = RetainedSalary(id=2, contract_id=1, retention=7)
        db.session.add(asset)
        db.session.add(retained)
        db.session.commit()
        self.assertEqual(team.get_total_retention(7), 32)

    def test_get_contract_years(self):
        seasons = [
            Season(
                id=1,
                name="Test-1",
            ),
            Season(
                id=2,
                name="Test-2",
            ),
            Season(
                id=3,
                name="Test-3",
            ),
            Season(
                id=4,
                name="Test-4",
            ),
            Season(
                id=5,
                name="Test-5",
            ),
            Season(
                id=6,
                name="Test-6",
            ),
        ]
        contract = Contract(id=1, person_id=9)
        contract_year = [
            ContractYear(
                contract_id=1,
                season_id=1,
                cap_hit=1,
                aav=2,
                nhl_salary=3,
                performance_bonuses=4,
                signing_bonus=5,
            ),
            ContractYear(
                contract_id=1,
                season_id=2,
                cap_hit=2,
                aav=2,
                nhl_salary=4,
                performance_bonuses=3,
                signing_bonus=6,
            ),
            ContractYear(
                contract_id=1,
                season_id=3,
                cap_hit=3,
                aav=2,
                nhl_salary=5,
                performance_bonuses=2,
                signing_bonus=7,
            ),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.add(contract)
        for year in contract_year:
            db.session.add(year)
        db.session.commit()
        years = team.get_contract_years(9)
        self.assertEqual(years[0]["cap_hit"], 1)
        self.assertEqual(years[0]["aav"], 2)
        self.assertEqual(years[0]["nhl_salary"], 3)
        self.assertEqual(years[0]["performance_bonuses"], 4)
        self.assertEqual(years[0]["signing_bonus"], 5),
        self.assertEqual(years[0]["season"], "Test-1")
        self.assertEqual(years[1]["cap_hit"], 2)
        self.assertEqual(years[1]["aav"], 2)
        self.assertEqual(years[1]["nhl_salary"], 4)
        self.assertEqual(years[1]["performance_bonuses"], 3)
        self.assertEqual(years[1]["signing_bonus"], 6),
        self.assertEqual(years[1]["season"], "Test-2")
        self.assertEqual(years[2]["cap_hit"], 3)
        self.assertEqual(years[2]["aav"], 2)
        self.assertEqual(years[2]["nhl_salary"], 5)
        self.assertEqual(years[2]["performance_bonuses"], 2)
        self.assertEqual(years[2]["signing_bonus"], 7),
        self.assertEqual(years[2]["season"], "Test-3")

    def test_get_query_non_roster_years(self):
        contract_id = 2
        person_id = -2
        base_szn = self.app.config["CURRENT_SEASON"]
        seasons = [
            Season(
                name="Test1",
                free_agency_opening=date(year=2000, month=1, day=1),
                id=base_szn,
                max_buriable_hit=1000,
            ),
            Season(
                name="Test2",
                free_agency_opening=date(year=2005, month=1, day=1),
                id=base_szn + 1,
                max_buriable_hit=2000,
            ),
            Season(
                name="Test3",
                free_agency_opening=date(year=2015, month=1, day=1),
                id=base_szn + 2,
                max_buriable_hit=3000,
            ),
            Season(
                name="Test4",
                free_agency_opening=date(year=2010, month=1, day=1),
                id=base_szn + 3,
                max_buriable_hit=4000,
            ),
        ]
        asset = Asset(id=contract_id)
        contract = Contract(id=contract_id, non_roster=True, person_id=person_id)
        years = [
            ContractYear(
                contract_id=contract_id,
                season_id=base_szn + 0,
                cap_hit=1234,
                nhl_salary=5678,
                signing_bonus=2468,
            ),
            ContractYear(
                contract_id=contract_id,
                season_id=base_szn + 1,
                cap_hit=1235,
                nhl_salary=5675,
                signing_bonus=3468,
            ),
            ContractYear(
                contract_id=contract_id,
                season_id=base_szn + 2,
                cap_hit=1236,
                nhl_salary=5676,
                signing_bonus=2568,
            ),
            ContractYear(
                contract_id=contract_id,
                season_id=base_szn + 3,
                cap_hit=1237,
                nhl_salary=5677,
                signing_bonus=2478,
            ),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.add(asset)
        db.session.add(contract)
        for year in years:
            db.session.add(year)
        db.session.commit()

        query = team.query_non_roster_years(person_id)
        self.assertEqual(query[0][0], 1234)  # cap_hit
        self.assertEqual(query[0][1], 5678)  # nhl_salary
        self.assertEqual(query[0][2], 2468)  # signing_bonus
        self.assertEqual(query[0][3], "Test1")  # season name
        self.assertEqual(query[0][4], 1000)  # max_buriable
        self.assertEqual(query[1][0], 1235)  # cap_hit
        self.assertEqual(query[1][1], 5675)  # nhl_salary
        self.assertEqual(query[1][2], 3468)  # signing_bonus
        self.assertEqual(query[1][3], "Test2")  # season name
        self.assertEqual(query[1][4], 2000)  # max_buriable
        self.assertEqual(query[2][0], 1237)  # cap_hit
        self.assertEqual(query[2][1], 5677)  # nhl_salary
        self.assertEqual(query[2][2], 2478)  # signing_bonus
        self.assertEqual(query[2][3], "Test4")  # season name
        self.assertEqual(query[2][4], 4000)  # max_buriable
        self.assertEqual(query[3][0], 1236)  # cap_hit
        self.assertEqual(query[3][1], 5676)  # nhl_salary
        self.assertEqual(query[3][2], 2568)  # signing_bonus
        self.assertEqual(query[3][3], "Test3")  # season name
        self.assertEqual(query[3][4], 3000)  # max_buriable

        query2 = team.get_non_roster_years(person_id)
        self.assertEqual(query2[0]["cap_hit"], 234)  # cap_hit
        self.assertEqual(query2[0]["nhl_salary"], 5678)  # nhl_salary
        self.assertEqual(query2[0]["signing_bonus"], 2468)  # signing_bonus
        self.assertEqual(query2[0]["season"], "Test1")  # season name
        self.assertEqual(query2[0]["amount_buried"], 1000)  # max_buriable
        self.assertEqual(query2[1]["cap_hit"], 0)  # cap_hit
        self.assertEqual(query2[1]["nhl_salary"], 5675)  # nhl_salary
        self.assertEqual(query2[1]["signing_bonus"], 3468)  # signing_bonus
        self.assertEqual(query2[1]["season"], "Test2")  # season name
        self.assertEqual(query2[1]["amount_buried"], 1235)  # max_buriable
        self.assertEqual(query2[2]["cap_hit"], 0)  # cap_hit
        self.assertEqual(query2[2]["nhl_salary"], 5677)  # nhl_salary
        self.assertEqual(query2[2]["signing_bonus"], 2478)  # signing_bonus
        self.assertEqual(query2[2]["season"], "Test4")  # season name
        self.assertEqual(query2[2]["amount_buried"], 1237)  # max_buriable
        self.assertEqual(query2[3]["cap_hit"], 0)  # cap_hit
        self.assertEqual(query2[3]["nhl_salary"], 5676)  # nhl_salary
        self.assertEqual(query2[3]["signing_bonus"], 2568)  # signing_bonus
        self.assertEqual(query2[3]["season"], "Test3")  # season name
        self.assertEqual(query2[3]["amount_buried"], 1236)  # max_buriable

    def test_query_non_roster(self):
        team_id = 8
        people = [
            Person(name="Test Yes", position="Z", birthdate=date.today()),
            Person(name="Test Yes2", position="Y", birthdate=date.today()),
            Person(name="Test No", position="X", birthdate=date.today()),
        ]
        assets = [
            Asset(active=True, current_team=team_id),
            Asset(active=True, current_team=team_id),
            Asset(active=True, current_team=team_id),
        ]
        for person in people:
            db.session.add(person)
        for asset in assets:
            db.session.add(asset)
        db.session.flush()

        contracts = [
            Contract(
                id=assets[0].id,
                expiration_status="UFA",
                person_id=people[0].id,
                non_roster=True,
            ),
            Contract(
                id=assets[1].id,
                expiration_status="RFA",
                person_id=people[1].id,
                non_roster=True,
            ),
            Contract(
                id=assets[2].id,
                expiration_status="ZFA",
                person_id=people[2].id,
                non_roster=False,
            ),
        ]
        contract_years = [
            ContractYear(
                contract_id=assets[0].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=1000,
            ),
            ContractYear(
                contract_id=assets[1].id,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=500,
            ),
            ContractYear(
                contract_id=assets[2].id, season_id=self.app.config["CURRENT_SEASON"]
            ),
        ]
        for contract in contracts:
            db.session.add(contract)
        for year in contract_years:
            db.session.add(year)
        db.session.commit()

        query = team.query_non_roster(team_id)
        self.assertEqual(len(query), 2)
        self.assertEqual(query[0][0], people[0].name)  # name
        self.assertEqual(query[0][1], people[0].position)  # position
        self.assertEqual(query[0][2], date.today())  # birthdate
        self.assertEqual(query[0][3], people[0].id)  # person id
        self.assertEqual(query[1][0], people[1].name)  # name
        self.assertEqual(query[1][1], people[1].position)  # position
        self.assertEqual(query[1][2], date.today())  # birthdate
        self.assertEqual(query[1][3], people[1].id)  # person id

        contracts[0].on_loan = True
        db.session.add(contracts[0])
        db.session.commit()
        query = team.query_non_roster(team_id)
        self.assertEqual(len(query), 2)
        self.assertEqual(query[0][3], people[1].id)  # person id
        self.assertEqual(query[1][3], people[0].id)  # person id

        contracts[1].on_loan = True
        contracts[1].in_juniors = True
        db.session.add(contracts[1])
        db.session.commit()
        query = team.query_non_roster(team_id)
        self.assertEqual(len(query), 2)
        self.assertEqual(query[0][3], people[0].id)  # person id
        self.assertEqual(query[1][3], people[1].id)  # person id

    def test_get_query_buyout_years(self):
        base_szn = self.app.config["CURRENT_SEASON"]
        contract_id = 3
        seasons = [
            Season(
                name="Test1",
                free_agency_opening=date(year=2000, month=1, day=1),
                id=base_szn,
                max_buriable_hit=1000,
            ),
            Season(
                name="Test2",
                free_agency_opening=date(year=2005, month=1, day=1),
                id=base_szn + 1,
                max_buriable_hit=2000,
            ),
            Season(
                name="Test3",
                free_agency_opening=date(year=2015, month=1, day=1),
                id=base_szn + 2,
                max_buriable_hit=3000,
            ),
            Season(
                name="Test4",
                free_agency_opening=date(year=2010, month=1, day=1),
                id=base_szn + 3,
                max_buriable_hit=4000,
            ),
        ]
        buyout_years = [
            BuyoutYear(
                contract_id=3,
                cap_hit=900,
                signing_bonus=700,
                nhl_salary=600,
                season_id=base_szn + 0,
            ),
            BuyoutYear(
                contract_id=3,
                cap_hit=800,
                signing_bonus=600,
                nhl_salary=600,
                season_id=base_szn + 1,
            ),
            BuyoutYear(
                contract_id=3,
                cap_hit=700,
                signing_bonus=500,
                nhl_salary=700,
                season_id=base_szn + 2,
            ),
            BuyoutYear(
                contract_id=3,
                cap_hit=600,
                signing_bonus=400,
                nhl_salary=800,
                season_id=base_szn + 3,
            ),
        ]
        for year in seasons:
            db.session.add(year)
        for year in buyout_years:
            db.session.add(year)
        db.session.commit()

        query = team.query_buyout_years(contract_id)
        self.assertEqual(len(query), 4)
        self.assertEqual(query[0][0], 900)  # cap_hit
        self.assertEqual(query[0][1], 600)  # nhl_salary
        self.assertEqual(query[0][2], 700)  # signing_bonus
        self.assertEqual(query[0][3], "Test1")  # name
        self.assertEqual(query[1][0], 800)  # cap_hit
        self.assertEqual(query[1][1], 600)  # nhl_salary
        self.assertEqual(query[1][2], 600)  # signing_bonus
        self.assertEqual(query[1][3], "Test2")  # name
        self.assertEqual(query[2][0], 600)  # cap_hit
        self.assertEqual(query[2][1], 800)  # nhl_salary
        self.assertEqual(query[2][2], 400)  # signing_bonus
        self.assertEqual(query[2][3], "Test4")  # name
        self.assertEqual(query[3][0], 700)  # cap_hit
        self.assertEqual(query[3][1], 700)  # nhl_salary
        self.assertEqual(query[3][2], 500)  # signing_bonus
        self.assertEqual(query[3][3], "Test3")  # name

        query2 = team.get_buyout_years(contract_id)
        self.assertEqual(len(query), 4)
        self.assertEqual(query2[0]["cap_hit"], 900)  # cap_hit
        self.assertEqual(query2[0]["nhl_salary"], 600)  # nhl_salary
        self.assertEqual(query2[0]["signing_bonus"], 700)  # signing_bonus
        self.assertEqual(query2[0]["season"], "Test1")  # name
        self.assertEqual(query2[1]["cap_hit"], 800)  # cap_hit
        self.assertEqual(query2[1]["nhl_salary"], 600)  # nhl_salary
        self.assertEqual(query2[1]["signing_bonus"], 600)  # signing_bonus
        self.assertEqual(query2[1]["season"], "Test2")  # name
        self.assertEqual(query2[2]["cap_hit"], 600)  # cap_hit
        self.assertEqual(query2[2]["nhl_salary"], 800)  # nhl_salary
        self.assertEqual(query2[2]["signing_bonus"], 400)  # signing_bonus
        self.assertEqual(query2[2]["season"], "Test4")  # name
        self.assertEqual(query2[3]["cap_hit"], 700)  # cap_hit
        self.assertEqual(query2[3]["nhl_salary"], 700)  # nhl_salary
        self.assertEqual(query2[3]["signing_bonus"], 500)  # signing_bonus
        self.assertEqual(query2[3]["season"], "Test3")  # name

    def test_query_buyouts(self):
        team_id = 9
        people = [
            Person(name="Test Yes", position="Z", birthdate=date.today()),
            Person(name="Test Yes2", position="Y", birthdate=date.today()),
            Person(name="Test No", position="X", birthdate=date.today()),
        ]
        assets = [
            Asset(active=False, current_team=team_id),
            Asset(active=False, current_team=team_id),
            Asset(active=False, current_team=team_id),
        ]
        for person in people:
            db.session.add(person)
        for asset in assets:
            db.session.add(asset)
        db.session.flush()

        contracts = [
            Contract(id=assets[0].id, person_id=people[0].id, non_roster=True),
            Contract(id=assets[1].id, person_id=people[1].id, non_roster=True),
            Contract(id=assets[2].id, person_id=people[2].id, non_roster=False),
        ]
        buyout_years = [
            BuyoutYear(
                contract_id=assets[0].id,
                cap_hit=900,
                signing_bonus=700,
                nhl_salary=600,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            BuyoutYear(
                contract_id=assets[1].id,
                cap_hit=800,
                signing_bonus=600,
                nhl_salary=600,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            BuyoutYear(
                contract_id=assets[1].id,
                cap_hit=700,
                signing_bonus=500,
                nhl_salary=700,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            BuyoutYear(
                contract_id=assets[2].id,
                cap_hit=600,
                signing_bonus=400,
                nhl_salary=800,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            BuyoutYear(
                contract_id=assets[2].id,
                cap_hit=600,
                signing_bonus=400,
                nhl_salary=800,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
            BuyoutYear(
                contract_id=assets[2].id,
                cap_hit=600,
                signing_bonus=400,
                nhl_salary=800,
                season_id=self.app.config["CURRENT_SEASON"],
            ),
        ]
        for contract in contracts:
            db.session.add(contract)
        for year in buyout_years:
            db.session.add(year)
        season = Season(
            id=self.app.config["CURRENT_SEASON"],
            free_agency_opening=date.today(),
            draft_date=date.today(),
        )
        db.session.add(season)
        db.session.commit()

        query = team.query_buyouts(team_id)
        self.assertEqual(len(query), 3)
        self.assertEqual(query[2][0], "Test Yes")  # name
        self.assertEqual(query[2][1], "Z")  # position
        self.assertEqual(query[2][2], date.today())  # birthdate
        self.assertEqual(query[2][3], people[0].id)  # person id
        self.assertEqual(query[2][4], contracts[0].id)  # contract id
        self.assertEqual(query[2][5], 1)  # year count
        self.assertEqual(query[1][0], "Test Yes2")  # name
        self.assertEqual(query[1][1], "Y")  # position
        self.assertEqual(query[1][2], date.today())  # birthdate
        self.assertEqual(query[1][3], people[1].id)  # person id
        self.assertEqual(query[1][4], contracts[1].id)  # contract id
        self.assertEqual(query[1][5], 2)  # year count
        self.assertEqual(query[0][0], "Test No")  # name
        self.assertEqual(query[0][1], "X")  # position
        self.assertEqual(query[0][2], date.today())  # birthdate
        self.assertEqual(query[0][3], people[2].id)  # person id
        self.assertEqual(query[0][4], contracts[2].id)  # contract id
        self.assertEqual(query[0][5], 3)  # year count

    def test_get_query_retained_years(self):
        base_szn = self.app.config["CURRENT_SEASON"]
        contract_id = 3
        seasons = [
            Season(
                name="Test1",
                free_agency_opening=date(year=2000, month=1, day=1),
                id=base_szn,
                max_buriable_hit=1000,
            ),
            Season(
                name="Test2",
                free_agency_opening=date(year=2005, month=1, day=1),
                id=base_szn + 1,
                max_buriable_hit=2000,
            ),
            Season(
                name="Test3",
                free_agency_opening=date(year=2015, month=1, day=1),
                id=base_szn + 2,
                max_buriable_hit=3000,
            ),
            Season(
                name="Test4",
                free_agency_opening=date(year=2010, month=1, day=1),
                id=base_szn + 3,
                max_buriable_hit=4000,
            ),
        ]
        contract_years = [
            ContractYear(
                contract_id=3,
                cap_hit=900,
                signing_bonus=700,
                nhl_salary=600,
                season_id=base_szn + 0,
            ),
            ContractYear(
                contract_id=3,
                cap_hit=800,
                signing_bonus=600,
                nhl_salary=600,
                season_id=base_szn + 1,
            ),
            ContractYear(
                contract_id=3,
                cap_hit=700,
                signing_bonus=500,
                nhl_salary=700,
                season_id=base_szn + 2,
            ),
            ContractYear(
                contract_id=3,
                cap_hit=600,
                signing_bonus=400,
                nhl_salary=800,
                season_id=base_szn + 3,
            ),
        ]
        for year in seasons:
            db.session.add(year)
        for year in contract_years:
            db.session.add(year)
        db.session.commit()

        query = team.query_retained_years(contract_id)
        self.assertEqual(len(query), 4)
        self.assertEqual(query[0][0], 900)  # cap_hit
        self.assertEqual(query[0][1], 600)  # nhl_salary
        self.assertEqual(query[0][2], 700)  # signing_bonus
        self.assertEqual(query[0][3], "Test1")  # name
        self.assertEqual(query[1][0], 800)  # cap_hit
        self.assertEqual(query[1][1], 600)  # nhl_salary
        self.assertEqual(query[1][2], 600)  # signing_bonus
        self.assertEqual(query[1][3], "Test2")  # name
        self.assertEqual(query[2][0], 600)  # cap_hit
        self.assertEqual(query[2][1], 800)  # nhl_salary
        self.assertEqual(query[2][2], 400)  # signing_bonus
        self.assertEqual(query[2][3], "Test4")  # name
        self.assertEqual(query[3][0], 700)  # cap_hit
        self.assertEqual(query[3][1], 700)  # nhl_salary
        self.assertEqual(query[3][2], 500)  # signing_bonus
        self.assertEqual(query[3][3], "Test3")  # name

        query2 = team.get_retained_years(contract_id, 50)
        self.assertEqual(len(query), 4)
        self.assertEqual(query2[0]["cap_hit"], 450)  # cap_hit
        self.assertEqual(query2[0]["nhl_salary"], 300)  # nhl_salary
        self.assertEqual(query2[0]["signing_bonus"], 350)  # signing_bonus
        self.assertEqual(query2[0]["season"], "Test1")  # name
        self.assertEqual(query2[1]["cap_hit"], 400)  # cap_hit
        self.assertEqual(query2[1]["nhl_salary"], 300)  # nhl_salary
        self.assertEqual(query2[1]["signing_bonus"], 300)  # signing_bonus
        self.assertEqual(query2[1]["season"], "Test2")  # name
        self.assertEqual(query2[2]["cap_hit"], 300)  # cap_hit
        self.assertEqual(query2[2]["nhl_salary"], 400)  # nhl_salary
        self.assertEqual(query2[2]["signing_bonus"], 200)  # signing_bonus
        self.assertEqual(query2[2]["season"], "Test4")  # name
        self.assertEqual(query2[3]["cap_hit"], 350)  # cap_hit
        self.assertEqual(query2[3]["nhl_salary"], 350)  # nhl_salary
        self.assertEqual(query2[3]["signing_bonus"], 250)  # signing_bonus
        self.assertEqual(query2[3]["season"], "Test3")  # name

    def test_query_retained(self):
        team_id = 9
        people = [
            Person(name="Test Yes", position="Z", birthdate=date.today()),
            Person(name="Test Yes2", position="Y", birthdate=date.today()),
            Person(name="Test No", position="X", birthdate=date.today()),
        ]
        assets = [
            Asset(active=True, current_team=team_id),
            Asset(active=True, current_team=team_id),
            Asset(active=True, current_team=team_id),
        ]
        for person in people:
            db.session.add(person)
        for asset in assets:
            db.session.add(asset)
        db.session.flush()

        contracts = [
            Contract(id=assets[0].id, person_id=people[0].id, non_roster=True),
            Contract(id=assets[1].id, person_id=people[1].id, non_roster=True),
            Contract(id=assets[2].id, person_id=people[2].id, non_roster=False),
        ]
        ret_assets = [
            Asset(active=False, current_team=team_id),
            Asset(active=False, current_team=team_id),
            Asset(active=False, current_team=team_id),
        ]
        for contract in contracts:
            db.session.add(contract)
        for asset in ret_assets:
            db.session.add(asset)
        db.session.flush()

        retained = [
            RetainedSalary(
                contract_id=assets[0].id,
                retained_by=team_id + 1,
                id=ret_assets[0].id,
                retention=50,
            ),
            RetainedSalary(
                contract_id=assets[1].id,
                retained_by=team_id + 1,
                id=ret_assets[1].id,
                retention=30,
            ),
            RetainedSalary(
                contract_id=assets[2].id,
                retained_by=team_id + 1,
                id=ret_assets[2].id,
                retention=40,
            ),
        ]
        for year in retained:
            db.session.add(year)
        db.session.commit()

        query = team.query_retained(team_id + 1)
        self.assertEqual(len(query), 3)
        self.assertEqual(query[0][0], 50)  # retention
        self.assertEqual(query[0][1], "Test Yes")  # name
        self.assertEqual(query[0][2], "Z")  # position
        self.assertEqual(query[0][3], date.today())  # birthdate
        self.assertEqual(query[0][4], people[0].id)  # person id
        self.assertEqual(query[0][5], contracts[0].id)  # contract id
        self.assertEqual(query[1][0], 40)  # retention
        self.assertEqual(query[1][1], "Test No")  # name
        self.assertEqual(query[1][2], "X")  # position
        self.assertEqual(query[1][3], date.today())  # birthdate
        self.assertEqual(query[1][4], people[2].id)  # person id
        self.assertEqual(query[1][5], contracts[2].id)  # contract id
        self.assertEqual(query[2][0], 30)  # retention
        self.assertEqual(query[2][1], "Test Yes2")  # name
        self.assertEqual(query[2][2], "Y")  # position
        self.assertEqual(query[2][3], date.today())  # birthdate
        self.assertEqual(query[2][4], people[1].id)  # person id
        self.assertEqual(query[2][5], contracts[1].id)  # contract id

    def test_query_penalties(self):
        asset_one = Asset(current_team=0, originating_team=0)
        asset_two = Asset(current_team=0, originating_team=0)
        db.session.add(asset_one)
        db.session.add(asset_two)
        db.session.flush()
        penalty_one = Penalty(
            id=asset_one.id,
            contract_id=asset_one.id,
            type="Cap Recapture",
            years=5,
            total_value=6600000,
        )
        penalty_two = Penalty(
            id=asset_two.id,
            contract_id=asset_one.id,
            type="Termination Fees",
            years=17,
            total_value=10500000,
        )
        contract = Contract(
            id=asset_one.id,
            person_id=9,
        )
        person = Person(
            id=9,
            name="Mike Richards",
            position="Z",
            birthdate=date(year=2000, month=7, day=1),
        )
        db.session.add(contract)
        db.session.add(person)
        db.session.add(penalty_one)
        db.session.add(penalty_two)
        penalty_years = [
            PenaltyYear(penalty_id=asset_one.id, season_id=0, cap_hit=1320000),
            PenaltyYear(penalty_id=asset_one.id, season_id=1, cap_hit=1320000),
            PenaltyYear(penalty_id=asset_one.id, season_id=2, cap_hit=1320000),
            PenaltyYear(penalty_id=asset_one.id, season_id=3, cap_hit=1320000),
            PenaltyYear(penalty_id=asset_one.id, season_id=4, cap_hit=1320000),
            PenaltyYear(penalty_id=asset_two.id, season_id=0, cap_hit=1800000),
            PenaltyYear(penalty_id=asset_two.id, season_id=1, cap_hit=250000),
            PenaltyYear(penalty_id=asset_two.id, season_id=2, cap_hit=250000),
            PenaltyYear(penalty_id=asset_two.id, season_id=3, cap_hit=250000),
            PenaltyYear(penalty_id=asset_two.id, season_id=4, cap_hit=250000),
            PenaltyYear(penalty_id=asset_two.id, season_id=5, cap_hit=700000),
            PenaltyYear(penalty_id=asset_two.id, season_id=6, cap_hit=900000),
            PenaltyYear(penalty_id=asset_two.id, season_id=7, cap_hit=900000),
            PenaltyYear(penalty_id=asset_two.id, season_id=8, cap_hit=700000),
            PenaltyYear(penalty_id=asset_two.id, season_id=9, cap_hit=700000),
            PenaltyYear(penalty_id=asset_two.id, season_id=10, cap_hit=600000),
            PenaltyYear(penalty_id=asset_two.id, season_id=11, cap_hit=600000),
            PenaltyYear(penalty_id=asset_two.id, season_id=12, cap_hit=600000),
            PenaltyYear(penalty_id=asset_two.id, season_id=13, cap_hit=600000),
            PenaltyYear(penalty_id=asset_two.id, season_id=14, cap_hit=500000),
            PenaltyYear(penalty_id=asset_two.id, season_id=15, cap_hit=500000),
            PenaltyYear(penalty_id=asset_two.id, season_id=16, cap_hit=400000),
        ]
        seasons = [
            Season(id=0, name="Test 0"),
            Season(id=1, name="Test 1"),
            Season(id=2, name="Test 2"),
            Season(id=3, name="Test 3"),
            Season(id=4, name="Test 4"),
            Season(id=5, name="Test 5"),
            Season(id=6, name="Test 6"),
            Season(id=7, name="Test 7"),
            Season(id=8, name="Test 8"),
            Season(id=9, name="Test 9"),
            Season(id=10, name="Test 10"),
            Season(id=11, name="Test 11"),
            Season(id=12, name="Test 12"),
            Season(id=13, name="Test 13"),
            Season(id=14, name="Test 14"),
            Season(id=15, name="Test 15"),
            Season(id=16, name="Test 16"),
        ]
        for year in penalty_years:
            db.session.add(year)
        for season in seasons:
            db.session.add(season)
        db.session.commit()

        queried = team.query_penalties(0)
        self.assertEqual(queried[0][0], "Mike Richards")
        self.assertEqual(queried[0][1], "Z")
        self.assertEqual(queried[0][2], date(year=2000, month=7, day=1))
        self.assertEqual(queried[0][3], 9)
        self.assertEqual(queried[0][5], "Cap Recapture")
        self.assertEqual(queried[0][4], penalty_one.id)

        self.assertEqual(queried[1][0], "Mike Richards")
        self.assertEqual(queried[1][1], "Z")
        self.assertEqual(queried[1][2], date(year=2000, month=7, day=1))
        self.assertEqual(queried[1][3], 9)
        self.assertEqual(queried[1][5], "Termination Fees")
        self.assertEqual(queried[1][4], penalty_two.id)

        years_one = team.get_penalty_years(penalty_one.id)
        self.assertEqual(years_one[1]["cap_hit"], 1320000)
        self.assertEqual(years_one[2]["cap_hit"], 1320000)
        self.assertEqual(years_one[3]["cap_hit"], 1320000)
        self.assertEqual(years_one[0]["cap_hit"], 1320000)
        self.assertEqual(years_one[0]["season"], "Test 1")
        self.assertEqual(years_one[1]["season"], "Test 2")
        self.assertEqual(years_one[2]["season"], "Test 3")
        self.assertEqual(years_one[3]["season"], "Test 4")

        years_two = team.get_penalty_years(penalty_two.id)
        self.assertEqual(years_two[0]["season"], "Test 1")
        self.assertEqual(years_two[1]["season"], "Test 2")
        self.assertEqual(years_two[2]["season"], "Test 3")
        self.assertEqual(years_two[3]["season"], "Test 4")
        self.assertEqual(years_two[4]["season"], "Test 5")
        self.assertEqual(years_two[5]["season"], "Test 6")
        self.assertEqual(years_two[6]["season"], "Test 7")
        self.assertEqual(years_two[7]["season"], "Test 8")
        self.assertEqual(years_two[8]["season"], "Test 9")
        self.assertEqual(years_two[9]["season"], "Test 10")
        self.assertEqual(years_two[10]["season"], "Test 11")
        self.assertEqual(years_two[11]["season"], "Test 12")
        self.assertEqual(years_two[12]["season"], "Test 13")
        self.assertEqual(years_two[13]["season"], "Test 14")
        self.assertEqual(years_two[14]["season"], "Test 15")
        self.assertEqual(years_two[15]["season"], "Test 16")
        self.assertEqual(years_two[0]["cap_hit"], 250000)
        self.assertEqual(years_two[1]["cap_hit"], 250000)
        self.assertEqual(years_two[2]["cap_hit"], 250000)
        self.assertEqual(years_two[3]["cap_hit"], 250000)
        self.assertEqual(years_two[4]["cap_hit"], 700000)
        self.assertEqual(years_two[5]["cap_hit"], 900000)
        self.assertEqual(years_two[6]["cap_hit"], 900000)
        self.assertEqual(years_two[7]["cap_hit"], 700000)
        self.assertEqual(years_two[8]["cap_hit"], 700000)
        self.assertEqual(years_two[9]["cap_hit"], 600000)
        self.assertEqual(years_two[10]["cap_hit"], 600000)
        self.assertEqual(years_two[11]["cap_hit"], 600000)
        self.assertEqual(years_two[12]["cap_hit"], 600000)
        self.assertEqual(years_two[13]["cap_hit"], 500000)
        self.assertEqual(years_two[14]["cap_hit"], 500000)
        self.assertEqual(years_two[15]["cap_hit"], 400000)

    def test_la_kings(self):
        self.app.config["CURRENT_SEASON"] = 18
        self.app.config["DRAFT_SEASON"] = 17
        team_record = Team(
            id=26, first_year=1967, name="Los Angeles Kings", abbreviation="LAK"
        )
        team_season = TeamSeason(
            season_id=18,
            team_id=26,
            cap_hit=100000,
            projected_cap_hit=125000,
            contract_count=7,
            retained_contracts=2,
        )
        db.session.add(team_record)
        db.session.add(team_season)

        assets = [
            Asset(
                current_team=team_record.id,
                originating_team=team_record.id,
                active=True,
                type="Retained Salary",
            ),
            Asset(
                current_team=team_record.id,
                originating_team=team_record.id,
                active=False,
                type="Contract",
            ),
            Asset(
                current_team=team_record.id,
                originating_team=team_record.id,
                active=True,
                type="Contract",
            ),
            Asset(
                current_team=team_record.id,
                originating_team=team_record.id,
                active=True,
                type="Contract",
            ),
            Asset(
                current_team=team_record.id,
                originating_team=team_record.id,
                active=False,
                type="Contract",
            ),
            Asset(
                current_team=team_record.id,
                originating_team=team_record.id,
                active=True,
                type="Contract",
            ),
            Asset(
                current_team=team_record.id,
                originating_team=team_record.id,
                active=True,
                type="Contract",
            ),
            Asset(
                current_team=team_record.id,
                originating_team=team_record.id,
                active=True,
                type="Contract",
            ),
            Asset(
                current_team=team_record.id,
                originating_team=team_record.id,
                active=True,
                type="Contract",
            ),
            Asset(
                current_team=team_record.id,
                originating_team=team_record.id,
                active=True,
                type="Contract",
            ),
            Asset(
                current_team=team_record.id,
                originating_team=team_record.id,
                active=True,
                type="Penalty",
            ),
            Asset(
                current_team=team_record.id,
                originating_team=team_record.id,
                active=True,
                type="Signing Rights",
            ),
            Asset(
                current_team=team_record.id,
                originating_team=team_record.id,
                active=True,
                type="Signing Rights",
            ),
            Asset(
                current_team=team_record.id,
                originating_team=team_record.id,
                active=False,
                type="Contract",
            ),
            Asset(
                current_team=team_record.id,
                originating_team=team_record.id,
                active=False,
                type="Contract",
            ),
        ]
        for asset in assets:
            db.session.add(asset)
        db.session.flush()

        seasons = [
            Season(
                id=10,
                name="Test 10",
                free_agency_opening=date(year=2020, month=1, day=1),
                draft_date=date(year=2020, month=1, day=1),
            ),
            Season(
                id=11,
                name="Test 11",
                free_agency_opening=date(year=2020, month=2, day=1),
                draft_date=date(year=2020, month=2, day=1),
            ),
            Season(
                id=12,
                name="Test 12",
                free_agency_opening=date(year=2020, month=3, day=1),
                draft_date=date(year=2020, month=3, day=1),
            ),
            Season(
                id=13,
                name="Test 13",
                free_agency_opening=date(year=2020, month=4, day=1),
                draft_date=date(year=2020, month=4, day=1),
            ),
            Season(
                id=14,
                name="Test 14",
                free_agency_opening=date(year=2020, month=5, day=1),
                draft_date=date(year=2020, month=5, day=1),
            ),
            Season(
                id=15,
                name="Test 15",
                free_agency_opening=date(year=2020, month=6, day=1),
                draft_date=date(year=2020, month=6, day=1),
            ),
            Season(
                id=16,
                name="Test 16",
                free_agency_opening=date(year=2020, month=7, day=1),
                draft_date=date(year=2020, month=7, day=1),
            ),
            Season(
                id=17,
                name="Test 17",
                free_agency_opening=date(year=2020, month=8, day=1),
                draft_date=date(year=2020, month=8, day=1),
            ),
            Season(
                id=18,
                name="Test 18",
                free_agency_opening=date(year=2020, month=9, day=1),
                draft_date=date(year=2020, month=9, day=1),
            ),
            Season(
                id=19,
                name="Test 19",
                free_agency_opening=date(year=2020, month=10, day=1),
                draft_date=date(year=2020, month=10, day=1),
            ),
            Season(
                id=20,
                name="Test 20",
                free_agency_opening=date(year=2020, month=11, day=1),
                draft_date=date(year=2020, month=11, day=1),
            ),
            Season(
                id=21,
                name="Test 21",
                free_agency_opening=date(year=2020, month=12, day=1),
                draft_date=date(year=2020, month=12, day=1),
            ),
            Season(
                id=22,
                name="Test 22",
                free_agency_opening=date(year=2021, month=1, day=1),
                draft_date=date(year=2021, month=1, day=1),
            ),
            Season(
                id=23,
                name="Test 23",
                free_agency_opening=date(year=2021, month=2, day=1),
                draft_date=date(year=2021, month=2, day=1),
            ),
            Season(
                id=24,
                name="Test 24",
                free_agency_opening=date(year=2021, month=3, day=1),
                draft_date=date(year=2021, month=3, day=1),
            ),
            Season(
                id=25,
                name="Test 25",
                free_agency_opening=date(year=2021, month=4, day=1),
                draft_date=date(year=2021, month=4, day=1),
            ),
            Season(
                id=26,
                name="Test 26",
                free_agency_opening=date(year=2021, month=5, day=1),
                draft_date=date(year=2021, month=5, day=1),
            ),
            Season(
                id=27,
                name="Test 27",
                free_agency_opening=date(year=2021, month=6, day=1),
                draft_date=date(year=2021, month=6, day=1),
            ),
        ]
        for season in seasons:
            db.session.add(season)
        people = [
            Person(
                id=3719,
                name="Victor Arvidsson",
                position="LW",
                birthdate=date(year=1993, month=4, day=8),
            ),
            Person(
                id=1432,
                name="Blake Lizotte",
                position="C",
                birthdate=date(year=1997, month=12, day=13),
            ),
            Person(
                id=1315,
                name="Tobias Bjornfot",
                position="D",
                birthdate=date(year=2001, month=4, day=6),
            ),
            Person(
                id=6404,
                name="Drew Doughty",
                position="D",
                birthdate=date(year=1989, month=12, day=8),
            ),
            Person(
                id=4239,
                name="Cal Petersen",
                position="G",
                birthdate=date(year=1994, month=10, day=19),
            ),
            Person(
                id=8528,
                name="Mike Richards",
                position="C",
                birthdate=date(year=1985, month=2, day=11),
            ),
            Person(
                id=8543,
                name="Dion Phaneuf",
                position="D",
                birthdate=date(year=1985, month=4, day=10),
            ),
            Person(
                id=3388,
                name="Austin Wagner",
                position="LW",
                birthdate=date(year=1997, month=6, day=24),
            ),
            Person(
                id=571,
                name="Francesco Pinelli",
                position="C",
                birthdate=date(year=2003, month=4, day=11),
            ),
            Person(
                id=3800,
                name="Adrian Kempe",
                position="LW",
                birthdate=date(year=1996, month=9, day=13),
                current_status="RFA",
            ),
            Person(
                id=2122,
                name="Sean Durzi",
                position="D",
                birthdate=date(year=1998, month=10, day=21),
                current_status="RFA",
            ),
        ]
        for person in people:
            db.session.add(person)

        retention = RetainedSalary(
            id=assets[0].id,
            contract_id=assets[1].id,
            retained_by=9,
            retained_from=date(year=2018, month=2, day=14),
            retention=25,
        )
        db.session.add(retention)

        rights = [
            SigningRights(
                id=assets[11].id,
                person_id=3800,
            ),
            SigningRights(
                id=assets[12].id,
                person_id=2122,
            ),
        ]
        for record in rights:
            db.session.add(record)

        contracts = [
            Contract(
                id=assets[1].id,
                person_id=people[6].id,
                signing_date=date(year=2013, month=12, day=31),
                buyout_date=date(year=2019, month=9, day=15),
                expiration_status="UFA",
                total_value=49000000,
                years=7,
                non_roster=False,
            ),
            Contract(
                id=assets[2].id,
                person_id=people[7].id,
                signing_date=date(year=2020, month=9, day=1),
                expiration_status="RFA",
                non_roster=True,
                total_value=3400000,
                years=3,
            ),
            Contract(
                id=assets[3].id,
                person_id=people[8].id,
                signing_date=date(year=2022, month=4, day=11),
                expiration_status="UFA",
                total_value=2775000,
                non_roster=True,
                years=3,
            ),
            Contract(
                id=assets[4].id,
                person_id=people[5].id,
                signing_date=date(year=2007, month=12, day=13),
                termination_date=date(year=2015, month=9, day=16),
                expiration_status="UFA",
                total_value=69000000,
                years=12,
                non_roster=False,
            ),
            Contract(
                id=assets[5].id,
                person_id=people[4].id,
                signing_date=date(year=2021, month=9, day=22),
                expiration_status="UFA",
                total_value=15000000,
                years=3,
                non_roster=False,
            ),
            Contract(
                id=assets[6].id,
                person_id=people[3].id,
                signing_date=date(year=2018, month=7, day=1),
                expiration_status="UFA",
                total_value=88000000,
                years=8,
                non_roster=False,
            ),
            Contract(
                id=assets[7].id,
                person_id=people[2].id,
                signing_date=date(year=2019, month=7, day=14),
                expiration_status="RFA",
                total_value=3250000,
                years=3,
                non_roster=False,
            ),
            Contract(
                id=assets[8].id,
                person_id=people[1].id,
                signing_date=date(year=2022, month=3, day=21),
                expiration_status="RFA",
                total_value=3350000,
                years=2,
                non_roster=False,
            ),
            Contract(
                id=assets[9].id,
                person_id=people[0].id,
                signing_date=date(year=2017, month=7, day=22),
                expiration_status="UFA",
                total_value=29750000,
                years=7,
                non_roster=False,
            ),
            Contract(
                id=assets[13].id,
                person_id=people[9].id,
                signing_date=date(year=2017, month=7, day=22),
                expiration_status="RFA",
                total_value=29750000,
                years=7,
                non_roster=False,
            ),
            Contract(
                id=assets[14].id,
                person_id=people[10].id,
                signing_date=date(year=2017, month=7, day=22),
                expiration_status="RFA",
                total_value=29750000,
                years=7,
                non_roster=False,
            ),
        ]
        penalty = Penalty(
            id=assets[10].id,
            contract_id=assets[4].id,
            type="Termination Fees",
            years=17,
            total_value=10500000,
        )
        for contract in contracts:
            db.session.add(contract)
        db.session.add(penalty)

        contract_years = [
            ContractYear(
                contract_id=assets[9].id,
                season_id=13,
                nhl_salary=4250000,
                minors_salary=4250000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=4250000,
                aav=4250000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[9].id,
                season_id=14,
                nhl_salary=4250000,
                minors_salary=4250000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=4250000,
                aav=4250000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[9].id,
                season_id=15,
                nhl_salary=4250000,
                minors_salary=4250000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=4250000,
                aav=4250000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[9].id,
                season_id=16,
                nhl_salary=4250000,
                minors_salary=4250000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=4250000,
                aav=4250000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[9].id,
                season_id=17,
                nhl_salary=4250000,
                minors_salary=4250000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=4250000,
                aav=4250000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[9].id,
                season_id=18,
                nhl_salary=4250000,
                minors_salary=4250000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=4250000,
                aav=4250000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[9].id,
                season_id=19,
                nhl_salary=4250000,
                minors_salary=4250000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=4250000,
                aav=4250000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[8].id,
                season_id=18,
                nhl_salary=1675000,
                minors_salary=1675000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=1675000,
                aav=1675000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[8].id,
                season_id=19,
                nhl_salary=1675000,
                minors_salary=1675000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=1675000,
                aav=1675000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[7].id,
                season_id=16,
                nhl_salary=832500,
                minors_salary=70000,
                performance_bonuses=0,
                signing_bonus=92500,
                cap_hit=894167,
                aav=1052500,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[7].id,
                season_id=17,
                nhl_salary=832500,
                minors_salary=70000,
                performance_bonuses=0,
                signing_bonus=92500,
                cap_hit=894167,
                aav=1052500,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[7].id,
                season_id=18,
                nhl_salary=832500,
                minors_salary=70000,
                performance_bonuses=0,
                signing_bonus=92500,
                cap_hit=894167,
                aav=1052500,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[6].id,
                season_id=17,
                nhl_salary=7000000,
                minors_salary=7000000,
                performance_bonuses=0,
                signing_bonus=4000000,
                cap_hit=11000000,
                aav=11000000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=1,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[6].id,
                season_id=18,
                nhl_salary=7000000,
                minors_salary=7000000,
                performance_bonuses=0,
                signing_bonus=4000000,
                cap_hit=11000000,
                aav=11000000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=1,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[6].id,
                season_id=19,
                nhl_salary=11000000,
                minors_salary=11000000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=11000000,
                aav=11000000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=1,
                clause_limits="Seven team trade list",
            ),
            ContractYear(
                contract_id=assets[6].id,
                season_id=20,
                nhl_salary=11000000,
                minors_salary=11000000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=11000000,
                aav=11000000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=1,
                clause_limits="Seven team trade list",
            ),
            ContractYear(
                contract_id=assets[6].id,
                season_id=21,
                nhl_salary=11000000,
                minors_salary=11000000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=11000000,
                aav=11000000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=1,
                clause_limits="Seven team trade list",
            ),
            ContractYear(
                contract_id=assets[6].id,
                season_id=22,
                nhl_salary=11000000,
                minors_salary=11000000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=11000000,
                aav=11000000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=1,
                clause_limits="Seven team trade list",
            ),
            ContractYear(
                contract_id=assets[5].id,
                season_id=18,
                nhl_salary=1000000,
                minors_salary=1000000,
                performance_bonuses=0,
                signing_bonus=3000000,
                cap_hit=5000000,
                aav=5000000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=1,
                clause_limits="Ten team no trade list",
            ),
            ContractYear(
                contract_id=assets[5].id,
                season_id=19,
                nhl_salary=1000000,
                minors_salary=1000000,
                performance_bonuses=0,
                signing_bonus=4000000,
                cap_hit=5000000,
                aav=5000000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=1,
                clause_limits="Ten team no trade list",
            ),
            ContractYear(
                contract_id=assets[5].id,
                season_id=20,
                nhl_salary=6000000,
                minors_salary=6000000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=0,
                aav=5000000,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=1,
                clause_limits="Ten team no trade list",
            ),
            ContractYear(
                contract_id=assets[2].id,
                season_id=16,
                nhl_salary=900000,
                minors_salary=900000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=0,
                aav=1133333,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[2].id,
                season_id=17,
                nhl_salary=1100000,
                minors_salary=1100000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=0,
                aav=1133333,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[2].id,
                season_id=18,
                nhl_salary=1400000,
                minors_salary=1400000,
                performance_bonuses=0,
                signing_bonus=0,
                cap_hit=0,
                aav=1133333,
                bought_out=0,
                terminated=0,
                two_way=0,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[3].id,
                season_id=18,
                nhl_salary=750000,
                minors_salary=80000,
                performance_bonuses=82500,
                signing_bonus=92500,
                cap_hit=867500,
                aav=925000,
                bought_out=0,
                terminated=0,
                two_way=True,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[3].id,
                season_id=19,
                nhl_salary=775000,
                minors_salary=80000,
                performance_bonuses=57500,
                signing_bonus=92500,
                cap_hit=867500,
                aav=925000,
                bought_out=0,
                terminated=0,
                two_way=True,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[3].id,
                season_id=20,
                nhl_salary=800000,
                minors_salary=80000,
                performance_bonuses=32500,
                signing_bonus=92500,
                cap_hit=867500,
                aav=925000,
                bought_out=0,
                terminated=0,
                two_way=True,
                nmc=0,
                ntc=0,
                clause_limits="",
            ),
            ContractYear(
                contract_id=assets[13].id,
                season_id=17,
            ),
            ContractYear(
                contract_id=assets[14].id,
                season_id=17,
            ),
        ]
        for year in contract_years:
            db.session.add(year)

        buyout_years = [
            BuyoutYear(
                contract_id=assets[1].id,
                season_id=15,
                nhl_salary=1402500,
                signing_bonus=1000000,
                cap_hit=2187500,
                compliance=False,
            ),
            BuyoutYear(
                contract_id=assets[1].id,
                season_id=16,
                nhl_salary=1402500,
                signing_bonus=3000000,
                cap_hit=4062500,
                compliance=False,
            ),
            BuyoutYear(
                contract_id=assets[1].id,
                season_id=17,
                nhl_salary=1402500,
                cap_hit=1402500,
                compliance=False,
            ),
            BuyoutYear(
                contract_id=assets[1].id,
                season_id=18,
                nhl_salary=1402500,
                cap_hit=1402500,
                compliance=False,
            ),
        ]
        for year in buyout_years:
            db.session.add(year)

        penalty_years = [
            PenaltyYear(
                penalty_id=assets[10].id,
                season_id=18,
                cap_hit=900000,
            ),
            PenaltyYear(
                penalty_id=assets[10].id,
                season_id=19,
                cap_hit=700000,
            ),
            PenaltyYear(
                penalty_id=assets[10].id,
                season_id=20,
                cap_hit=700000,
            ),
            PenaltyYear(
                penalty_id=assets[10].id,
                season_id=21,
                cap_hit=600000,
            ),
            PenaltyYear(
                penalty_id=assets[10].id,
                season_id=22,
                cap_hit=600000,
            ),
            PenaltyYear(
                penalty_id=assets[10].id,
                season_id=23,
                cap_hit=600000,
            ),
            PenaltyYear(
                penalty_id=assets[10].id,
                season_id=24,
                cap_hit=600000,
            ),
            PenaltyYear(
                penalty_id=assets[10].id,
                season_id=25,
                cap_hit=500000,
            ),
            PenaltyYear(
                penalty_id=assets[10].id,
                season_id=26,
                cap_hit=500000,
            ),
            PenaltyYear(
                penalty_id=assets[10].id,
                season_id=27,
                cap_hit=400000,
            ),
        ]
        for year in penalty_years:
            db.session.add(year)
        # Current season: 18

        # Create draft picks
        draft_assets = [
            Asset(
                originating_team=team_record.id,
                current_team=team_record.id,
                type="Draft Pick",
            ),
            Asset(
                originating_team=team_record.id,
                current_team=team_record.id,
                type="Draft Pick",
            ),
            Asset(
                originating_team=team_record.id,
                current_team=team_record.id,
                type="Draft Pick",
            ),
            Asset(
                originating_team=team_record.id,
                current_team=team_record.id,
                type="Draft Pick",
            ),
            Asset(
                originating_team=team_record.id,
                current_team=team_record.id,
                type="Draft Pick",
            ),
            Asset(
                originating_team=team_record.id,
                current_team=team_record.id,
                type="Draft Pick",
            ),
            Asset(
                originating_team=team_record.id,
                current_team=team_record.id,
                type="Draft Pick",
            ),
        ]
        for asset in draft_assets:
            db.session.add(asset)
        db.session.flush()

        draft_picks = [
            DraftPick(
                id=draft_assets[0].id,
                season_id=17,
                round=1,
            ),
            DraftPick(
                id=draft_assets[1].id,
                season_id=17,
                round=2,
            ),
            DraftPick(
                id=draft_assets[2].id,
                season_id=18,
                round=1,
            ),
            DraftPick(
                id=draft_assets[3].id,
                season_id=18,
                round=2,
            ),
            DraftPick(
                id=draft_assets[4].id,
                season_id=19,
                round=1,
            ),
            DraftPick(
                id=draft_assets[5].id,
                season_id=19,
                round=2,
            ),
        ]
        for pick in draft_picks:
            db.session.add(pick)
        db.session.commit()

        data = team.get_data(team_record.id)
        self.assertEqual(data["abbreviation"], "LAK")
        self.assertEqual(data["name"], "Los Angeles Kings")
        self.assertEqual(data["first_year"], 1967)
        self.assertEqual(data["id"], 26)
        self.assertEqual(data["season"]["cap_hit"], 125000)
        self.assertEqual(data["season"]["contracts"], 7)
        self.assertEqual(data["season"]["retained_contracts"], 2)

        self.assertEqual(data["draft_picks"][0]["season"], "Test 17")
        self.assertEqual(data["draft_picks"][1]["season"], "Test 18")
        self.assertEqual(data["draft_picks"][2]["season"], "Test 19")
        self.assertIsNone(data["draft_picks"][0]["picks"][0]["position"])
        self.assertEqual(data["draft_picks"][0]["picks"][0]["round"], 1)
        self.assertEqual(data["draft_picks"][0]["picks"][0]["team"], "LAK")
        self.assertEqual(data["draft_picks"][0]["picks"][0]["traded_away"], False)
        self.assertIsNone(data["draft_picks"][0]["picks"][1]["position"])
        self.assertEqual(data["draft_picks"][0]["picks"][1]["round"], 2)
        self.assertEqual(data["draft_picks"][0]["picks"][1]["team"], "LAK")
        self.assertEqual(data["draft_picks"][0]["picks"][1]["traded_away"], False)
        self.assertIsNone(data["draft_picks"][1]["picks"][0]["position"])
        self.assertEqual(data["draft_picks"][1]["picks"][0]["round"], 1)
        self.assertEqual(data["draft_picks"][1]["picks"][0]["team"], "LAK")
        self.assertEqual(data["draft_picks"][1]["picks"][0]["traded_away"], False)
        self.assertIsNone(data["draft_picks"][1]["picks"][1]["position"])
        self.assertEqual(data["draft_picks"][1]["picks"][1]["round"], 2)
        self.assertEqual(data["draft_picks"][1]["picks"][1]["team"], "LAK")
        self.assertEqual(data["draft_picks"][1]["picks"][1]["traded_away"], False)
        self.assertIsNone(data["draft_picks"][2]["picks"][0]["position"])
        self.assertEqual(data["draft_picks"][2]["picks"][0]["round"], 1)
        self.assertEqual(data["draft_picks"][2]["picks"][0]["team"], "LAK")
        self.assertEqual(data["draft_picks"][2]["picks"][0]["traded_away"], False)
        self.assertIsNone(data["draft_picks"][2]["picks"][1]["position"])
        self.assertEqual(data["draft_picks"][2]["picks"][1]["round"], 2)
        self.assertEqual(data["draft_picks"][2]["picks"][1]["team"], "LAK")
        self.assertEqual(data["draft_picks"][2]["picks"][1]["traded_away"], False)

        self.assertEqual(data["roster"][0]["name"], "Victor Arvidsson")
        self.assertEqual(data["roster"][0]["birthdate"], "1993-04-08")
        self.assertEqual(data["roster"][0]["expires_as"], "UFA")
        self.assertEqual(data["roster"][0]["id"], 3719)
        self.assertEqual(data["roster"][0]["ir"], False)
        self.assertEqual(data["roster"][0]["nmc"], False)
        self.assertEqual(data["roster"][0]["ntc"], False)
        self.assertEqual(data["roster"][0]["clause_limits"], "")
        self.assertEqual(data["roster"][0]["position"], "LW")
        self.assertEqual(data["roster"][0]["retention"], 0)
        self.assertEqual(len(data["roster"][0]["years"]), 2)
        self.assertEqual(data["roster"][0]["years"][0]["aav"], 4250000)
        self.assertEqual(data["roster"][0]["years"][0]["cap_hit"], 4250000)
        self.assertEqual(data["roster"][0]["years"][0]["performance_bonuses"], 0)
        self.assertEqual(data["roster"][0]["years"][0]["signing_bonus"], 0)
        self.assertEqual(data["roster"][0]["years"][0]["season"], "Test 18")
        self.assertEqual(data["roster"][0]["years"][0]["nhl_salary"], 4250000)
        self.assertEqual(data["roster"][0]["years"][1]["aav"], 4250000)
        self.assertEqual(data["roster"][0]["years"][1]["cap_hit"], 4250000)
        self.assertEqual(data["roster"][0]["years"][1]["performance_bonuses"], 0)
        self.assertEqual(data["roster"][0]["years"][1]["signing_bonus"], 0)
        self.assertEqual(data["roster"][0]["years"][1]["season"], "Test 19")
        self.assertEqual(data["roster"][0]["years"][1]["nhl_salary"], 4250000)
        self.assertEqual(data["roster"][1]["name"], "Blake Lizotte")
        self.assertEqual(data["roster"][1]["birthdate"], "1997-12-13")
        self.assertEqual(data["roster"][1]["expires_as"], "RFA")
        self.assertEqual(data["roster"][1]["id"], 1432)
        self.assertEqual(data["roster"][1]["ir"], False)
        self.assertEqual(data["roster"][1]["nmc"], False)
        self.assertEqual(data["roster"][1]["ntc"], False)
        self.assertEqual(data["roster"][1]["clause_limits"], "")
        self.assertEqual(data["roster"][1]["position"], "C")
        self.assertEqual(data["roster"][1]["retention"], 0)
        self.assertEqual(len(data["roster"][1]["years"]), 2)
        self.assertEqual(data["roster"][1]["years"][0]["aav"], 1675000)
        self.assertEqual(data["roster"][1]["years"][0]["cap_hit"], 1675000)
        self.assertEqual(data["roster"][1]["years"][0]["performance_bonuses"], 0)
        self.assertEqual(data["roster"][1]["years"][0]["signing_bonus"], 0)
        self.assertEqual(data["roster"][1]["years"][0]["season"], "Test 18")
        self.assertEqual(data["roster"][1]["years"][0]["nhl_salary"], 1675000)
        self.assertEqual(data["roster"][1]["years"][1]["aav"], 1675000)
        self.assertEqual(data["roster"][1]["years"][1]["cap_hit"], 1675000)
        self.assertEqual(data["roster"][1]["years"][1]["performance_bonuses"], 0)
        self.assertEqual(data["roster"][1]["years"][1]["signing_bonus"], 0)
        self.assertEqual(data["roster"][1]["years"][1]["season"], "Test 19")
        self.assertEqual(data["roster"][1]["years"][1]["nhl_salary"], 1675000)

        self.assertEqual(data["roster"][2]["name"], "Drew Doughty")
        self.assertEqual(data["roster"][2]["birthdate"], "1989-12-08")
        self.assertEqual(data["roster"][2]["expires_as"], "UFA")
        self.assertEqual(data["roster"][2]["id"], 6404)
        self.assertEqual(data["roster"][2]["ir"], False)
        self.assertEqual(data["roster"][2]["nmc"], True)
        self.assertEqual(data["roster"][2]["ntc"], False)
        self.assertEqual(data["roster"][2]["position"], "D")
        self.assertEqual(data["roster"][2]["retention"], 0)
        self.assertEqual(len(data["roster"][2]["years"]), 5)
        for year in data["roster"][2]["years"]:
            self.assertEqual(year["aav"], 11000000)
            self.assertEqual(year["cap_hit"], 11000000)
            self.assertEqual(year["performance_bonuses"], 0)
        self.assertEqual(data["roster"][2]["years"][0]["signing_bonus"], 4000000)
        self.assertEqual(data["roster"][2]["years"][1]["signing_bonus"], 0)
        self.assertEqual(data["roster"][2]["years"][2]["signing_bonus"], 0)
        self.assertEqual(data["roster"][2]["years"][3]["signing_bonus"], 0)
        self.assertEqual(data["roster"][2]["years"][4]["signing_bonus"], 0)
        self.assertEqual(data["roster"][2]["years"][0]["season"], "Test 18")
        self.assertEqual(data["roster"][2]["years"][1]["season"], "Test 19")
        self.assertEqual(data["roster"][2]["years"][2]["season"], "Test 20")
        self.assertEqual(data["roster"][2]["years"][3]["season"], "Test 21")
        self.assertEqual(data["roster"][2]["years"][4]["season"], "Test 22")

        self.assertEqual(data["roster"][3]["name"], "Tobias Bjornfot")
        self.assertEqual(data["roster"][3]["birthdate"], "2001-04-06")
        self.assertEqual(data["roster"][3]["expires_as"], "RFA")
        self.assertEqual(data["roster"][3]["id"], 1315)
        self.assertEqual(data["roster"][3]["ir"], False)
        self.assertEqual(data["roster"][3]["nmc"], False)
        self.assertEqual(data["roster"][3]["ntc"], False)
        self.assertEqual(data["roster"][3]["position"], "D")
        self.assertEqual(data["roster"][3]["retention"], 0)
        self.assertEqual(len(data["roster"][3]["years"]), 1)
        self.assertEqual(data["roster"][3]["years"][0]["aav"], 1052500)
        self.assertEqual(data["roster"][3]["years"][0]["cap_hit"], 894167)
        self.assertEqual(data["roster"][3]["years"][0]["performance_bonuses"], 0)
        self.assertEqual(data["roster"][3]["years"][0]["signing_bonus"], 92500)
        self.assertEqual(data["roster"][3]["years"][0]["season"], "Test 18")
        self.assertEqual(data["roster"][3]["years"][0]["nhl_salary"], 832500)

        self.assertEqual(data["roster"][4]["id"], 4239)
        self.assertEqual(data["roster"][4]["ir"], False)
        self.assertEqual(data["roster"][4]["nmc"], False)
        self.assertEqual(data["roster"][4]["ntc"], True)
        self.assertEqual(data["roster"][4]["clause_limits"], "Ten team no trade list")
        self.assertEqual(data["roster"][4]["position"], "G")
        self.assertEqual(data["roster"][4]["retention"], 0)
        self.assertEqual(len(data["roster"][4]["years"]), 3)
        self.assertEqual(data["roster"][4]["years"][0]["aav"], 5000000)
        self.assertEqual(data["roster"][4]["years"][0]["cap_hit"], 5000000)
        self.assertEqual(data["roster"][4]["years"][0]["performance_bonuses"], 0)
        self.assertEqual(data["roster"][4]["years"][0]["signing_bonus"], 3000000)
        self.assertEqual(data["roster"][4]["years"][0]["season"], "Test 18")
        self.assertEqual(data["roster"][4]["years"][0]["nhl_salary"], 1000000)
        self.assertEqual(data["roster"][4]["years"][1]["aav"], 5000000)
        self.assertEqual(data["roster"][4]["years"][1]["cap_hit"], 5000000)
        self.assertEqual(data["roster"][4]["years"][1]["performance_bonuses"], 0)
        self.assertEqual(data["roster"][4]["years"][1]["signing_bonus"], 4000000)
        self.assertEqual(data["roster"][4]["years"][1]["season"], "Test 19")
        self.assertEqual(data["roster"][4]["years"][1]["nhl_salary"], 1000000)
        self.assertEqual(data["roster"][4]["years"][2]["aav"], 5000000)
        self.assertEqual(
            data["roster"][4]["years"][2]["cap_hit"], 0
        )  # was a data entry typo. i'm keeping it
        self.assertEqual(data["roster"][4]["years"][2]["performance_bonuses"], 0)
        self.assertEqual(data["roster"][4]["years"][2]["signing_bonus"], 0)
        self.assertEqual(data["roster"][4]["years"][2]["season"], "Test 20")
        self.assertEqual(data["roster"][4]["years"][2]["nhl_salary"], 6000000)

        self.assertEqual(data["roster"][5]["name"], "Adrian Kempe")
        self.assertEqual(data["roster"][5]["birthdate"], "1996-09-13")
        self.assertEqual(data["roster"][5]["expires_as"], "RFA")
        self.assertEqual(data["roster"][5]["id"], 3800)
        self.assertEqual(data["roster"][5]["ir"], False)
        self.assertEqual(data["roster"][5]["nmc"], False)
        self.assertEqual(data["roster"][5]["ntc"], False)
        self.assertEqual(data["roster"][5]["position"], "LW")
        self.assertEqual(len(data["roster"][5]["years"]), 0)
        self.assertEqual(data["roster"][6]["name"], "Sean Durzi")
        self.assertEqual(data["roster"][6]["birthdate"], "1998-10-21")
        self.assertEqual(data["roster"][6]["expires_as"], "RFA")
        self.assertEqual(data["roster"][6]["id"], 2122)
        self.assertEqual(data["roster"][6]["ir"], False)
        self.assertEqual(data["roster"][6]["nmc"], False)
        self.assertEqual(data["roster"][6]["ntc"], False)
        self.assertEqual(data["roster"][6]["position"], "D")
        self.assertEqual(len(data["roster"][6]["years"]), 0)

        self.assertEqual(data["roster"][4]["name"], "Cal Petersen")
        self.assertEqual(data["roster"][4]["birthdate"], "1994-10-19")
        self.assertEqual(data["roster"][4]["expires_as"], "UFA")
        buyout = data["non_roster"]["buyouts"][0]
        self.assertEqual(len(data["non_roster"]["buyouts"]), 1)
        self.assertEqual(buyout["name"], "Dion Phaneuf")
        self.assertEqual(buyout["position"], "D")
        self.assertEqual(buyout["id"], 8543)
        self.assertEqual(buyout["birthdate"], "1985-04-10")
        self.assertEqual(buyout["retention"], 75)
        self.assertEqual(len(buyout["years"]), 1)
        self.assertEqual(buyout["years"][0]["cap_hit"], 1051875)
        self.assertEqual(buyout["years"][0]["nhl_salary"], 1051875)
        self.assertEqual(buyout["years"][0]["signing_bonus"], 0)
        self.assertEqual(buyout["years"][0]["season"], "Test 18")

        penalty = data["non_roster"]["penalties"][0]
        self.assertEqual(len(data["non_roster"]["penalties"]), 1)
        self.assertEqual(penalty["name"], "Mike Richards")
        self.assertEqual(penalty["position"], "C")
        self.assertEqual(penalty["id"], 8528)
        self.assertEqual(penalty["birthdate"], "1985-02-11")
        self.assertEqual(penalty["type"], "Termination Fees")
        self.assertEqual(len(penalty["years"]), 10)
        self.assertEqual(penalty["years"][0]["cap_hit"], 900000)
        self.assertEqual(penalty["years"][0]["season"], "Test 18")
        self.assertEqual(penalty["years"][1]["cap_hit"], 700000)
        self.assertEqual(penalty["years"][1]["season"], "Test 19")
        self.assertEqual(penalty["years"][2]["cap_hit"], 700000)
        self.assertEqual(penalty["years"][2]["season"], "Test 20")
        self.assertEqual(penalty["years"][3]["cap_hit"], 600000)
        self.assertEqual(penalty["years"][3]["season"], "Test 21")
        self.assertEqual(penalty["years"][4]["cap_hit"], 600000)
        self.assertEqual(penalty["years"][4]["season"], "Test 22")
        self.assertEqual(penalty["years"][5]["cap_hit"], 600000)
        self.assertEqual(penalty["years"][5]["season"], "Test 23")
        self.assertEqual(penalty["years"][6]["cap_hit"], 600000)
        self.assertEqual(penalty["years"][6]["season"], "Test 24")
        self.assertEqual(penalty["years"][7]["cap_hit"], 500000)
        self.assertEqual(penalty["years"][7]["season"], "Test 25")
        self.assertEqual(penalty["years"][8]["cap_hit"], 500000)
        self.assertEqual(penalty["years"][8]["season"], "Test 26")
        self.assertEqual(penalty["years"][9]["cap_hit"], 400000)
        self.assertEqual(penalty["years"][9]["season"], "Test 27")

    def test_retained_and_extended(self):
        assets = [
            Asset(
                current_team=5,
                originating_team=3,
                active=True,
            ),
            Asset(
                current_team=5,
                originating_team=5,
                active=True,
            ),
            Asset(
                current_team=3,
                originating_team=3,
                active=True,
            ),
        ]
        for asset in assets:
            db.session.add(asset)
        db.session.flush()
        person = Person(
            id=1,
            name="Test Person",
            position="G",
            birthdate=date.today(),
        )
        contract = Contract(
            id=assets[0].id,
            non_roster=False,
            expiration_status="RFA",
            signing_date=date(year=2022, month=6, day=1),
            person_id=1,
        )
        extension = Contract(
            id=assets[1].id,
            non_roster=False,
            expiration_status="UFA",
            signing_date=date(year=2022, month=7, day=1),
            person_id=1,
        )
        retention = RetainedSalary(
            id=assets[2].id,
            contract_id=assets[0].id,
            retained_by=3,
            retention=50,
        )
        db.session.add(person)
        db.session.add(contract)
        db.session.add(extension)
        db.session.add(retention)
        seasons = [
            Season(id=self.app.config["CURRENT_SEASON"], name="Test 0"),
            Season(id=self.app.config["CURRENT_SEASON"] + 1, name="Test 1"),
            Season(id=self.app.config["CURRENT_SEASON"] + 2, name="Test 2"),
            Season(id=self.app.config["CURRENT_SEASON"] + 3, name="Test 3"),
        ]
        contract_years = [
            ContractYear(
                contract_id=contract.id,
                season_id=self.app.config["CURRENT_SEASON"],
                nhl_salary=1000000,
                minors_salary=1000000,
                aav=1000000,
                cap_hit=1000000,
                nmc=False,
                ntc=False,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
                nhl_salary=1000000,
                minors_salary=1000000,
                aav=1000000,
                cap_hit=1000000,
                nmc=False,
                ntc=False,
            ),
            ContractYear(
                contract_id=extension.id,
                season_id=self.app.config["CURRENT_SEASON"] + 2,
                nhl_salary=1500000,
                minors_salary=1500000,
                aav=1500000,
                cap_hit=1500000,
                nmc=False,
                ntc=False,
            ),
            ContractYear(
                contract_id=extension.id,
                season_id=self.app.config["CURRENT_SEASON"] + 3,
                nhl_salary=1500000,
                minors_salary=1500000,
                aav=1500000,
                cap_hit=1500000,
                nmc=False,
                ntc=False,
            ),
        ]
        for season in seasons:
            db.session.add(season)
        for year in contract_years:
            db.session.add(year)
        db.session.commit()

        data = team.get_roster(5)
        years = data[0]["years"]
        self.assertEqual(len(years), 4)
        self.assertEqual(years[0]["cap_hit"], 500000)
        self.assertEqual(years[0]["aav"], 500000)
        self.assertEqual(years[0]["nhl_salary"], 500000)
        self.assertEqual(years[0]["season"], "Test 0")
        self.assertEqual(years[1]["cap_hit"], 500000)
        self.assertEqual(years[1]["aav"], 500000)
        self.assertEqual(years[1]["nhl_salary"], 500000)
        self.assertEqual(years[1]["season"], "Test 1")
        self.assertEqual(years[2]["cap_hit"], 1500000)
        self.assertEqual(years[2]["aav"], 1500000)
        self.assertEqual(years[2]["nhl_salary"], 1500000)
        self.assertEqual(years[2]["season"], "Test 2")
        self.assertEqual(years[3]["cap_hit"], 1500000)
        self.assertEqual(years[3]["aav"], 1500000)
        self.assertEqual(years[3]["nhl_salary"], 1500000)
        self.assertEqual(years[3]["season"], "Test 3")

    def test_query_pending_ufas(self):
        team_id = 5
        seasons = [
            Season(name="Test Current", free_agency_opening=date.today()),
            Season(
                name="Test Past", free_agency_opening=date(year=2020, month=1, day=1)
            ),
        ]
        for season in seasons:
            db.session.add(season)
        people = [
            Person(
                name="Test F",
                position="C",
                birthdate=date.today(),
                current_status="UFA",
            ),
            Person(
                name="Test D",
                position="D",
                birthdate=date.today(),
                current_status="RFA",
            ),
            Person(
                name="Test G",
                position="G",
                birthdate=date.today(),
                current_status="UFA",
            ),
        ]
        for person in people:
            db.session.add(person)
        assets = [
            Asset(
                active=False,
                current_team=team_id,
                originating_team=team_id,
                type="Contract",
            ),
            Asset(
                active=False,
                current_team=team_id,
                originating_team=team_id,
                type="Contract",
            ),
            Asset(
                active=False,
                current_team=team_id,
                originating_team=team_id,
                type="Contract",
            ),
        ]
        for asset in assets:
            db.session.add(asset)
        db.session.flush()

        contracts = [
            Contract(
                id=assets[0].id,
                person_id=people[0].id,
                expiration_status="UFA",
                non_roster=False,
            ),
            Contract(
                id=assets[1].id,
                person_id=people[1].id,
                expiration_status="RFA",
                non_roster=False,
            ),
            Contract(
                id=assets[2].id,
                person_id=people[2].id,
                expiration_status="UFA",
                non_roster=False,
            ),
        ]
        for contract in contracts:
            db.session.add(contract)
        contract_years = [
            ContractYear(contract_id=contracts[0].id, season_id=seasons[1].id),
            ContractYear(contract_id=contracts[1].id, season_id=seasons[1].id),
            ContractYear(contract_id=contracts[2].id, season_id=seasons[1].id),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()

        data = team.query_pending_ufa(team_id)
        self.assertEqual(len(data), 2)
        self.assertEqual(data[0][0], "Test F")
        self.assertEqual(data[0][1], "C")
        self.assertEqual(data[0][2], date.today())
        self.assertTrue("UFA" in data[0][7])
        self.assertEqual(data[1][0], "Test G")
        self.assertEqual(data[1][1], "G")
        self.assertEqual(data[1][2], date.today())
        self.assertTrue("UFA" in data[1][7])
        self.assertEqual(team.query_pending_ufa(team_id, True), [])

        contracts[0].non_roster = True
        db.session.add(contracts[0])
        db.session.commit()

        data = team.query_pending_ufa(team_id)
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0][0], "Test G")
        self.assertEqual(data[0][1], "G")
        self.assertEqual(data[0][2], date.today())
        self.assertTrue("UFA" in data[0][7])
        data = team.query_pending_ufa(team_id, True)
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0][0], "Test F")
        self.assertEqual(data[0][1], "C")
        self.assertEqual(data[0][2], date.today())
        self.assertTrue("UFA" in data[0][7])

    def test_get_expiration(self):
        person_id = 4837
        asset = Asset(id=1)
        contract = Contract(
            id=asset.id,
            person_id=person_id,
            expiration_status="RFA",
            signing_date=date(year=2022, month=1, day=1),
        )
        db.session.add(asset)
        db.session.add(contract)
        db.session.commit()
        self.assertEqual(team.get_expiration(person_id), "RFA")

        asset = Asset(id=2)
        contract = Contract(
            id=asset.id,
            person_id=person_id,
            expiration_status="UFA",
            signing_date=date(year=2022, month=3, day=1),
        )
        db.session.add(asset)
        db.session.add(contract)
        db.session.commit()
        self.assertEqual(team.get_expiration(person_id), "UFA")

        asset = Asset(id=3)
        contract = Contract(
            id=asset.id,
            person_id=person_id,
            expiration_status="idk",
            signing_date=date(year=2022, month=4, day=1),
        )
        db.session.add(asset)
        db.session.add(contract)
        db.session.commit()
        self.assertEqual(team.get_expiration(person_id), "idk")

        asset = Asset(id=4)
        contract = Contract(
            id=asset.id,
            person_id=person_id,
            expiration_status="notyou",
            signing_date=date(year=2022, month=2, day=1),
        )
        db.session.add(asset)
        db.session.add(contract)
        db.session.commit()
        self.assertEqual(team.get_expiration(person_id), "idk")

    def test_soir_ratio(self):
        PREVIOUS = 283
        CURRENT = self.app.config["CURRENT_SEASON"]
        PERSON = 7
        CONTRACT = 29
        current = Season(
            id=CURRENT,
            regular_season_start=date(year=2020, month=1, day=1),
            regular_season_end=date(year=2020, month=1, day=11),
            free_agency_opening=date(year=2019, month=12, day=25),
        )
        previous = Season(
            id=PREVIOUS,
            regular_season_start=date(year=2019, month=1, day=1),
            regular_season_end=date(year=2019, month=1, day=11),
            free_agency_opening=date(year=2018, month=12, day=25),
        )
        person = Person(id=PERSON, name="Test Person", position="G")
        contract = Contract(id=CONTRACT, person_id=PERSON)
        goalie_season = GoalieSeason(
            person_id=PERSON,
            season_id=PREVIOUS,
            games_played=41,
        )
        skater_season = SkaterSeason(
            person_id=PERSON,
            season_id=PREVIOUS,
            games_played=20,
        )
        daily_cap = [
            DailyCap(
                cap_hit=1,
                retained=False,
                team_id=1,
                date=date.today(),
                contract_id=CONTRACT,
                season_id=PREVIOUS,
                non_roster=False,
                ir=False,
                soir=False,
                ltir=False,
            ),
            DailyCap(
                cap_hit=1,
                retained=False,
                team_id=1,
                date=date.today(),
                contract_id=CONTRACT,
                season_id=PREVIOUS,
                non_roster=False,
                ir=False,
                soir=False,
                ltir=False,
            ),
        ]
        db.session.add(current)
        db.session.add(previous)
        db.session.add(person)
        db.session.add(contract)
        db.session.commit()
        self.assertEqual(team.soir_ratio(PERSON), 0)

        db.session.add(goalie_season)
        db.session.add(skater_season)
        db.session.commit()
        self.assertEqual(team.soir_ratio(PERSON), 0.5)

        person.position = "Z"
        db.session.add(person)
        db.session.commit()
        self.assertEqual(team.soir_ratio(PERSON), 10 / 41)

        db.session.add(daily_cap[0])
        db.session.add(daily_cap[1])
        db.session.commit()
        self.assertEqual(team.soir_ratio(PERSON), 0.2)

    def test_get_future_contracts(self):
        seasons = [
            Season(
                id=self.app.config["CURRENT_SEASON"],
                name="Current",
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 1,
                name="Next",
            ),
            Season(
                id=self.app.config["CURRENT_SEASON"] + 2,
                name="Then",
            ),
        ]
        people = [
            Person(
                id=328,
                name="Test Person 1",
                position="F",
                birthdate=date(year=1995, month=1, day=1),
                number=0,
                current_status="Under Contract",
            ),
            Person(
                id=3887,
                name="Test Person 2",
                position="G",
                birthdate=date(year=1990, month=1, day=1),
                number=99,
                current_status="Drafted,Under Contract",
            ),
        ]
        assets = [
            Asset(
                id=4783,
                current_team=33,
                originating_team=33,
                active=True,
                type="Contract",
            ),
            Asset(
                id=783,
                current_team=33,
                originating_team=33,
                active=True,
                type="Contract",
            ),
            Asset(
                id=83,
                current_team=33,
                originating_team=33,
                active=True,
                type="Contract",
            ),
        ]
        contracts = [
            Contract(
                id=4783,
                person_id=328,
            ),
            Contract(
                id=783,
                person_id=328,
            ),
            Contract(
                id=83,
                person_id=3887,
                expiration_status="RFA",
                signing_season_id=self.app.config["CURRENT_SEASON"],
            ),
        ]
        contract_years = [
            ContractYear(
                contract_id=4783,
                season_id=self.app.config["CURRENT_SEASON"],
                cap_hit=1000,
                aav=2000,
                nhl_salary=3000,
                signing_bonus=4000,
                performance_bonuses=5000,
            ),
            ContractYear(
                contract_id=783,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
                cap_hit=1001,
                aav=2001,
                nhl_salary=3001,
                signing_bonus=4001,
                performance_bonuses=5001,
            ),
            ContractYear(
                contract_id=83,
                season_id=self.app.config["CURRENT_SEASON"] + 1,
                cap_hit=1002,
                aav=2002,
                nhl_salary=3002,
                signing_bonus=4002,
                performance_bonuses=5002,
            ),
        ]
        for record in seasons + people + assets + contracts + contract_years:
            db.session.add(record)
        db.session.commit()
        output = team.get_future_contracts(33)
        self.assertEqual(len(output), 1)
        self.assertEqual(output[0]["name"], "Test Person 2")
        self.assertEqual(output[0]["position"], "G")
        self.assertEqual(output[0]["expires_as"], "RFA")
        self.assertTrue(output[0]["signed"])

    def test_query_professional_tryouts(self):
        TEAM_ID = 282
        self.assertEqual(team.query_professional_tryouts(TEAM_ID), [])

        people = [
            Person(
                id=1,
                name="Test One",
                position="F",
                birthdate=date(year=1998, month=10, day=10),
                number=10,
            ),
            Person(
                id=2,
                name="Test Two",
                position="D",
                birthdate=date(year=1998, month=9, day=9),
                number=9,
            ),
            Person(
                id=3,
                name="Test Three",
                position="G",
                birthdate=date(year=1998, month=8, day=8),
                number=8,
            ),
        ]
        assets = [
            Asset(
                id=22,
                active=True,
                type="Professional Tryout",
                current_team=TEAM_ID,
                originating_team=TEAM_ID,
            ),
            Asset(
                id=23,
                active=True,
                type="Professional Tryout",
                current_team=TEAM_ID,
                originating_team=TEAM_ID,
            ),
        ]
        ptos = [
            ProfessionalTryout(
                id=22,
                person_id=2,
            ),
            ProfessionalTryout(
                id=23,
                person_id=3,
            ),
        ]
        for record in people + assets + ptos:
            db.session.add(record)
        db.session.commit()
        ptos = team.query_professional_tryouts(TEAM_ID)

        self.assertEqual(len(ptos), 2)
        self.assertEqual(ptos[0][0], "Test Three")
        self.assertEqual(ptos[1][0], "Test Two")
        self.assertEqual(ptos[0][1], "G")
        self.assertEqual(ptos[1][1], "D")
        self.assertEqual(ptos[0][3], 3)
        self.assertEqual(ptos[1][3], 2)
        self.assertEqual(ptos[0][8], "Professional Tryout")
        self.assertEqual(ptos[1][8], "Professional Tryout")

        assets[1].active = False
        db.session.add(assets[1])
        db.session.commit()
        ptos = team.query_professional_tryouts(TEAM_ID)
        self.assertEqual(len(ptos), 1)
        self.assertEqual(ptos[0][0], "Test Two")

        assets[0].current_team = TEAM_ID + 2
        db.session.add(assets[0])
        db.session.commit()
        ptos = team.query_professional_tryouts(TEAM_ID)
        self.assertEqual(ptos, [])

    # TODO: test: get_non_roster
    # TODO: test: query_signing_rights
    # TODO: test: query_pending_ufa


if __name__ == "__main__":
    unittest.main(verbosity=2)
