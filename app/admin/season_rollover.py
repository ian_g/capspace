import json
import urllib.request
from urllib.error import HTTPError, URLError
from time import sleep
from datetime import date, timedelta
from flask import current_app
from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    BooleanField,
    SubmitField,
)
from wtforms.validators import DataRequired
from sqlalchemy import func, case, select, or_, and_

from .. import db
from ..models import (
    Configuration,
    Season,
    Person,
    Asset,
    SigningRights,
    DraftPick,
    Team,
    Contract,
    ContractYear,
    RetainedSalary,
    PenaltyYear,
    GoalieSeason,
    SkaterSeason,
    TeamSeason,
    Note,
)
from .qualify_person import calculate_qualifying_offer
from .update_person import update_goalie_stats, update_skater_stats

"""
    - `confirm()` when you click "Start Rollover"

    - Prompts to offer bona-fide offers to players (extends signing rights one year. Read CBA for other circumstances)
    - other things?
"""


def rollover(skip_stats=False):
    if not skip_stats:
        update_season_stats()
    current_season = Configuration.query.filter(
        Configuration.key == "CURRENT_SEASON"
    ).first()
    next_season = (
        Season.query.filter(
            Season.free_agency_opening
            > Season.query.get(current_season.value).free_agency_opening
        )
        .order_by(Season.free_agency_opening.asc())
        .first()
    )

    slide_entry_level()
    calculate_bonus_overage(next_season.id)
    deactivate_expiring(next_season.id)
    deactivate_penalties(next_season.id)
    deactivate_retention()
    new_id = create_new_season()
    create_new_draft(new_id)
    activate_draft_picks()
    clear_emergency_callups()
    clear_nonroster_statuses()

    current_season.value = next_season.id
    db.session.add(current_season)
    current_app.config["CURRENT_SEASON"] = current_season.value
    db.session.commit()


def create_new_season():
    last = Season.query.order_by(Season.regular_season_start.desc()).first()
    new_season = Season(
        name=f"{last.draft_date.year}-{last.draft_date.year+1}",
        salary_cap=last.salary_cap,
        salary_floor=last.salary_floor,
        minimum_salary=last.minimum_salary,
        max_buriable_hit=last.max_buriable_hit,
        bonus_overage_limit=last.bonus_overage_limit,
        free_agency_opening=last.free_agency_opening.replace(
            year=last.free_agency_opening.year + 1
        ),
        regular_season_start=last.regular_season_start.replace(
            year=last.regular_season_start.year + 1
        ),
        regular_season_end=last.regular_season_end.replace(
            year=last.regular_season_end.year + 1
        ),
        playoffs_start=last.playoffs_start.replace(year=last.playoffs_start.year + 1),
        draft_date=last.draft_date.replace(year=last.draft_date.year + 1),
        max_elc_salary=last.max_elc_salary,
        max_elc_minor_salary=last.max_elc_minor_salary,
        max_elc_performance_bonuses=last.max_elc_performance_bonuses,
    )
    db.session.add(new_season)
    db.session.flush()
    return new_season.id


def create_new_draft(season_id):
    teams = Team.query.filter(Team.active == True).with_entities(Team.id).all()
    for team in teams:
        for idx in range(current_app.config["DRAFT_ROUNDS"]):
            asset = Asset(
                type="Draft Pick",
                active=False,
                originating_team=team[0],
                current_team=team[0],
            )
            db.session.add(asset)
            db.session.flush()
            draft_pick = DraftPick(
                id=asset.id,
                round=idx + 1,
                season_id=season_id,
            )
            db.session.add(draft_pick)
    return


def activate_draft_picks():
    # Get next draft chronologically with inactive picks. Activate that draft's
    first_inactive = (
        DraftPick.query.join(Season, Season.id == DraftPick.season_id)
        .join(Asset, Asset.id == DraftPick.id)
        .filter(Asset.active == False)
        .filter(Season.draft_date >= func.date("now"))
        .order_by(Season.draft_date.asc())
        .with_entities(Season.id)
        .first()[0]
    )
    pick_ids = (
        DraftPick.query.filter(DraftPick.season_id == first_inactive)
        .with_entities(DraftPick.id)
        .subquery()
    )
    return (
        Asset.query.filter(Asset.id.in_(select(pick_ids)))
        .filter(Asset.active == False)
        .update(
            {
                Asset.active: True,
            },
            synchronize_session="fetch",
        )
    )


def clear_emergency_callups():
    emergency_callups = Person.query.filter(
        Person.current_status.like("%Emergency Callup%")
    ).all()
    for person in emergency_callups:
        status = person.get_status()
        status.remove("Emergency Callup")
        person.set_status(status)
        db.session.add(person)


def set_performance_bonuses(contract_id, bonuses_earned):
    update_count = (
        ContractYear.query.filter(
            ContractYear.season_id == current_app.config["CURRENT_SEASON"]
        )
        .filter(ContractYear.contract_id == contract_id)
        .update(
            {ContractYear.earned_performance_bonuses: bonuses_earned},
            synchronize_session="fetch",
        )
    )
    if update_count == 1:
        db.session.commit()
        return {}
    else:
        db.session.rollback()
        return {"error": f"{update_count} records updated. Expected 1"}


def calculate_bonus_overage(next_season):
    performance_bonuses = (
        db.session.query(
            Asset.current_team.label("current_team"),
            func.sum(ContractYear.earned_performance_bonuses).label(
                "total_earned_bonuses"
            ),
        )
        .join(ContractYear, ContractYear.contract_id == Asset.id)
        .filter(Asset.active == True)
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .group_by(Asset.current_team)
        .subquery()
    )
    overages = (
        TeamSeason.query.join(Team, Team.id == TeamSeason.team_id)
        .join(Season, Season.id == TeamSeason.season_id)
        .join(
            performance_bonuses,
            performance_bonuses.c.current_team == TeamSeason.team_id,
        )
        .filter(TeamSeason.season_id == current_app.config["CURRENT_SEASON"])
        .with_entities(
            performance_bonuses.c.current_team,
            case(
                (
                    TeamSeason.cap_hit + performance_bonuses.c.total_earned_bonuses
                    < Season.salary_cap + TeamSeason.max_ltir_relief,
                    0,
                ),
                else_=TeamSeason.cap_hit
                + performance_bonuses.c.total_earned_bonuses
                - Season.salary_cap
                - TeamSeason.max_ltir_relief,
            ),
            Team.name,
            performance_bonuses.c.total_earned_bonuses,
            TeamSeason.cap_hit,
            Season.salary_cap,
        )
        .all()
    )
    overage_amounts = {overage[0]: overage[1] for overage in overages}
    next_team_seasons = TeamSeason.query.filter(
        TeamSeason.season_id == next_season
    ).all()
    for record in next_team_seasons:
        if record.bonus_overage != 0:
            continue
        if record.team_id not in overage_amounts:
            continue
        record.bonus_overage = overage_amounts[record.team_id]
        db.session.add(record)
    return


def get_performance_bonuses():
    query = (
        ContractYear.query.join(Contract, Contract.id == ContractYear.contract_id)
        .join(Asset, Asset.id == Contract.id)
        .join(Person, Person.id == Contract.person_id)
        .join(Team, Team.id == Asset.current_team)
        .filter(Asset.active == True)
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .filter(ContractYear.performance_bonuses > 0)
        .with_entities(
            Person.name,
            Person.id,
            Contract.id,
            ContractYear.performance_bonuses,
            ContractYear.earned_performance_bonuses,
            Contract.entry_level,
            Team.abbreviation,
        )
        .order_by(
            ContractYear.earned_performance_bonuses.asc(),
            Contract.entry_level.asc(),
            ContractYear.performance_bonuses.desc(),
            Person.name.asc(),
        )
        .all()
    )
    return [
        {
            "name": entry[0],
            "person_id": entry[1],
            "contract_id": entry[2],
            "performance_bonuses": entry[3],
            "earned_bonuses": entry[4],
            "entry_level": entry[5],
            "team": entry[6],
        }
        for entry in query
    ]


def deactivate_penalties(next_season):
    future_penalties = (
        PenaltyYear.query.filter(PenaltyYear.season_id == next_season)
        .with_entities(PenaltyYear.penalty_id)
        .subquery()
    )
    (
        Asset.query.filter(Asset.active == True)
        .filter(Asset.type == "Penalty")
        .filter(Asset.id.not_in(select(future_penalties)))
        .update(
            {
                Asset.active: False,
            },
            synchronize_session="fetch",
        )
    )


def deactivate_retention():
    inactive_retention = (
        RetainedSalary.query.join(Asset, Asset.id == RetainedSalary.contract_id)
        .filter(Asset.active == False)
        .with_entities(RetainedSalary.id)
        .subquery()
    )
    Asset.query.filter(Asset.id.in_(select(inactive_retention))).update(
        {
            Asset.active: False,
        },
        synchronize_session="fetch",
    )


# Contracts
def deactivate_expiring(next_season):
    future_contracts = (
        ContractYear.query.join(Asset, Asset.id == ContractYear.contract_id)
        .filter(Asset.active == True)
        .filter(ContractYear.season_id == next_season)
        .with_entities(Asset.id)
        .subquery()
    )
    still_contracted = (  # Make sure folks with extensions in place aren't listed as free agents
        Contract.query.filter(Contract.id.in_(select(future_contracts)))
        .with_entities(Contract.person_id)
        .subquery()
    )
    pending_ufas = (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .filter(Asset.active == True)
        .filter(Asset.id.not_in(select(future_contracts)))
        .filter(Contract.expiration_status == "UFA")
        .with_entities(
            Contract.person_id,
        )
        .subquery()
    )
    people = (
        Person.query.filter(Person.id.in_(select(pending_ufas)))
        .filter(Person.id.not_in(select(still_contracted)))
        .all()
    )
    for person in people:
        current_app.logger.debug(f"UFA {person.name=} {person.current_status=}")
        status = person.get_status()
        status.remove("Under Contract")
        status.append("UFA")
        person.set_status(status)
        db.session.add(person)
    pending_rfas = (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .filter(Asset.active == True)
        .filter(Asset.id.not_in(select(future_contracts)))
        .filter(Contract.expiration_status == "RFA")
        .with_entities(
            Contract.person_id,
        )
        .subquery()
    )
    people = (
        Person.query.filter(Person.id.in_(select(pending_rfas)))
        .filter(Person.id.not_in(select(still_contracted)))
        .all()
    )
    for person in people:
        current_app.logger.debug(f"RFA {person.name=} {person.current_status=}")
        status = person.get_status()
        status.remove("Under Contract")
        status.append("RFA")
        person.set_status(status)
        db.session.add(person)
        create_signing_rights(person)

    people = Person.query.filter(Person.id.in_(select(still_contracted))).all()
    for person in people:
        status = person.get_status()
        if "RFA" in status:
            status.remove("RFA")
        if "UFA" in status:
            status.remove("UFA")
        status.append("Under Contract")
        person.set_status(status)
        db.session.add(person)
    contracts = (
        Asset.query.filter(Asset.id.not_in(select(future_contracts)))
        .filter(Asset.active == True)
        .filter(Asset.type == "Contract")
        .update(
            {
                Asset.active: False,
            },
            synchronize_session="fetch",
        )
    )
    return contracts


def create_signing_rights(person):
    team = (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .filter(Asset.active == True)
        .filter(Contract.person_id == person.id)
        .with_entities(Asset.current_team)
        .first()
    )
    if not team:
        return
    asset = Asset(
        originating_team=team[0], current_team=team[0], active=1, type="Signing Rights"
    )
    db.session.add(asset)
    db.session.flush()
    qualifying_data = calculate_qualifying_offer(person.id)
    rights = SigningRights(
        id=asset.id,
        person_id=person.id,
        expiration_date=date.today().replace(month=7, day=15),
        qualifying_offer=True,
        qualifying_salary=qualifying_data["qualifying_salary"],
        qualifying_minors=(
            qualifying_data["qualifying_salary"]
            if qualifying_data["one_way"]
            else qualifying_data["last_minors_salary"]
        ),
    )
    db.session.add(rights)


def clear_nonroster_statuses():
    active_contracts = select(
        Asset.query.filter(Asset.active == True)
        .filter(Asset.type == "Contract")
        .with_entities(Asset.id)
        .subquery()
    )
    Contract.query.filter(Contract.id.in_(active_contracts)).update(
        {
            Contract.non_roster: False,
            Contract.on_loan: False,
            Contract.in_juniors: False,
        },
        synchronize_session="fetch",
    )


def get_slide_candidates():
    slide_count = (
        db.session.query(
            ContractYear.contract_id.label("contract_id"),
            func.count(ContractYear.entry_level_slide).label("slide_count"),
        )
        .filter(ContractYear.entry_level_slide == True)
        .group_by(ContractYear.contract_id)
        .subquery()
    )

    # Exclude deals where a previous year didn't slide
    previous_non_slide = (
        ContractYear.query.join(Season, Season.id == ContractYear.season_id)
        .filter(
            ContractYear.contract_id.in_(
                ContractYear.query.filter(
                    ContractYear.season_id == current_app.config["CURRENT_SEASON"]
                )
                .with_entities(ContractYear.contract_id)
                .scalar_subquery()
            )
        )
        .filter(
            Season.draft_date
            < Season.query.get(current_app.config["CURRENT_SEASON"]).draft_date
        )
        .filter(ContractYear.entry_level_slide == False)
        .with_entities(ContractYear.contract_id)
        .subquery()
    )

    skater_games_played = (
        db.session.query(
            SkaterSeason.person_id.label("person_id"),
            func.sum(SkaterSeason.games_played).label("games_played"),
        )
        .filter(SkaterSeason.season_id == current_app.config["CURRENT_SEASON"])
        .group_by(SkaterSeason.person_id)
    )
    goalie_games_played = (
        db.session.query(
            GoalieSeason.person_id.label("person_id"),
            func.sum(GoalieSeason.games_played).label("games_played"),
        )
        .filter(GoalieSeason.season_id == current_app.config["CURRENT_SEASON"])
        .group_by(GoalieSeason.person_id)
    )
    games_played = skater_games_played.union(goalie_games_played).subquery()
    do_not_slide = (
        Contract.query.filter(Contract.elc_slide == False)
        .with_entities(Contract.id)
        .subquery()
    )

    slide_candidates = (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .join(Person, Person.id == Contract.person_id)
        .join(
            ContractYear, ContractYear.contract_id == Contract.id
        )  # to exclude future contracts
        .join(slide_count, Contract.id == slide_count.c.contract_id, isouter=True)
        .join(games_played, Person.id == games_played.c.person_id, isouter=True)
        .filter(Contract.entry_level == True)
        .filter(Contract.id.not_in(select(do_not_slide)))
        .filter(Contract.id.not_in(select(previous_non_slide)))
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .filter(Asset.active == True)
        .filter(func.coalesce(games_played.c.games_played, 0) < 10)
        .filter(
            func.coalesce(Person.elc_signing_age, 0)
            + func.coalesce(slide_count.c.slide_count, 0)
            < 20
        )
        .with_entities(
            Person.id,
            Contract.id,
            func.coalesce(games_played.c.games_played, 0),
            func.coalesce(slide_count.c.slide_count, 0),
            Person.elc_signing_age,
            Person.name,
            ContractYear.cap_hit,
        )
        .order_by(
            ContractYear.cap_hit.desc(),
            Person.name.asc(),
        )
        .all()
    )
    return slide_candidates


def slide_entry_level():
    slide_candidates = get_slide_candidates()
    for contract in slide_candidates:
        slide_contract(contract[1])

    return slide_candidates


def slide_contract(contract_id):
    contract_record = Contract.query.get(contract_id)
    contract_years = (
        ContractYear.query.filter(ContractYear.contract_id == contract_id)
        .filter(ContractYear.entry_level_slide == False)
        .order_by(ContractYear.id.asc())
        .all()
    )
    season_ids = list(map(lambda year: year.season_id, contract_years))
    next_season = (
        Season.query.filter(
            Season.free_agency_opening
            > (
                Season.query.filter(Season.id.in_(season_ids))
                .with_entities(func.max(Season.free_agency_opening))
                .scalar_subquery()
            )
        )
        .order_by(Season.free_agency_opening.asc())
        .with_entities(Season.id)
        .first()
    )
    contract_years.append(
        ContractYear(
            contract_id=contract_id,
            season_id=next_season[0],
            signing_bonus=0,
            bought_out=False,
            nmc=False,
            ntc=False,
            terminated=False,
            clause_limits="",
            earned_performance_bonuses=0,
            two_way=True,
        )
    )
    for idx in range(len(contract_years) - 1, 0, -1):
        contract_years[idx].nhl_salary = contract_years[idx - 1].nhl_salary
        contract_years[idx].minors_salary = contract_years[idx - 1].minors_salary
        contract_years[idx].performance_bonuses = contract_years[
            idx - 1
        ].performance_bonuses
    contract_years[0].entry_level_slide = True
    contract_years[0].nhl_salary = 0
    contract_years[0].performance_bonuses = 0

    for year in contract_years:
        if year.nhl_salary == 0:
            continue
        season = Season.query.get(year.season_id)
        if season.minimum_salary <= year.nhl_salary:
            continue
        contract_record.total_value += season.minimum_salary - year.nhl_salary
        db.session.add(contract_record)
        db.session.add(
            Note(
                record_table="Contract",
                record_id=contract_id,
                note=f"{season.name}: NHL salary increased from {year.nhl_salary} to {season.minimum_salary} (league minimum)",
            )
        )
        year.nhl_salary = season.minimum_salary

    # recalculate AAV, cap hit, contract value over final ${contract length} years
    # Inner sum flattens array -> sum(arrays, []). Outer sum adds flattened values
    length = len(contract_years) - 1
    cap_hit = (
        sum(
            sum(
                map(
                    lambda year: [year.nhl_salary, year.signing_bonus],
                    contract_years[1:],
                ),
                [],
            )
        )
        // length  # integer division, not JS-style comment
    )
    aav = (
        sum(
            sum(
                map(
                    lambda year: [
                        year.nhl_salary,
                        year.signing_bonus,
                        year.performance_bonuses,
                    ],
                    contract_years[1:],
                ),
                [],
            )
        )
        // length
    )

    db.session.add(contract_years[0])
    for year in contract_years[1:]:
        year.aav = aav
        year.cap_hit = cap_hit
        db.session.add(year)
    return


def set_group_six(contract_id):
    contract = Contract.query.get(contract_id)
    if not contract:
        return {"error": f"Contract ID {contract_id} does not exist"}
    contract.expiration_status = "UFA"
    db.session.add(contract)
    db.session.add(
        Note(
            record_id=contract.id,
            record_table="contract",
            note="RFA -> G6 UFA (Age >= 25; 3+ pro seasons on NHL deal; <80/<28 skater/goalie NHL games)",
        )
    )
    db.session.commit()
    return {}


def get_group_six_candidates():
    next_season = (
        Season.query.filter(
            Season.free_agency_opening
            > Season.query.get(current_app.config["CURRENT_SEASON"]).free_agency_opening
        )
        .order_by(Season.free_agency_opening.asc())
        .first()
    )
    current_app.logger.info(f"Next season: ({next_season.id}, {next_season.name})")
    next_season_contracts = (
        ContractYear.query.join(Asset, Asset.id == ContractYear.contract_id)
        .join(Contract, Contract.id == ContractYear.contract_id)
        .filter(Asset.active == True)
        .filter(ContractYear.season_id == next_season.id)
        .with_entities(Contract.person_id)
        .scalar_subquery()
    )
    contract_seasons = (
        db.session.query(ContractYear)
        .join(Season, Season.id == ContractYear.season_id)
        .join(Contract, Contract.id == ContractYear.contract_id)
        .filter(ContractYear.terminated == False)
        .filter(Season.free_agency_opening < date.today())
        .group_by(Contract.person_id)
        .with_entities(
            Contract.person_id.label("person_id"),
            func.count(ContractYear.id).label("contract_seasons"),
        )
        .subquery()
    )
    skater_games_played = db.session.query(
        SkaterSeason.person_id.label("person_id"),
        func.sum(SkaterSeason.games_played).label("games_played"),
    ).group_by(SkaterSeason.person_id)
    goalie_games_played = db.session.query(
        GoalieSeason.person_id.label("person_id"),
        func.sum(GoalieSeason.games_played).label("games_played"),
    ).group_by(GoalieSeason.person_id)
    games_played = skater_games_played.union(goalie_games_played).subquery()
    cutoff_year = next_season.free_agency_opening.year - 25
    age_cutoff = next_season.free_agency_opening.replace(year=cutoff_year)
    ufa_cutoff = next_season.free_agency_opening.replace(year=(cutoff_year - 2))

    candidate_query = (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .join(ContractYear, ContractYear.contract_id == Contract.id)
        .join(Person, Person.id == Contract.person_id)
        .join(contract_seasons, Person.id == contract_seasons.c.person_id, isouter=True)
        .join(games_played, Person.id == games_played.c.person_id, isouter=True)
        .filter(Contract.person_id.not_in(next_season_contracts))
        .filter(Contract.expiration_status == "RFA")
        .filter(Asset.active == True)
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .filter(Person.birthdate <= age_cutoff)
        .filter(Person.birthdate > ufa_cutoff)
        .filter(contract_seasons.c.contract_seasons >= 3)
        .filter(
            or_(
                and_(Person.position != "G", games_played.c.games_played <= 80),
                and_(Person.position == "G", games_played.c.games_played <= 28),
                games_played.c.games_played == None,
            )
        )
        .with_entities(
            Person.name,
            Person.id,
            Person.position,
            contract_seasons.c.contract_seasons,
            games_played.c.games_played,
            Contract.id,
            Person.birthdate,
        )
        .order_by(
            Person.position.asc(),
            Person.name.asc(),
        )
        .all()
    )
    exp_date = next_season.free_agency_opening
    candidates = [
        {
            "name": candidate[0],
            "person_id": candidate[1],
            "position": candidate[2],
            "season_count": candidate[3],
            "nhl_games": candidate[4],
            "contract_id": candidate[5],
            "expiration_age": exp_date.year
            - candidate[6].year
            - ((exp_date.month, exp_date.day) < (candidate[6].month, candidate[6].day)),
        }
        for candidate in candidate_query
    ]
    return candidates


# Player stats
def update_season_stats():
    skater_ids = (
        SkaterSeason.query.filter(
            SkaterSeason.season_id == current_app.config["CURRENT_SEASON"]
        )
        .with_entities(SkaterSeason.person_id)
        .distinct()
        .subquery()
    )
    goalie_ids = (
        GoalieSeason.query.filter(
            GoalieSeason.season_id == current_app.config["CURRENT_SEASON"]
        )
        .with_entities(GoalieSeason.person_id)
        .distinct()
        .subquery()
    )
    people_to_update = Person.query.filter(
        Person.id.in_(select(goalie_ids)) | Person.id.in_(select(skater_ids))
    ).all()
    SkaterSeason.query.filter(
        SkaterSeason.season_id == current_app.config["CURRENT_SEASON"]
    ).delete()
    GoalieSeason.query.filter(
        GoalieSeason.season_id == current_app.config["CURRENT_SEASON"]
    ).delete()
    season_name = Season.query.get(current_app.config["CURRENT_SEASON"]).name.replace(
        "-", ""
    )
    for person in people_to_update:
        sleep(1)
        current_app.logger.info(
            f"Updating stats: name={person.name} id={person.id} nhl_id={person.nhl_id}"
        )
        if person.position == "G":
            update_goalie_stats(person.id)
        else:
            update_skater_stats(person.id)
    current_app.logger.info("All stats updated")


class SeasonRolloverChecklist(FlaskForm):
    set_slides = BooleanField("Entry-level slides set?")
    skip_stats = BooleanField("Check this box to skip updating player stats")
    set_unqualified = BooleanField("All unqualified RFAs (so far) have been set?")
    set_bonafide = BooleanField("All bona-fide offers have been created?")
    created_stats = BooleanField(
        "Created dummy stats records for players with one game this season?"
    )
    roll_over = SubmitField("Start rollover")
