from markupsafe import escape
from flask import current_app
from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    TextAreaField,
    HiddenField,
    IntegerField,
    BooleanField,
    SubmitField,
)
from wtforms.validators import DataRequired, Optional

from .. import db
from ..models import Note


def get_note_data(note_id):  # For editing /admin/notes/<note_id>
    note = Note.query.filter(Note.id == note_id).first()
    if not note:
        return False
    return {
        "record_id": note.record_id,
        "record_table": note.record_table,
        "note": escape(note.note) if not note.safe else note.note,
        "safe": note.safe,
    }


def get_notes(table=None, record_id=None):
    query = Note.query
    if table:
        query = query.filter(Note.record_table == table)
    if table and record_id:
        query = query.filter(Note.record_id == record_id)
    raw_notes = query.all()
    return [
        {
            "note_id": note.id,
            "record_table": note.record_table,
            "record_id": note.record_id,
            "note": escape(note.note) if not note.safe else note.note,
        }
        for note in raw_notes
    ]


class WriteNote(FlaskForm):
    def write_note(self):
        if self.delete.data == True and self.note_id.data:
            Note.query.filter(Note.id == self.note_id.data).delete()
            db.session.commit()
            return

        if self.note_id.data:
            note = Note.query.filter(Note.id == self.note_id.data).first()
        else:
            note = Note()
        note.record_id = self.record_id.data
        note.record_table = self.record_table.data
        note.note = self.note.data
        note.safe = self.safe.data
        db.session.add(note)
        db.session.commit()
        return note.id

    note_id = HiddenField("Note ID")
    record_id = IntegerField("Record ID", validators=[Optional()])
    record_table = StringField("Table", validators=[DataRequired()])
    note = TextAreaField("Note", validators=[DataRequired()])
    delete = BooleanField("Delete Note", default=False)
    safe = BooleanField("Safe for HTML", default=False)
    submit = SubmitField("Submit")
