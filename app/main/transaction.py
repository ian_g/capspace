from datetime import datetime
from flask import current_app, render_template
from sqlalchemy import func, case
from .notes import get_notes
from .. import db
from ..models import (
    Transaction,
    TransactionAsset,
    Contract,
    Person,
    Team,
    SigningRights,
    DraftPick,
    ProfessionalTryout,
)


def transaction_page(from_date, to_date):
    data = transaction_data(0, from_date, to_date)
    teams = (
        Team.query.filter(Team.active == True)
        .order_by(Team.name)
        .with_entities(Team.id, Team.name)
        .all()
    )
    teams = [list(team) for team in teams]
    return render_template(
        "transactions.html",
        title="Roster Transactions",
        css="transaction.css",
        teams=teams,
        data=data,
    )


def transaction_rows(offset, from_date=None, to_date=None):
    data = transaction_data(offset, from_date, to_date)
    return render_template("_transaction_rows.html", data=data)


def transaction_data(offset=0, from_date=None, to_date=None):
    display_assets = db.union(
        db.session.query(
            Contract.id.label("asset_id"),
            Contract.person_id.label("person_id"),
        ),
        db.session.query(
            SigningRights.id.label("asset_id"),
            SigningRights.person_id.label("person_id"),
        ),
        db.session.query(
            DraftPick.id.label("asset_id"),
            DraftPick.person_id.label("person_id"),
        ),
        db.session.query(
            ProfessionalTryout.id.label("asset_id"),
            ProfessionalTryout.person_id.label("person_id"),
        ),
    ).subquery()
    query = (
        TransactionAsset.query.join(
            Transaction, Transaction.id == TransactionAsset.transaction_id
        )
        .join(display_assets, TransactionAsset.asset_id == display_assets.c.asset_id)
        .join(Person, Person.id == display_assets.c.person_id)
        .join(Team, Team.id == TransactionAsset.former_team)
    )
    if from_date:
        from_date = datetime.strptime(from_date, current_app.config["DATE_FMT"]).date()
        query = query.filter(Transaction.date >= from_date)
    if to_date:
        to_date = datetime.strptime(to_date, current_app.config["DATE_FMT"]).date()
        query = query.filter(Transaction.date <= to_date)
    query = (
        query.order_by(
            func.date(Transaction.date).desc(),
            case(
                (Transaction.type == "Trade", Transaction.id),
                (Transaction.type == "Drafted", -Transaction.id),
                else_=0,
            ).desc(),
            Team.abbreviation.asc(),
            Person.id.asc(),
            Transaction.date.desc(),
            Transaction.id.desc(),
        )
        .with_entities(
            Team.name,
            Team.logo,
            Team.abbreviation,
            Transaction.type,
            Person.name,
            Person.id,
            Transaction.date,
            TransactionAsset.id,
            Team.id,
        )
        .offset(offset)
        .limit(current_app.config["RECORD_BATCH_SIZE"])
        .all()
    )

    return [
        {
            "team": record[0],
            "team_id": record[8],
            "logo": record[1],
            "abbreviation": record[2].lower(),
            "type": record[3],
            "player": record[4],
            "player_url": f"{record[4].lower().replace(' ', '-')}-{record[5]}",
            "date": record[6].strftime(current_app.config["DATE_FMT"]),
            "notes": get_notes("transaction_asset", record[7]),
        }
        for record in query
    ]
