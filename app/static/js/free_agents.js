shadeRows();
function shadeRows() {
    let rows = document.querySelector("tbody").querySelectorAll("tr");
    rows.forEach((row, idx) => {
        if (idx % 2 == 0) {
            return;
        }
        row.classList.add("shaded");
    })
}
