document.querySelectorAll('[id$="_dequalify"]').forEach(element => element.onclick=dequalifyPlayer);
document.querySelectorAll('[id$="_deactivate"]').forEach(element => element.onclick=deactivateRights);
document.querySelectorAll('[id$="_adjust"]').forEach(element => element.onchange=updateInputs);
document.querySelectorAll('[id$="_submit"]').forEach(element => element.onclick=reassignPlayer);
reshadeRows();
setDate();

function setDate() {
    const parameters = (new URL(window.location)).searchParams;
    const date = parameters.get('date');
    if (date) {
        document.getElementById('transaction-date').value = date;
        const dateParam = `?date=${date}`;
        document.querySelectorAll('.name a').forEach((el) => el.href = el.href + dateParam);
    }
}

async function deactivateRights(event) {
    if (!confirm('Click \'Ok\' to confirm rights deactivation')) {
        return;
    }
    let date = document.querySelector('#transaction-date')?.value;
    let personID = event.target.id.split('_')[0];
    console.log(personID);
    let url = `/admin/api/deactivate-rights/${personID}`;
    if (date) {
        url += `?date=${date}`;
    }
    let response = await fetch(url, {'method': 'POST'});
    let data = await response.json();
    if (data.error) {
        alert(`Invalid update: ${data.error}`);
        return;
    }
    event.target.parentElement.parentElement.remove();
    reshadeRows();
}

async function dequalifyPlayer(event) {
    if (!confirm('Click \'Ok\' to confirm player de-qualification')) {
        return;
    }
    let date = document.querySelector('#transaction-date')?.value;
    let personID = event.target.id.split('_')[0];
    console.log(personID);
    let url = `/admin/api/dequalify/${personID}`;
    if (date) {
        url += `?date=${date}`;
    }
    let response = await fetch(url, {'method': 'POST'});
    let data = await response.json();
    if (data.error) {
        alert(`Invalid update: ${data.error}`);
        return;
    }
    event.target.parentElement.parentElement.remove();
    reshadeRows();
}

function updateInputs(event) {
    let selectValue = event.target.value;
    if (selectValue) {
        event.target.nextElementSibling.disabled = false;
    } else {
        event.target.nextElementSibling.disabled = true;
    }

    if (selectValue == 'ad_hoc') {
        event.target.parentElement.querySelector('.ad_hoc').style.display = 'block';
    } else {
        event.target.parentElement.querySelector('.ad_hoc').style.display = 'none';
    }
}

async function reassignPlayer(event) {
    let personID = event.target.id.split('_').shift();
    let type = document.getElementById(`${personID}_adjust`).value;
    let stringType;
    if (type == '') {
        console.info(
            'Cannot submit reassignment without an action submitted',
            event.target.parentElement.parentElement
        );
        return;
    } else if (type == 'ad_hoc') {
        stringType = document.getElementById(`${personID}_adhoc`).value;
    }

    if (type == 'ad_hoc' && !stringType) {
        console.info(
            'Cannot submit ad-hoc reassignment without a transaction string',
            event.target.parentElement.parentElement
        );
        return;
    }

    if (!confirm(`Click 'Ok' to confirm player reassignment (${type})`)) {
        return;
    }
    let date = document.querySelector('#transaction-date')?.value;
    console.log(personID);
    let url = `/admin/api/reassign/${personID}?type=${type}`;
    if (date) {
        url += `&date=${date}`;
    }
    if (stringType) {
        url += `&adhocType=${stringType}`;
    }
    let response = await fetch(url, {'method': 'POST'});
    let data = await response.json();
    if (data.error) {
        alert(`Invalid update: ${data.error}`);
        return;
    }
    event.target.parentElement.parentElement.remove();
    reshadeRows();
}

async function reshadeRows() {
    document.querySelectorAll('table').forEach(table => {
        let rows = table.querySelectorAll('tr');
        for (let idx = 0; idx < rows.length; idx++) {
            if (idx % 2 == 0) {
                rows[idx].classList.remove('shaded');
            } else {
                rows[idx].classList.add('shaded');
            }
        }
    });
}
