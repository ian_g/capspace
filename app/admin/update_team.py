import sys
import json
import urllib.request
from time import sleep
from datetime import date, timedelta
from urllib.error import HTTPError, URLError
from http.client import RemoteDisconnected
from flask import current_app
from flask_wtf import FlaskForm
from sqlalchemy import select
from wtforms import (
    StringField,
    HiddenField,
    IntegerField,
    SelectMultipleField,
    BooleanField,
    SubmitField,
)
from wtforms.validators import DataRequired, NumberRange, Length, Optional

from .. import db, scheduler
from ..models import Team, TeamSeason, Season


class UpdateTeam(FlaskForm):
    def update_team(self):
        team_record = Team.query.get(self.id.data)
        if not team_record:
            return
        team_record.name = self.name.data
        team_record.first_year = self.first_year.data
        team_record.team_location = self.team_location.data
        team_record.team_name = self.team_name.data
        team_record.logo = self.team_logo.data
        team_record.active = self.active.data
        db.session.add(team_record)

        ts_ids = (
            TeamSeason.query.join(Season, Season.id == TeamSeason.season_id)
            .filter(TeamSeason.team_id == self.id.data)
            .filter(
                Season.draft_date
                >= Season.query.get(current_app.config["CURRENT_SEASON"]).draft_date
            )
            .with_entities(TeamSeason.id)
            .subquery()
        )
        TeamSeason.query.filter(TeamSeason.id.in_(select(ts_ids))).update(
            {
                TeamSeason.logo: self.team_logo.data,
            },
            synchronize_session="fetch",
        )
        db.session.commit()

    def set_values(self, team_id):
        team_record = Team.query.get(team_id)
        if not team_record:
            return
        self.name.data = team_record.name
        self.first_year.data = team_record.first_year
        self.team_location.data = team_record.team_location
        self.team_name.data = team_record.team_name
        self.team_logo.data = team_record.logo
        self.active.data = team_record.active
        self.id.data = team_record.id

    name = StringField("Name")
    id = HiddenField("Team ID", validators=[DataRequired()])
    first_year = IntegerField("First Year")
    team_location = StringField("Team Location")
    team_name = StringField("Team Name")
    team_logo = StringField("Logo")
    active = BooleanField("Active")
    update = SubmitField("Update")
