import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get("SECRET_KEY") or "you-wont-guess"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DISABLE_SCHEDULED = False

    # MAIL_SERVER = os.environ.get("MAIL_SERVER")
    # MAIL_PORT = int(os.environ.get("MAIL_PORT") or 25)
    # MAIL_USE_TLS = os.environ.get("MAIL_USE_TLS") is not None
    # MAIL_USERNAME = os.environ.get("MAIL_USERNAME")
    # MAIL_PASSWORD = os.environ.get("MAIL_PASSWORD")
    # ADMINS = ["iang4llmeister@gmail.com"]


class DevelopmentConfig(Config):
    SQLALCHEMY_DATABASE_URI = (
        os.environ.get("DATABASE_URL") or f"sqlite:///{os.path.join(basedir, 'app.db')}"
    )
    SEND_FILE_MAX_AGE_DEFAULT = 0
    CURRENT_SEASON = 1
    DRAFT_SEASON = 1
    RECORD_BATCH_SIZE = 1
    DEBUG = True
    LOG_FILE = "capspace.dev.log"


class TestConfig(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL") or "sqlite://"
    # Much slower + db ends up empty at the end anyway
    # SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL") or f"sqlite:///{os.path.join(basedir, 'test.db')}"
    LOG_FILE = "capspace.test.log"
    WTF_CSRF_ENABLED = False
    CURRENT_SEASON = 1
    DRAFT_SEASON = 1
    DISPLAY_X_SEASONS = 6
    RECORD_BATCH_SIZE = 40
    DATE_FMT = "%Y-%m-%d"
    DRAFT_ROUNDS = 5


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = (
        os.environ.get("DATABASE_URL") or f"sqlite:///{os.path.join(basedir, 'app.db')}"
    )
    LOG_FILE = "capspace.log"
    DISABLE_SCHEDULED = True


config = {
    "test": TestConfig,
    "development": DevelopmentConfig,
    "production": ProductionConfig,
    "default": DevelopmentConfig,
}
