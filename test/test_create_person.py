import sys
import random
import unittest
from datetime import datetime, date
from app import create_app, db
from app.models import Season, Person, Alias

from app.admin.create_person import CreatePerson

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class AdminPersonTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_form_validate_position(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreatePerson()
        self.assertTrue(form.validate_position(form.position))

        form.position.data = ["C", "RW"]
        self.assertTrue(form.validate_position(form.position))

        form.position.data = ["LD"]
        self.assertTrue(form.validate_position(form.position))

        form.position.data = ["G"]
        self.assertTrue(form.validate_position(form.position))

        form.position.data = ["LD", "G"]
        self.assertFalse(form.validate_position(form.position))

        form.position.data = ["LD", "LW"]
        self.assertFalse(form.validate_position(form.position))

        form.position.data = ["G", "LW"]
        self.assertFalse(form.validate_position(form.position))

        ctx.pop()

    def test_form_validate_nhl_id(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreatePerson()
        form.nhl_id.errors = list(form.nhl_id.errors)
        self.assertTrue(form.validate_nhl_id(form.nhl_id))

        form.nhl_id.data = 12345
        self.assertFalse(form.validate_nhl_id(form.nhl_id))

        form.nhl_id.data = 8450000
        self.assertTrue(form.validate_nhl_id(form.nhl_id))

        ctx.pop()

    def test_form_duplicate_person_exists(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreatePerson()

        person = Person(
            name="Test Person",
            birthdate=date(year=1948, month=5, day=22),
            birthplace="Winnipeg, MB, CAN",
        )
        self.assertFalse(form.duplicate_person_exists(person))
        db.session.add(person)
        self.assertTrue(form.duplicate_person_exists(person))

        person_id = Person(nhl_id=12345)
        self.assertFalse(form.duplicate_person_exists(person_id))
        db.session.add(person_id)
        self.assertTrue(form.duplicate_person_exists(person_id))

        ctx.pop()

    def test_form_create_person(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreatePerson()

        form.name.data = "Test Name"
        form.birthdate.data = date.today()
        form.birthplace.data = "Stanford, California, USA"
        form.nationality.data = "GRE"
        form.nhl_id.data = 25
        form.feet.data = 5
        form.inches.data = 2
        form.weight.data = 150
        form.position.data = "LD"
        form.shoots.data = "R"
        form.waivers.data = True
        form.arbitration.data = True
        form.active.data = True
        form.rookie.data = True
        form.captain.data = False
        form.alternate_captain.data = False
        form.non_roster.data = True
        self.assertIsNotNone(form.create_person())

        form.submit.errors = list(form.submit.errors)
        self.assertIsNone(form.create_person())
        self.assertEqual(
            form.submit.errors,
            ["Submission aborted: person already exists in database"],
        )

        self.assertEqual(Alias.query.count(), 1)
        alias = Alias.query.first()
        self.assertEqual(alias.value, "Test Name")

        ctx.pop()

    def test_form_create_person_with_aliases(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreatePerson()

        form.name.data = "Joel Nyström"
        form.birthdate.data = date.today()
        form.birthplace.data = "Stanford, California, USA"
        form.nationality.data = "GRE"
        form.nhl_id.data = 25
        form.feet.data = 5
        form.inches.data = 2
        form.weight.data = 150
        form.position.data = "LD"
        form.shoots.data = "R"
        form.waivers.data = True
        form.arbitration.data = True
        form.active.data = True
        form.rookie.data = True
        form.captain.data = False
        form.alternate_captain.data = False
        form.non_roster.data = True
        form.aliases.data = ["Jo Ny", "JJ"]
        self.assertIsNotNone(form.create_person())

        form.submit.errors = list(form.submit.errors)
        self.assertIsNone(form.create_person())
        self.assertEqual(
            form.submit.errors,
            ["Submission aborted: person already exists in database"],
        )

        self.assertEqual(Alias.query.count(), 4)
        aliases = Alias.query.order_by(Alias.value).all()
        self.assertEqual(aliases[0].value, "JJ")
        self.assertEqual(aliases[1].value, "Jo Ny")
        self.assertEqual(aliases[2].value, "Joel Nystrom")
        self.assertEqual(aliases[3].value, "Joel Nyström")

        ctx.pop()

    def test_validate_ep_id(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = CreatePerson()

        form.name.data = "Test Name"
        form.birthdate.data = date.today()
        form.birthplace.data = "Stanford, California, USA"
        form.nationality.data = "GRE"
        form.nhl_id.data = 25
        form.feet.data = 5
        form.inches.data = 2
        form.weight.data = 150
        form.position.data = "LD"
        form.shoots.data = "R"
        form.waivers.data = True
        form.arbitration.data = True
        form.active.data = True
        form.rookie.data = True
        form.captain.data = False
        form.alternate_captain.data = False

        # True because empty
        self.assertTrue(form.validate_ep_id(form.ep_id))

        # True because no users with that ID
        form.ep_id.data = 847
        self.assertTrue(form.validate_ep_id(form.ep_id))

        # False because now there's a user with that ID
        form.create_person()
        form.ep_id.errors = list(form.ep_id.errors)
        self.assertFalse(form.validate_ep_id(form.ep_id))


if __name__ == "__main__":
    unittest.main(verbosity=2)
