from datetime import date
from flask import render_template, url_for, redirect, abort, current_app
from sqlalchemy import select
from sqlalchemy.exc import MultipleResultsFound
from .. import db
from ..models import Person, Team, Asset, Season, Contract, ContractYear, BuyoutYear
from .person import get_url_person


def buyout_page(form):
    if not form:
        abort(500, "Buyout selection form not found")
    current_app.logger.info(f"/buyout - Rendering")
    return render_template("buyout.html", title="Buyout", css="buyout.css", form=form)


def buyout_results(person_id, season=None):
    person_data, id_type = get_url_person(person_id, "buyout")
    if id_type == "NAMESTUB":
        return redirect(
            url_for(
                "person_route", person_id=person_data.name.replace(" ", "-").lower()
            ),
            code=303,
        )

    data = get_data(person_id, season)
    current_app.logger.info(f"/buyout/PERSON - Rendering for {person_data.name}")
    return render_template(
        "buyout_results.html",
        title=f"Buyout {data['name']}",
        css="buyout.css",
        data=data,
    )


def get_data(person_id, season):
    person_data = Person.query.get(person_id)
    data = {}
    data["name"] = person_data.name
    data["contract"] = get_contract_id(person_data.id)

    season_id = get_season_id(season)
    buyout_seasons = get_seasons(season_id, data["contract"])
    buyout_date = date.today()
    if season_id != current_app.config["CURRENT_SEASON"]:
        season_record = Season.query.get(season_id)
        buyout_date = season_record.draft_date.replace(month=6, day=30)
    birth = person_data.birthdate
    print(f"{season=} {buyout_date=} {birth=}")
    player_age = (
        buyout_date.year
        - birth.year
        - ((buyout_date.month, buyout_date.day) < (birth.month, birth.day))
    )

    buyout_data = get_summary(buyout_seasons, player_age)
    if "error" in buyout_data:
        buyout_data["name"] = person_data.name
        return buyout_data
    data["buyout"] = buyout_data
    buyout_amount = data["buyout"]["yearly_buyout_amount"]
    buyout_years, buyout_savings = get_buyout_years(buyout_seasons, buyout_amount)
    data["buyout"]["buyout_years"] = buyout_years
    data["buyout"]["buyout_savings"] = buyout_savings
    data["buyout"]["bought_out_seasons"] = buyout_seasons
    current_app.logger.info(
        f"[/api]/buyout/PERSON - Gathering data for {person_data.name}"
    )
    return data


def get_contract_id(person_id):
    data = (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .filter(Asset.active == True)
        .filter(Contract.person_id == person_id)
        .with_entities(
            Contract.id,
        )
        .first()
    )
    if not data:
        return None
    return data[0]


def get_season_id(season):
    if season:
        try:
            season = Season.query.filter(Season.name == season).one_or_none()
        except MultipleResultsFound:
            season = Season.query.filter(Season.id == season).one_or_none()
    if not season:  # Nothing provided OR nothing found
        return current_app.config["CURRENT_SEASON"]
    return season.id


def get_buyout_years(buyout_seasons, yearly_buyout_amount):
    buyout_years = [
        {
            "season": contract["season"],
            "season_id": contract["season_id"],
            "original_salary": contract["nhl_salary"],
            "original_cap_hit": contract["cap_hit"],
            "signing_bonus": contract["signing_bonus"],
            "buyout_amount": round(yearly_buyout_amount),
            "buyout_pay": round(yearly_buyout_amount + contract["signing_bonus"]),
            "buyout_savings": round(contract["nhl_salary"] - yearly_buyout_amount),
            "buyout_cap_hit": round(
                contract["cap_hit"] - contract["nhl_salary"] + yearly_buyout_amount
            ),
        }
        for contract in buyout_seasons
    ]
    buyout_season_info = get_buyout_season_info(
        buyout_years[-1]["season"], len(buyout_seasons)
    )
    for season in buyout_season_info:
        buyout_years.append(
            {
                "season": season["name"],
                "season_id": season["id"],
                "original_salary": 0,
                "original_cap_hit": 0,
                "signing_bonus": 0,
                "buyout_amount": round(yearly_buyout_amount),
                "buyout_pay": round(yearly_buyout_amount),
                "buyout_savings": 0,
                "buyout_cap_hit": round(yearly_buyout_amount),
            }
        )
    buyout_savings = (round(sum([year["buyout_savings"] for year in buyout_years])),)
    current_app.logger.info("[/api]/buyout - Calculating individual buyout seasons")
    return buyout_years, buyout_savings[0]


def get_summary(buyout_seasons, player_age):
    # CBA: https://cdn.nhlpa.com/img/assets/file/NHL_NHLPA_2013_CBA.pdf
    #   Page 318
    #   If the player is under 26 years of age at the time the termination is effective: 1/3
    #   Else: 2/3
    #   Of the total fixed amount of the Player's Paragraph 1 NHL Salary, for the unexpired fixed-term of this SPC
    #       reduced by any advance payment of Paragraph 1 Salary received prior to the effective termination date
    #
    #   Page 268 - Treatment of Ordinary Course Buy-Outs for Purposes of Calculating Club Averaged Salary
    #       Buy out amount: sum 1/3 or 2/3 of each remaining season's NHL salary
    #       Buy out amount per year: evenly spread
    #       Buy out "savings" for remaining contract: NHL Salary - yearly buy out amount
    #       Buy out cap hit for remaining contract: Cap Hit - "savings"
    #       Buy out cap hit for post-contract seasons: buy out amount per year
    remaining_salary = sum([year["nhl_salary"] for year in buyout_seasons])
    remaining_seasons = len(buyout_seasons)
    if remaining_seasons == 0:
        return {"error": "Player's contract already expired"}
    buyout_ratio = 2 / 3
    if player_age <= 26:
        buyout_ratio = 1 / 3
    buyout_amount = remaining_salary * buyout_ratio
    yearly_buyout_amount = buyout_amount / (2 * remaining_seasons)
    buyout_data = {
        "remaining_salary": remaining_salary,
        "remaining_signing_bonuses": sum(
            [year["signing_bonus"] for year in buyout_seasons]
        ),
        "remaining_seasons": remaining_seasons,
        "buyout_ratio": round(buyout_ratio, 2),
        "player_age": player_age,
        "buyout_amount": round(buyout_amount),
        "yearly_buyout_amount": round(yearly_buyout_amount),
    }
    current_app.logger.info("[/api]/buyout - Calculating buyout summary")
    return buyout_data


def get_buyout_candidates(team_id=None):
    query = (
        Person.query.with_entities(Person.id, Person.name)
        .order_by(Person.name)
        .join(Contract, Contract.person_id == Person.id)
        .join(Asset, Asset.id == Contract.id)
        .filter(Asset.active == True)
    )
    if team_id:
        query = query.filter(Asset.current_team == team_id)
    raw_people = query.distinct().all()
    if team_id:
        return [str(person[0]) for person in raw_people]
    return [(str(person[0]), person[1]) for person in raw_people]


def get_buyout_form_seasons(person_id):
    raw_seasons = query_buyout_form_seasons(person_id)
    return [season[0] for season in raw_seasons]


def get_seasons(season_id, contract_id):
    raw_seasons = (
        ContractYear.query.join(Season, Season.id == ContractYear.season_id)
        .filter(ContractYear.contract_id == contract_id)
        .filter(ContractYear.season_id > season_id)
        .order_by(Season.free_agency_opening.asc(), Season.id.asc())
        .with_entities(
            Season.name,
            ContractYear.cap_hit,
            ContractYear.aav,
            ContractYear.nhl_salary,
            ContractYear.minors_salary,
            ContractYear.signing_bonus,
            ContractYear.performance_bonuses,
            Season.id,
            ContractYear.id,
        )
        .all()
    )
    buyout_seasons = [
        {
            "season": year[0],
            "cap_hit": year[1],
            "aav": year[2],
            "nhl_salary": year[3],
            "minors_salary": year[4],
            "signing_bonus": year[5],
            "performance_bonuses": year[6],
            "season_id": year[7],
            "id": year[8],
        }
        for year in raw_seasons
        if year[7] > season_id
    ]
    current_app.logger.debug(
        f"[/api]/buyout/person - Getting buyout seasons for contract {contract_id}"
    )
    return buyout_seasons


def get_buyout_season_info(season_name, season_count):
    raw_seasons = query_buyout_seasons(season_name, season_count)
    return [
        {
            "name": season[0],
            "id": season[1],
        }
        for season in raw_seasons
    ]


def query_buyout_seasons(season_name, season_count):
    # select id, name from season where name > "2021-2022" order by name asc limit 1;
    provided = (
        Season.query.filter(Season.name == season_name)
        .with_entities(Season.free_agency_opening)
        .first()
    )
    return (
        Season.query.filter(Season.free_agency_opening > provided[0])
        .order_by(Season.free_agency_opening.asc(), Season.name.asc())
        .with_entities(Season.name, Season.id)
        .limit(season_count)
        .all()
    )


def query_buyout_form_seasons(person_id):
    season_ids = (
        ContractYear.query.join(Contract, Contract.id == ContractYear.contract_id)
        .join(Asset, Asset.id == Contract.id)
        .filter(Asset.active == True)
        .filter(Contract.person_id == person_id)
        .with_entities(ContractYear.season_id)
        .subquery()
    )
    return (
        Season.query.filter(Season.id.in_(select(season_ids)))
        .order_by(Season.free_agency_opening.asc(), Season.name.asc())
        .with_entities(Season.name)
        .limit(9)
        .all()
    )
