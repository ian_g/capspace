from flask import render_template, current_app
from .. import db
from . import bp


@bp.app_errorhandler(400)
def bad_request(error):
    return render_template("errors/400.html", message=error), 400


@bp.app_errorhandler(403)
def not_authorized(error):
    return render_template("errors/403.html", message=error), 403


@bp.app_errorhandler(404)
def not_found(error):
    return render_template("errors/404.html"), 404


@bp.app_errorhandler(500)
def internal_error(error):
    db.session.rollback()
    current_app.logger.error(error)
    return render_template("errors/500.html", message=error), 500
