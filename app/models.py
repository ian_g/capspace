from datetime import date, datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import db, login

#####
# Site Data
#####


class User(UserMixin, db.Model):
    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String, index=True, unique=True)
    email = db.Column(db.String, index=True, unique=True)
    password_hash = db.Column(db.String)
    created = db.Column(db.Date, default=date.today())
    bio = db.Column(db.String)
    last_active = db.Column(db.DateTime, default=datetime.utcnow)
    admin = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return f"<User {self.username}>"

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class Configuration(db.Model):
    __tablename__ = "configuration"

    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String, unique=True)
    value = db.Column(db.String)
    note = db.Column(db.String)


######
# League Data
######


class Team(db.Model):
    __tablename__ = "team"

    id = db.Column(db.Integer, primary_key=True)
    active = db.Column(db.Boolean, default=True, nullable=False)
    name = db.Column(db.String, unique=True)
    abbreviation = db.Column(db.String, unique=True)
    team_name = db.Column(db.String)
    team_location = db.Column(db.String)
    first_year = db.Column(db.Integer)
    general_manager = db.Column(db.String)
    head_coach = db.Column(db.String)
    logo = db.Column(db.String)
    successor = db.Column(db.Integer, db.ForeignKey("team.id"))
    predecessor = db.Column(db.Integer, db.ForeignKey("team.id"))

    def __repr__(self):
        return f"{self.name} ({self.abbreviation})"


class Season(db.Model):
    __tablename__ = "season"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, index=True, unique=True)
    salary_cap = db.Column(db.Integer)
    salary_floor = db.Column(db.Integer)
    minimum_salary = db.Column(db.Integer)
    max_buriable_hit = db.Column(db.Integer, default=0)
    bonus_overage_limit = db.Column(db.Integer)
    free_agency_opening = db.Column(db.Date)
    regular_season_start = db.Column(db.Date)
    regular_season_end = db.Column(db.Date)
    playoffs_start = db.Column(db.Date)
    draft_date = db.Column(db.Date)
    max_elc_salary = db.Column(db.Integer)
    max_elc_minor_salary = db.Column(db.Integer)
    max_elc_performance_bonuses = db.Column(db.Integer)

    def __repr__(self):
        return f"(Season {self.id} - {self.name})"


class TeamSeason(db.Model):
    __tablename__ = "team_season"

    id = db.Column(db.Integer, primary_key=True)
    season_id = db.Column(db.Integer, db.ForeignKey("season.id"))
    team_id = db.Column(db.Integer, db.ForeignKey("team.id"))
    cap_hit = db.Column(db.Integer)
    projected_cap_hit = db.Column(db.Integer)
    standings_points = db.Column(db.Integer)
    contract_count = db.Column(db.Integer)
    retained_contracts = db.Column(db.Integer)
    max_ltir_relief = db.Column(db.Integer, default=0)
    ltir_contracts = db.Column(db.Integer, default=0)
    bonus_overage = db.Column(db.Integer, default=0)
    logo = db.Column(db.String)

    season = db.relationship("Season")
    team = db.relationship("Team")

    def __repr__(self):
        return f"team={self.team_id} season={self.season_id}"


class DailyCap(db.Model):
    __tablename__ = "daily_cap"

    id = db.Column(db.Integer, primary_key=True)
    team_id = db.Column(db.Integer, db.ForeignKey("team.id"), nullable=False)
    season_id = db.Column(db.Integer, db.ForeignKey("season.id"), nullable=False)
    contract_id = db.Column(db.Integer, db.ForeignKey("contract.id"), nullable=False)
    date = db.Column(db.Date, nullable=False)
    non_roster = db.Column(db.Boolean, default=True, nullable=False)
    retained = db.Column(db.Boolean, default=False, nullable=False)
    cap_hit = db.Column(db.Float, nullable=False)
    ir = db.Column(db.Boolean, default=False, nullable=False)
    ltir = db.Column(db.Boolean, default=False, nullable=False)
    soir = db.Column(db.Boolean, default=False, nullable=False)
    on_loan = db.Column(db.Boolean, default=False, nullable=False)
    in_juniors = db.Column(db.Boolean, default=False, nullable=False)
    suspended = db.Column(db.Boolean, default=False, nullable=False)
    emergency_loan = db.Column(db.Boolean, default=False, nullable=False)
    pay = db.Column(db.Float, default=0)

    team = db.relationship("Team")
    season = db.relationship("Season")
    contract = db.relationship("Contract")


class Transaction(db.Model):
    __tablename__ = "transaction"

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, index=True, default=datetime.now)
    season_id = db.Column(db.Integer, db.ForeignKey("season.id"))
    type = db.Column(db.String)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    season = db.relationship("Season")

    def __repr__(self):
        return f"id={self.id} type={self.type}"


class TransactionAsset(db.Model):
    __tablename__ = "transaction_asset"

    id = db.Column(db.Integer, primary_key=True)
    transaction_id = db.Column(db.Integer, db.ForeignKey("transaction.id"))
    asset_id = db.Column(db.Integer, db.ForeignKey("asset.id"))
    former_team = db.Column(db.Integer, db.ForeignKey("team.id"))
    new_team = db.Column(db.Integer, db.ForeignKey("team.id"))
    condition = db.Column(db.String)
    condition_met = db.Column(db.Boolean, default=False)

    transaction = db.relationship("Transaction")
    asset = db.relationship("Asset")
    former = db.relationship("Team", foreign_keys=[former_team])
    new = db.relationship("Team", foreign_keys=[new_team])


class Asset(db.Model):
    __tablename__ = "asset"

    id = db.Column(db.Integer, primary_key=True)
    originating_team = db.Column(db.Integer, db.ForeignKey("team.id"))
    current_team = db.Column(db.Integer, db.ForeignKey("team.id"))
    active = db.Column(db.Boolean, default=True)
    type = db.Column(db.String)

    originating = db.relationship("Team", foreign_keys=[originating_team])
    current = db.relationship("Team", foreign_keys=[current_team])
    draft_pick = db.relationship("DraftPick")
    contract = db.relationship("Contract")


class Person(db.Model):
    __tablename__ = "person"

    id = db.Column(db.Integer, primary_key=True)
    nhl_id = db.Column(db.Integer, unique=True)
    name = db.Column(db.String, index=True)
    birthdate = db.Column(db.Date)
    birthplace = db.Column(db.String)
    nationality = db.Column(db.String)
    position = db.Column(db.String)
    height = db.Column(db.Integer)
    weight = db.Column(db.Integer)
    shoots = db.Column(db.String)
    elc_signing_age = db.Column(db.String)
    waivers_signing_age = db.Column(db.String)
    draft_pick = db.Column(db.Integer, db.ForeignKey("asset.id"))
    arbitration = db.Column(db.Boolean, default=False)
    waivers_exempt = db.Column(db.Boolean, default=False)
    current_status = db.Column(db.String, default="")
    active = db.Column(db.Boolean, default=False)
    rookie = db.Column(db.Boolean, default=False)
    captain = db.Column(db.Boolean, default=False)
    alternate_captain = db.Column(db.Boolean, default=False)
    non_roster = db.Column(db.Boolean, default=True)
    number = db.Column(db.Integer)
    ep_id = db.Column(db.Integer, unique=True)
    hall_of_fame = db.Column(db.Boolean, default=False)
    death_date = db.Column(db.Date)

    contracts = db.relationship("Contract", back_populates="person")
    aliases = db.relationship("Alias", back_populates="person")

    def __repr__(self):
        return f"{self.name} ({self.id})"

    def get_status(self):
        return list(
            filter(None, map(lambda x: x.strip(), self.current_status.split(",")))
        )

    def set_status(self, status_list):
        for item in status_list:
            if "," in item:
                return False
        self.current_status = ",".join(set(status_list))
        return True

    def status_is(self, status):
        statuses = self.get_status()
        return status in statuses


class Alias(db.Model):
    __tablename__ = "alias"

    id = db.Column(db.Integer, primary_key=True)
    person_id = db.Column(db.Integer, db.ForeignKey("person.id"))
    value = db.Column(db.String)
    search = db.Column(db.Boolean, default=False)
    person = db.relationship("Person")

    def __repr__(self):
        return f"{self.value} ({self.person_id})"


class SkaterSeason(db.Model):
    __tablename__ = "skater_season"

    id = db.Column(db.Integer, primary_key=True)
    team_id = db.Column(db.Integer, db.ForeignKey("team.id"))
    person_id = db.Column(db.Integer, db.ForeignKey("person.id"), nullable=False)
    season_id = db.Column(db.Integer, db.ForeignKey("season.id"), nullable=False)
    goals = db.Column(db.Integer, default=0)
    assists = db.Column(db.Integer, default=0)
    points = db.Column(db.Integer, default=0)
    games_played = db.Column(db.Integer, default=0)
    hits = db.Column(db.Integer, default=0)
    shots = db.Column(db.Integer, default=0)
    penalty_minutes = db.Column(db.Integer, default=0)

    person = db.relationship("Person")
    season = db.relationship("Season")
    team = db.relationship("Team")


class GoalieSeason(db.Model):
    __tablename__ = "goalie_season"

    id = db.Column(db.Integer, primary_key=True)
    team_id = db.Column(db.Integer, db.ForeignKey("team.id"))
    person_id = db.Column(db.Integer, db.ForeignKey("person.id"), nullable=False)
    season_id = db.Column(db.Integer, db.ForeignKey("season.id"), nullable=False)
    starts = db.Column(db.Integer, default=0)
    wins = db.Column(db.Integer, default=0)
    losses = db.Column(db.Integer, default=0)
    overtime_losses = db.Column(db.Integer, default=0)
    shutouts = db.Column(db.Integer, default=0)
    shots_against = db.Column(db.Integer, default=0)
    goals_against = db.Column(db.Integer, default=0)
    games_played = db.Column(db.Integer, default=0)

    person = db.relationship("Person")
    season = db.relationship("Season")
    team = db.relationship("Team")


class DraftPick(db.Model):
    __tablename__ = "draft_pick"

    id = db.Column(db.Integer, db.ForeignKey("asset.id"), primary_key=True)
    round = db.Column(db.Integer)
    position = db.Column(db.Integer)
    season_id = db.Column(db.Integer, db.ForeignKey("season.id"))
    person_id = db.Column(db.Integer, db.ForeignKey("person.id"))
    overall = db.Column(db.Integer)

    season = db.relationship("Season")
    person = db.relationship("Person")

    def __repr__(self):
        return f"season={self.season.name} round={self.round} position={self.position}"


class SigningRights(db.Model):
    __tablename__ = "signing_rights"

    id = db.Column(db.Integer, db.ForeignKey("asset.id"), primary_key=True)
    person_id = db.Column(db.Integer, db.ForeignKey("person.id"))
    bonafide_offer = db.Column(db.Boolean)
    qualifying_offer = db.Column(db.Boolean)
    qualifying_salary = db.Column(db.Integer)
    qualifying_minors = db.Column(db.Integer)
    expiration_date = db.Column(db.Date)

    person = db.relationship("Person")


class ProfessionalTryout(db.Model):
    __tablename__ = "pto"

    id = db.Column(db.Integer, db.ForeignKey("asset.id"), primary_key=True)
    person_id = db.Column(db.Integer, db.ForeignKey("person.id"))
    season_id = db.Column(db.Integer, db.ForeignKey("season.id"))
    signing_date = db.Column(db.Date, default=date.today())

    person = db.relationship("Person")
    season = db.relationship("Season")

    def __repr__(self):
        return f"pto.{self.id} - {person.name} - {season.name}"


class Contract(db.Model):
    __tablename__ = "contract"

    id = db.Column(db.Integer, db.ForeignKey("asset.id"), primary_key=True)
    person_id = db.Column(db.Integer, db.ForeignKey("person.id"))
    years = db.Column(db.Integer)
    signing_date = db.Column(db.Date, default=date.today())
    buyout_date = db.Column(db.Date)
    termination_date = db.Column(db.Date)
    total_value = db.Column(db.Integer)
    expiration_status = db.Column(db.String)
    entry_level = db.Column(db.Boolean)
    elc_slide = db.Column(db.Boolean)
    contract_limit_exempt = db.Column(db.Boolean, default=False)
    thirty_five_plus = db.Column(db.Boolean, default=False)
    non_roster = db.Column(db.Boolean, default=False)
    on_loan = db.Column(db.Boolean, default=False)
    in_juniors = db.Column(db.Boolean, default=False)
    extension = db.Column(db.Boolean, default=False)
    signing_season_id = db.Column(db.Integer, db.ForeignKey("season.id"))
    cap_hit_scale_factor = db.Column(db.Float, default=1)
    offer_sheet = db.Column(db.Boolean, default=False)

    contract_years = db.relationship("ContractYear")
    buyout_years = db.relationship("BuyoutYear")
    signing_season = db.relationship("Season")
    person = db.relationship("Person")
    penalties = db.relationship("Penalty")
    retention = db.relationship("RetainedSalary")

    def __repr__(self):
        value = str(round(self.total_value / 1000000, 2)) + "M"
        if self.total_value < 1000000:
            value = str(self.total_value // 1000) + "k"
        return f"{self.id} - ${value}/{self.years} years"


class ContractYear(db.Model):
    __tablename__ = "contract_year"

    id = db.Column(db.Integer, primary_key=True)
    contract_id = db.Column(db.Integer, db.ForeignKey("contract.id"))
    season_id = db.Column(db.Integer, db.ForeignKey("season.id"))
    aav = db.Column(db.Integer)
    nhl_salary = db.Column(db.Integer)
    minors_salary = db.Column(db.Integer)
    performance_bonuses = db.Column(db.Integer, default=0)
    earned_performance_bonuses = db.Column(db.Integer, default=0)
    signing_bonus = db.Column(db.Integer, default=0)
    cap_hit = db.Column(db.Integer)
    bought_out = db.Column(db.Boolean, default=False)
    terminated = db.Column(db.Boolean, default=False)
    two_way = db.Column(db.Boolean)
    entry_level_slide = db.Column(db.Boolean, default=False)
    nmc = db.Column(db.Boolean, default=False)
    ntc = db.Column(db.Boolean, default=False)
    clause_limits = db.Column(db.String)

    contract = db.relationship("Contract", back_populates="contract_years")
    season = db.relationship("Season")

    def __repr__(self):
        return f"({self.id} - contract {self.contract_id} - season {self.season_id} - ${self.cap_hit} cap)"


class BuyoutYear(db.Model):
    __tablename__ = "buyout_year"

    id = db.Column(db.Integer, primary_key=True)
    contract_id = db.Column(db.Integer, db.ForeignKey("contract.id"))
    season_id = db.Column(db.Integer, db.ForeignKey("season.id"))
    signing_bonus = db.Column(db.Integer, default=0)
    nhl_salary = db.Column(db.Integer)
    cap_hit = db.Column(db.Integer)
    compliance = db.Column(db.Boolean, default=False)

    contract = db.relationship("Contract", back_populates="buyout_years")
    season = db.relationship("Season")


class Penalty(db.Model):
    __tablename__ = "penalty"

    id = db.Column(db.Integer, db.ForeignKey("asset.id"), primary_key=True)
    contract_id = db.Column(db.Integer, db.ForeignKey("contract.id"))
    type = db.Column(db.String)
    years = db.Column(db.Integer)
    total_value = db.Column(db.Integer)

    contract = db.relationship("Contract", back_populates="penalties")
    penalty_years = db.relationship("PenaltyYear")


class PenaltyYear(db.Model):
    __tablename__ = "penalty_year"

    id = db.Column(db.Integer, primary_key=True)
    penalty_id = db.Column(db.Integer, db.ForeignKey("penalty.id"))
    season_id = db.Column(db.Integer, db.ForeignKey("season.id"))
    cap_hit = db.Column(db.Integer)

    penalty = db.relationship("Penalty", back_populates="penalty_years")
    season = db.relationship("Season")


class RetainedSalary(db.Model):
    __tablename__ = "retained_salary"

    id = db.Column(db.Integer, db.ForeignKey("asset.id"), primary_key=True)
    contract_id = db.Column(db.Integer, db.ForeignKey("contract.id"))
    retained_by = db.Column(db.Integer, db.ForeignKey("team.id"))
    retained_from = db.Column(db.Date, default=date.today())
    retention = db.Column(db.Float)

    contract = db.relationship("Contract", back_populates="retention")
    retained_by_team = db.relationship("Team")

    def __repr__(self):
        return (
            f"{self.retention}% retained from {self.retained_from.strftime('%Y-%m-%d')}"
        )


class Note(db.Model):
    __tablename__ = "note"

    id = db.Column(db.Integer, primary_key=True)
    record_id = db.Column(db.Integer, index=True)
    record_table = db.Column(db.String, index=True)
    note = db.Column(db.String)
    safe = db.Column(db.Boolean, default=False)


class CachedContent(db.Model):
    __tablename__ = "cached_content"

    id = db.Column(db.Integer, primary_key=True)
    record_id = db.Column(db.Integer, index=True)
    record_table = db.Column(db.String, index=True)
    created = db.Column(db.DateTime, default=datetime.now())
    expiration = db.Column(db.DateTime)
    source = db.Column(db.String)
    content = db.Column(db.String)
