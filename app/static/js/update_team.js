document.getElementById("team_logo").onchange = updatePreview;
updatePreview({"target": document.getElementById("team_logo")});

async function updatePreview(event) {
    console.log(event);
    let preview = document.getElementById("logo-preview");
    preview.innerHTML = "";
    let logoName = event.target.value;
    let logo = document.createElement("img");
    logo.classList.add("team_logo");
    logo.src = `/static/${logoName}`;
    preview.appendChild(logo);

