from flask import current_app
from sqlalchemy import func, Integer, or_
from ..models import TeamSeason, Team, DailyCap, Season, Contract, Person, ContractYear


def get_team_season(team_id, season_id):
    if not season_id:
        season_id = current_app.config["CURRENT_SEASON"]
    raw_data = query_team_season(team_id, season_id)
    if not raw_data:
        return {}
    return {
        "cap_hit": raw_data[0],  # Projected
        "contracts": raw_data[1],
        "retained_contracts": raw_data[2],
        "max_ltir_relief": raw_data[3],
    }


def query_team_season(team_id, season_id):
    return (
        TeamSeason.query.filter(TeamSeason.season_id == season_id)
        .filter(TeamSeason.team_id == team_id)
        .with_entities(
            TeamSeason.projected_cap_hit,
            TeamSeason.contract_count,
            TeamSeason.retained_contracts,
            TeamSeason.max_ltir_relief,
        )
        .first()
    )


def homepage_data():
    query = (
        TeamSeason.query.join(Team, Team.id == TeamSeason.team_id)
        .join(Season, TeamSeason.season_id == Season.id)
        .filter(TeamSeason.season_id == current_app.config["CURRENT_SEASON"])
        .filter(Team.active == True)
        .order_by(TeamSeason.projected_cap_hit.desc())
        .with_entities(
            Team.id,
            Team.name,
            Team.logo,
            TeamSeason.contract_count,
            TeamSeason.projected_cap_hit,
            TeamSeason.cap_hit,
            Team.abbreviation,
            TeamSeason.ltir_contracts,
            TeamSeason.max_ltir_relief,
            Season.salary_cap,
        )
        .all()
    )

    return [
        {
            "id": data[0],
            "name": data[1],
            "logo": data[2],
            "contracts": data[3],
            "projected_cap": data[4],
            "used_cap": data[5],
            "abbreviation": data[6],
            "ltir_contracts": data[7],
            "max_ltir": data[8],
            "salary_cap": data[9],
        }
        for data in query
    ]


def daily_cap_data(team_abbr, season_name=None):
    team_abbr = team_abbr.upper()
    season = Season.query.filter(Season.name == season_name).first()
    if not season:
        season = Season.query.get(current_app.config["CURRENT_SEASON"])
    season_id = season.id
    season_length = (
        Season.query.filter(Season.id == season_id)
        .with_entities(
            func.julianday(Season.regular_season_end)
            - func.julianday(Season.regular_season_start),
        )
        .scalar_subquery()
    )
    query = (
        DailyCap.query.join(Contract, Contract.id == DailyCap.contract_id)
        .join(Person, Person.id == Contract.person_id)
        .join(ContractYear, ContractYear.contract_id == Contract.id)
        .join(Team, Team.id == DailyCap.team_id)
        .filter(ContractYear.season_id == season_id)
        .filter(DailyCap.season_id == season_id)
        .filter(Team.abbreviation == team_abbr)
        .group_by(
            Person.id,
            DailyCap.retained,
        )
        .order_by(
            func.max(DailyCap.retained).asc(),
            Contract.non_roster.asc(),
            DailyCap.cap_hit.desc(),
            ContractYear.cap_hit.desc(),
        )
        .with_entities(
            Person.name,
            Person.id,
            ContractYear.cap_hit,
            Contract.non_roster,
            func.cast(func.sum(DailyCap.cap_hit), Integer),
            DailyCap.retained,
            func.count(DailyCap.id),  # Days in season
            func.sum(func.cast(DailyCap.non_roster, Integer)),
            func.count(DailyCap.id) - func.sum(DailyCap.non_roster),
            func.cast(
                func.round(
                    func.sum(DailyCap.cap_hit) / func.count(DailyCap.id) * season_length
                ),
                Integer,
            ),
            func.sum(func.cast(DailyCap.ir, Integer)),
            func.sum(func.cast(DailyCap.ltir, Integer)),
            func.sum(func.cast(DailyCap.soir, Integer)),
        )
        .all()
    )

    seasons = (
        Season.query.filter(Season.salary_cap > 0)
        .order_by(Season.regular_season_start.asc())
        .with_entities(Season.name)
        .all()
    )
    seasons = [season[0] for season in seasons]

    data = {
        "roster": [
            {
                "person_name": person[0],
                "person_id": person[1],
                "cap_hit": person[2],
                "non_roster": person[3],
                "accumulated_cap": person[4],
                "retained": person[5],
                "total_days": person[6],
                "minors_days": person[7],
                "nhl_days": person[8],
                "projected_cap": person[9],
                "ir_days": person[10],
                "ltir_days": person[11],
                "soir_days": person[12],
                "url_id": f"{person[0].lower().replace(' ', '-')}-{person[1]}",
            }
            for person in query
        ],
        "season": season.name,
        "team": Team.query.filter(Team.abbreviation == team_abbr).first(),
        "seasons": seasons,
    }
    return data
