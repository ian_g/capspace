from datetime import date
from app.models import (
    Team,
    Season,
    Person,
    Asset,
    Contract,
    ContractYear,
    BuyoutYear,
    Penalty,
    PenaltyYear,
)


def create_zach_parise(db):
    person = Person(
        id=779,
        nhl_id=8470610,
        name="Zach Parise",
        birthdate=date.today().replace(year=date.today().year - 35),
        birthplace="Minneapolis, MN, USA",
        nationality="USA",
        position="LW",
        height=71,
        weight=193,
        shoots="L",
        arbitration=False,
        waivers_exempt=False,
        active=True,
        alternate_captain=True,
        captain=False,
        non_roster=False,
        rookie=False,
        number=11,
    )
    db.session.add(person)

    seasons = [
        Season(
            name="2005-2006",
            regular_season_start=date(year=2005, month=10, day=5),
            free_agency_opening=date(year=2005, month=7, day=27),
        ),
        Season(
            name="2006-2007",
            regular_season_start=date(year=2006, month=10, day=10),
            free_agency_opening=date(year=2006, month=7, day=1),
        ),
        Season(
            name="2007-2008",
            regular_season_start=date(year=2007, month=9, day=29),
            free_agency_opening=date(year=2007, month=7, day=1),
        ),
        Season(
            name="2008-2009",
            regular_season_start=date(year=2008, month=10, day=4),
            free_agency_opening=date(year=2008, month=7, day=1),
        ),
        Season(
            name="2009-2010",
            regular_season_start=date(year=2009, month=10, day=1),
            free_agency_opening=date(year=2009, month=7, day=1),
        ),
        Season(
            name="2010-2011",
            regular_season_start=date(year=2010, month=10, day=7),
            free_agency_opening=date(year=2010, month=7, day=1),
        ),
        Season(
            name="2011-2012",
            regular_season_start=date(year=2011, month=10, day=6),
            free_agency_opening=date(year=2011, month=7, day=1),
        ),
        Season(
            name="2012-2013",
            regular_season_start=date(year=2012, month=1, day=19),
            free_agency_opening=date(year=2012, month=7, day=1),
        ),
        Season(
            name="2013-2014",
            regular_season_start=date(year=2013, month=10, day=1),
            free_agency_opening=date(year=2013, month=7, day=1),
        ),
        Season(
            name="2014-2015",
            regular_season_start=date(year=2014, month=10, day=8),
            free_agency_opening=date(year=2014, month=7, day=1),
        ),
        Season(
            name="2015-2016",
            regular_season_start=date(year=2015, month=10, day=7),
            free_agency_opening=date(year=2015, month=7, day=1),
        ),
        Season(
            name="2016-2017",
            regular_season_start=date(year=2016, month=10, day=12),
            free_agency_opening=date(year=2016, month=7, day=1),
        ),
        Season(
            name="2017-2018",
            regular_season_start=date(year=2017, month=10, day=4),
            free_agency_opening=date(year=2017, month=7, day=1),
        ),
        Season(
            name="2018-2019",
            regular_season_start=date(year=2018, month=10, day=3),
            free_agency_opening=date(year=2018, month=7, day=1),
        ),
        Season(
            name="2019-2020",
            regular_season_start=date(year=2019, month=10, day=2),
            free_agency_opening=date(year=2019, month=7, day=1),
        ),
        Season(
            name="2020-2021",
            regular_season_start=date(year=2020, month=1, day=13),
            free_agency_opening=date(year=2020, month=10, day=9),
        ),
        Season(
            name="2021-2022",
            regular_season_start=date(year=2021, month=10, day=12),
            free_agency_opening=date(year=2021, month=7, day=1),
        ),
        Season(
            name="2022-2023",
            regular_season_start=date(year=2022, month=10, day=10),
            free_agency_opening=date(year=2022, month=7, day=1),
        ),
        Season(
            name="2023-2024",
            regular_season_start=date(year=2023, month=10, day=10),
            free_agency_opening=date(year=2023, month=7, day=1),
        ),
        Season(
            name="2024-2025",
            regular_season_start=date(year=2024, month=10, day=10),
            free_agency_opening=date(year=2024, month=7, day=1),
        ),
        Season(
            name="2025-2026",
            regular_season_start=date(year=2025, month=10, day=10),
            free_agency_opening=date(year=2025, month=7, day=1),
        ),
        Season(
            name="2026-2027",
            regular_season_start=date(year=2026, month=10, day=10),
            free_agency_opening=date(year=2026, month=7, day=1),
        ),
        Season(
            name="2027-2028",
            regular_season_start=date(year=2027, month=10, day=10),
            free_agency_opening=date(year=2027, month=7, day=1),
        ),
        Season(
            name="2028-2029",
            regular_season_start=date(year=2028, month=10, day=10),
            free_agency_opening=date(year=2028, month=7, day=1),
        ),
    ]
    for season in seasons:
        db.session.add(season)

    teams = [
        Team(name="New Jersey Devils", abbreviation="NJD"),
        Team(name="Minnesota Wild", abbreviation="MIN"),
        Team(name="New York Islanders", abbreviation="NYI"),
    ]
    for team in teams:
        db.session.add(team)

    db.session.commit()
    assets = [
        Asset(active=False, originating_team=teams[0].id, current_team=teams[0].id),
        Asset(active=False, originating_team=teams[0].id, current_team=teams[0].id),
        Asset(active=False, originating_team=teams[0].id, current_team=teams[0].id),
        Asset(active=False, originating_team=teams[1].id, current_team=teams[1].id),
        Asset(active=True, originating_team=teams[2].id, current_team=teams[2].id),
    ]
    for asset in assets:
        db.session.add(asset)
    db.session.commit()

    contracts = [
        Contract(
            id=assets[0].id,
            person_id=person.id,
            years=3,
            signing_date=date(1900, 6, 6),
            total_value=2331000,
            expiration_status="RFA",
            entry_level=True,
        ),
        Contract(
            id=assets[1].id,
            person_id=person.id,
            years=4,
            signing_date=date(1920, 5, 6),
            expiration_status="RFA",
            total_value=12500000,
        ),
        Contract(
            id=assets[2].id,
            person_id=person.id,
            years=1,
            signing_date=date(1940, 6, 2),
            expiration_status="UFA",
            total_value=6000000,
        ),
        Contract(
            id=assets[3].id,
            person_id=person.id,
            years=13,
            signing_date=date(1960, 11, 6),
            buyout_date=date(1967, 11, 6),
            expiration_status="UFA",
            total_value=98000000,
        ),
        Contract(
            id=assets[4].id,
            person_id=person.id,
            years=1,
            signing_date=date(1980, 1, 6),
            expiration_status="UFA",
            total_value=1500000,
            thirty_five_plus=True,
        ),
    ]
    for contract in contracts:
        db.session.add(contract)
    db.session.commit()

    contract_years = [
        ContractYear(
            contract_id=contracts[0].id,
            season_id=seasons[0].id,
            aav=468667,
            cap_hit=468667,
            bought_out=False,
            minors_salary=0,
            nhl_salary=703000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[0].id,
            season_id=seasons[1].id,
            aav=468667,
            cap_hit=468667,
            bought_out=False,
            minors_salary=50000,
            nhl_salary=703000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[2].id,
            aav=3125000,
            cap_hit=3125000,
            bought_out=False,
            minors_salary=2000000,
            nhl_salary=2000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[3].id,
            aav=3125000,
            cap_hit=3125000,
            bought_out=False,
            minors_salary=2500000,
            nhl_salary=2500000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[4].id,
            aav=3125000,
            cap_hit=3125000,
            bought_out=False,
            minors_salary=3000000,
            nhl_salary=3000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[5].id,
            aav=3125000,
            cap_hit=3125000,
            bought_out=False,
            minors_salary=5000000,
            nhl_salary=5000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[2].id,
            season_id=seasons[6].id,
            aav=6000000,
            cap_hit=6000000,
            bought_out=False,
            minors_salary=6000000,
            nhl_salary=6000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[3].id,
            season_id=seasons[7].id,
            aav=7538462,
            cap_hit=7538462,
            bought_out=False,
            minors_salary=2000000,
            nhl_salary=2000000,
            performance_bonuses=0,
            signing_bonus=10000000,
        ),
        ContractYear(
            contract_id=contracts[3].id,
            season_id=seasons[8].id,
            aav=7538462,
            cap_hit=7538462,
            bought_out=False,
            minors_salary=2000000,
            nhl_salary=2000000,
            performance_bonuses=0,
            signing_bonus=10000000,
        ),
        ContractYear(
            contract_id=contracts[3].id,
            season_id=seasons[9].id,
            aav=7538462,
            cap_hit=7538462,
            bought_out=False,
            minors_salary=6000000,
            nhl_salary=6000000,
            performance_bonuses=0,
            signing_bonus=5000000,
        ),
        ContractYear(
            contract_id=contracts[3].id,
            season_id=seasons[10].id,
            aav=7538462,
            cap_hit=7538462,
            bought_out=False,
            minors_salary=9000000,
            nhl_salary=9000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[3].id,
            season_id=seasons[11].id,
            aav=7538462,
            cap_hit=7538462,
            bought_out=False,
            minors_salary=9000000,
            nhl_salary=9000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[3].id,
            season_id=seasons[12].id,
            aav=7538462,
            cap_hit=7538462,
            bought_out=False,
            minors_salary=9000000,
            nhl_salary=9000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[3].id,
            season_id=seasons[13].id,
            aav=7538462,
            cap_hit=7538462,
            bought_out=False,
            minors_salary=9000000,
            nhl_salary=9000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[3].id,
            season_id=seasons[14].id,
            aav=7538462,
            cap_hit=7538462,
            bought_out=False,
            minors_salary=9000000,
            nhl_salary=9000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[3].id,
            season_id=seasons[15].id,
            aav=7538462,
            cap_hit=7538462,
            bought_out=False,
            minors_salary=8000000,
            nhl_salary=8000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[3].id,
            season_id=seasons[16].id,
            aav=7538462,
            cap_hit=7538462,
            bought_out=True,
            minors_salary=6000000,
            nhl_salary=6000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[3].id,
            season_id=seasons[17].id,
            aav=7538462,
            cap_hit=7538462,
            bought_out=True,
            minors_salary=2000000,
            nhl_salary=2000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[3].id,
            season_id=seasons[18].id,
            aav=7538462,
            cap_hit=7538462,
            bought_out=True,
            minors_salary=1000000,
            nhl_salary=1000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[3].id,
            season_id=seasons[19].id,
            aav=7538462,
            cap_hit=7538462,
            bought_out=True,
            minors_salary=1000000,
            nhl_salary=1000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[4].id,
            season_id=seasons[20].id,
            aav=1500000,
            cap_hit=1500000,
            bought_out=False,
            minors_salary=750000,
            nhl_salary=750000,
            performance_bonuses=750000,
            signing_bonus=0,
        ),
    ]
    for year in contract_years:
        db.session.add(year)

    buyout_years = [
        BuyoutYear(
            contract_id=contracts[3].id,
            season_id=seasons[16].id,
            signing_bonus=0,
            nhl_salary=833333,
            cap_hit=2371794,
        ),
        BuyoutYear(
            contract_id=contracts[3].id,
            season_id=seasons[17].id,
            signing_bonus=0,
            nhl_salary=833333,
            cap_hit=6371794,
        ),
        BuyoutYear(
            contract_id=contracts[3].id,
            season_id=seasons[18].id,
            signing_bonus=0,
            nhl_salary=833333,
            cap_hit=7371794,
        ),
        BuyoutYear(
            contract_id=contracts[3].id,
            season_id=seasons[19].id,
            signing_bonus=0,
            nhl_salary=833333,
            cap_hit=7371794,
        ),
        BuyoutYear(
            contract_id=contracts[3].id,
            season_id=seasons[20].id,
            signing_bonus=0,
            nhl_salary=833333,
            cap_hit=833333,
        ),
        BuyoutYear(
            contract_id=contracts[3].id,
            season_id=seasons[21].id,
            signing_bonus=0,
            nhl_salary=833333,
            cap_hit=833333,
        ),
        BuyoutYear(
            contract_id=contracts[3].id,
            season_id=seasons[22].id,
            signing_bonus=0,
            nhl_salary=833333,
            cap_hit=833333,
        ),
        BuyoutYear(
            contract_id=contracts[3].id,
            season_id=seasons[23].id,
            signing_bonus=0,
            nhl_salary=833333,
            cap_hit=833333,
        ),
    ]
    for year in buyout_years:
        db.session.add(year)
    db.session.commit()
    db_data = {
        "person": person,
        "seasons": seasons,
        "teams": teams,
        "assets": assets,
        "contracts": contracts,
        "contract_years": contract_years,
        "buyout_years": buyout_years,
    }
    return db_data


def create_jared_cowen(db):
    person = Person(
        id=5616,
        nhl_id=8475174,
        name="Jared Cowen",
        birthdate=date.today().replace(year=date.today().year - 25),
        birthplace="Saskatoon, SK, CAN",
        position="D",
        arbitration=False,
        waivers_exempt=False,
        active=False,
        alternate_captain=False,
        captain=False,
        non_roster=True,
        rookie=False,
        number=29,
    )
    db.session.add(person)

    seasons = [
        Season(name="2009-2010", free_agency_opening=date(year=2009, month=7, day=1)),
        Season(name="2010-2011", free_agency_opening=date(year=2010, month=7, day=1)),
        Season(name="2011-2012", free_agency_opening=date(year=2011, month=7, day=1)),
        Season(name="2012-2013", free_agency_opening=date(year=2012, month=7, day=1)),
        Season(name="2013-2014", free_agency_opening=date(year=2013, month=7, day=1)),
        Season(name="2014-2015", free_agency_opening=date(year=2014, month=7, day=1)),
        Season(name="2015-2016", free_agency_opening=date(year=2015, month=7, day=1)),
        Season(name="2016-2017", free_agency_opening=date(year=2016, month=7, day=1)),
        Season(name="2017-2018", free_agency_opening=date(year=2017, month=7, day=1)),
    ]
    for season in seasons:
        db.session.add(season)

    teams = [
        Team(name="Ottawa Senators", abbreviation="OTT"),
        Team(name="Toronto Maple Leafs", abbreviation="TOR"),
    ]
    for team in teams:
        db.session.add(team)
    db.session.commit()

    assets = [
        Asset(
            active=False,
            originating_team=teams[0].id,
            current_team=teams[0].id,
        ),
        Asset(
            active=False,
            originating_team=teams[0].id,
            current_team=teams[1].id,
        ),
    ]
    for asset in assets:
        db.session.add(asset)
    db.session.commit()

    contracts = [
        Contract(
            id=assets[0].id,
            person_id=person.id,
            years=3,
            signing_date=date(1900, 6, 6),
            total_value=3975000,
            expiration_status="RFA",
            entry_level=True,
        ),
        Contract(
            id=assets[1].id,
            person_id=person.id,
            years=4,
            signing_date=date(1920, 2, 2),
            buyout_date=date(1923, 2, 2),
            total_value=12400000,
            expiration_status="RFA",
            entry_level=True,
        ),
    ]
    for contract in contracts:
        db.session.add(contract)

    contract_years = [
        ContractYear(
            contract_id=contracts[0].id,
            season_id=seasons[0].id,
            aav=1325000,
            cap_hit=900000,
            bought_out=False,
            minors_salary=0,
            nhl_salary=0,
            performance_bonuses=0,
            signing_bonus=90000,
            entry_level_slide=True,
        ),
        ContractYear(
            contract_id=contracts[0].id,
            season_id=seasons[1].id,
            aav=1295000,
            cap_hit=870000,
            bought_out=False,
            minors_salary=67500,
            nhl_salary=810000,
            performance_bonuses=425000,
            signing_bonus=90000,
        ),
        ContractYear(
            contract_id=contracts[0].id,
            season_id=seasons[2].id,
            aav=1295000,
            cap_hit=870000,
            bought_out=False,
            minors_salary=67500,
            nhl_salary=810000,
            performance_bonuses=425000,
            signing_bonus=90000,
        ),
        ContractYear(
            contract_id=contracts[0].id,
            season_id=seasons[3].id,
            aav=1295000,
            cap_hit=870000,
            bought_out=False,
            minors_salary=67500,
            nhl_salary=810000,
            performance_bonuses=425000,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[4].id,
            aav=3100000,
            cap_hit=3100000,
            bought_out=False,
            minors_salary=1500000,
            nhl_salary=1500000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[5].id,
            aav=3100000,
            cap_hit=3100000,
            bought_out=False,
            minors_salary=2700000,
            nhl_salary=2700000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[5].id,
            aav=3100000,
            cap_hit=3100000,
            bought_out=False,
            minors_salary=3700000,
            nhl_salary=3700000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[6].id,
            aav=3100000,
            cap_hit=3100000,
            bought_out=True,
            minors_salary=4500000,
            nhl_salary=4500000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
    ]
    for year in contract_years:
        db.session.add(year)

    buyout_years = [
        BuyoutYear(
            contract_id=contracts[1].id,
            season_id=seasons[6].id,
            signing_bonus=0,
            nhl_salary=750000,
            cap_hit=-650000,
        ),
        BuyoutYear(
            contract_id=contracts[1].id,
            season_id=seasons[7].id,
            signing_bonus=0,
            nhl_salary=750000,
            cap_hit=750000,
        ),
    ]
    for year in buyout_years:
        db.session.add(year)
    db.session.commit()
    db_data = {
        "person": person,
        "seasons": seasons,
        "teams": teams,
        "assets": assets,
        "contracts": contracts,
        "contract_years": contract_years,
        "buyout_years": buyout_years,
    }
    return db_data


def create_nathan_gerbe(db):
    person = Person(
        id=7546,
        nhl_id=8471804,
        name="Nathan Gerbe",
        birthdate=date.today().replace(year=date.today().year - 25),
        birthplace="Oxford, MI, USA",
        position="C",
        shoots="L",
        arbitration=False,
        waivers_exempt=False,
        active=True,
        alternate_captain=False,
        captain=False,
        non_roster=True,
        rookie=False,
        number=24,
    )
    db.session.add(person)

    seasons = [
        Season(name="2008-2009", free_agency_opening=date(year=2009, month=7, day=1)),
        Season(name="2009-2010", free_agency_opening=date(year=2009, month=7, day=1)),
        Season(name="2010-2011", free_agency_opening=date(year=2010, month=7, day=1)),
        Season(name="2011-2012", free_agency_opening=date(year=2011, month=7, day=1)),
        Season(name="2012-2013", free_agency_opening=date(year=2012, month=7, day=1)),
        Season(name="2013-2014", free_agency_opening=date(year=2013, month=7, day=1)),
        Season(name="2014-2015", free_agency_opening=date(year=2014, month=7, day=1)),
        Season(name="2015-2016", free_agency_opening=date(year=2015, month=7, day=1)),
        Season(name="2016-2017", free_agency_opening=date(year=2016, month=7, day=1)),
        Season(name="2017-2018", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2018-2019", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2019-2020", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2020-2021", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2021-2022", free_agency_opening=date(year=2017, month=7, day=1)),
    ]
    for season in seasons:
        db.session.add(season)

    teams = [
        Team(name="Buffalo Sabres", abbreviation="BUF"),
        Team(name="Carolina Hurricanes", abbreviation="CAR"),
        Team(name="New York Rangers", abbreviation="NYR"),
        Team(name="Columbus Blue Jackets", abbreviation="CBJ"),
    ]
    for team in teams:
        db.session.add(team)
    db.session.commit()

    assets = [
        Asset(
            active=False,
            originating_team=teams[0].id,
            current_team=teams[0].id,
        ),
        Asset(
            active=False,
            originating_team=teams[0].id,
            current_team=teams[0].id,
        ),
        Asset(
            active=False,
            originating_team=teams[1].id,
            current_team=teams[1].id,
        ),
        Asset(
            active=False,
            originating_team=teams[1].id,
            current_team=teams[1].id,
        ),
        Asset(
            active=False,
            originating_team=teams[2].id,
            current_team=teams[2].id,
        ),
        Asset(
            active=False,
            originating_team=teams[3].id,
            current_team=teams[3].id,
        ),
        Asset(
            active=False,
            originating_team=teams[3].id,
            current_team=teams[3].id,
        ),
        Asset(
            active=False,
            originating_team=teams[3].id,
            current_team=teams[3].id,
        ),
    ]
    for asset in assets:
        db.session.add(asset)
    db.session.flush()

    contracts = [
        Contract(
            id=assets[0].id,
            person_id=person.id,
            years=3,
            signing_date=date(1900, 6, 6),
            total_value=2550000,
            expiration_status="RFA",
            entry_level=True,
        ),
        Contract(
            id=assets[1].id,
            person_id=person.id,
            years=3,
            signing_date=date(1900, 7, 6),
            total_value=4350000,
            expiration_status="RFA",
            entry_level=False,
            buyout_date=date(1900, 8, 6),
        ),
        Contract(
            id=assets[2].id,
            person_id=person.id,
            years=1,
            signing_date=date(1900, 9, 6),
            total_value=550000,
            expiration_status="RFA",
            entry_level=False,
        ),
        Contract(
            id=assets[3].id,
            person_id=person.id,
            years=2,
            signing_date=date(1900, 10, 6),
            total_value=3500000,
            expiration_status="UFA",
            entry_level=False,
            extension=True,
        ),
        Contract(
            id=assets[4].id,
            person_id=person.id,
            years=1,
            signing_date=date(1900, 11, 6),
            total_value=3500000,
            expiration_status="UFA",
            entry_level=False,
            termination_date=date(1900, 12, 6),
        ),
        Contract(
            id=assets[5].id,
            person_id=person.id,
            years=1,
            signing_date=date(1901, 1, 6),
            total_value=750000,
            expiration_status="UFA",
            entry_level=False,
        ),
        Contract(
            id=assets[6].id,
            person_id=person.id,
            years=2,
            signing_date=date(1901, 2, 6),
            total_value=1350000,
            expiration_status="UFA",
            entry_level=False,
            extension=True,
        ),
        Contract(
            id=assets[7].id,
            person_id=person.id,
            years=2,
            signing_date=date(1901, 3, 6),
            total_value=1500000,
            expiration_status="UFA",
            entry_level=False,
        ),
    ]
    for contract in contracts:
        db.session.add(contract)

    contract_years = [
        ContractYear(
            contract_id=contracts[0].id,
            season_id=seasons[0].id,
            aav=850000,
            cap_hit=850000,
            bought_out=False,
            terminated=False,
            minors_salary=62500,
            nhl_salary=765000,
            performance_bonuses=0,
            signing_bonus=85000,
        ),
        ContractYear(
            contract_id=contracts[0].id,
            season_id=seasons[1].id,
            aav=850000,
            cap_hit=850000,
            bought_out=False,
            terminated=False,
            minors_salary=62500,
            nhl_salary=765000,
            performance_bonuses=0,
            signing_bonus=85000,
        ),
        ContractYear(
            contract_id=contracts[0].id,
            season_id=seasons[2].id,
            aav=850000,
            cap_hit=850000,
            bought_out=False,
            terminated=False,
            minors_salary=62500,
            nhl_salary=765000,
            performance_bonuses=0,
            signing_bonus=85000,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[3].id,
            aav=1450000,
            cap_hit=1450000,
            bought_out=False,
            terminated=False,
            minors_salary=0,
            nhl_salary=1250000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[4].id,
            aav=1450000,
            cap_hit=1450000,
            bought_out=False,
            terminated=False,
            minors_salary=0,
            nhl_salary=1250000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[5].id,
            aav=1450000,
            cap_hit=1450000,
            bought_out=True,
            terminated=False,
            minors_salary=0,
            nhl_salary=1250000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[2].id,
            season_id=seasons[5].id,
            aav=550000,
            cap_hit=550000,
            bought_out=False,
            terminated=False,
            minors_salary=0,
            nhl_salary=550000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[3].id,
            season_id=seasons[6].id,
            aav=1750000,
            cap_hit=1750000,
            bought_out=False,
            terminated=False,
            minors_salary=0,
            nhl_salary=1500000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[3].id,
            season_id=seasons[7].id,
            aav=1750000,
            cap_hit=1750000,
            bought_out=False,
            terminated=False,
            minors_salary=0,
            nhl_salary=2000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[4].id,
            season_id=seasons[8].id,
            aav=600000,
            cap_hit=600000,
            bought_out=False,
            terminated=False,
            minors_salary=0,
            nhl_salary=600000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[5].id,
            season_id=seasons[9].id,
            aav=750000,
            cap_hit=750000,
            bought_out=False,
            terminated=False,
            minors_salary=250000,
            nhl_salary=750000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[6].id,
            season_id=seasons[10].id,
            aav=675000,
            cap_hit=675000,
            bought_out=False,
            terminated=False,
            minors_salary=250000,
            nhl_salary=650000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[6].id,
            season_id=seasons[11].id,
            aav=675000,
            cap_hit=675000,
            bought_out=False,
            terminated=False,
            minors_salary=250000,
            nhl_salary=700000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[7].id,
            season_id=seasons[12].id,
            aav=750000,
            cap_hit=750000,
            bought_out=False,
            terminated=False,
            minors_salary=500000,
            nhl_salary=700000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[7].id,
            season_id=seasons[13].id,
            aav=750000,
            cap_hit=750000,
            bought_out=False,
            terminated=False,
            minors_salary=500000,
            nhl_salary=800000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
    ]
    for year in contract_years:
        db.session.add(year)

    buyout_years = [
        BuyoutYear(
            contract_id=contracts[1].id,
            season_id=seasons[5].id,
            signing_bonus=0,
            nhl_salary=308333,
            cap_hit=-91667,
        ),
        BuyoutYear(
            contract_id=contracts[1].id,
            season_id=seasons[6].id,
            signing_bonus=0,
            nhl_salary=308333,
            cap_hit=308333,
        ),
    ]
    for year in buyout_years:
        db.session.add(year)
    db.session.commit()

    db_data = {
        "person": person,
        "seasons": seasons,
        "teams": teams,
        "assets": assets,
        "contracts": contracts,
        "contract_years": contract_years,
        "buyout_years": buyout_years,
    }
    return db_data


def create_mike_richards(db):
    person = Person(
        id=8528,
        nhl_id=8470617,
        name="Mike Richards",
        birthdate=date.today().replace(year=date.today().year - 25),
        birthplace="Kenora, ON, CAN",
        position="C",
        shoots="L",
        arbitration=False,
        waivers_exempt=False,
        active=False,
        alternate_captain=False,
        captain=False,
        non_roster=True,
        rookie=False,
        number=10,
    )
    db.session.add(person)

    seasons = [
        Season(name="2005-2006", free_agency_opening=date(year=2009, month=7, day=1)),
        Season(name="2006-2007", free_agency_opening=date(year=2009, month=7, day=1)),
        Season(name="2007-2008", free_agency_opening=date(year=2009, month=7, day=1)),
        Season(name="2008-2009", free_agency_opening=date(year=2009, month=7, day=1)),
        Season(name="2009-2010", free_agency_opening=date(year=2009, month=7, day=1)),
        Season(name="2010-2011", free_agency_opening=date(year=2010, month=7, day=1)),
        Season(name="2011-2012", free_agency_opening=date(year=2011, month=7, day=1)),
        Season(name="2012-2013", free_agency_opening=date(year=2012, month=7, day=1)),
        Season(name="2013-2014", free_agency_opening=date(year=2013, month=7, day=1)),
        Season(name="2014-2015", free_agency_opening=date(year=2014, month=7, day=1)),
        Season(name="2015-2016", free_agency_opening=date(year=2015, month=7, day=1)),
        Season(name="2016-2017", free_agency_opening=date(year=2016, month=7, day=1)),
        Season(name="2017-2018", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2018-2019", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2019-2020", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2020-2021", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2021-2022", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2022-2023", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2023-2024", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2024-2025", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2025-2026", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2026-2027", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2027-2028", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2028-2029", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2029-2030", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2030-2031", free_agency_opening=date(year=2017, month=7, day=1)),
        Season(name="2031-2032", free_agency_opening=date(year=2017, month=7, day=1)),
    ]
    for season in seasons:
        db.session.add(season)

    teams = [
        Team(name="Philadelphia Flyers", abbreviation="PHI"),
        Team(name="Los Angeles Kings", abbreviation="LAK"),
        Team(name="Washington Capitals", abbreviation="WSH"),
    ]
    for team in teams:
        db.session.add(team)
    db.session.flush()

    assets = [
        Asset(
            active=False,
            originating_team=teams[0].id,
            current_team=teams[0].id,
        ),
        Asset(
            active=False,
            originating_team=teams[0].id,
            current_team=teams[1].id,
        ),
        Asset(
            active=False,
            originating_team=teams[2].id,
            current_team=teams[2].id,
        ),
        Asset(
            active=False,
            originating_team=teams[1].id,
            current_team=teams[1].id,
        ),
        Asset(
            active=False,
            originating_team=teams[1].id,
            current_team=teams[1].id,
        ),
    ]
    for asset in assets:
        db.session.add(asset)
    db.session.commit()

    contracts = [
        Contract(
            id=assets[0].id,
            person_id=person.id,
            years=3,
            signing_date=date(1900, 1, 6),
            total_value=2827200,
            expiration_status="RFA",
            entry_level=True,
        ),
        Contract(
            id=assets[1].id,
            person_id=person.id,
            years=12,
            signing_date=date(1900, 2, 6),
            total_value=69000000,
            expiration_status="UFA",
            entry_level=False,
            termination_date=date(1900, 3, 6),
        ),
        Contract(
            id=assets[2].id,
            person_id=person.id,
            years=1,
            signing_date=date(1900, 4, 6),
            total_value=1000000,
            expiration_status="UFA",
            entry_level=False,
        ),
    ]
    for contract in contracts:
        db.session.add(contract)

    penalties = [
        Penalty(
            id=assets[3].id,
            contract_id=contracts[1].id,
            type="Cap Recapture",
            years=5,
            total_value=6600000,
        ),
        Penalty(
            id=assets[4].id,
            contract_id=contracts[1].id,
            type="Termination Fees",
            years=17,
            total_value=10500000,
        ),
    ]
    for penalty in penalties:
        db.session.add(penalty)

    contract_years = [
        ContractYear(
            contract_id=contracts[0].id,
            season_id=seasons[0].id,
            aav=942400,
            cap_hit=942400,
            bought_out=False,
            terminated=False,
            minors_salary=70300,
            nhl_salary=659700,
            performance_bonuses=0,
            signing_bonus=282700,
        ),
        ContractYear(
            contract_id=contracts[0].id,
            season_id=seasons[1].id,
            aav=942400,
            cap_hit=942400,
            bought_out=False,
            terminated=False,
            minors_salary=70300,
            nhl_salary=800000,
            performance_bonuses=0,
            signing_bonus=282700,
        ),
        ContractYear(
            contract_id=contracts[0].id,
            season_id=seasons[2].id,
            aav=942400,
            cap_hit=942400,
            bought_out=False,
            terminated=False,
            minors_salary=70300,
            nhl_salary=800000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[3].id,
            aav=5750000,
            cap_hit=5750000,
            bought_out=False,
            terminated=False,
            minors_salary=5400000,
            nhl_salary=5400000,
            performance_bonuses=0,
            signing_bonus=0,
            nmc=False,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[4].id,
            aav=5750000,
            cap_hit=5750000,
            bought_out=False,
            terminated=False,
            minors_salary=5600000,
            nhl_salary=5600000,
            performance_bonuses=0,
            signing_bonus=0,
            nmc=False,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[5].id,
            aav=5750000,
            cap_hit=5750000,
            bought_out=False,
            terminated=False,
            minors_salary=6400000,
            nhl_salary=6400000,
            performance_bonuses=0,
            signing_bonus=0,
            nmc=False,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[6].id,
            aav=5750000,
            cap_hit=5750000,
            bought_out=False,
            terminated=False,
            minors_salary=6600000,
            nhl_salary=6600000,
            performance_bonuses=0,
            signing_bonus=0,
            nmc=False,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[7].id,
            aav=5750000,
            cap_hit=5750000,
            bought_out=False,
            terminated=False,
            minors_salary=8400000,
            nhl_salary=8400000,
            performance_bonuses=0,
            signing_bonus=0,
            nmc=True,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[8].id,
            aav=5750000,
            cap_hit=5750000,
            bought_out=False,
            terminated=False,
            minors_salary=7600000,
            nhl_salary=7600000,
            performance_bonuses=0,
            signing_bonus=0,
            nmc=True,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[9].id,
            aav=5750000,
            cap_hit=5750000,
            bought_out=False,
            terminated=False,
            minors_salary=7000000,
            nhl_salary=7000000,
            performance_bonuses=0,
            signing_bonus=0,
            nmc=True,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[10].id,
            aav=5750000,
            cap_hit=5750000,
            bought_out=False,
            terminated=True,
            minors_salary=6000000,
            nhl_salary=6000000,
            performance_bonuses=0,
            signing_bonus=0,
            nmc=True,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[11].id,
            aav=5750000,
            cap_hit=5750000,
            bought_out=False,
            terminated=True,
            minors_salary=5500000,
            nhl_salary=5500000,
            performance_bonuses=0,
            signing_bonus=0,
            nmc=True,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[12].id,
            aav=5750000,
            cap_hit=5750000,
            bought_out=False,
            terminated=True,
            minors_salary=4500000,
            nhl_salary=4500000,
            performance_bonuses=0,
            signing_bonus=0,
            nmc=True,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[13].id,
            aav=5750000,
            cap_hit=5750000,
            bought_out=False,
            terminated=True,
            minors_salary=3000000,
            nhl_salary=3000000,
            performance_bonuses=0,
            signing_bonus=0,
            nmc=True,
        ),
        ContractYear(
            contract_id=contracts[1].id,
            season_id=seasons[14].id,
            aav=5750000,
            cap_hit=5750000,
            bought_out=False,
            terminated=True,
            minors_salary=3000000,
            nhl_salary=3000000,
            performance_bonuses=0,
            signing_bonus=0,
            nmc=True,
        ),
        ContractYear(
            contract_id=contracts[2].id,
            season_id=seasons[15].id,
            aav=1000000,
            cap_hit=1000000,
            bought_out=False,
            terminated=True,
            minors_salary=1000000,
            nhl_salary=1000000,
            performance_bonuses=0,
            signing_bonus=0,
        ),
    ]
    for year in contract_years:
        db.session.add(year)

    penalty_years = [
        PenaltyYear(
            penalty_id=assets[3].id,
            season_id=seasons[10].id,
            cap_hit=1320000,
        ),
        PenaltyYear(
            penalty_id=assets[3].id,
            season_id=seasons[11].id,
            cap_hit=1320000,
        ),
        PenaltyYear(
            penalty_id=assets[3].id,
            season_id=seasons[12].id,
            cap_hit=1320000,
        ),
        PenaltyYear(
            penalty_id=assets[3].id,
            season_id=seasons[13].id,
            cap_hit=1320000,
        ),
        PenaltyYear(
            penalty_id=assets[3].id,
            season_id=seasons[14].id,
            cap_hit=1320000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[10].id,
            cap_hit=1800000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[11].id,
            cap_hit=250000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[12].id,
            cap_hit=250000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[13].id,
            cap_hit=250000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[14].id,
            cap_hit=250000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[15].id,
            cap_hit=700000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[16].id,
            cap_hit=900000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[17].id,
            cap_hit=900000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[18].id,
            cap_hit=700000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[19].id,
            cap_hit=700000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[20].id,
            cap_hit=600000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[21].id,
            cap_hit=600000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[22].id,
            cap_hit=600000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[23].id,
            cap_hit=600000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[24].id,
            cap_hit=500000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[25].id,
            cap_hit=500000,
        ),
        PenaltyYear(
            penalty_id=assets[4].id,
            season_id=seasons[26].id,
            cap_hit=400000,
        ),
    ]
    for year in penalty_years:
        db.session.add(year)
    db.session.commit()

    db_data = {
        "person": person,
        "seasons": seasons,
        "teams": teams,
        "assets": assets,
        "contracts": contracts,
        "contract_years": contract_years,
        "penalties": penalties,
        "penalty_years": penalty_years,
    }
    return db_data
