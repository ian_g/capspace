import sys
import random
import unittest
from datetime import datetime, date
from app import create_app, db
from app.models import Season

from app.main import season

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class SeasonTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def create_seasons(self):
        to_create = random.randint(5, 20)
        season_data = []
        year = 1000
        for _ in range(to_create):
            year = random.randint(year, year + 100)
            data = {
                "name": "".join(random.choices(LETTERS, k=9)),
                "salary_cap": random.randint(1, 100000000),
                "salary_floor": random.randint(1, 50000000),
                "minimum_salary": random.randint(1, 1500000),
                "max_buriable_hit": random.randint(1, 2000000),
                "bonus_overage_limit": random.randint(1, 10000000),
                "draft_date": datetime.strptime(
                    f"{year}-{random.randint(1,12):02}-{random.randint(1,28):02}",
                    self.app.config["DATE_FMT"],
                ).date(),
            }
            if random.randint(1, 100) > 90:
                del data["draft_date"]
            season_data.append(data)
            season_row = Season(**data)
            db.session.add(season_row)
            db.session.commit()
            data["id"] = season_row.id
        return season_data

    def test_get_draft_years(self):
        data = self.create_seasons()
        draft_years = [
            season["draft_date"].year for season in data if "draft_date" in season
        ]
        draft_years.sort()
        queried_draft_years = season.get_draft_years()
        self.assertEqual(len(draft_years), len(queried_draft_years))
        for idx in range(len(draft_years)):
            self.assertEqual(draft_years[idx], queried_draft_years[idx])

    def test_get_current_draft_year(self):
        season_record = Season(
            name="Test Season",
            salary_cap=12,
            salary_floor=1000,
            minimum_salary=1500,
            max_buriable_hit=0,
            bonus_overage_limit=12,
        )
        db.session.add(season_record)
        db.session.commit()
        queried_year = season.get_current_draft_year()
        self.assertIsNone(queried_year)
        season_record.draft_date = date.today()
        db.session.add(season_record)
        db.session.commit()
        queried_year = season.get_current_draft_year()
        self.assertEqual(date.today().year, queried_year)
        # current_draft_year = None
        # for season_row in data:
        #    if season_row["id"] == self.app.config["CURRENT_SEASON"] and "draft_date" in season_row:
        #        current_draft_year = season_row["draft_date"].year
        # queried_year = season.get_current_draft_year()
        # self.assertEqual(current_draft_year, queried_year)

    def test_get_season_id_from_draft_year(self):
        data = self.create_seasons()
        test_season = {}
        while "draft_date" not in test_season:
            test_season = random.choice(data)
        queried_id = season.get_season_id_from_draft_year(
            test_season["draft_date"].year
        )
        self.assertEqual(queried_id, test_season["id"])

        draft_years = [
            season["draft_date"].year for season in data if "draft_date" in season
        ]
        test_year = 1993
        while test_year in draft_years:
            test_year = random.randint(1000, 2000)
        queried_id = season.get_season_id_from_draft_year(test_year)
        self.assertIsNone(queried_id)

    def test_get_draft_years2(self):
        data = self.create_seasons()
        delete_these = []
        for idx in range(len(data)):
            if "draft_date" not in data[idx]:
                delete_these.append(idx)
        while len(delete_these):
            del data[delete_these.pop()]
        data.sort(key=lambda season: season["draft_date"].year)
        queried = season.get_draft_years()
        for idx in range(len(queried)):
            self.assertEqual(queried[idx], data[idx]["draft_date"].year)

    def test_get_season_data(self):
        season_id = 2
        self.assertEqual(season.get_season_data(season_id), {})

        db.session.add(
            Season(
                id=season_id,
                name="Test Season",
                salary_cap=10,
                salary_floor=500,
                minimum_salary=4,
                max_buriable_hit=1,
                bonus_overage_limit=2,
            )
        )
        data = season.get_season_data(season_id)
        self.assertEqual(data["salary_cap"], 10)
        self.assertEqual(data["salary_floor"], 500)
        self.assertEqual(data["minimum_salary"], 4)
        self.assertEqual(data["max_buriable_hit"], 1)
        self.assertEqual(data["bonus_overage_limit"], 2)

        db.session.commit()
        data = season.get_season_data(season_id)
        self.assertEqual(data["salary_cap"], 10)
        self.assertEqual(data["salary_floor"], 500)
        self.assertEqual(data["minimum_salary"], 4)
        self.assertEqual(data["max_buriable_hit"], 1)
        self.assertEqual(data["bonus_overage_limit"], 2)


if __name__ == "__main__":
    unittest.main(verbosity=2)
