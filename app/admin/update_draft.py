from datetime import date, timedelta
from sqlalchemy import or_, extract
from sqlalchemy.orm import aliased
from flask import (
    render_template,
    url_for,
    redirect,
    request,
    session,
    abort,
    jsonify,
    current_app,
)
from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField, IntegerField
from wtforms.validators import DataRequired

from .. import db
from ..models import (
    Asset,
    DraftPick,
    Team,
    Person,
    Season,
    Note,
    TeamSeason,
    SigningRights,
    Transaction,
    TransactionAsset,
)
from .notes import get_notes


def draft_page(year=None):
    default_season = year or get_current_draft_year()
    picks = get_draft_picks(default_season)
    season_id = query_season_id(default_season)
    if season_id is not None:
        season = season_id
    logos = get_logos(season)
    data = {
        "picks": picks,
        "logos": logos,
    }
    data["teams"] = sorted(list(set([pick["team"] for pick in data["picks"]])))
    season_picker = SelectDraftSeason()
    season_picker.season.data = str(default_season)
    return render_template(
        "admin/update_draft.html",
        title=f"Update Draft {year}",
        css="draft.css",
        data=data,
        form=season_picker,
        compensatory=not year,
    )


def add_compensatory_pick(team, position):
    team_record = Team.query.filter(Team.abbreviation == team).first()
    if not team_record:
        return f"Invalid team: {team}"
    picks = (
        DraftPick.query.filter(
            DraftPick.season_id == current_app.config["DRAFT_SEASON"]
        )
        .filter(DraftPick.round == 2)
        .filter(DraftPick.position >= position)
        .all()
    )
    for pick in picks:
        pick.position += 1
        db.session.add(pick)
    asset = Asset(
        current_team=team_record.id,
        originating_team=team_record.id,
        type="Draft Pick",
        active=True,
    )
    db.session.add(asset)
    db.session.flush()
    draft_pick = DraftPick(
        id=asset.id,
        round=2,
        position=position,
        season_id=current_app.config["DRAFT_SEASON"],
    )
    db.session.add(draft_pick)
    db.session.add(
        Note(
            record_table="draft_pick",
            record_id=asset.id,
            safe=False,
            note='Compensatory pick for <a href="/person/name-id" target="_blank">UPDATE THIS</a> (unsigned first rounder)',
        )
    )
    db.session.commit()


def set_name(pick_id, person_id):
    draftee = Person.query.filter(Person.id == person_id).first()
    if not draftee:
        db.session.rollback()
        return {"error": f"Expected: 1 update. Actual: 0 updates"}

    draftee.draft_pick = pick_id
    draftee.active = True
    status = draftee.get_status()
    status.append("Drafted")
    draftee.set_status(status)
    db.session.add(draftee)

    pick = DraftPick.query.get(pick_id)
    if not pick:
        db.session.rollback()
        return {"error": "Pick not found"}
    pick.person_id = person_id

    asset = Asset.query.filter(Asset.id == pick_id).first()
    asset.active = False
    db.session.add(asset)
    db.session.add(pick)
    create_transaction(pick)
    create_signing_rights(pick_id, person_id)

    db.session.commit()
    return {}


def create_transaction(pick):
    season = Season.query.get(pick.season_id)
    asset = Asset.query.get(pick.id)
    transaction = Transaction(
        season_id=pick.season_id,
        date=season.draft_date,
        type="Drafted",
    )
    if pick.round != 1:
        transaction.date += timedelta(days=1)
    db.session.add(transaction)
    db.session.flush()

    transaction_asset = TransactionAsset(
        transaction_id=transaction.id,
        asset_id=pick.id,
        former_team=asset.current_team,
        new_team=asset.current_team,
    )
    db.session.add(transaction_asset)


def create_signing_rights(pick_id, person_id):
    exp_year = (
        Season.query.get(DraftPick.query.get(pick_id).season_id).draft_date.year + 2
    )
    expiration = date(year=exp_year, month=6, day=1)
    if expiration < date.today():
        return

    pick_asset = Asset.query.filter(Asset.id == pick_id).first()
    asset = Asset(
        current_team=pick_asset.current_team,
        originating_team=pick_asset.current_team,
        active=True,
        type="Signing Rights",
    )
    db.session.add(asset)
    db.session.flush()

    rights = SigningRights(
        id=asset.id,
        person_id=person_id,
        expiration_date=expiration,
    )
    db.session.add(rights)


def validate_name(name):
    name_count = Person.query.filter(
        or_(
            Person.id == name,
            Person.nhl_id == name,
            Person.name == name,
        )
    ).count()
    if name_count != 1:
        return {"error": f"Identifier {name} belongs to {name_count} people"}
    person = Person.query.filter(
        or_(
            Person.id == name,
            Person.nhl_id == name,
            Person.name == name,
        )
    ).first()
    return {"name": person.name, "id": person.id, "nhl_id": person.nhl_id}


def get_pick_position(pick_id):
    pick = DraftPick.query.filter(DraftPick.id == pick_id).first()
    if not pick:
        return {"error": "No pick"}
    return {"position": pick.position or "", "round": pick.round or ""}


def set_pick_position(pick_id, draft_round, position):
    current_app.logger.debug(
        f"Update draft pick inputs: {pick_id=} {draft_round=} {position=}"
    )
    season_id = DraftPick.query.filter(DraftPick.id == pick_id).first().season_id
    position_taken = (
        DraftPick.query.filter(DraftPick.position == position)
        .filter(DraftPick.round == draft_round)
        .filter(DraftPick.season_id == season_id)
        .count()
    )
    if position_taken > 0:
        return "Draft pick position already occupied"
    value = DraftPick.query.filter(DraftPick.id == pick_id).update(
        {"position": int(position)}
    )
    db.session.commit()
    return True


def get_current_draft_year():
    season = Season.query.get(
        current_app.config["DRAFT_SEASON"] or current_app.config["CURRENT_SEASON"]
    )
    if season:
        return season.draft_date.year
    return (
        Season.query.filter(Season.draft_date >= date.today())
        .with_entities(
            Season.draft_date,
        )
        .order_by(Season.draft_date.asc())
        .first()
        .draft_date.year
    )


def get_logos(season_id=None):
    current_logos = Team.query.with_entities(Team.abbreviation, Team.logo).all()
    logos = {row[0]: row[1] for row in current_logos}
    if season_id:
        season_logos = (
            TeamSeason.query.join(Team, Team.id == TeamSeason.team_id)
            .join(Season, Season.id == TeamSeason.season_id)
            .filter(Season.id == season_id)
            .with_entities(Team.abbreviation, TeamSeason.logo, Team.logo)
            .all()
        )
        for entry in season_logos:
            if entry[1]:
                logos[entry[0]] = entry[1]
    return logos


def get_draft_picks(year=None):
    season = current_app.config["CURRENT_SEASON"]
    if year:
        season_id = query_season_id(year)
        if season_id is not None:
            season = season_id
    raw_picks = query_draft_picks(season)
    picks = [
        {
            "round": pick[0],
            "position": pick[1],
            "team": pick[3] or "NHL",
            "original": pick[4] if pick[4] != pick[3] else None,
            "player": pick[5],
            "note": [item["note"] for item in get_notes("draft_pick", pick[2])],
            "person_id": pick[6],
            "id": pick[2],
        }
        for pick in raw_picks
    ]
    return picks


def query_season_id(year):
    season_id = (
        Season.query.filter(extract("year", Season.draft_date) == year)
        .order_by(Season.id)
        .with_entities(Season.id)
        .first()
    )
    if not season_id:
        current_app.logger.debug("\tSeason ID: None")
        return None
    current_app.logger.debug(f"\tSeason ID: {season_id[0]}")
    return season_id[0]


def query_draft_picks(season_id):
    owner = aliased(Team)
    creator = aliased(Team)
    return (
        DraftPick.query.join(Asset, Asset.id == DraftPick.id)
        .join(creator, creator.id == Asset.originating_team, isouter=True)
        .join(owner, owner.id == Asset.current_team, isouter=True)
        .join(Person, Person.id == DraftPick.person_id, isouter=True)
        .filter(DraftPick.season_id == season_id)
        .order_by(DraftPick.round.asc())
        .order_by(DraftPick.position.asc().nullslast())
        .order_by(creator.abbreviation.asc())
        .with_entities(
            DraftPick.round,
            DraftPick.position,
            DraftPick.id,
            owner.abbreviation,
            creator.abbreviation,
            Person.name,
            Person.id,
        )
        .all()
    )


class SelectDraftSeason(FlaskForm):
    def __init__(self):
        super().__init__()
        self.season.choices = self.get_draft_years()

    def get_draft_years(self):
        max_year = date.today().year + 4
        draft_years = (
            Season.query.with_entities(
                extract("year", Season.draft_date),
            )
            .filter(Season.draft_date <= date.today().replace(year=max_year))
            .order_by(Season.draft_date.asc())
            .all()
        )

        return list(map(lambda year: year[0], draft_years))

    season = SelectField("Season", validators=[DataRequired()], id="season")
    submit = SubmitField("Select Draft Year")
