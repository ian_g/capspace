import sys
import random
import unittest
from datetime import datetime, date
from app import create_app, db
from app.models import Transaction, TransactionAsset, Asset, Contract, Person, Team

from app.main import transaction

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class TransactionTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app.config.update(SERVER_NAME="127.0.0.1")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_transaction_data(self):
        TEAM = 38
        PERSON = 378
        CONTRACT = 488
        team = Team(id=TEAM, abbreviation="TEST", name="Test Team", logo="looogo")
        person = Person(id=PERSON, name="Test Person")
        asset = Asset(
            id=CONTRACT, originating_team=team.id, current_team=team.id, type="Contract"
        )
        contract = Contract(id=CONTRACT, person_id=PERSON)
        transactions = [
            Transaction(
                id=88, type="Waivers", date=datetime(year=2020, month=1, day=1)
            ),
            Transaction(id=98, type="AHL", date=datetime(year=2021, month=1, day=1)),
            Transaction(
                id=48, type="Loaned to CHL", date=datetime(year=2022, month=1, day=1)
            ),
        ]
        transaction_assets = [
            TransactionAsset(
                transaction_id=transactions[0].id,
                asset_id=CONTRACT,
                former_team=TEAM,
                new_team=TEAM,
            ),
            TransactionAsset(
                transaction_id=transactions[1].id,
                asset_id=CONTRACT,
                former_team=TEAM,
                new_team=TEAM,
            ),
            TransactionAsset(
                transaction_id=transactions[2].id,
                asset_id=CONTRACT,
                former_team=TEAM,
                new_team=TEAM,
            ),
        ]
        for record in (
            transactions + transaction_assets + [asset, team, person, contract]
        ):
            db.session.add(record)
        db.session.commit()

        # No restrictions
        data = transaction.transaction_data()
        self.assertEqual(len(data), 3)
        self.assertEqual(data[2]["type"], "Waivers")
        self.assertEqual(data[2]["team_id"], TEAM)
        self.assertEqual(data[2]["player_url"], "test-person-378")
        self.assertEqual(data[2]["date"], "2020-01-01")
        self.assertEqual(data[1]["type"], "AHL")
        self.assertEqual(data[1]["team_id"], TEAM)
        self.assertEqual(data[1]["player_url"], "test-person-378")
        self.assertEqual(data[1]["date"], "2021-01-01")
        self.assertEqual(data[0]["type"], "Loaned to CHL")
        self.assertEqual(data[0]["team_id"], TEAM)
        self.assertEqual(data[0]["player_url"], "test-person-378")
        self.assertEqual(data[0]["date"], "2022-01-01")

        # After june 2020
        data = transaction.transaction_data(0, "2020-06-01")
        self.assertEqual(data[1]["type"], "AHL")
        self.assertEqual(data[1]["team_id"], TEAM)
        self.assertEqual(data[1]["player_url"], "test-person-378")
        self.assertEqual(data[1]["date"], "2021-01-01")
        self.assertEqual(data[0]["type"], "Loaned to CHL")
        self.assertEqual(data[0]["team_id"], TEAM)
        self.assertEqual(data[0]["player_url"], "test-person-378")
        self.assertEqual(data[0]["date"], "2022-01-01")

        # Before June 2021
        data = transaction.transaction_data(0, None, "2021-06-01")
        self.assertEqual(data[1]["type"], "Waivers")
        self.assertEqual(data[1]["team_id"], TEAM)
        self.assertEqual(data[1]["player_url"], "test-person-378")
        self.assertEqual(data[1]["date"], "2020-01-01")
        self.assertEqual(data[0]["type"], "AHL")
        self.assertEqual(data[0]["team_id"], TEAM)
        self.assertEqual(data[0]["player_url"], "test-person-378")
        self.assertEqual(data[0]["date"], "2021-01-01")

        # Between June 2020 and June 2021
        data = transaction.transaction_data(0, "2020-06-01", "2021-06-01")
        self.assertEqual(data[0]["type"], "AHL")
        self.assertEqual(data[0]["team_id"], TEAM)
        self.assertEqual(data[0]["player_url"], "test-person-378")
        self.assertEqual(data[0]["date"], "2021-01-01")

        # Offset once
        data = transaction.transaction_data(1)
        self.assertEqual(data[1]["type"], "Waivers")
        self.assertEqual(data[1]["team_id"], TEAM)
        self.assertEqual(data[1]["player_url"], "test-person-378")
        self.assertEqual(data[1]["date"], "2020-01-01")
        self.assertEqual(data[0]["type"], "AHL")
        self.assertEqual(data[0]["team_id"], TEAM)
        self.assertEqual(data[0]["player_url"], "test-person-378")
        self.assertEqual(data[0]["date"], "2021-01-01")

        # Between June 2020 and June 2021, offset once
        data = transaction.transaction_data(1, "2020-06-01", "2021-06-01")
        self.assertEqual(data, [])

    def test_transaction_rows(self):
        TEAM = 38
        PERSON = 378
        CONTRACT = 488
        team = Team(id=TEAM, abbreviation="TEST", name="Test Team", logo="looogo")
        person = Person(id=PERSON, name="Test Person")
        asset = Asset(
            id=CONTRACT, originating_team=team.id, current_team=team.id, type="Contract"
        )
        contract = Contract(id=CONTRACT, person_id=PERSON)
        transactions = [
            Transaction(
                id=88, type="Waivers", date=datetime(year=2020, month=1, day=1)
            ),
            Transaction(id=98, type="AHL", date=datetime(year=2021, month=1, day=1)),
        ]
        transaction_assets = [
            TransactionAsset(
                transaction_id=transactions[0].id,
                asset_id=CONTRACT,
                former_team=TEAM,
                new_team=TEAM,
            ),
            TransactionAsset(
                transaction_id=transactions[1].id,
                asset_id=CONTRACT,
                former_team=TEAM,
                new_team=TEAM,
            ),
        ]
        for record in (
            transactions + transaction_assets + [asset, team, person, contract]
        ):
            db.session.add(record)
        db.session.commit()

        data = "".join(transaction.transaction_rows(0).splitlines())
        data = "".join(data.split("    "))
        output = '<tr class="team_38"><td><span>2021-01-01</span></td>'
        output += '<td><a class="team" href="http://127.0.0.1/team/test">'
        output += '<img class="logo" alt="Test Team logo" src="http://127.0.0.1/static/looogo"/>'
        output += '<div class="team-name">Test Team</div></a></td>'
        output += '<td><a href="http://127.0.0.1/person/test-person-378">'
        output += 'Test Person</a></td><td>AHL</td><td class="note"></td></tr>'
        output += '<tr class="team_38"><td><span>2020-01-01</span></td>'
        output += '<td><a class="team" href="http://127.0.0.1/team/test">'
        output += '<img class="logo" alt="Test Team logo" src="http://127.0.0.1/static/looogo"/>'
        output += '<div class="team-name">Test Team</div></a></td>'
        output += '<td><a href="http://127.0.0.1/person/test-person-378">'
        output += 'Test Person</a></td><td>Waivers</td><td class="note"></td></tr>'
        output2 = output.replace("http://127.0.0.1", "")
        self.assertTrue(data in [output, output2])


if __name__ == "__main__":
    unittest.main(verbosity=2)
