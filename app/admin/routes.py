import json
import urllib.request
from datetime import date
from flask import (
    render_template,
    url_for,
    redirect,
    request,
    session,
    abort,
    jsonify,
    current_app,
    flash,
    Response,
)
from flask_login import current_user

from .. import db
from . import bp
from .create_person import CreatePerson
from .update_person import UpdatePerson, validate_person
from .create_contract import (
    CreateContract,
    CreatePTO,
    get_extendable,
    get_inactive,
    get_unsigned,
    get_rights,
)
from .update_draft import (
    SelectDraftSeason,
    draft_page,
    add_compensatory_pick,
    get_pick_position,
    set_pick_position,
    validate_name,
    set_name,
)
from .update_team import UpdateTeam
from .input_trade import (
    TradeProposal,
    ApplyToSite,
    trade_proposal,
    save_proposal,
    trade_page,
    get_teams,
)
from .input_buyout import (
    BuyoutForm,
    buyout_page,
    get_season_id,
    apply_buyout,
    buyout_results,
    ApplyBuyout,
)
from .qualify_person import QualifyingOffer, qualifying_offer, qualify_player
from .notes import WriteNote, get_note_data, get_notes
from .season import SeasonForm, valid_id, season_ids
from .adjust_roster import (
    adjustment_page,
    reassign,
    loan,
    waivers_page,
    clear,
    claim,
    terminate,
    dequalify,
    deactivate_rights,
)
from .team_season import (
    query_team_season,
    manually_update_team_season,
    daily_cap_updates,
    RecalculateDailyCap,
)
from .season_rollover import (
    get_slide_candidates,
    get_performance_bonuses,
    get_group_six_candidates,
    set_performance_bonuses,
    set_group_six,
    rollover,
    update_season_stats,
    SeasonRolloverChecklist,
)


@bp.before_request
def before_request():
    if not current_user.is_authenticated:
        return redirect(url_for("user.login"))
    if current_user.admin != True:
        abort(403)


@bp.route("/admin/index")
@bp.route("/admin/")
@bp.route("/admin")
def index():
    teams = get_teams()
    seasons = season_ids()
    return render_template(
        "admin/index.html", title="Admin Home", seasons=seasons, teams=teams
    )


@bp.route("/admin/proxy/<path:route>")
def proxy(route):
    if "nhl" not in route:
        abort(403)
    if "api" not in route:
        abort(403)
    try:
        request = urllib.request.Request(
            f"https://{route}",
            data=None,
            headers={
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/118.0"
            },
        )
        data = urllib.request.urlopen(request).read().decode("utf-8")
        response = Response(data)
        response.headers["Content-Type"] = "application/json"
        return response
    except urllib.error.HTTPError as e:
        abort(500, e)


@bp.route("/admin/create-person", methods=["GET", "POST"])
def create_person():
    form = CreatePerson()
    if form.validate_on_submit():
        person_id = form.create_person()
        current_app.logger.debug(f"Created new person id={person_id}")
        if person_id:
            return redirect(url_for("main.person_route", person_id=person_id))
    return render_template(
        "admin/create_person.html",
        title="Create Person Record",
        css="create_record.css",
        form=form,
    )


@bp.route("/admin/update-person", methods=["GET", "POST"])
@bp.route("/admin/update-person/<person_id>", methods=["GET", "POST"])
def update_person(person_id=None):
    form = UpdatePerson()
    if form.validate_on_submit():
        person_id = form.update_person()
        current_app.logger.debug(f"Updated person id={person_id}")
        if person_id:
            return redirect(url_for("main.person_route", person_id=person_id))
    if person_id:
        data = validate_person(person_id)
        if "error" in data:
            return redirect(url_for("admin.update_person"))
        form.set_values(data)
    return render_template(
        "admin/update_person.html",
        title="Update Person Record",
        css="update_record.css",
        form=form,
    )


@bp.route("/admin/api/validate_person/<person_id>")
def check_person(person_id):
    data = validate_person(person_id)
    if "error" in data:
        return jsonify(data), 400
    return jsonify(data)


@bp.route("/admin/update-team/<team_id>", methods=["GET", "POST"])
def update_team(team_id):
    form = UpdateTeam()
    print(f"{form.validate_on_submit()=}")
    if form.validate_on_submit():
        form.update_team()
        current_app.logger.debug(f"Updated team id={team_id}")
        return redirect(url_for("main.team_route", team_id=team_id))
    form.set_values(team_id)
    return render_template(
        "admin/update_team.html",
        title="Update Team Record",
        css="update_record.css",
        form=form,
    )


@bp.route("/admin/create-contract", methods=["GET", "POST"])
def create_contract():
    form = CreateContract()
    if form.validate_on_submit():
        person_id = form.create_contract()
        return redirect(url_for("main.person_route", person_id=person_id))
    return render_template(
        "admin/create_contract.html",
        title="Create Contract Record",
        css="create_record.css",
        form=form,
    )


@bp.route("/admin/create-pto", methods=["GET", "POST"])
def create_pto():
    form = CreatePTO()
    if form.validate_on_submit():
        person_id = form.create_pto()
        return redirect(url_for("main.person_route", person_id=person_id))
    return render_template(
        "admin/create_pto.html",
        title="Create PTO",
        css="create_record.css",
        form=form,
    )


@bp.route("/admin/update-draft", methods=["GET", "POST"])
def update_draft():
    form = SelectDraftSeason()
    if form.validate_on_submit():
        print(f"{form.season.data=}")
        return redirect(url_for("admin.update_specified_draft", year=form.season.data))
    return draft_page()


@bp.route("/admin/update-draft/<year>", methods=["GET", "POST"])
def update_specified_draft(year):
    form = SelectDraftSeason()
    if form.validate_on_submit():
        return redirect(url_for("admin.update_specified_draft", year=form.season.data))
    if not year.isdigit():
        abort(400, "Invalid draft year")
    return draft_page(year)


@bp.route("/admin/api/add-compensatory", methods=["POST"])
def add_compensatory():
    team = request.args.get("team")
    position = request.args.get("position")
    if not team or not position:
        abort(400, "Team and position must be set")
    output = add_compensatory_pick(team, position)
    if output:
        return output, 500
    return ("", 204)


@bp.route("/admin/api/unsigned")
def get_unsigned_people():
    return jsonify(get_unsigned())


@bp.route("/admin/api/extendable/<team_id>")
def get_extendable_people(team_id):
    return jsonify(get_extendable(team_id))


@bp.route("/admin/api/signing-rights/<team_id>")
def get_signing_rights_people(team_id):
    return jsonify(get_rights(team_id))


@bp.route("/admin/api/inactive")
def get_inactive_people():
    return jsonify(get_inactive())


@bp.route("/admin/api/pick_position/<pick_id>", methods=["GET", "POST"])
def pick_position(pick_id):
    if not pick_id.isdigit():
        return jsonify({"error": f"Invalid pick id: {pick_id}"}), 400
    if request.method == "POST":
        position = request.args.get("posn")
        draft_round = get_pick_position(pick_id)["round"]
        if not position:
            return jsonify({"error": f"Pick ID not provided"}), 400
        if not draft_round:
            return jsonify({"error": f"Pick round not set"}), 500
        result = set_pick_position(pick_id, draft_round, position)
        if result == True:
            return jsonify({"position": position, "success": True})
        else:
            return jsonify({"error": f"Unable to set position for pick: {result}"}), 500
    else:
        return jsonify(get_pick_position(pick_id))


@bp.route("/admin/api/validate_name/<name>")
def check_name(name):
    data = validate_name(name)
    # Return {"name": <name>, "id": <id>}
    if "error" in data:
        return jsonify(data), 400
    return jsonify(data)


@bp.route("/admin/api/pick_name/<pick_id>", methods=["POST"])
def set_pick_name(pick_id):
    if not pick_id.isdigit():
        return jsonify({"error": f"Invalid pick id: {pick_id}"}), 400
    if request.method == "POST":
        name = request.args.get("name")
        if not name:
            return jsonify({"error": f"Pick player name/ID not provided"}), 400
        pick_data = validate_name(name)
        if "error" in pick_data:
            return jsonify(pick_data), 400
        update_data = set_name(pick_id, pick_data["id"])
        if "error" in update_data:
            return jsonify(update_data)
        pick_data["success"] = True
        return jsonify(pick_data)


@bp.route("/admin/input-trade", methods=["GET", "POST"])
def input_trade():
    form = TradeProposal()
    if form.validate_on_submit():
        trade_data = {
            "team_one": form.team_one.data,
            "team_two": form.team_two.data,
            "picks_one": json.loads(form.picks_one.data),
            "contracts_one": json.loads(form.contracts_one.data),
            "picks_two": json.loads(form.picks_two.data),
            "contracts_two": json.loads(form.contracts_two.data),
            "future_considerations_one": form.future_considerations_one.data,
            "future_considerations_two": form.future_considerations_two.data,
            "future_considerations": form.future_considerations.data,
            "date": (form.date.data or date.today()).strftime(
                current_app.config["DATE_FMT"]
            ),
        }
        session["trade_data"] = trade_data
        print(trade_data)
        return redirect(url_for("admin.trade_display"))
    return trade_proposal(form)


@bp.route("/admin/trade", methods=["GET", "POST"])
def trade_display():
    data = session.get("trade_data", None)
    apply = None
    if not data:
        flash("Trade proposal data not found.")
        return redirect(url_for("admin.trade_display"))
    apply = ApplyToSite()
    if apply.validate_on_submit():
        trade_id = save_proposal(data)
        del session["trade_data"]
        return redirect(url_for("main.trade_display", trade_id=trade_id))
    return trade_page(data, apply)


@bp.route("/admin/buyout", methods=["GET", "POST"])
def buyout_route():
    form = BuyoutForm()
    if form.validate_on_submit():
        # Get data
        person_id = form.person.data
        season_id = form.buyout_after.data
        return redirect(
            url_for(
                "admin.buyout_with_results", person_id=person_id, after_season=season_id
            )
        )
    return buyout_page(form)


@bp.route("/admin/buyout/<person_id>", methods=["GET", "POST"])
def buyout_with_results(person_id):
    season = request.args.get("after_season")
    apply = ApplyBuyout()
    if apply.validate_on_submit():
        apply_buyout(person_id, season, apply.date.data)
        return redirect(url_for("main.person_route", person_id=person_id))
    return buyout_results(person_id, season, apply)


@bp.route("/admin/qualify", methods=["GET", "POST"])
def qualifying_route():
    form = QualifyingOffer()
    if form.validate_on_submit():
        qualify_player(form.person.data)
    return qualifying_offer(form)


@bp.route("/admin/note/new", methods=["GET", "POST"])
@bp.route("/admin/note/<table>/<record_id>", methods=["GET", "POST"])
def create_new_note(table=None, record_id=None):
    form = WriteNote()
    form.delete.render_kw = {"onclick": "return false"}
    if form.validate_on_submit():
        note_id = form.write_note()
        return redirect(url_for("admin.edit_note", note_id=note_id))
    if table:
        form.record_table.data = table
    if table and record_id:
        form.record_id.data = record_id
    return render_template("admin/note.html", form=form, css="notes.css")


@bp.route("/admin/note/<note_id>", methods=["GET", "POST"])
def edit_note(note_id):
    data = get_note_data(note_id)
    if not data:
        return redirect(url_for("admin.create_new_note"))
    form = WriteNote()
    if form.validate_on_submit():
        form.write_note()
        data = get_note_data(note_id)
        if not data:
            return redirect(url_for("admin.create_new_note"))
    form.note_id.data = note_id
    form.record_table.data = data["record_table"]
    form.record_table.render_kw = {"readonly": True}
    form.record_id.data = data["record_id"]
    form.record_id.render_kw = {"readonly": True}
    form.note.data = data["note"]
    form.safe.data = data["safe"]
    return render_template("admin/note.html", form=form, css="notes.css")


@bp.route("/admin/notes/<table>/<record_id>")
@bp.route("/admin/notes/<table>/")
@bp.route("/admin/notes/<table>")
@bp.route("/admin/notes/")
@bp.route("/admin/notes")
def display_notes(table=None, record_id=None):
    data = get_notes(table, record_id)
    return render_template(
        "admin/notes.html", table=table, record=record_id, data=data, css="notes.css"
    )


@bp.route("/admin/season/<season_id>", methods=["GET", "POST"])
def season_route(season_id):
    if not valid_id(season_id):
        abort(404)
    form = SeasonForm()
    if form.validate_on_submit():
        form.update_season()
    form.set_data(season_id)
    return render_template(
        "admin/season.html", title="Season Update", form=form, css="season.css"
    )


@bp.route("/admin/adjust-roster/<team_id>")
@bp.route("/admin/adjust-roster/<team_id>/")
def adjust_roster(team_id):
    if not team_id or not team_id.isdigit():
        abort(404)
    return adjustment_page(team_id)


@bp.route("/admin/api/reassign/<person_id>", methods=["POST"])
def reassign_person(person_id):
    if not person_id.isdigit():
        return jsonify({"error": "ID given must be an integer"}), 500
    transaction_date = request.args.get("date")
    transaction_type = request.args.get("type")
    person_status = None
    transaction_field = "non_roster"
    match transaction_type:
        case "callup":
            transaction_type = "Reassigned to NHL"
        case "non_roster":
            transaction_type = "Reassigned to Minors"
        case "on_loan":
            transaction_type = "Loaned"
            transaction_field = "on_loan"
        case "to_juniors":
            transaction_type = "Reassigned to Juniors"
            transaction_field = "in_juniors"
        case "waive":
            transaction_type = "Waived"
            person_status = "On Waivers"
            transaction_field = None
        case "emergency_callup":
            transaction_type = "Reassigned to NHL (Emergency)"
            person_status = "Emergency Callup"
        case "ad_hoc":
            transaction_type = request.args.get("adhocType")
            transaction_field = None
        case _:
            return (
                jsonify(
                    {"error": f"Unhandled reassignment type: '{transaction_type}'"}
                ),
                400,
            )
    current_app.logger.debug(f"  {transaction_type} => {transaction_field}")
    data = reassign(
        person_id, transaction_field, transaction_type, transaction_date, person_status
    )
    return jsonify(data), data["code"]


@bp.route("/admin/api/dequalify/<person_id>", methods=["POST"])
def dequalify_person(person_id):
    if not person_id.isdigit():
        return jsonify({"error": "ID given must be an integer"}), 500
    transaction_date = request.args.get("date")
    data = dequalify(person_id, transaction_date)
    return jsonify(data), data["code"]


@bp.route("/admin/api/deactivate-rights/<person_id>", methods=["POST"])
def deactivate_signing_rights(person_id):
    if not person_id.isdigit():
        return jsonify({"error": "ID given must be an integer"}), 500
    transaction_date = request.args.get("date")
    data = deactivate_rights(person_id, transaction_date)
    return jsonify(data), data["code"]


@bp.route("/admin/waivers/")
@bp.route("/admin/waivers")
def manage_waivers():
    return waivers_page()


@bp.route("/admin/api/clear/<transaction_asset>", methods=["POST"])
def clear_waivers(transaction_asset):
    to_minors = request.args.get("minors")
    print(f"{to_minors=}")
    if not transaction_asset.isdigit():
        return jsonify({"error": "Transaction asset ID must be an integer"}), 400
    transaction_date = request.args.get("date")
    data = clear(transaction_asset, to_minors == "true", transaction_date)
    return jsonify(data), data["code"]


@bp.route("/admin/api/claim/<transaction_asset>", methods=["POST"])
def claim_waivers(transaction_asset):
    team = request.args.get("team")
    if not team:
        return jsonify({"error": f"Claiming team ID not provided"}), 400
    if not transaction_asset.isdigit():
        return jsonify({"error": "Transaction asset ID must be an integer"}), 400
    transaction_date = request.args.get("date")
    data = claim(transaction_asset, team, transaction_date)
    return jsonify(data), data["code"]


@bp.route("/admin/api/terminate/<transaction_asset>", methods=["POST"])
def terminate_contract(transaction_asset):
    if not transaction_asset.isdigit():
        return jsonify({"error": "Transaction asset ID must be an integer"}), 400
    transaction_date = request.args.get("date")
    data = terminate(transaction_asset, transaction_date)
    return jsonify(data), data["code"]


@bp.route("/admin/recalculate", methods=["GET", "POST"])
def recalculate_daily_cap():
    form = RecalculateDailyCap()
    message = None
    if form.validate_on_submit():
        message = f"No updates. Did you give an offseason date? ({form.date.data})"
        updates = daily_cap_updates(form.date.data, form.team.data)
        if len(updates) != 0:
            message = f"deleted={updates[0]} created={updates[1]} team={form.team.data} date={form.date.data}"
    return render_template(
        "admin/recalculate.html", css="buyout.css", form=form, message=message
    )


@bp.route("/admin/api/set-p-bonuses/<contract_id>", methods=["POST"])
def set_p_bonuses(contract_id):
    earned = request.args.get("earned")
    if not earned:
        return jsonify({"error": "No `earned` argument provided"}), 404
    if not earned.isdigit():
        return jsonify({"error": "Performance bonuses earned must be a number"}), 400
    return set_performance_bonuses(contract_id, earned)


@bp.route("/admin/api/set-g6/<contract_id>", methods=["POST"])
def set_g6(contract_id):
    if not contract_id.isdigit():
        return jsonify({"error": "Group 6 UFA contract ID must be a number"}), 400
    return set_group_six(contract_id)


@bp.route("/admin/api/update-stats", methods=["POST"])
def update_stats():
    update_season_stats()
    db.session.commit()
    return "{}"


@bp.route("/admin/season-rollover", methods=["GET", "POST"])
def roll_over():
    form = SeasonRolloverChecklist()
    if form.validate_on_submit():
        flash("Season rollover in progress")
        rollover(form.skip_stats.data)
    data = {
        "slide": get_slide_candidates(),
        "bonuses": get_performance_bonuses(),
        "group_six": get_group_six_candidates(),
    }
    return render_template(
        "admin/rollover.html", css="rollover.css", form=form, data=data
    )


@bp.route("/admin/team-season", methods=["GET", "POST"])
def update_team_season():
    return render_template(
        "admin/team_season.html",
        css="team-season.css",
        teams=get_teams(),
        seasons=season_ids(),
    )


@bp.route("/admin/api/team-season-data/<team_id>/<season_id>")
def team_season_data(team_id, season_id):
    if not team_id.isdigit() or not season_id.isdigit():
        return "<p class='error'><strong>Team or Season is invalid</strong></p>", 400
    team_season_record = query_team_season(team_id, season_id)
    if not team_season_record:
        return (
            "<p class='error'><strong>TeamSeason record does not exist or was not found</strong></p>",
            404,
        )
    return render_template("admin/_team_season_data.html", record=team_season_record)


@bp.route("/admin/api/update-team-season", methods=["POST"])
def update_team_season_entry():
    values = request.args
    if "season" not in values or "team" not in values:
        return (
            jsonify({"error": "true", "message": "Team ID or season ID not provided"}),
            400,
        )
    if not values["season"].isdigit() or not values["team"].isdigit():
        return (
            jsonify(
                {"error": "true", "message": "Team ID and season ID must be integers"}
            ),
            400,
        )
    team_season_record = query_team_season(values["team"], values["season"])
    if not team_season_record:
        return (
            jsonify(
                {
                    "error": "true",
                    "message": "TeamSeason record does not exist or was not found",
                }
            ),
            404,
        )
    manually_update_team_season(team_season_record, values)
    return jsonify({"message": "TeamSeason successfully updated"})
