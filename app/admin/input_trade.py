import re
import json
from datetime import datetime, date
from flask import render_template, url_for, redirect, abort, current_app, session
from flask_wtf import FlaskForm
from wtforms import (
    SelectField,
    HiddenField,
    BooleanField,
    TextAreaField,
    SubmitField,
    DateField,
)
from wtforms.validators import DataRequired, Length
from sqlalchemy import func, select, literal, insert, String
from sqlalchemy.exc import MultipleResultsFound
from .. import db
from ..models import (
    Person,
    DraftPick,
    Team,
    Asset,
    Season,
    Contract,
    ContractYear,
    Transaction,
    TransactionAsset,
    RetainedSalary,
    Season,
    SigningRights,
    Note,
)
from . import team_season


def trade_proposal(form):
    if not form:
        abort(500, "Trade proposal form not found")
    current_app.logger.info(f"/trade_propodal - Rendering")
    return render_template(
        "admin/input_trade.html",
        title="Trade Proposal",
        css="trade-proposal.css",
        form=form,
    )


def trade_page(trade_data, apply_to_site=None):
    if type(trade_data) == type({}):
        current_app.logger.debug(
            "trade_page - Proposal data provided. Querying for details"
        )
        # data = proposal_details(trade_data)
        data = get_form_details(trade_data)
        return render_template(
            "admin/trade.html",
            title="Trade Proposal",
            css="trade.css",
            trade=data,
            proposal=True,
            apply=apply_to_site,
        )
    elif type(trade_data) == type("0") and trade_data.isdigit():
        current_app.logger.debug(
            "trade_page - Trade ID provided. Querying for trade information"
        )
        data = get_transaction_details(trade_data)
        page_title = data["type"]
        proposal = data["type"].lower() == "trade proposal"
        return render_template(
            "admin/trade.html",
            title=page_title,
            css="trade.css",
            trade=data,
            proposal=proposal,
            apply=apply_to_site,
        )
    else:
        current_app.logger.debug(f"trade_page - Undefined input type - {trade_data}")
        abort(500)


def get_form_details(trade_data):
    current_app.logger.debug(f"Getting details from form data")
    data = {
        "team_one": get_form_team(trade_data["team_one"]),
        "team_two": get_form_team(trade_data["team_two"]),
        "future_considerations_one": trade_data["future_considerations_one"],
        "future_considerations_two": trade_data["future_considerations_two"],
        "future_considerations": trade_data["future_considerations"],
        "picks_one": get_form_picks(trade_data["picks_one"]),
        "picks_two": get_form_picks(trade_data["picks_two"]),
        "contracts_one": get_form_contracts(trade_data["contracts_one"]),
        "contracts_two": get_form_contracts(trade_data["contracts_two"]),
        "date": date.today().strftime(current_app.config["DATE_FMT"]),
        "clause_warning": get_clause_warning(
            trade_data["contracts_one"] + trade_data["contracts_two"]
        ),
        "cap_warning": get_form_cap_warning(trade_data),
    }
    return data


def save_proposal(data):
    current_app.logger.debug(f"Saving trade proposal to database")
    check_data(data)
    try:
        trade_date = datetime.strptime(data["date"], current_app.config["DATE_FMT"])
        trade_date = datetime.combine(trade_date.date(), datetime.now().time())
    except TypeError as e:
        current_app.logger.error("Trade proposal date provided as datetime")
        trade_date = datetime.combine(data["date"], datetime.now().time())
    trade_id = create_transaction(type="Trade", date=trade_date)
    create_transaction_picks(
        trade_id, data["picks_one"], data["team_one"], data["team_two"]
    )
    create_transaction_picks(
        trade_id, data["picks_two"], data["team_two"], data["team_one"]
    )
    create_transaction_contracts(
        trade_id, data["contracts_one"], data["team_one"], data["team_two"]
    )
    create_transaction_contracts(
        trade_id, data["contracts_two"], data["team_two"], data["team_one"]
    )
    if data["future_considerations_one"]:
        create_future_considerations(
            trade_id, data["future_considerations"], data["team_one"], data["team_two"]
        )
    if data["future_considerations_two"]:
        create_future_considerations(
            trade_id, data["future_considerations"], data["team_two"], data["team_one"]
        )
    db.session.flush()
    apply_to_database(trade_id, data["date"])
    team_season.update_all_teams_seasons(data["team_one"])
    team_season.update_all_teams_seasons(data["team_two"])
    db.session.commit()
    return trade_id


# Make sure works directly and as part of trade.save_proposal
def apply_to_database(trade_id, trade_date):
    current_app.logger.debug("Applying trade to site")
    check_transaction_assets(trade_id)
    team_ids = query_transaction_team_ids(trade_id)
    for team_id in team_ids:
        update_assets(trade_id, team_id)
        create_retained_salary(trade_id, team_id, trade_date)
    create_notes(trade_id)
    db.session.flush()
    current_app.logger.debug("Trade applied")


def create_notes(trade_id):
    query = (
        TransactionAsset.query.join(Team, Team.id == TransactionAsset.new_team)
        .filter(TransactionAsset.transaction_id == trade_id)
        .with_entities(
            TransactionAsset.id,
            literal("transaction_asset"),
            literal('To <a href="/team/')
            + Team.abbreviation
            + literal('"><img class="logo" alt="')
            + Team.name
            + literal(' logo" src="/static/')
            + Team.logo
            + literal('"> ')
            + Team.abbreviation
            + literal('</a> - <a href="/trade/')
            + func.cast(TransactionAsset.transaction_id, String)
            + literal('">Details</a>'),
            True,
        )
        .subquery()
    )
    create_records = insert(Note).from_select(
        [
            "record_id",
            "record_table",
            "note",
            "safe",
        ],
        select(query),
    )
    result = db.session.execute(create_records)
    return


def update_assets(trade_id, new_team):
    current_app.logger.debug(f"Setting ownership of team {new_team}'s trade assets")
    assets = (
        TransactionAsset.query.filter(TransactionAsset.transaction_id == trade_id)
        .filter(TransactionAsset.new_team == new_team)
        .with_entities(TransactionAsset.asset_id)
        .scalar_subquery()
    )
    update_owners = Asset.query.filter(Asset.id.in_(assets)).update(
        {Asset.current_team: new_team}, synchronize_session="fetch"
    )
    people = (
        TransactionAsset.query.join(Contract, Contract.id == TransactionAsset.asset_id)
        .filter(TransactionAsset.transaction_id == trade_id)
        .filter(TransactionAsset.new_team == new_team)
        .with_entities(Contract.person_id)
        .subquery()
    )
    extensions = (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .filter(Contract.person_id.in_(select(people)))
        .filter(Asset.active == True)
        .filter(Asset.current_team != new_team)
        .with_entities(Asset.id)
        .subquery()
    )
    Person.query.filter(Person.id.in_(select(people))).update(
        {Person.captain: False, Person.alternate_captain: False},
        synchronize_session="fetch",
    )
    Asset.query.filter(Asset.id.in_(select(extensions))).update(
        {Asset.current_team: new_team}, synchronize_session="fetch"
    )
    return


def create_retained_salary(trade_id, old_team, trade_date):
    current_app.logger.debug(
        f"Creating retained salary records for team {old_team}'s retention"
    )
    retained_asset_ids = get_retained_assets(trade_id, old_team)
    for contract in retained_asset_ids:
        retention = float(
            re.match(r".*?(?P<retention>\d+)", contract["retention"]).group("retention")
        )
        if retention == 0:
            continue
        past_retention = get_total_retention(contract["id"])
        retention = float((100 - past_retention) * (retention / 100))
        asset = Asset(
            originating_team=old_team,
            current_team=old_team,
            active=True,
            type="Retained Salary",
        )
        db.session.add(asset)
        db.session.flush()
        # Write to transaction. Gets asset ID set
        # Can still roll back
        # Found: https://www.reddit.com/r/flask/comments/dpeo8t/flask_sqlalchemy_how_to_get_the_last_inserted/
        retained_salary = RetainedSalary(
            id=asset.id,
            contract_id=contract["id"],
            retained_by=old_team,
            retained_from=datetime.strptime(
                trade_date, current_app.config["DATE_FMT"]
            ).date(),
            retention=retention,
        )
        db.session.add(retained_salary)
        """
        Article 50.5(e)(iii)
        --------------------
        Trades only of Averaged Amount + related Player Ssalary and Bonuses payable under SPC

        Limitations
        A. Maximum percentage retained 50%. Cannot change year to year
        B. All pay split between teams paticipating at agreed percent - Example
        C. May not
          1. Retain more than three deals in any one season
          2. Retain more than 15% of the salary cap ceiling per year
          3. Reacquire within one calendar year of a retained salary transaction the retained contract
             Does not apply if a player's deal expires or is terminated. Contract not player
          4. Reacquire as part of a retained salary transaction the contract of a player on the reserve
             list in the past year. Note: only applies in a retained salary transaction
          5. Retain salary on a contract that has been retained twice already
          6. If a contract is already retained, the second cannot change the originally retaining team's
             retention
        D. Retained salary obligations also apply to cap advantage recapture penalties
        E. Retained salary obligations also apply to buyouts
        F. If retained salary contract is loaned to another league, retained salary portions remain. If
           total compensation changes, retained salary obligation also changes
        
        Examples
        B. Team A and B. A retains 30% of a contract.
           Team A is responsible for 30% of bonuses and 30% of salary
        """


def get_total_retention(contract_id):
    raw_ret = (
        RetainedSalary.query.filter(RetainedSalary.contract_id == contract_id)
        .with_entities(func.sum(RetainedSalary.retention))
        .first()
    )
    return raw_ret[0] or 0


def check_transaction_assets(transaction_id):
    assets = get_asset_data(transaction_id)
    for asset in assets:
        if asset["former_team"] != asset["current_team"]:
            abort(500, f"Transaction asset no longer belongs to trading team\n{asset}")
        if not asset["active"]:
            abort(500, f"Transaction asset no longer active\n{asset}")
        if asset["condition"] and "Retention" in asset["condition"]:
            retention = float(
                re.match(r".*?(?P<retention>\d+)", asset["condition"]).group(
                    "retention"
                )
            )
            if retention == 0:
                del asset["condition"]
                continue
            if retention > 50:
                abort(500, f"Greater than 50% retention on asset\n{asset}")
            if retention <= 0:
                abort(500, f"0% or less retention on asset\n{asset}")
    return True


def check_data(data):
    if "team_one" not in data:
        abort(500, "Missing team one. Please make sure two teams are selected to trade")
    if "team_two" not in data:
        abort(500, "Missing team two. Please make sure two teams are selected to trade")
    if "picks_one" not in data:
        abort(500, "Missing team one picks data")
    if "picks_two" not in data:
        abort(500, "Missing team two picks data")
    for pick in data["picks_one"]:
        if "id" not in pick:
            abort(500, f"Missing pick ID for team one. Pick {pick}")
    for pick in data["picks_two"]:
        if "id" not in pick:
            abort(500, f"Missing pick ID for team two. Pick {pick}")
    if "contracts_one" not in data:
        abort(500, "Missing team one contracts data")
    if "contracts_two" not in data:
        abort(500, "Missing team two contracts data")
    for contract in data["contracts_one"]:
        if "id" not in contract:
            abort(500, f"Missing contract ID for team one. Pick {contract}")
    for contract in data["contracts_two"]:
        if "id" not in contract:
            abort(500, f"Missing contract ID for team two. Pick {contract}")
    if "future_considerations_one" not in data:
        abort(500, "Future considerations not indicated for team one")
    if "future_considerations_two" not in data:
        abort(500, "Future considerations not indicated for team two")
    if "future_considerations" not in data:
        abort(500, "Future considerations details not provided")
    return


def create_transaction(**kwargs):
    if "date" not in kwargs:
        kwargs["date"] = datetime.now()
    if "season_id" not in kwargs:
        kwargs["season_id"] = current_app.config["CURRENT_SEASON"]
    if "user_id" in kwargs and kwargs["user_id"] is None:
        del kwargs["user_id"]
    if "type" not in kwargs:
        kwargs["type"] = "Trade Proposal"
    keys = set(kwargs.keys())
    valid_keys = {"date", "season_id", "type", "user_id"}
    if not keys.issubset(valid_keys):
        current_app.logger.error(f"trade.create_proposal - Unknown args\n\t{kwargs}")
        abort(
            500,
            "Unknown data found while creating transaction* record\n*Trades, Trade proposals, Player reassignments, etc...",
        )
    trade = Transaction(**kwargs)
    db.session.add(trade)
    db.session.flush()
    return trade.id


def create_transaction_picks(transaction, picks, former, new):
    for pick in picks:
        transaction_pick = TransactionAsset(
            transaction_id=transaction,
            asset_id=pick["id"],
            former_team=former,
            new_team=new,
        )
        if "condition" in pick:
            transaction_pick.condition = pick["condition"]
        db.session.add(transaction_pick)
    return


def create_transaction_contracts(transaction, contracts, former, new):
    for contract in contracts:
        transaction_contract = TransactionAsset(
            transaction_id=transaction,
            asset_id=contract["id"],
            former_team=former,
            new_team=new,
        )
        if "retention" in contract and contract["retention"] != "0":
            transaction_contract.condition = f"Retention: {contract['retention']}%"
        db.session.add(transaction_contract)
    return


def create_future_considerations(transaction, considerations, former, new):
    future_considerations = TransactionAsset(
        former_team=former,
        new_team=new,
        transaction_id=transaction,
        condition="Future Considerations",
    )
    if considerations:
        future_considerations.condition += f":\n{considerations}"
    db.session.add(future_considerations)
    return


def get_asset_data(transaction_id):
    raw_data = query_asset_data(transaction_id)
    return [
        {
            "id": asset[0],
            "former_team": asset[1],
            "new_team": asset[2],
            "current_team": asset[3],
            "active": asset[4],
            "condition": asset[5],
        }
        for asset in raw_data
    ]


def get_retained_assets(transaction_id, old_team):
    raw_data = query_retained_assets(transaction_id, old_team)
    return [
        {
            "id": asset[0],
            "retention": asset[1],
        }
        for asset in raw_data
    ]


def get_form_team(team_id):
    raw_team = query_team(team_id)
    return {
        "name": raw_team[0],
        "logo": raw_team[1],
    }


def get_clause_warning(contracts):
    ids = [contract["id"] for contract in contracts]
    raw_data = query_clauses(ids)
    if len(raw_data) == 0:
        return None
    warnings = []
    for item in raw_data:
        warning = create_warning(item)
        if warning:
            warnings.append(warning)
    return warnings


def create_warning(item):
    clauses = []
    if item[0]:
        clauses.append("NMC")
    if item[1]:
        clauses.append("NTC")
    if clauses == []:
        return
    clauses = ", ".join(clauses)
    warning = f"{item[3]}: {clauses}"
    if item[2]:
        warning += f" ({item[2]})"
    return warning


def get_form_contracts(assets):
    """
    'contracts_two': [
        {'id': '670', 'retention': '0'}
    ],
    """
    return list(
        filter(
            lambda x: x is not None, [contract_data(contract) for contract in assets]
        )
    )


def contract_data(contract):
    raw_data = query_contract(contract["id"])
    if not raw_data:
        return None
    data = {
        "id": raw_data[4],
        "retention": f"Retained: {contract['retention']}%",
        "name": raw_data[1],
        "cap_hit": raw_data[2],
        "nhl_salary": raw_data[3],
        "type": raw_data[5],
    }
    if data["retention"] == "Retained: 0%":
        del data["retention"]
    return data


def get_form_picks(assets):
    """
    'picks_one': [
        {'id': '441', 'condition': 'idk'},
        {'id': '508'}
    ],
    """
    return [pick_data(pick) for pick in assets]


def pick_data(pick):
    raw_data = query_pick(pick["id"])
    data = {
        "id": pick["id"],
        "originating_team": raw_data[0],
        "round": raw_data[1],
        "season": raw_data[2].year,
        "position": raw_data[3],
        "condition": None,
    }
    if "condition" in pick:
        data["condition"] = pick["condition"]
    return data


def check_contracts(team_id, contracts):
    contract_ids = set([int(contract["id"]) for contract in contracts])
    queried_contracts = query_team_contracts(team_id)
    queried_contracts = set([value[0] for value in queried_contracts])
    if contract_ids.issubset(queried_contracts):
        return []

    errors = []
    for contract in contract_ids:
        if contract not in queried_contracts:
            error_str = f"Contract {contract} not owned by team {team_id}"
            current_app.logger.error(error_str)
            errors.append(error_str)
    return errors


def check_picks(team_id, picks):
    pick_ids = set([int(pick["id"]) for pick in picks])
    queried_picks = query_team_picks(team_id)
    queried_picks = set([value[0] for value in queried_picks])
    if pick_ids.issubset(queried_picks):
        return []

    errors = []
    for pick in pick_ids:
        if pick not in queried_picks:
            error_str = f"Pick {pick} not owned by team {team_id}"
            current_app.logger.error(error_str)
            errors.append(error_str)
    return errors


def get_form_cap_warning(data):
    season_data = get_season_data(current_app.config["CURRENT_SEASON"])
    team_one = team_season.get_team_season(data["team_one"], None)
    team_two = team_season.get_team_season(data["team_two"], None)
    contracts_one = get_form_contracts(data["contracts_one"])
    contracts_two = get_form_contracts(data["contracts_two"])
    cap_contracts_one = sum([contract["cap_hit"] for contract in contracts_one])
    cap_contracts_two = sum([contract["cap_hit"] for contract in contracts_two])

    cap_one = team_one["cap_hit"] + cap_contracts_two - cap_contracts_one
    cap_two = team_two["cap_hit"] + cap_contracts_one - cap_contracts_two

    warnings = []
    if cap_one > season_data["salary_cap"]:
        warnings.append(
            f"{get_form_team(data['team_one'])['name']}: over the salary cap with this trade"
        )
    elif cap_two > season_data["salary_cap"]:
        warnings.append(
            f"{get_form_team(data['team_two'])['name']}: over the salary cap with this trade"
        )

    if cap_one < season_data["salary_floor"]:
        warnings.append(
            f"{get_form_team(data['team_one'])['name']}: under the cap floor with this trade"
        )
    if cap_two < season_data["salary_floor"]:
        warnings.append(
            f"{get_form_team(data['team_two'])['name']}: under the cap floor with this trade"
        )
    return warnings


def check_previously_retained(team_id, contract_ids):
    return (
        RetainedSalary.query.filter(RetainedSalary.contract_id.in_(contract_ids))
        .filter(RetainedSalary.retained_by == team_id)
        .with_entities(RetainedSalary.contract_id)
        .order_by(RetainedSalary.contract_id.asc())
        .all()
    )


def query_trade_ids():
    return [
        id[0]
        for id in Transaction.query.filter(Transaction.type == "Trade")
        .with_entities(Transaction.id)
        .all()
    ]


def query_retained_assets(transaction_id, old_team):
    return (
        TransactionAsset.query.filter(TransactionAsset.transaction_id == transaction_id)
        .filter(TransactionAsset.former_team == old_team)
        .filter(TransactionAsset.condition.like("Retention: %"))
        .with_entities(
            TransactionAsset.asset_id,
            TransactionAsset.condition,
        )
        .all()
    )


def query_asset_data(transaction_id):
    return (
        TransactionAsset.query.join(Asset, Asset.id == TransactionAsset.asset_id)
        .filter(TransactionAsset.transaction_id == transaction_id)
        .order_by(TransactionAsset.asset_id)
        .with_entities(
            TransactionAsset.asset_id,
            TransactionAsset.former_team,
            TransactionAsset.new_team,
            Asset.current_team,
            Asset.active,
            TransactionAsset.condition,
        )
        .all()
    )


def query_pick(asset_id):
    return (
        DraftPick.query.join(Asset, Asset.id == DraftPick.id)
        .join(Season, Season.id == DraftPick.season_id)
        .join(Team, Asset.originating_team == Team.id)
        .filter(DraftPick.id == asset_id)
        .with_entities(
            Team.abbreviation,
            DraftPick.round,
            Season.draft_date,
            DraftPick.position,
        )
        .first()
    )


def query_contract(asset_id):
    if Contract.query.filter(Contract.id == asset_id).count() == 0:
        return query_rights_details(asset_id)
    return (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .join(Person, Person.id == Contract.person_id)
        .join(ContractYear, ContractYear.contract_id == Contract.id)
        .filter(Asset.active == True)
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .filter(Contract.id == asset_id)
        .with_entities(
            Contract.id,
            Person.name,
            ContractYear.cap_hit,
            ContractYear.nhl_salary,
            Person.id,
            Asset.type,
        )
        .first()
    )


def query_rights_details(asset_id):
    return (
        SigningRights.query.join(Asset, Asset.id == SigningRights.id)
        .join(Person, Person.id == SigningRights.person_id)
        .filter(Asset.active == True)
        .filter(SigningRights.id == asset_id)
        .with_entities(
            SigningRights.id,
            Person.name,
            0,
            0,
            Person.id,
            Asset.type,
        )
        .first()
    )


def query_clauses(ids):
    return (
        ContractYear.query.join(Contract, ContractYear.contract_id == Contract.id)
        .join(Person, Contract.person_id == Person.id)
        .filter(ContractYear.contract_id.in_(ids))
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .with_entities(
            ContractYear.nmc,
            ContractYear.ntc,
            ContractYear.clause_limits,
            Person.name,
        )
        .all()
    )


def query_transaction_team_ids(transaction_id):
    raw_ids = (
        TransactionAsset.query.filter(TransactionAsset.transaction_id == transaction_id)
        .with_entities(
            TransactionAsset.former_team,
            TransactionAsset.new_team,
        )
        .distinct()
        .all()
    )
    return set([team for result in raw_ids for team in result])


def query_team(team_id):
    return (
        Team.query.filter(Team.id == team_id)
        .with_entities(Team.name, Team.logo)
        .first()
    )


def query_team_contracts(team_id):
    return (
        Asset.query.filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .filter(Asset.type.in_(["Contract", "Signing Rights"]))
        .with_entities(Asset.id)
        .all()
    )


def query_team_assets(team_id):
    return (
        Asset.query.filter(Asset.current_team == team_id)
        .filter(Asset.active == True)
        .order_by(Asset.id.asc())
        .with_entities(Asset.id)
        .all()
    )


def query_team_picks(team_id):
    return (
        DraftPick.query.join(Asset, Asset.id == DraftPick.id)
        .join(Team, Team.id == Asset.originating_team)
        .join(Season, DraftPick.season_id == Season.id)
        .filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .order_by(Season.draft_date.asc())
        .order_by(DraftPick.round.asc())
        .with_entities(
            Asset.id,
            Team.abbreviation,
            Season.draft_date,
            DraftPick.round,
            DraftPick.position,
        )
        .all()
    )


def get_season_data(season_id):
    season = Season.query.filter(Season.id == season_id).first()
    if not season:
        return {}
    return {
        "name": season.name,
        "salary_cap": season.salary_cap,
        "salary_floor": season.salary_floor,
        "minimum_salary": season.minimum_salary,
        "max_buriable_hit": season.max_buriable_hit,
        "bonus_overage_limit": season.bonus_overage_limit,
    }


def get_teams():
    raw_teams = (
        Team.query.with_entities(Team.id, Team.name)
        .order_by(Team.name.asc())
        .filter(Team.active == True)
        .all()
    )
    return [tuple(team) for team in raw_teams]


class TradeProposal(FlaskForm):
    def __init__(self):
        super().__init__()
        self.team_one.choices = get_teams()
        self.team_two.choices = get_teams()

    def validate(self, extra_validators=None):
        if not super().validate(extra_validators=extra_validators):
            return False
        if self.team_one.data == self.team_two.data:
            self.team_one.errors.append("One team cannot trade with itself")
            self.team_two.errors.append("One team cannot trade with itself")
        if not self.valid_json(self.picks_one):
            current_app.logger.error(
                "TradeProposal.validate.valid_json - Picks One - Invalid JSON"
            )
            return False
        if not self.valid_json(self.picks_two):
            current_app.logger.error(
                "TradeProposal.validate.valid_json - Picks Two - Invalid JSON"
            )
            return False
        if not self.valid_json(self.contracts_one):
            current_app.logger.error(
                "TradeProposal.validate.valid_json - Contracts One - Invalid JSON"
            )
            return False
        if not self.valid_json(self.contracts_two):
            current_app.logger.error(
                "TradeProposal.validate.valid_json - Contracts Two - Invalid JSON"
            )
            return False
        if not self.check_contracts(self.team_one.data, self.contracts_one):
            current_app.logger.error(
                "TradeProposal.validate.check_contracts - Team One - Invalid Contracts"
            )
            return False
        if not self.check_contracts(self.team_two.data, self.contracts_two):
            current_app.logger.error(
                "TradeProposal.validate.check_contracts - Team Two - Invalid Contracts"
            )
            return False
        if not self.check_picks(self.team_one.data, self.picks_one):
            current_app.logger.error(
                "TradeProposal.validate.check_picks - Team One - Invalid Picks"
            )
            return False
        if not self.check_picks(self.team_two.data, self.picks_two):
            current_app.logger.error(
                "TradeProposal.validate.check_picks - Team Two - Invalid Picks"
            )
            return False
        if not self.check_contract_count():
            current_app.logger.error(
                "TradeProposal.validate.check_contract_count - Invalid contract count"
            )
            return False
        if not self.check_retention():
            current_app.logger.error(
                "TradeProposal.validate.check_retention - Invalid retention amount"
            )
            return False
        if not self.check_retained(self.contracts_one, self.team_one):
            current_app.logger.error(
                "TradeProposal.validate.check_retained - Team One - Too many retained"
            )
            return False
        if not self.check_retained(self.contracts_two, self.team_two):
            current_app.logger.error(
                "TradeProposal.validate.check_retained - Team Two - Too many retained"
            )
            return False
        if not self.check_previously_retained():
            current_app.logger.error(
                "TradeProposal.validate.check_previously_retained - Cannot re-acquire a previously retained contract"
            )
            return False
        return True

    def valid_json(self, form_field):
        try:
            json.loads(form_field.data)
            return True
        except TypeError:
            form_field.errors.append(
                "JSON variable type must be str, bytes or bytearray"
            )
        except json.decoder.JSONDecodeError:
            form_field.errors.append("Incorrectly formed JSON object")
        return False

    def check_contracts(self, team_id, contracts_field):
        data = json.loads(contracts_field.data)
        errors = check_contracts(team_id, data)
        for error in errors:
            contracts_field.errors.append(error)
        return errors == []

    def check_picks(self, team_id, picks_field):
        data = json.loads(picks_field.data)
        errors = check_picks(team_id, data)
        for error in errors:
            picks_field.errors.append(error)
        return errors == []

    def check_contract_count(self):
        data_one = json.loads(self.contracts_one.data)
        data_two = json.loads(self.contracts_two.data)
        if len(data_one) == len(data_two):
            return True
        ts_one = team_season.get_team_season(
            self.team_one.data, current_app.config["CURRENT_SEASON"]
        )
        ts_two = team_season.get_team_season(
            self.team_two.data, current_app.config["CURRENT_SEASON"]
        )
        if "contracts" not in ts_one:
            current_app.logger.error(f"No team 1 team_season contract count")
            return False
        if "contracts" not in ts_two:
            current_app.logger.error(f"No team 2 team_season contract count")
            return False
        count_one = ts_one["contracts"]
        count_two = ts_two["contracts"]
        current_app.logger.debug(
            f"New contract count 1: {count_one - len(data_one) + len(data_two)}"
        )
        current_app.logger.debug(
            f"New contract count 2: {count_two - len(data_two) + len(data_one)}"
        )
        if count_one - len(data_one) + len(data_two) > 50:
            self.contracts_one.errors.append(
                "Exceeded 50 contract count. More contracts need to be traded away"
            )
            return False
        if count_two - len(data_two) + len(data_one) > 50:
            self.contracts_two.errors.append(
                "Exceeded 50 contract count. More contracts need to be traded away"
            )
            return False
        return True

    def check_retention(self):
        data_one = json.loads(self.contracts_one.data)
        data_two = json.loads(self.contracts_two.data)
        errors = False
        for contract in data_one:
            if "retention" not in contract:
                continue
            retention = float(contract["retention"])
            if retention > 50 or retention < 0:
                errors = True
                message = (
                    f"Contract {contract['id']}: Retention outside 1% - 50% limits"
                )
                self.contracts_one.errors.append(message)
        for contract in data_two:
            if "retention" not in contract:
                continue
            retention = float(contract["retention"])
            if retention > 50 or retention < 0:
                errors = True
                message = (
                    f"Contract {contract['id']}: Retention outside 1% - 50% limits"
                )
                self.contracts_two.errors.append(message)
        return errors == False

    def check_retained(self, contracts_field, team):
        data = json.loads(contracts_field.data)
        retained_count = 0
        for contract in data:
            if contract["retention"] == "0":
                continue
            retained_count += 1
        season_data = team_season.get_team_season(team.data, None)
        total_retained = season_data["retained_contracts"] + retained_count
        if total_retained > 3:
            contracts_field.errors.append(
                f"Team {team.data} cannot retain more than three contracts"
            )
            return False
        return True

    def check_previously_retained(self):
        team_one = self.team_one.data
        team_two = self.team_two.data
        contracts_one = json.loads(self.contracts_one.data)
        contracts_two = json.loads(self.contracts_two.data)
        ids_one = [contract["id"] for contract in contracts_one]
        ids_two = [contract["id"] for contract in contracts_two]
        no_errors = True
        if check_previously_retained(team_one, ids_two) != []:
            no_errors = False
            self.contracts_one.errors.append(
                f"Team {team_one} cannot reacquire a previously retained contract"
            )
        if check_previously_retained(team_two, ids_one) != []:
            no_errors = False
            self.contracts_two.errors.append(
                f"Team {team_two} cannot reacquire a previously retained contract"
            )
        return no_errors

    date = DateField("Date", default="", id="trade_date")
    team_one = SelectField(
        "Team", default="", validators=[DataRequired()], id="team_one"
    )
    team_two = SelectField(
        "Team", default="", validators=[DataRequired()], id="team_two"
    )
    picks_one = HiddenField(default="", id="picks_one")
    picks_two = HiddenField(default="", id="picks_two")
    contracts_one = HiddenField(default="", id="contracts_one")
    contracts_two = HiddenField(default="", id="contracts_two")
    future_considerations_one = BooleanField(
        "Future Considerations", default="", id="fut_con_one"
    )
    future_considerations_two = BooleanField(
        "Future Considerations", default="", id="fut_con_two"
    )
    future_considerations = TextAreaField(
        "Future Considerations Details",
        default="",
        validators=[Length(min=0, max=1024)],
    )
    submit = SubmitField("Validate Trade", id="submit")


class ApplyToSite(FlaskForm):
    submit = SubmitField("Apply to Site")
